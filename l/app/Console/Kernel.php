<?php

namespace App\Console;

use App\Appointment;
use App\Events\AppointmentCompletedEvent;
use Carbon\Carbon;
use App\Booking;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use SmsTools;
use PaymentTools;
use App\StripeConnectedAccount;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //

    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
      
      ///automatic payment to carer if booking completed and payout not done
         $schedule->call(function () {
			   $appointments = Appointment::where('status_id', 4)
                ->where('payout', 0)
                ->get();
    
           $datetime = date('Y-m-d H:i:s');	
			 foreach ($appointments as $appointment) {
				//Auto pay for appointment 
				$booking = $appointment->booking;
               
				$stripeConnectedAccount = StripeConnectedAccount::where('carer_id', $booking->carer_id)->first();
               //echo "stripeConnectedAccount<pre>"; print_r($stripeConnectedAccount); echo "<pre>";// exit;
				if(is_object($stripeConnectedAccount)){
                   $comment = 'Payment to Carer '.$booking->bookingCarerProfile->full_name.' ('.$booking->carer_id.') for appointment '.$appointment->id.' in booking '.$booking->id.' (dispute resolving)'; 
                   
                    DB::insert('insert into cron_test (date, action) values ("'.$datetime.'","'.$appointment->id .'")');
                 // echo "<pre>"; print_r($comment); echo "<pre>"; exit; 
                    $res = PaymentTools::createTransfer($stripeConnectedAccount->id, $appointment->id, $booking->id, $appointment->price_for_carer*100, $comment); 
                       //Mark certain appointments as with poyout
                    Appointment::where('id', $appointment->id)->where('status_id', 4)->where('payout', 0)->update(['payout' => 1]);
//echo "<pre>"; print_r($comment); echo "<pre>"; exit; 
                }
               
			 } 

		 })->everyMinute();
        
      
      //Get each day stripe balance
		/* $schedule->call(function () {
			$balance = PaymentTools::getBalance();
			DB::table('stripe_balance')->insert(
				 [
					'balance'   =>   $balance
				 ]
			);
          })->daily(); */
      
       //Paid Bonus  to Carer after completing their First Booking with the 'REGISTER' Code.
      $schedule->call(function(){
         $bookings = Booking::get();
    	foreach ($bookings as $booking) {
       // $test = $this->user['completed_appointments_hours'];
      	$bookingAppointments = $booking->appointments()->get();
        $carer = $booking->bookingCarer()->first();
        if($carer->use_register_code){
         //Create bonus for first booking, if has not yet
              if(!$carer->bonusPayouts()->where('bonus_type_id', 1)->get()->count()){
                  $carer->bonusPayouts()->create([
                     'bonus_type_id' => 1,
                     'amount' => 50,
                  ]);
              }
          return 'Success';
          }
        }
      })->everyMinute();
		
      //Send 
         $schedule->call(function () {

            $timeTask = Carbon::now();

            DB::table('mails')->where('time_to_send', '<', $timeTask)->where('status', '=', 'new')
                ->update(['status' => 'in_progress', 'time_when_sent' => $timeTask]);

            $mails = DB::table('mails')->select('id','email', 'subject', 'text')
                ->where('time_when_sent', $timeTask)->get();

            if (count($mails) > 0) {
                foreach ($mails as $mail) {
                    Mail::send(config('settings.frontTheme') . '.emails.mail',
                        ['text'=>$mail->text],
                        function ($m) use ($mail) {
                            $m->to($mail->email)->subject($mail->subject);
                        });
                    //todo add mail error exception
                    DB::table('mails')->where('id', $mail->id)
                        ->update(['status' => 'sent']);

                }
            }else{
           return response(['message'=>'Can Not find Appointment']);
         }
        })->everyMinute();

        //Sending sms about appointments
    	  $schedule->call(function () {
          $startDate = strtotime("31-03-".date('Y')); //start date
     	  $endDate = strtotime("27-10-".date('Y')); //end date
      	  $todayDate =  strtotime(date('d-m-Y')); //Today's date  
          //$todayDate =  strtotime("01"); //Today's date  
          
          if (($todayDate > $startDate) && ($todayDate < $endDate))
          {
            
              $res = DB::select("SELECT a.id FROM appointments a JOIN bookings b ON a.booking_id = b.id
                              WHERE UNIX_TIMESTAMP(STR_TO_DATE(CONCAT(DATE_FORMAT(a.date_start, '%Y-%m-%d'), ' ', 								  a.time_from), \"%Y-%m-%d %H.%i\")) - UNIX_TIMESTAMP(NOW()) <= 7200 AND a.reminder_sent 							   = 0 AND b.status_id = 5 AND a.status_id < 3");
          	
            if($res){
                foreach ($res[0] as $appointmentId){
                    $appointment = Appointment::find($appointmentId);
                    $carerProfile = $appointment->booking->bookingCarerProfile;
                    $serviceUserProfile = $appointment->booking->bookingServiceUser;
                    $purchaserProfile = $appointment->booking->bookingPurchaserProfile;

                    //send to carer
                    $message = 'Hi, '.$carerProfile->full_name.'. We just wanted to remind you that you have an 						appointment with '.$serviceUserProfile->full_name.' at '.$appointment->formatted_time_from.' 						today.';
                    SmsTools::sendSmsToCarer($message, $carerProfile);

                    if($serviceUserProfile->hasMobile())
                    {
                        //end to service user
                        $message = 'Hi, '.$serviceUserProfile->full_name.'. We just wanted to remind you that 								'.$carerProfile->full_name.' will be visiting you at '.$appointment->formatted_time_from.' 							today.';
                        SmsTools::sendSmsToServiceUser($message, $serviceUserProfile);
                    } else 
                    {
                      //send to purchaser
                      $message = 'Hi, '.$purchaserProfile->full_name.'. We just wanted to remind you that 								'.$carerProfile->full_name.' will be visiting '.$serviceUserProfile->full_name.' at 								'.$appointment->formatted_time_from.' today.';
                      SmsTools::sendSmsToPurchaser($message, $purchaserProfile);
                    }

                    $appointment->reminder_sent = true;
                    $appointment->save();
                }
            }          
            
          }
          else{
            
              $res = DB::select("SELECT a.id FROM appointments a JOIN bookings b ON a.booking_id = b.id
                              WHERE UNIX_TIMESTAMP(STR_TO_DATE(CONCAT(DATE_FORMAT(a.date_start, '%Y-%m-%d'), ' ', 								  a.time_from), \"%Y-%m-%d %H.%i\")) - UNIX_TIMESTAMP(NOW()) <= 3600 AND a.reminder_sent 							   = 0 AND b.status_id = 5 AND a.status_id < 3");
          	
            if($res)
            {
                foreach ($res[0] as $appointmentId)
                {
                    $appointment = Appointment::find($appointmentId);
                    $carerProfile = $appointment->booking->bookingCarerProfile;
                    $serviceUserProfile = $appointment->booking->bookingServiceUser;
                    $purchaserProfile = $appointment->booking->bookingPurchaserProfile;

                    //send to carer
                    $message = 'Hi, '.$carerProfile->full_name.'. We just wanted to remind you that you have an 						appointment with '.$serviceUserProfile->full_name.' at '.$appointment->formatted_time_from.' 						today.';
                    SmsTools::sendSmsToCarer($message, $carerProfile);

                    if($serviceUserProfile->hasMobile())
                    {
                        //end to service user
                        $message = 'Hi, '.$serviceUserProfile->full_name.'. We just wanted to remind you that 								'.$carerProfile->full_name.' will be visiting you at '.$appointment->formatted_time_from.' 							today.';
                        SmsTools::sendSmsToServiceUser($message, $serviceUserProfile);
                    } else 
                    {
                        //send to purchaser
                        $message = 'Hi, '.$purchaserProfile->full_name.'. We just wanted to remind you that 								'.$carerProfile->full_name.' will be visiting '.$serviceUserProfile->full_name.' at 								'.$appointment->formatted_time_from.' today.';
                        SmsTools::sendSmsToPurchaser($message, $purchaserProfile);
                    }

                    $appointment->reminder_sent = true;
                    $appointment->save();
                }
            }
            
          }
          
        })->everyMinute(); 

        //Automatic Agreement of appointment after 5 days carer has completed it but purchaser not
        $schedule->call(function () {
			$datetime = date('Y-m-d H:i:s');
          	$now = Carbon::now();
            $appointments = Appointment::where('status_id', 2)
                ->where('carer_status_id', 2)
                ->where('purchaser_status_id', 1)->get();
          	
            foreach ($appointments as $appointment) {
	 			 $diff = $now->diffInDays(Carbon::parse($appointment->date_start));
				  //DB::insert('insert into cron_test (date, action) values ("'.$datetime.'", "'.$diff.'")');
                if ($diff >= 5) {
                
                    $appointment->purchaser_status_id = 2;
                    $appointment->status_id = 4;
                    $appointment->save();
                    event(new AppointmentCompletedEvent($appointment));

                    //Auto pay for appointment 
                     $booking = $appointment->booking;
                     $stripeConnectedAccount = StripeConnectedAccount::where('carer_id', $booking->carer_id)->first();
                     $comment = 'Payment to Carer '.$booking->bookingCarerProfile->full_name.' ('.$booking->carer_id.') for appointment '.$appointment->id.' in booking '.$booking->id.' (dispute resolving)';
                     $res = PaymentTools::createTransfer($stripeConnectedAccount->id, $appointment->id, $booking->id, $appointment->price_for_carer*100, $comment);
                  	
                  

                    //check if booking is over
                    $booking = $appointment->booking;
                    $bookingAppointments = $booking->appointments()->get();
                    $numberOfBookings = $bookingAppointments->count();
                    $count = 0;

                    $allBookingAppointmentsCancelled = false;
                    $atLeastOneBookingAppointmentCompleted = false;
                    foreach ($bookingAppointments as $bookingAppointment) {
                        if ($bookingAppointment->status_id != 4 && $bookingAppointment->status_id != 5) {
                            break;
                        } elseif ($bookingAppointment->status_id == 4) {
                            $atLeastOneBookingAppointmentCompleted = true;
                        } elseif ($bookingAppointment->status_id == 5) {
                            $allBookingAppointmentsCancelled = true;
                        }

                        $count++;
                    }

                    if ($numberOfBookings == $count) {
                        if ($atLeastOneBookingAppointmentCompleted) {
                            $booking->status_id = 7;
                            //event(new BookingCompletedEvent($booking));
                        } elseif ($allBookingAppointmentsCancelled) {
                            $booking->status_id = 4;
                        }

                        $booking->save();
                    }
                }
            }
        })->everyMinute(); 
    }


    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
