<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
use App\StripeCharge;

class Booking extends Model
{
    protected $fillable = ['purchaser_id', 'service_user_id', 'carer_id', 'date_start', 'date_end', 'frequency_id', 'amount_for_purchaser', 'amount_for_carer', 'status_id', 'carer_status_id', 'purchaser_status_id', 'payment_method', 'use_register_code'];

    public function getDateStartAttribute()
    {
        if($this->appointments()->get()->count())
            return date('d-m-Y', strtotime($this->appointments()->orderBy('date_start')->get()->first()->date_start));
    }

    public function getDateEndAttribute()
    {
        if($this->appointments()->get()->count())
        return date('d-m-Y', strtotime($this->appointments()->orderByDesc('date_start')->get()->first()->date_start));
    }


    //relation

    public function bookingPurchaser()
    {
        return $this->belongsTo('App\User','purchaser_id','id');
    }

    public function bookingPurchaserProfile()
    {
        return $this->belongsTo('App\PurchasersProfile','purchaser_id','id');
    }

//    public function bookingServiceUser()
//    {
//        return $this->belongsTo('App\User','service_user_id','id');
//    }

    public function bookingServiceUser()
    {
        return $this->belongsTo('App\ServiceUsersProfile','service_user_id','id');
    }
    public function bookingCarer()
    {
        return $this->belongsTo('App\User','carer_id','id');
    }

    public function bookingCarerProfile()
    {
        return $this->belongsTo('App\CarersProfile','carer_id','id');
    }

    public function bookingStatus ()
    {
        return $this->belongsTo('App\BookingStatus','status_id','id');
    }

    public function transaction()
    {
        if($this->payment_method == 'credit_card')
            return $this->hasOne(StripeCharge::class);
        elseif($this->payment_method ==  'bonus_wallet')
            return $this->hasOne(Transaction::class);
    }


    public function appointments()
    {
        return $this->hasMany('App\Appointment');
    }

//    public function assistance_types()
//    {
//        return $this->belongsToMany('App\AssistanceType', 'bookings_assistance_types');
//    }

    public function overviews(){
        return $this->hasMany(BookingOverview::class, 'booking_id');
    }

    public function bookingReview()
    {
        return $this->hasMany('App\BookingOverview');
    }

    //Accessors
    public function getCarerRateAttribute(){
        return 10;
    }

    public function getPurchaserRateAttribute(){
        return 13;
    }
  	public function getHoursAttribute(){
//        $hours = 0;
        $minutes = 0;
      	$days = 0;
        $liveInCarer = 0;
        $appointments = $this->appointments()->get();
        foreach ($appointments as $appointment) {
          
            list($hour, $minute) = explode('.', $appointment->hours);
            $minutes += $hour * 60;
            $minutes += $minute;
//            $hours += $appointment->hours;
          	if($appointment->periodicity == 'live_in_carer'){
                $liveInCarer = 1;
                $days++;
            }
        }
      //cho "minutes".$minutes."days".$days;
      if(isset($minutes) && $minutes > 0 && $days == 0 ){ 
        $hours = floor($minutes / 60);
        $minutes -= $hours * 60;
       //echo $hours."+++".$minutes;
//        $hours = intdiv($hours, 60) . "." . ($hours % 60);
        $int_hours = (int)$hours;
        if ($int_hours != 0) {
            if ($hours / $int_hours != 1) {
                $hours = number_format($hours, 2);
            }
        }
		//change minute format
		 $minutes = ($minutes/60)*100;	
        return sprintf('%02d.%02d', $hours, $minutes);
      }elseif(isset($days) && $days >0){  //echo "12+++".$days; exit;
        	return $days*24;
      }else{
		$hours = 0;
        //$minutes = 0;
      	//return sprintf('%02d.%02d', $hours, $minutes);
        return $hours;
      }
//        
    }
 
    /*public function getHoursAttribute(){
        //$hours = 0;
        $minutes = 0;
        $appointments = $this->appointments()->get();
        $days = 0;
        $liveInCarer = 0;
      
      	//echo "No of Appointment ".$appointmentCnt; 
        foreach ($appointments as $appointment) {
         // echo "<pre> hours: "; print_r($appointment); echo "<pre>";
            list($hour, $minute) = explode('.', $appointment->hours);
            $minutes += $hour * 60;
            $minutes += $minute;
//            $hours += $appointment->hours;

            if($appointment->periodicity == 'live_in_carer'){
                $liveInCarer = 1;
                $days++;
            }
        }
		//echo "++".$minutes;
		if(isset($minutes) && $minutes > 0 && $days == 0 ){ //echo 13; exit;
        	$hours = floor($minutes / 60);
          	$minutes -= $hours * 60;
  //        $hours = intdiv($hours, 60) . "." . ($hours % 60);
          	$int_hours = (int)$hours;
          //echo $int_hours."***";
          	if ($int_hours != 0) {
              	if ($hours / $int_hours != 1) {
                  	$hours = number_format($hours, 2);
              	}
          	}
          //echo $hours."++".$minutes;
          //change minute format
           $minutes = ($minutes/60)*100;
          return sprintf('%02d.%02d', $hours, $minutes);
        }else if(isset($days) && $days >0){  //echo 12; exit;
        	return $days*24;
        }
        //return sprintf('%02d.%02d', $hours, $minutes);
         
    }*/



    
    public function getPriceAttribute(){// echo 11;
        $price = 0;
        $user = Auth::user();
        $appointments = $this->appointments()->get();
        $startDate = $endDate = "";
        $liveInCarer =  0;
        $i = 1; 
//echo "<pre>"; print_r($user); echo "</pre>"; exit;
        foreach ($appointments as $appointment){

            if($user && $user->user_type_id == 3) {  
				//echo "Cares Price : " .$appointment->carer_price;
                $price += $appointment->carer_price;
            } else { //price for purchaser  echo "Deepak";
              // echo "Purchaser Price".$appointment->purchaser_price;
                if ($user->use_register_code == 1) {
					// echo "Deepak3"; 
                    $price += $appointment->purchaser_price;
                  $price += $appointment->purchaser_price;
                } else { 
                   //echo $appointment->purchaser_price."<br/>";
                    $price += $appointment->purchaser_price;
                }
            }
            if($appointment->periodicity == 'live_in_carer'){
                $liveInCarer =  1;
                if($i==1){
                    $startDate = $appointment->date_start ;
                }
                if($i==count($appointments)){
                    $endDate = $appointment->date_start ;
                }
            }
        	$i++;
    	}
    // echo "Price".$price;
		return $price;
        if($liveInCarer  && !empty($startDate) && !empty($endDate)){
            //Get price 
           $datediff =  strtotime($endDate) - strtotime($startDate);
          
           $days =  round($datediff / (60 * 60 * 24)) > 0 ?  round($datediff / (60 * 60 * 24))+1 : 1;
          //echo " Days".$days."<br>";
			
           $liveDetails = LiveCarer::find(1);
           $perDayprice = $liveDetails->purchaser_rate;
           $liveDetails = LiveCarer::find(2);
           $perWeekprice = $liveDetails->purchaser_rate;
          
          	$noOfWeek 		=	floor($days/7);
          	$remaindays 	=	floor($days%7);
          //echo "Days".$days."No of Week".$noOfWeek."Remain Days".$remaindays;
          	$weekPrice 			=	floor($noOfWeek*$perWeekprice);
			$remainDaysPrice 	=	floor($remaindays*$perDayprice);
          	$price 				=	$weekPrice+$remainDaysPrice;
			
			return $price;
        }

        
    }


    public function getLiveInCarerAttribute(){
        $appointments = $this->appointments()->get();
      	//echo "<pre>"; print_r($appointments); echo "<pre>"; exit;
        foreach ($appointments as $appointment){
            if($appointment->periodicity == 'live_in_carer'){
                return true;
            }
        }
        return false;
    }

    public function getCarerPriceAttribute(){
        $price = 0;
        $appointments = $this->appointments()->get();
     // echo "<pre>Booking Data : ";print_r($appointments); echo "</pre>"; exit;
        foreach ($appointments as $appointment)
            $price += $appointment->carer_price;
//ho "<pre>Carer Price : ";print_r($appointments); echo "</pre>"; exit;
        return round($price, 2);
    }

    public function getPurchaserPriceAttribute(){
        $price = 0;
        $appointments = $this->appointments()->get();
      	//echo "<pre> Price :"; print_r($appointments); echo "<pre>"; exit;
        foreach ($appointments as $appointment)
            $price += $appointment->purchaser_price;
		//exit;
       return round($price, 2);
    }

    public function getDateFromAttribute(){
        if($this->appointments()->get()->count())
            return $this->appointments()->orderBy('date_start')->get()->first()->date_start;
    }

    public function getDateToAttribute(){
        if($this->appointments()->get()->count())
            return $this->appointments()->orderByDesc('date_start')->get()->first()->date_start;
    }

    public function getHasActiveAppointmentsAttribute(){
        return $this->appointments()->whereIn('status_id', [1, 2, 3])->get()->count() > 0;
    }
}
