<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Page;
use App\Faq;

class FaqController extends FrontController
{

    public function __construct() {
        parent::__construct();

        $this->template = config('settings.frontTheme').'.templates.homePage';
    }

    public function index(){
		
		$data = [];
        $this->title = 'Frequently Asked Questions - The Best Elderly Homecare for Greater Manchester';
        $this->keywords='homecare, home care cost , carer, care agency, manchester, trafford, salford, bolton, bury, wigan, stockport, tameside, salford, oldham';
        $this->description='Homecare for the Elderly. Helping you find the best affordable care at home quickly and easily in Manchester, Bolton, Sale, Rochdale, Stockport, Salford, Oldham, Bury, Tameside, Trafford and Wigan.';

		 //Get page data 
        $page = Page::with('user')->where('slug', 'faq')->first();
		$pageId = $page->id;
		$this->title = $page->meta_title;
        $this->keywords = $page->meta_keyword;
        $this->description = $page->meta_description;
        
        //get all faqs
         $faqs = Faq::orderBy('faq_order', 'asc')->get();
		 $data = array_add($data,'faqs',$faqs);

        $header = view(config('settings.frontTheme').'.headers.baseHeader')->render();
        $footer = view(config('settings.frontTheme').'.footers.baseFooter')->render();
        $modals = view(config('settings.frontTheme').'.includes.modals')->render();

        $this->vars = array_add($this->vars,'header',$header);
        $this->vars = array_add($this->vars,'footer',$footer);
        $this->vars = array_add($this->vars,'modals',$modals);

        $this->content = view(config('settings.frontTheme').'.homePage.faqPage',$data)->render();

        return $this->renderOutput();
    }
}
