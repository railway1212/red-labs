<?php
namespace App\Http\Controllers;
use App\PostCodes;
use App\PurchasersProfile;
use App\ServiceUsersProfile;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Language;
use App\AssistanceType;
use App\ServiceType;
use App\CarersProfile;
use App\Page;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\URL;

class SearchListController extends Controller
{
    public function SearchList($data = null)
        {
     
                 $carers             = '';
                 $baseDir                    =   public_path();
                 $baseUrl                    =   URL::to('/');

                 $carers =  DB::select("SELECT cp.id, cp.first_name,cp.family_name,cp.sentence_yourself 
                 FROM `carers_profiles` as cp
                WHERE registration_progress=20 and profiles_status_id=2  LIMIT 0,12");

                 $carerdata = [];                                               

                foreach($carers as &$carer) 
              {

                $carers_id   =   $carer->id;
                $carers_first_name   =   $carer->first_name;
                $carers_family_name   =   $carer->family_name;
                $carers_sentence  =   $carer->sentence_yourself;

                 $isServiceImageFound = $baseDir."/img/profile_photos/".$carers_id.".png";
                 $serviceImageName = $baseUrl."/no_image.png";

                    if (!file_exists($isServiceImageFound)) {
                        $serviceImageName  =   $baseUrl."/img/profile_photos/".$carers_id .".png";
                    }
                    
                    else{
                        $serviceImageName          =   $baseUrl."/no_image.png";

                    }
                   

                    $carerdata[] = array(
                        "first_name" => $carer->first_name,
                        "family_name" => $carer->family_name,
                        "id" =>  $carer->id,
                        "sentence"=>$carer->sentence_yourself,
                        "image_name" => $serviceImageName
                    );

             }

              
            
                    $success['carers']  = $carerdata;
               
                    return response()->json([
                        'status'=>200 ,
                        'message'=> ' successful',
                        'data'=>$success
                    ]); 
                    }
    
    }
