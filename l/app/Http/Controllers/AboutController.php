<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Page;
use DB;

class AboutController extends FrontController
{

    public function __construct() {
        parent::__construct();

        $this->template = config('settings.frontTheme').'.templates.homePage';
    }

    public function index(){


        $this->title = 'About our Homecare Service';
        $this->keywords='homecare, live in elderly care, carer respite, manchester, trafford, salford, bolton, bury, wigan, stockport, tameside, salford, oldham';
        $this->description='We at Holm Care offer assistance to individuals to connect directly to skilled elderly care workers in the Manchester area. Our primary goal is to make finding quality home care services easier. If you are looking for elderly care support workers in the Manchester area, then please reviewe the details of the carers provided by us. ';
        
        //Get page data 
        $data = [];
        $page = Page::with('user')->where('slug', 'about')->first();
		$pageId = $page->id;
		$this->title = $page->meta_title;
        $this->keywords = $page->meta_keyword;
        $this->description = $page->meta_description;
        $extraBlocks =  unserialize($page->extra_content);
        $banner =  DB::table('banners')->where('page_id', '1')->first();  //banner
        $trusts =  DB::table('trusts')->orderBy('id','desc')->get();  //trusts
        $ourMission = DB::table('contacts')->where('data_key','our_mission')->first();
        $founder = DB::table('contacts')->where('data_key','founder')->first();

        $data = array_add($data,'content',$page->content);
        $data = array_add($data,'banner',$banner);
        $data = array_add($data,'trusts',$trusts);
        $data = array_add($data,'extraBlocks',$extraBlocks);
        $data = array_add($data,'ourMission',$ourMission->data_val);
        $data = array_add($data,'founder',$founder->data_val);


        $header = view(config('settings.frontTheme').'.headers.baseHeader')->render();
        $footer = view(config('settings.frontTheme').'.footers.baseFooter')->render();
        $modals = view(config('settings.frontTheme').'.includes.modals')->render();

        $this->vars = array_add($this->vars,'header',$header);
        $this->vars = array_add($this->vars,'footer',$footer);
        $this->vars = array_add($this->vars,'modals',$modals);

        $this->content = view(config('settings.frontTheme').'.homePage.aboutPage', $data)->render();

        return $this->renderOutput();
    }

    public function job(){
        $this->title = 'Holm Care Jobs';

        $this->keywords='home care assistance, home help for the elderly, personal assistant services';


        $header = view(config('settings.frontTheme').'.headers.baseHeader')->render();
        $footer = view(config('settings.frontTheme').'.footers.baseFooter')->render();
        $modals = view(config('settings.frontTheme').'.includes.modals')->render();

        $this->vars = array_add($this->vars,'header',$header);
        $this->vars = array_add($this->vars,'footer',$footer);
        $this->vars = array_add($this->vars,'modals',$modals);

        $this->content = view(config('settings.frontTheme').'.homePage.jobs')->render();

        return $this->renderOutput();
    }

    public function onepage(){
//        $this->title = 'ONEPAGE - HOLM CARE';
//        $this->description = '';
//        $this->keywords='home care assistance, home help for the elderly, personal assistant services';
//
//
//        $header = view(config('settings.frontTheme').'.headers.baseHeader')->render();
//        $footer = view(config('settings.frontTheme').'.footers.baseFooter')->render();
//        $modals = view(config('settings.frontTheme').'.includes.modals')->render();
//
//        $this->vars = array_add($this->vars,'header',$header);
//        $this->vars = array_add($this->vars,'footer',$footer);
//        $this->vars = array_add($this->vars,'modals',$modals);
//
//        $this->content = view(config('settings.frontTheme').'.homePage.onepage')->render();
//
//        return $this->renderOutput();
        return redirect('/pdf/Holm_A5info_print.pdf');
    }
}

