<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Page;

class TermsController extends FrontController
{

    public function __construct() {
        parent::__construct();

        $this->template = config('settings.frontTheme').'.templates.homePage';
    }

    public function index(){


        $this->title = 'Terms and conditions -  The Best Elderly Homecare for Greater Manchester';

        $this->keywords='';
      
        //Get page data 
      	$data = [];
        $page = Page::with('user')->where('slug', 'terms')->first();
		$pageId = $page->id;
		$this->title = $page->meta_title;
        $this->keywords = $page->meta_keyword;
        $this->description = $page->meta_description;
		$data = array_add($data,'content',$page->content);
        $data = array_add($data,'title',$page->title);


        $header = view(config('settings.frontTheme').'.headers.baseHeader')->render();
        $footer = view(config('settings.frontTheme').'.footers.baseFooter')->render();
        $modals = view(config('settings.frontTheme').'.includes.modals')->render();

        $this->vars = array_add($this->vars,'header',$header);
        $this->vars = array_add($this->vars,'footer',$footer);
        $this->vars = array_add($this->vars,'modals',$modals);

        $this->content = view(config('settings.frontTheme').'.homePage.termsPage', $data)->render();

        return $this->renderOutput();
    }
}
