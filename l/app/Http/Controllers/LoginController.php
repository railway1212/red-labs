<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Alert;
use Validator;
use App\User;
use App\UserType;
use JWTFactory;
use JWTAuth;
use Response;
use Illuminate\Support\Facades\Input;
use App\PurchasersProfile;
use App\CarersProfile;
use App\ServiceUsersProfile;

use Illuminate\Support\Facades\URL;
use App\Events\LogSuccessfulLogin;




class LoginController extends Controller
{
    
        public function login(Request $request){ 
            $baseDir                    =   public_path();
            $baseUrl                    =   URL::to('/');
            $userDataObj                =   '';
            $serveiceuser               = '';
            $request                    = file_get_contents('php://input');
            $formData                   =   json_decode($request, true);
            if(Auth::attempt(['email' => $formData['email'], 'password' => $formData['password']]))
            { 
                $user                   = Auth::user(); 
                event(new LogSuccessfulLogin($user));
                $user->api_token        = str_random(60);
                $user->save();
                $usertypes              =   User::with('userType')
                                                ->where('id','=',$user->id)
                                                ->first();
                
                                       
                if($usertypes->user_type_id == 3){ // get Carers Profile
                    $userDataObj        =   CarersProfile::with('user')
                                            ->select([
                                                'first_name',
                                                'family_name',
                                                'like_name',
                                                'gender',
                                                'mobile_number',
                                                'address_line1',
                                                'address_line2',
                                                'town',
                                                'postcode',
                                            ])
                                            ->where('id','=',$user->id)
                                            ->first();

                }else if($usertypes->user_type_id == 1){ //Purchaser profile
                    $userDataObj        =   PurchasersProfile::with('user')
                                            ->select([
                                                'first_name',
                                                'family_name',
                                                'like_name',
                                                'gender',
                                                'mobile_number',
                                                'address_line1',
                                                'address_line2',
                                                'town',
                                                'postcode',
                                            ])
                      						->where('id','=',$user->id)			
                                            ->first();
                 $purchaserProfile =  PurchasersProfile::findOrFail($usertypes->id);
                 $purchaserId = $purchaserProfile->id;
                            
                $serveiceuser  =  ServiceUsersProfile::where('purchaser_id', $purchaserId)
                                                            ->select([
                                                                'id',
                                                            'first_name',
                                                            'family_name',
                                                            'like_name',
                                                            'gender',
                                                            'care_for'
                                                            ])
                                                          ->orderBy('id', 'desc')
                                                          ->get();

                $servicedata = $serveiceuser->toArray();
//  dd($servicedata);

                 $service_user = [];                                               
            

                 for($i=0;$i<count($servicedata);$i++)
                {
                    $serveiceuser_id            =  $servicedata[$i]['id'];
                   
                    $serveiceuser_first_name    =  $servicedata[$i] ['first_name'];
                    $serveiceuser_like_name     =  $servicedata[$i]['like_name'];
                    $serveiceuser_care_for      =  $servicedata[$i]['care_for'];
                    $serveiceuser_family_name   =  $servicedata[$i]['family_name'];

                    $isServiceImageFound =       $baseDir."/public/img/service_user_profile_photos/".$serveiceuser_id.".png";
                    $serviceImageName = $baseUrl."/l/public/no_image.png";
                    if (!file_exists($isServiceImageFound)) {
                        $serviceImageName  =   $baseUrl."/public/img/service_user_profile_photos/".$serveiceuser_id.".png";
                    }
                    
                    else{
                        $serviceImageName          =   $baseUrl."/l/public/no_image.png";
                       
                    }

                    $service_user[] = array(
                                        "first_name" => $servicedata[$i] ['first_name'],
                                        "family_name" => $servicedata[$i]['family_name'],
                                        "id" =>  $servicedata[$i]['id'],
                                        "care_for"=>$servicedata[$i]['care_for'],
                                        "image_name" => $serviceImageName
                                    );
                                    
        
                }
                


                $success['service_user']  =  $service_user ;


    
                }    

                // echo $baseUrl; exit;
                $isImagFound            =   $baseDir."/public/img/profile_photos/".$user->id.".png";
                
                
                if (!file_exists($isImagFound)) {
                    $imageName          =   $baseUrl."/public/img/profile_photos/".$user->id.".png";
                   
                    
                }else{
                    $imageName          =   $baseUrl."/l/public/no_image.png";
                   
                }


                 $success['userData']                 =     $userDataObj;

                $success['id']          =     $user->id;
                $success['api_token']   =     $user->api_token; 
                $success['user_type']   =     $usertypes->userType->name; 
                $success['image_name']  =     $imageName;



                return response()->json([
                    'status'=>200 ,
                    'message'=> 'login successful',
                    'data'=>$success
                ]); 
                

                // $success['token'] =  $user->createToken('MyApp')-> accessToken; 
                // return response()->json(['success' => $success], $this-> successStatus); 
            } 
            else
            { 
                return response()->json([
                    'status'=>401 ,
                    'message'=> 'Incorrect credential',
                
                ]); 
                
            } 

        
    }



        public function logout(Request $request)
        {
            //echo "<pre>"; print_r($request->all()); echo "<pre>"; exit;
            // $user = Auth::user();
            $request                            = file_get_contents('php://input');
            $formData                           =   json_decode($request, true);
            
            $recordCnt     =   User::where('id','=',$formData['id'])->count();
            if($recordCnt > 0){
                $isLoginCnt     =   User::where('id','=',$formData['id'])
                                        ->where('api_token','!=','')->count();
                if($isLoginCnt > 0){
                    $record     =   User::where('id','=',$formData['id'])->first();
                    $record->api_token = null;
                    $record->save();
					if (isset($_COOKIE['api_token'])) {
                        unset($_COOKIE['api_token']);
                        unset($_COOKIE['userid']);
                        setcookie('api_token', null,  time() - 3600, '/');
                        setcookie('userid', null,  time() - 3600, '/');
                        setcookie('holmdata', null,  time() - 3600, '/');
                        setcookie('family_name', null,  time() - 3600, '/');
                        setcookie('service_user', null,  time() - 3600, '/');
                        setcookie('user_type', null,  time() - 3600, '/');
                        setcookie('first_name', null,  time() - 3600, '/');
                        setcookie('image_name', null,  time() - 3600, '/');
                        setcookie('ckeck_cookie', null,  time() - 3600, '/');
                        setcookie('laravel_session', null,  time() - 3600, '/');
                    } 
                     $this->performLogout($request);
                    Auth::logout();
                    setcookie('laravel_session', null,  time() - 3600, '/');
                    //Auth::logout();
                    return response()->json([
                        'status'=>200 ,
                        'message'=> 'Successfully Logged Out',
                        
                    ]);
                }else{
                    return response()->json([
                        'status'=>401 ,
                        'message'=> 'This user already logged out',
                        
                    ]);
                } 
            }else{
                return response()->json([
                    'status'=>401 ,
                    'message'=> 'Invalid User. Please try later!!!',
                    
                ]);
            }
            
            
            
        }
}
