<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Blog;
use App\Page;


class BlogController extends FrontController
{

    public function __construct() {
        parent::__construct();

        $this->template = config('settings.frontTheme').'.templates.homePage';
    }

    public function index(Request $request){

        $data = [];
        $this->title = 'Holm Care - Blog';
        $this->description='Get updated with all types of latest information on homecare support services by reviewing our blog section. If you or any of your family members need elderly care services, consider Holm Care to find a suitable care support worker.';
       
       //Get page data 
        $page = Page::with('user')->where('slug', 'blog')->first();
		$pageId = $page->id;
		$this->title = $page->meta_title;
        $this->keywords = $page->meta_keyword;
        $this->description = $page->meta_description;
       
       
        $input = $request->all();
        $header = view(config('settings.frontTheme').'.headers.baseHeader')->render();
        $footer = view(config('settings.frontTheme').'.footers.baseFooter')->render();
        $modals = view(config('settings.frontTheme').'.includes.modals')->render();

        if(!isset($input['search']))
            $blog = Blog::with('user')
                ->where('status', 'published')
                ->orderBy('created_at','DESC')
                ->paginate(8);
        else
            $blog = Blog::with('user')
                ->where('status', 'published')
                ->where(function ($query) use ($input) {
                    $query->where('body', 'like', '%' . $input['search'] . '%')
                        ->orWhere('title', 'like', '%' . $input['search'] . '%');
                })
                ->orderBy('created_at','DESC')
                ->paginate(8);
//dd($blog->items());
        $blogDate = Blog::where('status', 'published')->selectRaw("date_format(`published_at`,'%M %Y') as cdate,date_format(`published_at`,'%m')as month,date_format(`published_at`,'%Y')as year")->groupBy('published_at')->distinct()->get();

        $data = array_add($data,'blog',$blog);
        $data = array_add($data,'blogDate',$blogDate);

        $this->vars = array_add($this->vars,'header',$header);
        $this->vars = array_add($this->vars,'footer',$footer);
        $this->vars = array_add($this->vars,'modals',$modals);


        $this->content = view(config('settings.frontTheme').'.homePage.blogPage',$data)->render();

        return $this->renderOutput();
    }

    public function viewFilter(Request $request,$month,$year){
        $input = $request->all();

        $data = [];
        $this->title = 'Holm Care - Blog';

        $header = view(config('settings.frontTheme').'.headers.baseHeader')->render();
        $footer = view(config('settings.frontTheme').'.footers.baseFooter')->render();
        $modals = view(config('settings.frontTheme').'.includes.modals')->render();
        if(!isset($input['search']))
            $blog = Blog::with('user')->where('status', 'published')->whereMonth('published_at', '=', $month)->whereYear('published_at', '=', $year)
            ->paginate(8);
        else
            $blog = Blog::with('user')
                ->where('status', 'published')
                ->whereMonth('published_at', '=', $month)
                ->whereYear('published_at', '=', $year)
                ->where(function ($query) use ($input) {
                    $query->where('body', 'like', '%' . $input['search'] . '%')
                        ->orWhere('title', 'like', '%' . $input['search'] . '%');
                })
                ->paginate(8);
        $blogDate = Blog::where('status', 'published')->selectRaw("date_format(`published_at`,'%M %Y') as cdate,date_format(`published_at`,'%m')as month,date_format(`published_at`,'%Y')as year")->groupBy('published_at')->distinct()->get();

        $data = array_add($data,'blog',$blog);
        $data = array_add($data,'month',$month);
        $data = array_add($data,'year',$year);
        $data = array_add($data,'blogDate',$blogDate);

        $this->vars = array_add($this->vars,'header',$header);
        $this->vars = array_add($this->vars,'footer',$footer);
        $this->vars = array_add($this->vars,'modals',$modals);


        $this->content = view(config('settings.frontTheme').'.homePage.blogPage',$data)->render();

        return $this->renderOutput();
    }

    public function view(Request $request, $slug){
        $data = [];
        
        

        $header = view(config('settings.frontTheme').'.headers.baseHeader')->render();
        $footer = view(config('settings.frontTheme').'.footers.baseFooter')->render();
        $modals = view(config('settings.frontTheme').'.includes.modals')->render();

         //$blog = Blog::with('user')->findOrFail($blog_id);
         $blog = Blog::with('user')->where('slug', $slug)->first();
		 $blog_id = $blog->id;
		 
		$this->title = $blog->meta_title;
        $this->keywords = $blog->meta_keywords;
        $this->description = $blog->meta_description;
		 
		 		

        if (Blog::all()->count() > 2) {
            $relationPost = Blog::all()->where('id', '!=', $blog_id)->random(2);
            $data = array_add($data, 'relationPost', $relationPost);
        }

        //dd($relationPost);

        $data = array_add($data,'blog',$blog);
        $data = array_add($data,'image','');
        $data = array_add($data,'title',$blog->title);
        $data = array_add($data,'url',route('BlogPage').'/'.$blog->slug);

        $this->vars = array_add($this->vars,'header',$header);
        $this->vars = array_add($this->vars,'footer',$footer);
        $this->vars = array_add($this->vars,'modals',$modals);


        $this->content = view(config('settings.frontTheme').'.homePage.blogViewPage',$data)->render();

        return $this->renderOutput();
    }
}

