<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use DB;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public static function getFooterData(){
       $copyright = DB::table('contacts')->where('data_key','copyright')->first();
       $footerAddress = DB::table('contacts')->where('data_key','footer_address')->first();
       $facebookLink = DB::table('contacts')->where('data_key','facebook_link')->first();
       $twitterLink = DB::table('contacts')->where('data_key','twitter_link')->first();
       $googleLink = DB::table('contacts')->where('data_key','google_link')->first();
       $footerData = array(
                        'copyright'     => $copyright->data_val,
                        'footerAddress' => $footerAddress->data_val,
                        'facebookLink'  => $facebookLink->data_val,
                        'twitterLink'   => $twitterLink->data_val,
                        'googleLink'    => $googleLink->data_val
                        );   

        return $footerData;
    }



}

