<?php

namespace App\Http\Controllers;

use App\MailError;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Mail\OrderShipped;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

use Swift_TransportException;
use App\Page;

class ContactController extends FrontController
{

    public function __construct() {
        parent::__construct();

        $this->template = config('settings.frontTheme').'.templates.homePage';
    }

    public function index(){
		
        $this->title = 'Contact Us - HOLM CARE';
        $this->keywords='';
        $this->description='If you need any information related to Holm Care services, then use our contact page to write your message there. The team at Holm Care will contact you at the earliest possible time. You may even call us or email us with your query.';
        
        
         //Get page data 
        $data = [];
        $page = Page::with('user')->where('slug', 'contact')->first();
		$pageId = $page->id;
		$this->title = $page->meta_title;
        $this->keywords = $page->meta_keyword;
        $this->description = $page->meta_description;
        $address = DB::table('contacts')->where('data_key','address')->first();
        $phoneNumber = DB::table('contacts')->where('data_key','phone_number')->first();
        $contactTime = DB::table('contacts')->where('data_key','contact_time')->first();
        $emailAddress = DB::table('contacts')->where('data_key','email_address')->first();
        $googleMap = DB::table('contacts')->where('data_key','google_map')->first();
        $writeUs = DB::table('contacts')->where('data_key','write_us')->first();
        
        $data = array_add($data,'address',$address->data_val);
        $data = array_add($data,'phoneNumber',$phoneNumber->data_val);
        $data = array_add($data,'contactTime',$contactTime->data_val);
        $data = array_add($data,'emailAddress',$emailAddress->data_val);
        $data = array_add($data,'googleMap',$googleMap->data_val);
        $data = array_add($data,'writeUs',$writeUs->data_val);

        $header = view(config('settings.frontTheme').'.headers.baseHeader')->render();
        $footer = view(config('settings.frontTheme').'.footers.baseFooter')->render();
        $modals = view(config('settings.frontTheme').'.includes.modals')->render();

        $this->vars = array_add($this->vars,'header',$header);
        $this->vars = array_add($this->vars,'footer',$footer);
        $this->vars = array_add($this->vars,'modals',$modals);

        $this->content = view(config('settings.frontTheme').'.homePage.contactPage',$data)->render();

        return $this->renderOutput();
    }

    public function thank(){

        $this->title = 'Contact us - HOLM CARE';
        $this->keywords='';

        $header = view(config('settings.frontTheme').'.headers.baseHeader')->render();
        $footer = view(config('settings.frontTheme').'.footers.baseFooter')->render();
        $modals = view(config('settings.frontTheme').'.includes.modals')->render();

        $this->vars = array_add($this->vars,'header',$header);
        $this->vars = array_add($this->vars,'footer',$footer);
        $this->vars = array_add($this->vars,'modals',$modals);

        $this->content = view(config('settings.frontTheme').'.homePage.ThankYouContact')->render();

        return $this->renderOutput();
    }

    public function send(Request $request){

        $input = $request->all();

        $text = view(config('settings.frontTheme') . '.emails.contact')->with([
            'userMessage'=>$input['message'],'phone'=>$input['phone'],'email'=>$input['email'],'userName'=>$input['name']
        ])->render();

        DB::table('mails')
            ->insert(
                [
                    'email' =>'info@holm.care',
                    'subject' =>$input['topic'],
                    'text' =>$text,
                    'time_to_send' => Carbon::now(),
                    'status'=>'new'
                ]);

        return redirect()->route('ThankPage');

    }
  
  
  
  
  
  
  
}
