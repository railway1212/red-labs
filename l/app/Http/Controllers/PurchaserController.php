<?php

namespace App\Http\Controllers;

use App\BonusTransaction;
use App\Booking;
use App\Interfaces\Constants;
use App\PurchasersProfile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Cookie;
use Auth;
use App\User;
use App\Appointment;
use App\StripeConnectedAccount;
use Illuminate\Support\Facades\DB;
use PaymentTools;

class PurchaserController extends FrontController implements Constants
{
    //public function __construct()
    //{
    //    parent::__construct();

   // }
  
    public function __construct()
    {
        parent::__construct();
    }

    public function index($id=null)
    {
      
    if( isset($_COOKIE['api_token'])){
            $api_token = $_COOKIE['api_token'];
            $user = User::where('api_token',$api_token)->first();
            if($user)
            {
                Auth::login($user); // login user automatically
				$this->user = $user;
                // dd('hello2');
            }   
            //  dd('hello1');
        }
        $this->template = config('settings.frontTheme') . '.templates.purchaserPrivateProfile';
        $this->title = 'Holm Care';


        if (!$this->user) {
            return redirect('/enter');
            //$this->content = view(config('settings.frontTheme') . '.ImCarer.ImCarer')->render();
        } else {

            $newBookings = Booking::whereIn('status_id', [self::AWAITING_CONFIRMATION])->where('purchaser_id', $this->user->id)->get();
       
            $this->vars = array_add($this->vars, 'bookingCount', $newBookings->count());

            $this->vars = array_add($this->vars, 'newBookings', $newBookings);
            $inProgressBookings = Booking::whereIn('status_id', [self::IN_PROGRESS])->where('purchaser_id', $this->user->id)->get();
            $this->vars = array_add($this->vars, 'inProgressBookings', $inProgressBookings);

            if(!empty($id) && Auth::user()->user_type_id==4){ //админ
                $purchaserProfile = PurchasersProfile::findOrFail($id);
            } else {
                $purchaserProfile = PurchasersProfile::findOrFail($this->user->id);
            }
          	
            $purchaserProfile->active_user=null;
            $purchaserProfile->save();

            $serviceUsers = $purchaserProfile->serviceUsers;
//            $serviceUsers = $purchaserProfile->serviceUsers()->where('profiles_status_id', '!=', 5)->get();
//            dd($serviceUsers);
     
            $this->vars = array_add($this->vars, 'user', $this->user);
            $this->vars = array_add($this->vars, 'purchaserProfile', $purchaserProfile);
            $this->vars = array_add($this->vars, 'serviceUsers', $serviceUsers);
//            $serviceUsersNotDeletes = array();
//            $serviceUsersNotDeletes = $serviceUsers->filter(function ($serviceUsers) {
//                return ($serviceUsers->deleted !='Yes'&&$serviceUsers->care_for=='Myself');
//            });
//            $this->vars = array_add($this->vars, 'showDropDown', $serviceUsersNotDeletes->count()==0);

            $forMyselfCreated = false;

            foreach($serviceUsers as $su){
                if($su->deleted != 'Yes' && strtolower($su->care_for) == 'myself') {
                    $forMyselfCreated = true;//Уже есть СЮ для себя
                }
            }

            ///deduct all used bonus 
            $bonusBalance  =  BonusTransaction::where('user_id', $purchaserProfile->id)->sum('amount');
            /*$bonusUsed     =  Booking::where('purchaser_id',$purchaserProfile->id)
                                      ->where('payment_method','bonus_wallet')
                                      ->sum('amount_for_purchaser');
                        
            
            $bonusBalance = $bonusBalance - $bonusUsed;*/
          
          
          
            $this->vars = array_add($this->vars, 'forMyselfCreated', $forMyselfCreated);
            $this->vars = array_add($this->vars, 'bonusBalance', $bonusBalance);

            $header = view(config('settings.frontTheme').'.headers.baseHeader')->render();
            $footer = view(config('settings.frontTheme').'.footers.baseFooter')->render();
            $modals = view(config('settings.frontTheme').'.includes.modals')->render();

            $this->vars = array_add($this->vars,'header',$header);
            $this->vars = array_add($this->vars,'footer',$footer);
            $this->vars = array_add($this->vars,'modals',$modals);
            $this->content = view(config('settings.frontTheme') . '.purchaserProfiles.PrivateProfile')->with($this->vars)->render();

        }

        return $this->renderOutput();
    }




    public function update(Request $request) {

        $input = $request->all();
        // echo "<pre>"; print_r($input); echo "<pre>"; exit;
        $purchaserProfile = PurchasersProfile::findOrFail($input['id']);
        $userProfile = User::findOrFail($input['id']);


        if($input['stage'] == 'payment'){
            return response(json_encode(['status'=>'does not save | function don`t result']),400);
        }

        $depart = '';
        $validate_postcode = 'regex:/^(([Bb][Ll][0-9])|([Mm][0-9]{1,2})|([Oo][Ll][0-9]{1,2})|([Ss][Kk][0-9]{1,2})|([Ww][AaNn][0-9]{1,2})) {0,}([0-9][A-Za-z]{2})$/';
        //add postcode validate 
        if(isset($input['purchaser_flag']) && $input['purchaser_flag']==1){
			$validate_postcode="";
		}
        
        

        if ($input['stage'] == 'general') {

            $this->validate($request, [

                'first_name' =>
                    array(
                        'required',
                        'string',
                        'max:20'
                    ),
                'family_name' =>
                    array(
                        'required',
                        'string',
                        'max:20'
                    ),
                'gender' =>
                    array(
                        'required',
                        'in:"Male","Female"',
                    ),
                'DoB' =>
                    array(
                        'required',
                    ),
                'like_name' =>
                    array(
                        'required',
                        'string',
                        'max:128'
                    ),
                'mobile_number' =>
                    array(
                        'required',
                        'max:30'
                    ),
                'address_line1' =>
                    array(
                        'required',
                        'string',
                        'max:256'
                    ),
                'address_line2' =>
                    array(
                        'nullable',
                        'string',
                        'max:256'
                    ),
                'town' =>
                    array(
                        'required',
                        'string',
                        'max:128'
                    ),
                'postcode' =>
                    array(
                        'required',
                        $validate_postcode
                       ,)
            ]);

            $depart = "#PrivateGeneral";
            
            		
          	if (isset($input['user_email'])) $userProfile->email = $input['user_email'];
          	if (isset($input['referral_code'])) $userProfile->referral_code = $input['referral_code'];
          
			//echo "<pre>"; print_r($input['user_email']."++".$userProfile->email."++".$input['id']); echo "<pre>"; exit;
          	$userProfile->save();
            if (isset($input['first_name'])) $purchaserProfile->first_name = $input['first_name'];
            if (isset($input['family_name'])) $purchaserProfile->family_name = $input['family_name'];
            if (isset($input['gender'])) $purchaserProfile->gender = $input['gender'];
            if(isset($input['admin_edit_purchaser_flag'])) {$purchaserProfile->DoB = $input['DoBPurchaser'];}
            else{
				if (isset($input['DoB'])) $purchaserProfile->DoB = $input['DoB'];
			}
          	
            if (isset($input['like_name'])) $purchaserProfile->like_name = $input['like_name'];
            if (isset($input['address_line1'])) $purchaserProfile->address_line1 = $input['address_line1'];
            if (isset($input['address_line2'])) $purchaserProfile->address_line2 = $input['address_line2'];
            if (isset($input['town'])) $purchaserProfile->town = $input['town'];
            if (isset($input['postcode'])) $purchaserProfile->postcode = $input['postcode'];
            if (isset($input['mobile_number'])) $purchaserProfile->mobile_number = $input['mobile_number'];
            
          	
           
            
/*            if (isset($input['sentence_yourself'])) $purchaserProfile->sentence_yourself = $input['sentence_yourself'];
            if (isset($input['description_yourself'])) $purchaserProfile->description_yourself = $input['description_yourself'];*/

            $purchaserProfile->save();
          	
           unset($purchaserProfile); 
          unset($userProfile); 
        }



        if ($input['stage'] == 'payment') {

            $depart = "#Payment";

            $purchaserProfile = PurchasersProfile::findOrFail($input['id']);

            if (isset($input['name_of_cardholder'])) $purchaserProfile->name_of_cardholder = $input['name_of_cardholder'];
            if (isset($input['payment_card_number'])) $purchaserProfile->payment_card_number = $input['payment_card_number'];
            if (isset($input['expiry_date'])) $purchaserProfile->sort_code = $input['expiry_date'];
            if (isset($input['cvv'])) $purchaserProfile->cvv = $input['cvv'];
            if (isset($input['paypal_amazon_details'])) $purchaserProfile->paypal_amazon_details = $input['paypal_amazon_details'];

            $purchaserProfile->save();
            unset($purchaserProfile);
        }
        return $depart; 
      //Redirect::to(URL::previous() . $depart); commennted on 13 june 2019
    }


    public function bookingFilter($status = 'all',$id = 0,Request $request)
    {
        $user = Auth::user();

        $this->template = config('settings.frontTheme') . '.templates.purchaserPrivateProfile';
        $this->title = 'Holm Care';

        $header = view(config('settings.frontTheme').'.headers.baseHeader')->render();
        $footer = view(config('settings.frontTheme').'.footers.baseFooter')->render();
        $modals = view(config('settings.frontTheme').'.includes.modals')->render();

        $this->vars = array_add($this->vars,'header',$header);
        $this->vars = array_add($this->vars,'footer',$footer);
        $this->vars = array_add($this->vars,'modals',$modals);

        if (!Auth::check()) {
            if(request()->has('refer')){
                $cookie = Cookie::make('bookingFilterPurchaser', 1,2);
                return redirect()->route('session_timeout')->withCookie($cookie);
            }
            if(request()->has('purchaserbookingFilterCanceled')){
                $cookie = Cookie::make('purchaserbookingFilterCanceled', 1,2);
                return redirect()->route('session_timeout')->withCookie($cookie);
            }
            return redirect('/');
            //$this->content = view(config('settings.frontTheme') . '.ImCarer.ImCarer')->render();
        } else {

			if($user->user_type_id == 4){
			  $purchaserProfile = PurchasersProfile::findOrFail($id);
			  $user->id = $id;
			}else{
				$purchaserProfile = PurchasersProfile::findOrFail($this->user->id);
			}
		
			$this->vars = array_add($this->vars, 'userID', $user->id);

            $serviceUsers = $purchaserProfile->serviceUsers;

          //  $purchaserProfile = PurchasersProfile::findOrFail($this->user->id);
            $serviceUsers = $purchaserProfile->serviceUsers;

            $this->vars = array_add($this->vars, 'user', $this->user);
            $this->vars = array_add($this->vars, 'purchaserProfile', $purchaserProfile);
            $this->vars = array_add($this->vars, 'serviceUsers', $serviceUsers);

            $this->vars = array_add($this->vars, 'status', $status);
            $page = $request->get('page',1);
            $perPage = 5;
            $start = ($page - 1) * $perPage;
            if ($page == 1) {
                $start = 0;
            }
            $this->vars = array_add($this->vars, 'page', $page);


            $newBookingsAll = Booking::whereIn('status_id', [self::AWAITING_CONFIRMATION])->where('purchaser_id', $user->id)->get();
            $newBookings = Booking::whereIn('status_id', [self::AWAITING_CONFIRMATION])->where('purchaser_id', $user->id)->skip($start)->take($perPage)->orderBy('created_at', 'desc')->get();
            $this->vars = array_add($this->vars, 'newBookings', $newBookings);
            $this->vars = array_add($this->vars, 'newBookingsAll', $newBookingsAll);
            $this->vars = array_add($this->vars, 'bookingCount', $newBookingsAll->count());
            if($request->ajax()&&$status=='new'){
                $this->content = view(config('settings.frontTheme') . '.purchaserProfiles.Booking.BookingRowNewAjax')->with($this->vars)->render();
                return response()->json(["result" => true,'content'=>$this->content,'hideLoadMore'=>$newBookingsAll->count()<=($perPage*$page),'countAll'=>$newBookingsAll->count()]);
            }
            // ---------------  In progress booking --------------------------------
            $inProgressBookingsAll = Booking::whereIn('status_id', [self::IN_PROGRESS])->where('purchaser_id', $user->id)->get();
            $inProgressBookings = Booking::whereIn('status_id', [self::IN_PROGRESS])->where('purchaser_id', $user->id)->skip($start)->take($perPage)->orderBy('created_at', 'desc')->get();
            $inProgressAmount = 0;
            foreach ($inProgressBookingsAll as $booking){                                                                       
                $inProgressAmount += $booking->purchaser_price;
            }

            $this->vars = array_add($this->vars, 'inProgressBookingsAll', $inProgressBookingsAll);
            $this->vars = array_add($this->vars, 'inProgressBookings', $inProgressBookings);
            $this->vars = array_add($this->vars, 'inProgressAmount', $inProgressAmount);
            if($request->ajax()&&$status=='progress'){
                $this->content = view(config('settings.frontTheme') . '.purchaserProfiles.Booking.BookingRowInProgressAjax')->with($this->vars)->render();
                return response()->json(["result" => true,'content'=>$this->content,'hideLoadMore'=>$inProgressBookingsAll->count()<=($perPage*$page),'countAll'=>$inProgressBookingsAll->count()]);
            }
            // ---------------------------------------------------------------------

            // --------------- Completed booking --------------------------------
            $completedBookingsAll = Booking::where('status_id', 7)->where('purchaser_id', $user->id)->get();
            $completedBookings = Booking::where('status_id', 7)->where('purchaser_id', $user->id)->skip($start)->take($perPage)->orderBy('updated_at', 'desc')->get();
            $completedAmount = 0;
            foreach ($completedBookingsAll as $booking){
                $completedAmount += $booking->purchaser_price;
            }
            $this->vars = array_add($this->vars, 'completedBookingsAll', $completedBookingsAll);
            $this->vars = array_add($this->vars, 'completedBookings', $completedBookings);
            $this->vars = array_add($this->vars, 'completedAmount', $completedAmount);

            if($request->ajax()&&$status=='completed'){
                $this->content = view(config('settings.frontTheme') . '.purchaserProfiles.Booking.BookingRowCompletedAjax')->with($this->vars)->render();
                return response()->json(["result" => true,'content'=>$this->content,'hideLoadMore'=>$completedBookingsAll->count()<=($perPage*$page),'countAll'=>$completedBookingsAll->count()]);
            }
            // -------------------------------------------------------------------

            // --------------- Canceled booking --------------------------------
            $canceledBookingsAll = Booking::where('status_id', 4)->where('purchaser_id', $user->id)->get();
            
            $canceledBookings = Booking::where('status_id', 4)->where('purchaser_id', $user->id)->skip($start)->take($perPage)->orderBy('created_at', 'desc')->get();
            $this->vars = array_add($this->vars, 'canceledBookings', $canceledBookings);
            $this->vars = array_add($this->vars, 'canceledBookingsAll', $canceledBookingsAll);
            if($request->ajax()&&$status=='canceled'){
                $this->content = view(config('settings.frontTheme') . '.CarerProfiles.Booking.BookingRowCanceledAjax')->with($this->vars)->render();
                return response()->json(["result" => true,'content'=>$this->content,'hideLoadMore'=>$canceledBookingsAll->count()<=($perPage*$page),'countAll'=>$canceledBookingsAll->count()]);
            }
            // ---------------favourite  booking --------------------------------
            
             $favouriteBookingsAll = Booking::where('status_id','=',7)
                                            ->where('purchaser_id','=' ,$user->id)
                                            ->groupBy('carer_id')
                                            ->get();
            // dd($favouriteBookingsAll);
            $favouriteBookings = Booking::where('status_id','=', 7)
                                            ->where('purchaser_id','=', $user->id)
                                            ->skip($start)
                                            ->take($perPage)
                                            ->groupBy('carer_id')
                                            ->get();
                                            
            $this->vars = array_add($this->vars, 'favouriteBookings', $favouriteBookings);
            $this->vars = array_add($this->vars, 'favouriteBookingsAll', $favouriteBookingsAll);
            if($request->ajax()&&$status=='favourite'){
                $this->content = view(config('settings.frontTheme') . '.purchaserProfiles.Booking.BookingRowfavouriteAjax')->with($this->vars)->render();
                return response()->json(["result" => true,'content'=>$this->content,'hideLoadMore'=>$favouriteBookingsAll->count()<=($perPage*$page),'countAll'=>$favouriteBookingsAll->count()]);
            }


            $this->content = view(config('settings.frontTheme') . '.purchaserProfiles.Booking.BookingTaball')->with($this->vars)->render();


        return $this->renderOutput();
    }

}
}
