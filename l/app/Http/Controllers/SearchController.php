<?php

namespace App\Http\Controllers;

use App\PostCodes;
use App\PurchasersProfile;
use App\ServiceUsersProfile;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Language;
use App\AssistanceType;
use App\ServiceType;
use App\CarersProfile;
use App\Page;
use Illuminate\Support\Facades\Input;

class SearchController extends FrontController
{

    public function __construct()
    {
        parent::__construct();

        $this->template = config('settings.frontTheme') . '.templates.homePage';
    }





   
    // public function autocomplete(){
    //     $term                       =   input::get('term');
    //     $finalData                  =   [];
    //     $codeObjArr                 =   PostCodes::where("code", "LIKE",'%'.$term.'%')
    //                                         // ->where('profiles_status_id', '=', 2)
    //                                         ->select('code')
    //                                         ->groupBy('code')
    //                                         ->get();

    //     foreach ($codeObjArr as $codeKey => $codeObj) {
    //         $codeObjCnt                             =   PostCodes::where("code","=",$codeObj->code)
    //                                                         // ->where('profiles_status_id', '=', 2)
    //                                                         ->count();
    //         if($codeObjCnt>=0){
    //             $finalData[$codeKey]['post_code']       =   $codeObj->code;
    //             $finalData[$codeKey]['result']          =   '(result '.$codeObjCnt.')';
    //         }
            
    //     }
    //     //echo "<pre>"; print_r($finalData); echo "</pre>";exit;
    //     if(count($finalData))
    //             return json_encode($finalData);
    //     else
    //         return ['value'=>'No Result Found','id'=>''];
    // }

    public function index(Request $request, $page = 1)
    {
        $data = []; 
        $postCode=null;
        $this->title = 'Find a personal carer - HOLM CARE';
        $this->keywords='homecare, live in elderly care, carer respite, manchester, trafford, salford, bolton, bury, wigan, stockport, tameside, salford, oldham';
       
       //Get page data 
        $pageSearch = Page::with('user')->where('slug', 'search')->first();
        
		$pageId = $pageSearch->id;
		$this->title = $pageSearch->meta_title;
        $this->keywords = $pageSearch->meta_keyword;
        $this->description = $pageSearch->meta_description;
      
      $input = $request->all();
        $header = view(config('settings.frontTheme') . '.headers.baseHeader')->render();
        $footer = view(config('settings.frontTheme') . '.footers.baseFooter')->render();
        $modals = view(config('settings.frontTheme') . '.includes.modals')->render();
        $this->vars = array_add($this->vars, 'header', $header);
        $this->vars = array_add($this->vars, 'footer', $footer);
        $this->vars = array_add($this->vars, 'modals', $modals);
        $user = Auth::user();
        if ($user && $user->isPurchaser() && $user->account_status == 'blocked') {
            $this->template = config('settings.frontTheme') . '.templates.blockedUserSorryTemplate';
            $content = view(config('settings.frontTheme') . '.homePage.sorryPageForBlockedPurchaser')->render();
            $this->vars = array_add($this->vars, 'content', $content);
            return $this->renderOutput();
        }
        $load_more_count = $request->get('load-more-count', 5);
        $languages = Language::all();
        $this->vars = array_add($this->vars, 'languages', $languages);
        $typeCare = AssistanceType::all();
        $this->vars = array_add($this->vars, 'typeCare', $typeCare);
        $typeService = ServiceType::all();
        $this->vars = array_add($this->vars, 'typeService', $typeService);
        $this->vars = array_add($this->vars, 'header', $header);
        $this->vars = array_add($this->vars, 'footer', $footer);
        $this->vars = array_add($this->vars, 'modals', $modals);
        $this->vars = array_add($this->vars, 'load_more_count', $load_more_count);
        $perPage = 5;
        $where = '';
        $having = '';
        if ($request->get('language')) {
           
            $having = ' having sum(cl.language_id)=' . array_sum(array_keys($request->get('language'))).' ';
            $where .= 'inner join carer_profile_language cl on cl.carer_profile_id = cp.id and cl.language_id in (' . implode(',',
                    array_keys($request->get('language'))) . ')';
        }
        if ($request->get('typeService')) {
            $where .= 'inner join carer_profile_service_type ct on ct.carer_profile_id = cp.id and ct.service_type_id in (' . $request->get('typeService') . ')';
        }
        $working_times[1] = [5, 6, 7];
        $working_times[2] = [8, 9, 10];
        $working_times[3] = [11, 12, 13];
        $working_times[4] = [14, 15, 16];
        $working_times[5] = [17, 18, 19];
        $working_times[6] = [20, 21, 22];
        $working_times[0] = [22, 24, 25];
        if ($request->get('findDate')) {
            $date = $request->get('findDate');
            $date = explode("/", $date);
            $dayofweek = date("w", mktime(0, 0, 0, $date[1], $date[0], $date[2]));
            $where .= 'inner join carer_profile_working_time cw on cw.carer_profile_id = cp.id and cw.working_times_id in (' . implode(',',
                    $working_times[$dayofweek]) . ')';
        }
        $where .= 'left join review r on cp.id=r.carer_id';
        $where .= ' where registration_progress=20 and profiles_status_id=2 ';
        if ($request->get('gender')) {
            $where .= " and cp.gender in ('" . implode("','", array_keys($request->get('gender'))) . "')";
        }
        if ($request->get('have_car')) {
            $where .= " and cp.have_car='Yes'";
        }
        if ($request->get('work_with_pets')) {
            $where .= " and cp.work_with_pets!='No'";
        }
        if ($request->get('typeCare')) {
            $careSelect = 'SELECT carer_profile_id FROM (
                              SELECT carer_profile_id,assistance_types_id FROM carer_profile_assistance_type cs WHERE assistance_types_id IN (' . implode(',',
                    array_keys($request->get('typeCare'))) . ')) AS tb
                            GROUP BY carer_profile_id
                           HAVING count(*)=' . count(array_keys($request->get('typeCare')));
            $careResult = DB::select($careSelect);
            $carerId = [];
            foreach ($careResult as $result) {
                $carerId[] = $result->carer_profile_id;
            }
            $where .= ' and cp.id in (' . implode(',', array_values($carerId)) . ') ';
        }
        if ($request->get('postCode') && !empty($request->get('postCode'))) {
            //   $postCode = trim($request->get('postCode'));
//    echo"hhhhhhh";
            // dd($request);
           $postCode = str_replace(' ', '', $request->get('postCode'));

             
            if(strlen($postCode)>1){
                $pCode='';
                if (strpos($postCode, ' ') === false) $pCode = $postCode.' ';
                else
                    $pCode = substr($postCode, 0, strpos($postCode, ' ') + 1);
                $code = PostCodes::where('code', '=' ,$pCode)->first();
                if(isset($code->code)){
                    $code->amount=$code->amount+1;
                    $code->save();
                }else{
                    $data=['code'=>$pCode,'amount'=>1,'frequency'=>0];
                    PostCodes::create($data);
                }
            }
            if (strpos($postCode, ' ') === false) {
                $postCode .= ' ';
//                $where .= " AND (SELECT COUNT(*) FROM postcodes p WHERE p.name = LEFT('" . $postCode . "', POSITION(' ' IN '" . $postCode . "')) and  p.name = LEFT(cp.postcode, POSITION(' ' IN '" . $postCode . "')))>0";
//            } else {
            }else{
                $carerCode = CarersProfile::where('postcode','=',$postCode)->get();
                // dd($carerCode);
                if($carerCode->count()==0){
                    $postCode = substr($postCode,0,strpos($postCode, ' ')+1);
                }
               
            }
            //$where .= " AND cp.postcode like '" . $postCode . "%'";
        }
        if ($request->get('load-more', 0) == 1) {
            $page = $request->get('page');
        }
        $order = [];
        if ($request->get('sort-rating', 0) == 1) {
            $order[] = 'avg_total ' . $request->get('sort-rating-order', 'desc');
        }
        if ($request->get('sort-id', 0) == 1) {
            $order[] = 'cp.id ' . $request->get('sort-id-order', 'desc');
        }
        if (empty($order)) {
            $order[] = 'cp.id desc';
        }
        $start = ($page - 1) * $perPage;
        if ($page == 1) {
            $start = 0;
        }
        $carerResult = [];
        $countAllResult = [];
      $sql = 'select cp.id,first_name,family_name,sentence_yourself,town,avg_total,creview,postcode
                  from carers_profiles cp ' . $where . ' 
                group by cp.id,first_name,family_name,sentence_yourself,town,avg_total,creview,postcode
                '.$having.' order by ' . implode(',', $order) . " limit $start,$perPage";
        //if (Auth::check() && Auth::user()->isAdmin()) {
        //echo($sql);
        $carerResult = DB::select($sql); //раскоментить
    //  dd($carerResult);
        //}
        if (Auth::check() && Auth::user()->user_type_id != 4) {
            $order_distance = $request->get('sort-distance-order', 'desc');
              
            $carerResult = $this->sortByDistanseToCarer($carerResult,$order_distance,$postCode);
            //  dd($careResult);
        }
       
        $start = (($page * $perPage) - $perPage == 0) ? '0' : ($page * $perPage) - $perPage;
        //раскоментить
        //if (Auth::check() && Auth::user()->isAdmin()){
        $countAllResult = DB::select('SELECT cp.id,first_name,family_name,sentence_yourself,town,avg_total,creview
                  FROM carers_profiles cp ' . $where . '
                GROUP BY cp.id,first_name,family_name,sentence_yourself,town,avg_total,creview 
                '.$having.' ORDER BY ' . implode(',', $order));
        //}
        $countAll = count($countAllResult);
      
        //if(count($carerResult)<=5)$start=0;
        $carerResultPage = $carerResult; //array_slice($carerResult,$start,$perPage);
     // dd($carerResultPage);
        $this->vars = array_add($this->vars, 'carerResult', $carerResultPage);
        $this->vars = array_add($this->vars, 'perPage', $perPage);
        $this->vars = array_add($this->vars, 'carerResultCount', count($carerResult));
        $this->vars = array_add($this->vars, 'page', $page);
        $this->vars = array_add($this->vars, 'requestSearch', $request->all());
        $this->vars = array_add($this->vars, 'countAll', $countAll);
        $this->vars = array_add($this->vars, 'carerResultArea', false);
        $load_more = $request->get('load_more');
        if (!$request->get('carerSearchAjax')) {
            if ($request->get('postCode') && !empty($request->get('postCode'))) {
                // dd($request);
                $postCode=substr($postCode,0,strpos($postCode, ' ')+1);
                if ($this->isExsistPostCode($postCode) == false) {
                    $this->vars['carerResult']=array();
                    $this->vars['countAll'] = 0;
                    $this->vars['page'] = 0;
                    $this->vars['perPage'] = $perPage;
                    $this->vars['start'] = $start;
                    $this->vars['carerResultArea']= true;
                    $this->vars['page'] =  $page;
                    $this->vars['id'] = (ceil($countAll / $perPage) > $page) ? $carerResult[count($carerResult) - 1]->id : 0;
                    $this->vars['count'] =  count($carerResult);
                }
            }
            $this->content = view(config('settings.frontTheme') . '.homePage.searchPage', $this->vars)->render();
            return $this->renderOutput();
        } else {
            $html = view(config('settings.frontTheme') . '.homePage.searchPageAjax', $this->vars)->render();
            $post_ = true;
            if ($request->get('postCode') && !empty($request->get('postCode'))) {
                if ($this->isExsistPostCode($postCode) == false) {
                    $post_ = false;
                    $countAll=0;
                    $carerResult=[];
                    $this->vars['carerResult']=array();
                    $this->vars['countAll'] = 0;
                    $this->vars['page'] = 0;
                    $this->vars['perPage'] = $perPage;
                    $this->vars['start'] = $start;
                    $this->vars['carerResultArea']= true;
                    $this->vars['page'] =  $page;
                    $this->vars['id'] = (ceil($countAll / $perPage) > $page) ? $carerResult[count($carerResult) - 1]->id : 0;
                    $this->vars['count'] =  count($carerResult);
                    $html = '<h3><p>Sorry</p> Holm is not yet available in this area. Please <a href="/contact">contact us</a> to request Holm in your area. Many thanks!</h3>';
                }
            }
            //var_dump($carerResult[count($carerResult)-1]->id);
            $htmlHeader = view(config('settings.frontTheme') . '.homePage.searchPageHeaderAjax', $this->vars)->render();
            $options = app('request')->header('accept-charset') == 'utf-8' ? JSON_UNESCAPED_UNICODE : null;
            return response()->json(array(
                'success' => (count($carerResult) > 0),
                'load-more' => isset($load_more) && !empty($load_more) ? 1 : 0,
                'html' => $html,
                'post_' => $post_,
                'sql'=>$sql,
                'start' => $start,
                'page' => $page,
                'id' => (ceil($countAll / $perPage) > $page) ? $carerResult[count($carerResult) - 1]->id : 0,
                'count' => count($carerResult),
                'countAll' => $countAll,
                'htmlHeader' => $htmlHeader
            ), 200, [$options]);
            exit;
        }
    }
    /**
     * @param $carerResult
     * @return mixed
     */
    public function searchCarers(Request $request){

        if($request->has('data')){           
            $carers = CarersProfile::getFilteredCarers($request->data);
        } else{
            $carers = CarersProfile::getFilteredCarers();           
        }
        return response($carers);
    }

    public function increasePostCode($postCode){
        $pc = PostCodes::where('code', $postCode)->first();
        $pc->amount += 1;
        $pc->save();
    }


    private function sortByDistanseToCarer($carerResult,$order_distance,$postCode)
    {

        foreach ($carerResult as $key => $item) {
            switch (Auth::user()->user_type_id) {
                case 1:
                    $purchaser = PurchasersProfile::find(Auth::user()->id);
                    if($purchaser->active_user!=null){
                        $servise =ServiceUsersProfile::find($purchaser->active_user);
                        $from = urlencode(trim(($servise->postcode)));
                    }else
                    $from = (trim($purchaser->town . ' ' . $purchaser->address_line1));

                    break;
                case 2:
                    $servise = ServiceUsersProfile::find(Auth::user()->id);
                    $from = urlencode(trim(($servise->postcode)));
                    break;
                case 3:
                    $carer = CarersProfile::find(Auth::user()->id);
                    $from = urlencode(trim(($carer->postcode)));
                    break;
            }
            $to = urlencode(trim($item->postcode));
            $distance = $this->getDistance($from, $to);
            $item->distance = ($distance=='0')?'0 MI':$distance;
            
        }
        if($order_distance=='asc')
            usort($carerResult, function ($a, $b) {
                return strcmp($a->distance, $b->distance);
            });
        elseif($order_distance=='desc')
            usort($carerResult, function ($a, $b) {
                return strcmp($b->distance, $a->distance);
            });

        return $carerResult;
    }

    /**
     * @param $from - Need post code
     * @param $to - Need post code
     * @return int - distance in mile
     */
    public static function getDistance($from, $to)
    {
        //  dd($to);
        $distance = 0;
        $data = array(
            'origins'=>$from,
            'destinations' => $to,
            'mode' => 'driving',
            'language' => 'driving',
            'mode' => 'nl-BE',
            'sensor' => false,
            'units' => 'imperial',
        );
        $url = "https://maps.googleapis.com/maps/api/distancematrix/json?".http_build_query($data)."&key=AIzaSyBft_HrycpeApGjGhofCldP2QxJYrICro4";
    //    dd($data);
        if ($from !== $to && !empty($from) && !empty($to)) {
            $result = json_decode(file_get_contents($url), true);
           
            if ($result['status'] == "OK") {
                $distance = (isset($result['rows'][0]['elements'][0]['distance']['text']))
                    ? ($result['rows'][0]['elements'][0]['distance']['text']) : 'NOT FOUND';


                    
            }else{
                $distance ='NOT_FOUND';
            }
        }
        return $distance;
    }

    private function isExsistPostCode($postCode)
    {
        $sql = "SELECT count(*) AS ctn FROM postcodes p WHERE p.name = LEFT('" . $postCode . "', POSITION(' ' IN '" . $postCode . "'))";
        $carerResult = DB::select($sql);

        // dd($carerResult);
        return $carerResult[0]->ctn > 0;
    }
}
   
