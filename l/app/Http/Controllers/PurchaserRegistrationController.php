<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Repo\PurchaserRegistration;
use App\Postcode;
use App\PurchasersProfile;
use App\ServiceUsersProfile;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Mail;
use Mockery\Exception;
use Swift_TransportException;
use App\Events\LogSuccessfulLogin;

class PurchaserRegistrationController extends FrontController
{
    private $purchaserProfile;

    public function __construct(PurchaserRegistration $purchaserProfile) {
        parent::__construct();
        $this->purchaserProfile = $purchaserProfile;

        $this->template = config('settings.frontTheme').'.templates.userRegistration';
    }

    public function index()
    {
     
        if (request()->has('ref')) {
            if (request()->get('ref') == 'WECARE') {
                $this->vars = array_add($this->vars, 'use_register_code', 1);
                $this->vars = array_add($this->vars, 'ref_code', request()->get('ref'));
            } else {
                $ref_code = $this->checkReferCode(request()->get('ref'));

                if ($ref_code != 0) {
                    $this->vars = array_add($this->vars, 'ref_code', $ref_code);
                }
            }
        }

        $this->title = 'Purchaser Registration';

        $header = view(config('settings.frontTheme') . '.headers.baseHeader')->render();
        $footer = view(config('settings.frontTheme') . '.footers.baseFooter')->render();
        $modals = view(config('settings.frontTheme') . '.includes.modals')->render();

        $this->vars = array_add($this->vars, 'header', $header);
        $this->vars = array_add($this->vars, 'footer', $footer);
        $this->vars = array_add($this->vars, 'modals', $modals);

        $user = Auth::user();


        if (!$user) {
            if(request()->has('refer')){
                $cookie = Cookie::make('PurchaserRegistration', 1,2);
                return redirect()->route('session_timeout')->withCookie($cookie);
            }
            $step = view(config('settings.frontTheme').'.purchaserRegistration.Step1_purchaserRegistration')->with($this->vars)->render();
            $this->vars = array_add($this->vars,'activeStep',1);
            $this->vars = array_add($this->vars,'activeSubStep',0);
        } else {
            $purchasersProfile = PurchasersProfile::findOrFail($user->id);
			 event(new LogSuccessfulLogin($user));
            try {
                $serviceUsersProfile = $purchasersProfile->serviceUsers->last();
            } catch(\Exception $e){}
            //Оба урла как для залогиненного так и для разлогиненного пурчейсера работают правильно
            //По умолчанию в письме генерится урл для разлогиненного, но если пурчейсер залогинен, то редиректим его по своему урлу
            //

            if(strpos(request()->fullUrl(), 'purchaser-registration?refer=continue') !== false && isset($serviceUsersProfile)) {
                return redirect(route('ServiceUserRegistration', ['serviceUserProfile' => $serviceUsersProfile->id]));
            }
            if ($purchasersProfile->registration_status == 'completed' && !$purchasersProfile->is_uncompleted_service_user) {
                //return redirect(route('purchaserSettings'));
            }


            $this->vars = array_add($this->vars, 'purchasersProfileID', $purchasersProfile->id);
            $this->vars = array_add($this->vars, 'purchasersProfile', $purchasersProfile);
            //dd($this->purchaserProfile->getNextStep());//Step4_1_2_1_Thank__you_Sign_up
            if ($this->purchaserProfile->getNextStep() == 'Step4_purchaserRegistration') {

                $this->vars = array_add($this->vars, 'user', $this->user);
            }
            if ($this->purchaserProfile->getNextStep() == 'Step4_1_purchaserRegistration') {

                if (!count($purchasersProfile->serviceUsers)) {
                    $serviceUsersProfile = new ServiceUsersProfile();
                    $serviceUsersProfile->purchaser_id = $purchasersProfile->id;
                    $serviceUsersProfile->save();
                } else {
                    $serviceUsersProfile = $purchasersProfile->serviceUsers->last();
                }
                    $this->vars = array_add($this->vars, 'user', $this->user);
                    $this->vars = array_add($this->vars, 'serviceUserProfile', $serviceUsersProfile);

            }
            if ($this->purchaserProfile->getNextStep() == 'Step4_2_purchaserRegistration') {

                if (count($purchasersProfile->serviceUsers)){

                    $serviceUsersProfile = $purchasersProfile->serviceUsers->last();
                }
                else {
                    $serviceUsersProfile = new ServiceUsersProfile();
                    $serviceUsersProfile->purchaser_id = $purchasersProfile->id;
                    $serviceUsersProfile->care_for = 'Someone else';
                    if ($purchasersProfile->purchasing_care_for == 'Myself') {
                        $serviceUsersProfile->title = $purchasersProfile->title;
                        $serviceUsersProfile->care_for = 'Myself';
                        $serviceUsersProfile->first_name = $purchasersProfile->first_name;
                        $serviceUsersProfile->family_name = $purchasersProfile->family_name;
                        $serviceUsersProfile->like_name = $purchasersProfile->like_name;
                        $serviceUsersProfile->gender = $purchasersProfile->gender;
                        $serviceUsersProfile->mobile_number = $purchasersProfile->mobile_number;
                        $serviceUsersProfile->email = $purchasersProfile->email;
                        $serviceUsersProfile->address_line1 = $purchasersProfile->address_line1;
                        $serviceUsersProfile->address_line2 = $purchasersProfile->address_line2;
                        $serviceUsersProfile->address_line1 = $purchasersProfile->address_line1;
                        $serviceUsersProfile->town = $purchasersProfile->town;
                        $serviceUsersProfile->postcode = $purchasersProfile->postcode;
                        $serviceUsersProfile->DoB = $purchasersProfile->DoB;
                    }
                    $serviceUsersProfile->save();

                }

                $this->vars = array_add($this->vars, 'user', $this->user);
                $this->vars = array_add($this->vars, 'serviceUsersProfile', $serviceUsersProfile);
            }
            //dd($this->purchaserProfile->getActiveStep($user->id));//2
            //dd($this->purchaserProfile->getActiveSubStep($user->id));//4
            $this->vars = array_add($this->vars,'activeStep',$this->purchaserProfile->getActiveStep($user->id));
            $this->vars = array_add($this->vars,'activeSubStep',$this->purchaserProfile->getActiveSubStep($user->id));

            $step = view(config('settings.frontTheme').'.purchaserRegistration.'.$this->purchaserProfile->getNextStep())->with($this->vars)->render();

        }

        $this->vars = array_add($this->vars,'step',$step);

        $this->content = view(config('settings.frontTheme') . '.purchaserRegistration.purchaserRegistration')->with($this->vars)->render();

        return $this->renderOutput();
    }

    public function update(Request $request) {
    
     
        if ($request->has('stepback')) {

            $stepback = $request->stepback;
            $purchaserProfiles = PurchasersProfile::findOrFail($request->input('purchasersProfileID'));

            if ($stepback=='4_1' && $purchaserProfiles->purchasing_care_for == 'Myself'){

                $purchaserProfiles->registration_progress = '4_2';

            } else {
                $purchaserProfiles->registration_progress = $request->input('stepback');
            }

            $purchaserProfiles->save();

        } else {
            $this->purchaserProfile->saveStep($request);

        }

        return redirect('/purchaser-registration#progress');

    }

    public function sendContinueRegistration()
    {

        $user = Auth::user();
        if(!$user) {
            return redirect('/');
        }

        $this->title = 'Purchaser Registration';
        $header = view(config('settings.frontTheme') . '.headers.baseHeader')->render();
        $footer = view(config('settings.frontTheme') . '.footers.baseFooter')->render();
        $modals = view(config('settings.frontTheme') . '.includes.modals')->render();

        $this->vars = array_add($this->vars, 'header', $header);
        $this->vars = array_add($this->vars, 'footer', $footer);
        $this->vars = array_add($this->vars, 'modals', $modals);


        $this->vars = array_add($this->vars, 'signUpUntil', $user->created_at->addWeek()->format('d/m/Y h:i A'));
        $purchaser = PurchasersProfile::findorFail($user->id);

        $text = view(config('settings.frontTheme') . '.emails.continue_sign_up_service_user')->with([
            'user' => $user,
            'like_name'=>$purchaser->like_name
        ])->render();

        DB::table('mails')
            ->insert(
                [
                    'email' =>$user->email,
                    'subject' =>'Registration on HOLM',
                    'text' =>$text,
                    'time_to_send' => Carbon::now(),
                    'status'=>'new'
                ]);

        $this->content = view(config('settings.frontTheme') . '.carerRegistration.thankYou')->with($this->vars)->render();

        $text = view(config('settings.frontTheme') . '.emails.new_user')->with([
            'user' => $user,
            'like_name' => $purchaser->like_name,
            'type' => 'purchaser'
        ])->render();

        DB::table('mails')
            ->insert(
                [
                    'email' =>'nik@holm.care',
                    'subject' =>'You have a new user',
                    'text' =>$text,
                    'time_to_send' => Carbon::now(),
                    'status'=>'new'
                ]);
      
        
     /*   DB::table('mails')
            ->insert(
                [
                    'email' => 'nataliabarladin@gmail.com',
                    'subject' =>'You have a new user',
                    'text' =>$text,
                    'time_to_send' => Carbon::now(),
                    'status'=>'new'
                ]); */
        return $this->renderOutput();
    }
}
