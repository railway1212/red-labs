<?php

namespace App\Http\Controllers;

use App;
use EllisTheDev\Robots\Robots;
use Illuminate\Http\Request;
use App\User;
use Carbon\Carbon;
use DB;
use Auth;
use Illuminate\Support\Facades\Mail;



class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

//    public function robots() {
//        if (App::environment() == 'production') {
//            // If on the live server, serve a nice, welcoming robots.txt.
//            Robots::addUserAgent('*');
//            Robots::addAllow
//            Robots::addDisallow('*');
//    } else {
//            // If you're on any other server, tell everyone to go away.
//            Robots::addDisallow('*');
//        }
//
//        return Response::make(Robots::generate(), 200, ['Content-Type' => 'text/plain']);
//    }

    public function index()
    {
      
      	//return view('home');
        return redirect('/admin');
    }

    public function  unsubscribe($id){

        $user = User::find($id);
        if(isset($user->email)){
            $user->subscribe=0;
            $user->save();
        }

        $header = view(config('settings.frontTheme').'.headers.baseHeader')->render();
        $footer = view(config('settings.frontTheme').'.footers.baseFooter')->render();
        $modals = view(config('settings.frontTheme').'.includes.modals')->render();

        $vars = array();
        $vars = array_add($vars,'title','Holm');
        $vars = array_add($vars,'description','');
        $vars = array_add($vars,'keywords','');
        $vars = array_add($vars,'keywords','');
        $vars = array_add($vars,'header',$header);
        $vars = array_add($vars,'footer',$footer);
        $vars = array_add($vars,'modals',$modals);


        $content = view(config('settings.frontTheme').'.homePage.ThankYouUnsubscribe')->render();
        $vars = array_add($vars,'content',$content);

        return view(config('settings.frontTheme').'.templates.homePage')->with($vars);
    }
  
  public function testCron()
  {
   /* $userId = Auth::user()->id;
    
    	//$userId = $this->id;
		$sql ="SELECT * from ip_monitoring where user_id = '".$userId."' Order by id  DESC LIMIT 1";
		$items = DB::select($sql);
		
		if(!empty($items)){
			$item = $items[0];
			return date("d-m-Y H:i:s",strtotime($item->created_at));
		}
		
		return '-';
    */
   	  $timeTask = Carbon::now();

      DB::table('mails')->where('time_to_send', '<', $timeTask)->where('status', '=', 'new')
        ->update(['status' => 'in_progress', 'time_when_sent' => $timeTask]);
    
  //   DB::table('mails')->where('time_to_send', '<', $timeTask)->where('status', '=', 'new')
  //      ->update(['time_when_sent' => $timeTask]);


      $mails = DB::table('mails')->select('id','email', 'subject', 'text')
        ->where('time_when_sent',$timeTask)->get();
   
  //   echo "<pre>"; print_r($mails); echo "<pre>"; exit;
      if (count($mails) > 0) {
          
        foreach ($mails as $mail) {
          Mail::send(config('settings.frontTheme') . '.emails.mail',
                     ['text'=>$mail->text],
                     function ($m) use ($mail) {
                       $m->to($mail->email)->subject($mail->subject);
                     });
          //todo add mail error exception
          DB::table('mails')->where('id', $mail->id)
            ->update(['status' => 'sent']);
        }        
      }    
  }
}
