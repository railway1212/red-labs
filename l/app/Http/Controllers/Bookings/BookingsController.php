<?php

namespace App\Http\Controllers\Bookings;

use App\Appointment;
use App\AppointmentOverview;
use App\Booking;
use App\BookingOverview;
use App\BookingsMessage;
use App\CarersProfile;
use App\DisputePayout;
use App\Events\BookingCompletedEvent;
use App\Http\Requests\BookingCreateRequest;
use App\Interfaces\Constants;
use App\PaymentServices\StripeService;
use App\Http\Controllers\FrontController;
use App\PurchasersProfile;
use App\ServiceUsersProfile;
use App\StripeCostumer;
use App\User;
use App\Exceptions\MessenteException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\DB;
use Auth;
use Carbon\Carbon;
use PaymentTools;
use SmsTools;  
use App\StripeCharge;
use App\StripeConnectedAccount; 

class BookingsController extends FrontController implements Constants
{
    public function create(BookingCreateRequest $request)
    {
   
        //~ if (!$this->appointmentIsMinimum24HoursAhead($request)) {
            //~ return response(['status' => 'lessthan24hours']);
        //~ }

     	//echo "<pre>"; print_r($request); echo "<pre>"; exit;
        $booking = $this->createBooking($request);

        if ($request->ajax()) // This is what i am needing.
        {
            return 'l/bookings/' . $booking->id . '/purchase';
        }

        return redirect('l/bookings/' . $booking->id . '/purchase');
    }

    //Подсчет разницы во времени между настоящим и временем первого аппоинтмента
    public function appointmentIsMinimum24HoursAhead($data){
        foreach ($data->bookings as $booking_item) {
            $i = 0;
            foreach ($booking_item as $bi => $appointment_item) {
                $theDay = Carbon::createFromFormat('d/m/Y H', $appointment_item[$i]['date_start'] . ' 00');
                $theHour = date("H.i", strtotime($appointment_item[$i]['time_from']));
                $startTime = $theDay->addHours(Carbon::parse($theHour)->hour);
                $startTime = $theDay->addMinutes(Carbon::parse($theHour)->minute);
                if (Carbon::now()->diffInDays($startTime) < 1) {
                    return false;
                }
                $i++;
            }
        }
        return true;
    }

    public function calculateBookingPrice(BookingCreateRequest $request)
    {
     //echo "<pre> price: "; print_r($request->all()); echo "<pre>"; exit;
      DB::beginTransaction();
      	
        $booking = $this->createBooking($request);
      	//echo "<pre> hours: "; print_r($booking->price); echo "<pre>"; exit;
       
        $price = number_format($booking->price, 2);
        $hours = $booking->hours;
        $liveInCarer = $booking->liveInCarer;
	 	  
      

        DB::rollBack();
       


        return response(['price' => $price, 'hours' => $hours,'liveIncarer'=>$liveInCarer]);
    }

    public function update(Booking $booking, Request $request)
    {
        //Offer alterntive time
        $user = Auth::user();

        foreach ($request->bookings as $booking_item) {
            $booking->appointments()->delete();
            //Generating appointments
          print_r($booking);
          exit();
            $this->createAppointments($booking, $booking_item['appointments']);
            $booking->amount_for_purchaser = $booking->purchaser_price;
            $booking->amount_for_carer = $booking->carer_price;
            $booking->save();
        }
      
      
      
        $sendTo = '';
        if ($user->user_type_id == 3) {
            //carer
            $booking->carer_status_id = 1;
            $booking->purchaser_status_id = 2;
            $sendTo = 'purchaser';
            
            //Sending sms notification about alternative time
            $message = $booking->bookingCarerProfile->full_name . ' has offered you an alternative time for the booking request. Please log into your account https://holm.care/ for more details. The Holm Team';
            SmsTools::sendSmsToPurchaser($message, $booking->bookingPurchaserProfile);

        } else {
            $booking->carer_status_id = 2;
            $booking->purchaser_status_id = 1;
            $sendTo = 'carer';
            
            //Sending sms notification about alternative time
            $message = $booking->bookingPurchaserProfile->full_name . ' has offered you an alternative time for the booking request. Please log into your account https://holm.care/ for more details. The Holm Team';
            SmsTools::sendSmsToCarer($message, $booking->bookingCarerProfile);
        }


        $booking->save();
        // отправить почту базируясь на $user->user_type_id (либо кереру, либо пурчасеру)
        $purchaser = User::find($booking->purchaser_id);
        $carer_users = User::find($booking->carer_id);
        $purchaserProfile = PurchasersProfile::find($booking->purchaser_id);
        $carerProfile = CarersProfile::find($booking->carer_id);
        $serviceUser = ServiceUsersProfile::find($booking->service_user_id);
        $text = view(config('settings.frontTheme') . '.emails.alternative_time')->with([
            'purchaser' => $purchaserProfile, 'booking' => $booking, 'serviceUser' => $serviceUser, 'carer' => $carerProfile, 'sendTo' => $sendTo
        ])->render();

        DB::table('mails')
            ->insert(
                [
                    'email' => ($user->user_type_id == 3 ? $purchaser->email:$carer_users->email),
                    'subject' => 'You have a new alternative time',
                    'text' => $text,
                    'time_to_send' => Carbon::now(),
                    'status' => 'new'
                ]);

        return response(['status' => 'success']);
    }

    public function getModalEditBooking(Booking $booking)
    {

        $sql = 'SELECT batch, min(date_start) as date_start,  max(date_start) as date_end, min(time_from) as time_from, min(time_to) as time_to, min(periodicity) as periodicity
                FROM appointments
                WHERE booking_id = ' . $booking->id . '
                GROUP BY batch ORDER BY batch';
        $appointments = DB::select($sql);

        array_map(function ($item) use ($booking) {
            $originAppointment = Appointment::where('booking_id', $booking->id)->where('batch', $item->batch)->first();
            $item->time_from = Carbon::parse($item->time_from)->format("h:i A");
            $item->time_to = Carbon::parse($item->time_to)->format("h:i A");
            $item->assistance_types = $originAppointment->assistance_types;
        }, $appointments);



        $user = Auth::user();
        $this->vars = array_add($this->vars, 'user', $user);
        $this->vars = array_add($this->vars, 'appointments', $appointments);
//        $this->vars = array_add($this->vars, 'assistance_types', $booking->assistance_types); //todo это теперь лежит внутри $appointments
        $this->vars = array_add($this->vars, 'serviceUsers', $booking->bookingServiceUser);
        $this->vars = array_add($this->vars, 'booking', $booking);
        $content = view(config('settings.frontTheme') . '.CarerProfiles.Booking.MessageEdit')->with($this->vars)->render();
        return $content;
        //todo букинги тут $booking. апоинтменты тут


        $this->vars = array_add($this->vars, 'appointments', $appointments);
        $this->vars = array_add($this->vars, 'assistance_types', $booking->assistance_types);
        $this->vars = array_add($this->vars, 'serviceUsers', $booking->bookingServiceUser);
        $this->vars = array_add($this->vars, 'booking', $booking);
        $content = view(config('settings.frontTheme') . '.CarerProfiles.Booking.MessageEdit')->with($this->vars)->render();
        return $content;
        //todo букинги тут $booking. апоинтменты тут
    }

    public function view_details(Booking $booking)
    {
     
        //todo костыль на логаут
        if (!Auth::check()) {
            if (request()->has('refer')) {
                $id = request()->get('refer');
                $cookie = Cookie::make('bookingDetailsOrPayment', '/bookings/'.$id.'/details', 2);
                if(request()->has('place')){
                    $cookie = Cookie::make('bookingView', request()->get('refer'), 2);
                    $place = Cookie::make('bookingViewPlace', request()->get('place'), 2);
                    $cookieArray = [$cookie, $place];
                    return redirect()->route('session_timeout')->withCookies($cookieArray);
                }
                return redirect()->route('session_timeout')->withCookie($cookie);
            }
            return redirect('/');
        } else{
            if (request()->has('place')){
                $this->vars = array_add($this->vars, 'place', 'comments');
            } else{
                $this->vars = array_add($this->vars, 'place', false);
            }
        }

        $user = Auth::user();
        $purchaserProfile = PurchasersProfile::where('id', $booking->purchaser_id)->get();
     
        $this->vars = array_add($this->vars, 'purchaserProfile', $purchaserProfile);
        
        $this->template = config('settings.frontTheme') . '.templates.purchaserPrivateProfile';
        $this->title = 'Booking details';

        $header = view(config('settings.frontTheme') . '.headers.baseHeader')->render();
        $footer = view(config('settings.frontTheme') . '.footers.baseFooter')->render();
        $modals = view(config('settings.frontTheme') . '.includes.modals')->render();

        $this->vars = array_add($this->vars, 'header', $header);
        $this->vars = array_add($this->vars, 'footer', $footer);
        $this->vars = array_add($this->vars, 'modals', $modals);

        if (!$this->user) {
            return redirect('/');
        } else {
            $appointments = $booking->appointments()->orderBy('date_start', 'asc')->orderBy('time_from', 'asc')->get();
            $this->vars = array_add($this->vars, 'appointments', $appointments);
            $this->vars = array_add($this->vars, 'user', $this->user);
            $this->vars = array_add($this->vars, 'booking', $booking);
            $bookingMessage = BookingsMessage::where('booking_id', $booking->id)->orderByDesc('booking_id')->get();
            $this->vars = array_add($this->vars, 'bookingMessage', $bookingMessage);

            $serviceUserProfile = $booking->bookingServiceUser()->first();
            $this->vars = array_add($this->vars, 'serviceUserProfile', $serviceUserProfile);
            $summary = array();


            $carerProfile = $booking->bookingCarer()->first()->userCarerProfile()->first();
            $this->vars = array_add($this->vars, 'carerProfile', $carerProfile);

            $sql = 'SELECT at.name as s_name, min(a.date_start) as date_start, max(a.date_start) as date_end,  p.name, min(a.time_from) as time_from, max(a.time_to) as time_to  FROM assistance_types at
                  JOIN appointments_assistance_types aat ON at.id = aat.assistance_type_id
                  JOIN appointments a ON a.id = aat.appointment_id
                  JOIN bookings b ON b.id = a.booking_id
                  JOIN periodicities p
                WHERE b.id = '.$booking->id.' AND a.periodicity = p.name
                GROUP BY at.name, p.name;';
            $res = DB::select($sql);
            $this->vars = array_add($this->vars, 'summary', $res);
            $allReviewAppointmentsIds = AppointmentOverview::pluck('appointment_id')->toArray();
            $this->vars = array_add($this->vars, 'allReviewAppointmentsIds', $allReviewAppointmentsIds);
            
            
            // check already added any comment by service user 
            $query='SELECT count(*) as count from bookings_messages where booking_id='.$booking->id;
            $bookingMessageRes = DB::select($query);
            $this->vars = array_add($this->vars, 'bookingMessageRes', $bookingMessageRes[0]->count);

            $this->content = view(config('settings.frontTheme') . '.booking.BookingDetails')->with($this->vars)->render();
        }

        return $this->renderOutput();
    }

    public function setPaymentMethod(Booking $booking, Request $request, StripeService $stripeService)
    {
     
   	//echo "<pre>Booking Data : ";print_r($booking); echo "</pre>"; exit;
        $user = Auth::user();
     if ($request->payment_method == 'credit_card') {
           if($request->card_id && !$request->card_number){
                $stripeCustomer = $user->credit_cards()->where('id', $request->card_id)->first();
             	if(!$stripeCustomer)
                    return response($this->formatResponse('error', 'card_not_found'));
                $booking->card_token = $stripeCustomer->token;
            } else {
                try {
                    $cardToken = PaymentTools::createCreditCardToken([
                        'card_number' => $request->card_number,
                        'exp_month' => $request->card_month,
                        'exp_year' => $request->card_year,
                        'cvc' => $request->card_cvc,
                    ]);
                    $booking->card_token = $cardToken;

                    if($request->save_card){
                        \Stripe\Stripe::setApiKey(env('STRIPE_SECRET_KEY'));
                        $customer = \Stripe\Customer::create(array(
                            'source'   => [
                                'object' => 'card',
                                'number' => $request->card_number,
                                'exp_month' => $request->card_month,
                                'exp_year' => $request->card_year,
                                'cvc' => $request->card_cvc,
                            ],
                            'description' => 'Credit Card of '.$user->full_name.' (ID: '.$user->id.') user.'
                        ));
                        $stripeCustomer = StripeCostumer::create([
                            'purchaser_id' => $user->id,
                            'token' => $customer->id,
                            'last_four' => substr(str_replace(' ','',$request->card_number), 12),
                        ]);
                    }
                } catch (\Exception $ex) {
                    return response($this->formatResponse('error', $ex->getMessage()));
                }
            }
        } elseif ($request->payment_method == 'bonus_wallet') {
            if ($booking->bookingPurchaser->bonus_balance < $booking->purchaser_price) {
                return response($this->formatResponse('error', 'You do not have enough credits in your bonus wallet.'));
            }
        }
      
      //echo "<pre>Booking Data : ";print_r($booking); echo "</pre>"; exit;

        $booking->payment_method = $request->payment_method;
        $booking->amount_for_purchaser = $booking->purchaser_price;
        $booking->amount_for_carer = $booking->carer_price;
        $booking->status_id = 2;

        $purchaserProfile = PurchasersProfile::find($booking->purchaser_id);
        $carerProfile = CarersProfile::find($booking->carer_id);
        $serviceUser = ServiceUsersProfile::find($booking->service_user_id);

        $purchaser = User::find($purchaserProfile->id);
     
        if ($purchaser->use_register_code == 1) {
            $purchaser->use_register_code = 0;
            $purchaser->had_use_register_code = 1;
            $purchaser->save();

            $firstAppointment = $booking->appointments()->get()->first();
            $firstAppointment->use_register_code = 1;
            $firstAppointment->save();

            $booking->use_register_code = 1;
        }
//echo "<pre>Booking Data : ";print_r($booking); echo "</pre>"; exit;
        $booking->save();
        
        //set appointment price
        $appointments = $booking->appointments()->get();
            foreach ($appointments as $appointment) {
                if ($appointment === reset($appointments)) {
                    $appointment->use_register_code = 1;
                }

                $appointment->price_for_purchaser = $appointment->purchaser_price;
                $appointment->price_for_carer = $appointment->carer_price;
             // echo "<pre> appointment "; print_r($appointment); echo "<pre>"; exit;
                $appointment->save();
            }
        
        

        //message for carer
        $text = view(config('settings.frontTheme') . '.emails.new_booking')->with([
            'purchaser' => $purchaserProfile, 'booking' => $booking, 'serviceUser' => $serviceUser, 'carer' => $carerProfile, 'sendTo' => 'carer'
        ])->render();

        DB::table('mails')
            ->insert(
                [
                    'email' => $carerProfile->email,
                    'subject' => 'New booking on HOLM',
                    'text' => $text,
                    'time_to_send' => Carbon::now(),
                    'status' => 'new'
                ]);
                
           

        //message for purchaser
        $text1 = view(config('settings.frontTheme') . '.emails.new_booking')->with([
            'purchaser' => $purchaserProfile, 'booking' => $booking, 'serviceUser' => $serviceUser, 'carer' => $carerProfile, 'sendTo' => 'purchaser'
        ])->render();

        DB::table('mails')
            ->insert(
                [
                    'email' => $purchaserProfile->email,
                    'subject' => 'New booking on HOLM',
                    'text' => $text1,
                    'time_to_send' => Carbon::now(),
                    'status' => 'new'
                ]);
                
                    

//        sms to carer
        $message = 'Hi. ' . $booking->bookingPurchaserProfile->full_name . ' would like to book you. Please log into your account https://holm.care/ to accept or reject the booking request. The Holm Team';

       SmsTools::sendSmsToCarer($message, $booking->bookingCarerProfile);
     /* //CREDIT 50 TO CARER if (carer used register code)
      	$bookingAppointments = $booking->appointments()->get();
     	$numberOfBookings = $bookingAppointments->count();
     	$carer = $booking->bookingCarer()->first();
      
      	$booking_count  = Booking::where('carer_id',$booking->carer_id)->count();
       	$count = 1;
     if ($booking_count == $count && $carer->referral_code == 'REGISTER'){
           if($carer->use_register_code){

               //Create bonus for first booking, if has not yet
                if(!$carer->bonusPayouts()->where('bonus_type_id', 1)->get()->count()){

                    $carer->bonusPayouts()->create([
                       'bonus_type_id' => 1,
                       'amount' => 50,
                    ]);
                }
            }
       }
       */

        return response(['status' => 'success']);
    }

    public function accept(Booking $booking, StripeService $stripeService)
    {
     
		//echo "<pre>"; print_r($booking); echo "<pre>"; exit;
      
      
        $purchaser = User::find($booking->purchaser_id);
        $purchaserProfile = PurchasersProfile::find($booking->purchaser_id);
        $carer_users = User::find($booking->carer_id);
     

        if ($booking->amount_for_purchaser == 0) { // for free appointment with code 'wecare'
            //Set prices for appointments
            $appointments = $booking->appointments()->get();
            foreach ($appointments as $appointment) {
                if ($appointment === reset($appointments)) {
                    $appointment->use_register_code = 1;
                }

                $appointment->price_for_purchaser = $appointment->purchaser_price;
                $appointment->price_for_carer = $appointment->carer_price;
                $appointment->save();
            }

            //Messages for workroom
            BookingsMessage::create([
                'booking_id' => $booking->id,
                'type' => 'status_change',
                'new_status' => 'in_progress',
            ]);

            //Set status of booking "in progress"
            $booking->status_id = $booking->carer_status_id = $booking->purchaser_status_id = self::IN_PROGRESS;
            $booking->amount_for_purchaser = $booking->purchaser_price;
            $booking->amount_for_carer = $booking->carer_price;
            $booking->save();
          
         

 
            $carerProfile = CarersProfile::find($booking->carer_id);
            $serviceUser = ServiceUsersProfile::find($booking->service_user_id);

            $text = view(config('settings.frontTheme') . '.emails.conform_booking')->with([
                'purchaser' => $purchaserProfile, 'booking' => $booking, 'serviceUser' => $serviceUser, 'carer' => $carerProfile, 'sendTo' => (Auth::user()->user_type_id == 3 ? 'purchaser' : 'carer'),
                'first_appointment_day' => $booking->appointments()->get()->first()->date_start, 'date', 'time'
            ])->render();

            DB::table('mails')
                ->insert(
                    [
                        'email' => (Auth::user()->user_type_id == 3 ? $purchaser->email : $carer_users->email),
                        'subject' => 'Booking confirmed',
                        'text' => $text,
                        'time_to_send' => Carbon::now(),
                        'status' => 'new'
                    ]);

            return response(['status' => 'success']);
        } else {
          
            if ($booking->payment_method == 'credit_card') {
             
                try {
					 
					
                    if (substr($booking->card_token, 0, 3) == 'cus') {
                        $purchase = PaymentTools::createCustomerCharge($booking->purchaser_price * 100, $booking->card_token, $booking->id);    
                    } elseif (substr($booking->card_token, 0, 3) == 'tok') {
                   $purchase = PaymentTools::createCharge($booking->purchaser_price * 100, $booking->card_token, $booking->id);
                     }
                    //Set prices for appointments
                    $appointments = $booking->appointments()->get();
                 
                    foreach ($appointments as $appointment) {
                        if ($appointment === reset($appointments)) {
                            $appointment->use_register_code = 1;
                        }

                        $appointment->price_for_purchaser = $appointment->purchaser_price;
                        $appointment->price_for_carer = $appointment->carer_price;
                        $appointment->save();
                    }

                    //Messages for workroom
                    BookingsMessage::create([
                        'booking_id' => $booking->id,
                        'type' => 'status_change',
                        'new_status' => 'in_progress',
                    ]);

                    //Set status of booking "in progress"
                    $booking->status_id = $booking->carer_status_id = $booking->purchaser_status_id = self::IN_PROGRESS;
                    $booking->amount_for_purchaser = $booking->purchaser_price;
                    $booking->amount_for_carer = $booking->carer_price;
                    $booking->save();

                    $carerProfile = CarersProfile::find($booking->carer_id);
                    $serviceUser = ServiceUsersProfile::find($booking->service_user_id);

                    $text = view(config('settings.frontTheme') . '.emails.conform_booking')->with([
                        'purchaser' => $purchaserProfile, 'booking' => $booking, 'serviceUser' => $serviceUser, 'carer' => $carerProfile, 'sendTo' => (Auth::user()->user_type_id == 3 ? 'purchaser' : 'carer'),
                        'first_appointment_day' => $booking->appointments()->get()->first()->date_start, 'date', 'time'
                    ])->render();
                  	$message = 'Hi '.$purchaserProfile->full_name.', '.$carerProfile->full_name.' has accepted your booking request. Best wishes The Holm Team';
					//$message = 'Hi. ' . $carerProfile->full_name . ' accept your booking request. The Holm Team';
                  
                  	SmsTools::sendSmsToPurchaser($message, $booking->bookingPurchaserProfile);
					//echo "<pre>"; print_r($booking->bookingPurchaserProfile); echo "<pre>"; exit;
                    DB::table('mails')
                        ->insert(
                            [
                                'email' => (Auth::user()->user_type_id == 3 ? $purchaser->email : $carer_users->email),
                                'subject' => 'Booking confirmed',
                                'text' => $text,
                                'time_to_send' => Carbon::now(),
                                'status' => 'new'
                            ]);

                    return response(['status' => 'success']);


                } catch (\Exception $ex) {
                    $text = view(config('settings.frontTheme') . '.emails.booking_payment_failed')->with([
                        'purchaser' => $purchaserProfile, 'booking' => $booking])->render();

                    DB::table('mails')
                        ->insert(
                            [
                                'email' => $purchaser->email,
                                'subject' => 'Payment error',
                                'text' => $text,
                                'time_to_send' => Carbon::now(),
                                'status' => 'new'
                            ]);

                    $text = view(config('settings.frontTheme') . '.emails.booking_payment_failed')->with([
                        'purchaser' => $purchaserProfile, 'carer' => $carer_users, 'booking' => $booking])->render();
                 

                    DB::table('mails')
                        ->insert(
                            [
                                'email' => 'nik@holm.care',
                                'subject' => 'Payment error',
                                'text' => $text,
                                'time_to_send' => Carbon::now(),
                                'status' => 'new'
                            ]);


                 /*   DB::table('mails')
                        ->insert(
                            [
                                'email' => 'nataliabarladin@gmail.com',
                                'subject' => 'Payment error',
                                'text' => $text,
                                'time_to_send' => Carbon::now(),
                                'status' => 'new'
                            ]); */

                    $res = substr($booking->card_token, 0, 3) == 'cus';
                    $true = 'true';
                    $true = ($res) ? 'true' : 'false';

                    return response($this->formatResponse('error', 'Sorry. But there has been an error with the booking and it is not confirmed. You might receive another message for you to confirm at some point.'));
                }
            } else {
                try {
                    PaymentTools::createBonusPayment($booking->purchaser_price, $booking->id);
                    //Set prices for appointments
                    $appointments = $booking->appointments()->get();
                    foreach ($appointments as $appointment) {
                        $appointment->price_for_purchaser = $appointment->purchaser_price;
                        $appointment->price_for_carer = $appointment->carer_price;
                        $appointment->save();
                    }

                    //Messages for workroom
                    BookingsMessage::create([
                        'booking_id' => $booking->id,
                        'type' => 'status_change',
                        'new_status' => 'in_progress',
                    ]);

                    //Set status of booking "in progress"
                    $booking->status_id = $booking->carer_status_id = $booking->purchaser_status_id = self::IN_PROGRESS;
                    $booking->amount_for_purchaser = $booking->purchaser_price;
                    $booking->amount_for_carer = $booking->carer_price;
                    $booking->save();
                    return response(['status' => 'success']);
                } catch (\Exception $ex) {
                 
                    return response($this->formatResponse('error', $ex->getMessage()));
                }
            }
        }


    }

    public function reject(Booking $booking)
    {
     
        $booking->status_id = self::CANCELLED;
        $booking->carer_status_id = self::CANCELLED;
        $booking->purchaser_status_id = self::CANCELLED;
        $booking->appointments()
            ->where('status_id', '!=', self::APPOINTMENT_STATUS_COMPLETED)
            ->update([
                'status_id' => self::APPOINTMENT_STATUS_CANCELLED,
                'carer_status_id' => self::APPOINTMENT_USER_STATUS_REJECTED,
                'purchaser_status_id' => self::APPOINTMENT_USER_STATUS_REJECTED,
            ]);

        BookingsMessage::create([
            'booking_id' => $booking->id,
            'type' => 'status_change',
            'new_status' => 'canceled',
        ]);

        $purchaserProfile = PurchasersProfile::find($booking->purchaser_id);
        $carerProfile = CarersProfile::find($booking->carer_id);
        $serviceUser = ServiceUsersProfile::find($booking->service_user_id);
 
        $purchaser = User::find($purchaserProfile->id);

        $firstAppointment = $booking->appointments()->get()->first();

        if ($firstAppointment->use_register_code == 1) {
            $purchaser->use_register_code = 1;
            $purchaser->had_use_register_code = 0;
            $purchaser->save();
            $firstAppointment->use_register_code = 0;
        }

        $firstAppointment->save();
        $booking->save();

        if(Auth::user()->isCarer()){
            $user = User::find($booking->purchaser_id);
            $email =  $user->email;
            $text = view(config('settings.frontTheme') . '.emails.reject_booking')->with([
                'purchaser' => $purchaserProfile, 'booking' => $booking, 'serviceUser' => $serviceUser, 'carer' => $carerProfile, 'sendTo' => 'purchaser'
            ])->render();

            //sms to purchaser
            $message = 'Sorry. ' . $carerProfile->full_name . ' rejected your booking request. The Holm Team';
            SmsTools::sendSmsToPurchaser($message, $purchaserProfile);
        }
        /*ALEXX changed variable from purchaser to purchaserProfile*/

        if(Auth::user()->isPurchaser()){
            //message for purchaser
            $user = User::find($booking->carer_id);
            $email = $user->email;
            $text = view(config('settings.frontTheme') . '.emails.reject_booking')->with([
                'purchaserProfile' => $purchaserProfile, 'booking' => $booking, 'serviceUser' => $serviceUser, 'carer' =>
                    $carerProfile, 'sendTo' => 'carer'
            ])->render();

            $message = 'Sorry. ' . $purchaserProfile->full_name . ' rejected your booking request. The Holm Team';
            SmsTools::sendSmsToCarer($message, $carerProfile);
        }

        DB::table('mails')
            ->insert(
                [
                    'email' => $email,
                    'subject' => 'Rejected booking on HOLM',
                    'text' => $text,
                    'time_to_send' => Carbon::now(),
                    'status' => 'new'
                ]);
       ////Refund charge to purchaser 
		$sql = 'SELECT
			  SUM(b.amount_for_purchaser) as total,  MAX(pp.id) as purchaser_id, MAX(pp.first_name) as first_name, MAX(pp.family_name) as family_name, a.booking_id
			FROM appointments a
			JOIN bookings b ON a.booking_id = b.id
			JOIN users p ON b.purchaser_id = p.id
			JOIN purchasers_profiles pp ON pp.id = p.id
			WHERE  a.payout = false AND pp.id = '.$booking->purchaser_id.' AND a.booking_id = '.$booking->id.'
			GROUP BY a.booking_id';
		$res = DB::select($sql);
		$data = $res[0];
		$comment = 'Payment to Purchaser '.$data->first_name.' '.$data->family_name.' ('.$data->purchaser_id.') for booking '.$booking->id;
		        
		try {
            if($booking->payment_method == 'credit_card'){
             
               //Get stripe account of carer
                $stripeCharge = StripeCharge::where('booking_id', $booking->id)->first();
             if(isset($stripeCharge))
                $res = PaymentTools::createRefund($data->total*100, $stripeCharge->id, $booking->id, $comment);
            } else {
                $res = PaymentTools::createBonusRefund($data->total, $booking->id, $comment);
            }
        } catch (\Exception $ex) {
          
            return response($this->formatResponse('error', $ex->getMessage()));
        }
	
	    //Mark certain appointments as with poyout
		Appointment::where('booking_id', $booking->id)
            ->where('payout', 0)
            ->update(['payout' => 1]);
        return response(['status' => 'success']);
    }

    public function cancel(Booking $booking)
    {
        
        
        $user = Auth::user();
        $purchaserProfile = PurchasersProfile::find($booking->purchaser_id);
        $carerProfile = CarersProfile::find($booking->carer_id);
        $serviceUser = ServiceUsersProfile::find($booking->service_user_id);

        $purchaser = User::find($purchaserProfile->id);

        $cancelBooking = true;
        if($booking->status_id != self::AWAITING_CONFIRMATION) {
            foreach ($booking->appointments as $appointment) {
				
				///check for admin 
				if($user->isAdmin()){
					   $appointment->status_id = self::APPOINTMENT_STATUS_CANCELLED;
						if ($appointment->use_register_code == 1) {
							$purchaser->use_register_code = 1;
							$purchaser->had_use_register_code = 0;
							$purchaser->save();

							$appointment->use_register_code = 0;
						}
						$appointment->save();
						
				}else{
				
					if ($appointment->cancelable) {
						$appointment->status_id = self::APPOINTMENT_STATUS_CANCELLED;

						if (Auth::user()->isCarer()) {
							$message = 'Sorry. ' . $appointment->booking->bookingCarerProfile->full_name . ' has cancelled your appointment for  ' . $appointment->formatted_date_start . ' ' . $appointment->formatted_time_from . '. Please check your account https://holm.care/ for more details. The Holm Team';
							SmsTools::sendSmsToServiceUser($message, $appointment->booking->bookingServiceUser);

							$message = 'Sorry. ' . $appointment->booking->bookingCarerProfile->full_name . ' has cancelled your appointment for  ' . $appointment->formatted_date_start . ' ' . $appointment->formatted_time_from . '. Please check your account https://holm.care/ for more details. The Holm Team';
							SmsTools::sendSmsToPurchaser($message, $appointment->booking->bookingPurchaserProfile);
						} elseif (Auth::user()->isPurchaser()) {
							$message = 'Sorry. ' . $appointment->booking->bookingServiceUser->full_name . ' has cancelled your appointment for  ' . $appointment->formatted_date_start . ' ' . $appointment->formatted_time_from . '. Please check your account https://holm.care/ for more details. The Holm Team';
							SmsTools::sendSmsToCarer($message, $appointment->booking->bookingCarerProfile);
						}
						if ($appointment->use_register_code == 1) {
							$purchaser->use_register_code = 1;
							$purchaser->had_use_register_code = 0;
							$purchaser->save();

							$appointment->use_register_code = 0;
						}
						$appointment->save();
					}else {
						$cancelBooking = false;
					}
                
			    }

            }
        }
      
       ///refund appointments get canceled
		$sql = 'SELECT
			  SUM(a.price_for_purchaser) as total,  MAX(pp.id) as purchaser_id, MAX(pp.first_name) as first_name, MAX(pp.family_name) as family_name, a.booking_id
			FROM appointments a
			JOIN bookings b ON a.booking_id = b.id
			JOIN users p ON b.purchaser_id = p.id
			JOIN purchasers_profiles pp ON pp.id = p.id
			WHERE  a.payout = false AND pp.id = '.$booking->purchaser_id.' AND  a.booking_id = '.$booking->id.' AND a.status_id = 5
			 GROUP BY a.booking_id';
    //echo $sql;

		$res = DB::select($sql);
      if($res){
		$data = $res[0];
		
		$comment = 'Payment to Purchaser '.$data->first_name.' '.$data->family_name.' ('.$data->purchaser_id.') for booking '.$booking->id;
		
		try {
			if($booking->payment_method == 'credit_card'){
				//Get stripe account of carer

				$stripeCharge = StripeCharge::where('booking_id', $booking->id)->first();
				$res = PaymentTools::createRefund($data->total*100, $stripeCharge->id, $booking->id, $comment);
			} else {
				$res = PaymentTools::createBonusRefund($data->total, $booking->id, $comment);
			}
		} catch (\Exception $ex) {
			return response($this->formatResponse('error', $ex->getMessage()));
		}

		//Mark certain appointments as with poyout
		Appointment::where('booking_id', $booking->id)
	    ->where('status_id', 5)
		->where('payout', 0)
		->update(['payout' => 1]);
      
      }
      
      
      
      

        if($cancelBooking){
            $booking->status_id = self::CANCELLED;
            BookingsMessage::create([
                'booking_id' => $booking->id,
                'type' => 'status_change',
                'new_status' => 'canceled',
            ]);

            $firstAppointment = $booking->appointments()->get()->first();
            if ($firstAppointment->use_register_code == 1) {
                $purchaser->use_register_code = 1;
                $purchaser->had_use_register_code = 0;
                $purchaser->save();

                $firstAppointment->use_register_code = 0;
            }

            $firstAppointment->save();
            $booking->save();

            $app_from = Appointment::where('booking_id', $booking->id)->orderBy('date_start')->first();
            $app_to = Appointment::where('booking_id', $booking->id)->orderBy('date_start', 'desc')->first();
            $appDateFrom = Carbon::parse($app_from->date_start)->format('d-m-Y');
            $appDateTo = Carbon::parse($app_to->date_start)->format('d-m-Y');
            $appTimeFrom = $app_from->time_from;
            $appTimeTo = $app_to->time_from;

            if(Auth::user()->isCarer()){
                $user = User::find($booking->purchaser_id);
                $email =  $user->email;
                $text = view(config('settings.frontTheme') . '.emails.reject_booking')->with([
                    'purchaser' => $purchaserProfile, 'booking' => $booking, 'serviceUser' => $serviceUser, 'carer' => $carerProfile, 'sendTo' => 'purchaser'
                ])->render();

                $message = 'Hi ' . $purchaserProfile->like_name . '. ' . $carerProfile->first_name . ' has cancelled your booking ' . $appDateFrom . ' ' . $appTimeFrom . ' - ' . $appDateTo . ' ' . $appTimeTo . '. Please check your profile on the website https://holm.care/ for more details, and contact us if you need any help. The Holm Team';
                SmsTools::sendSmsToPurchaser($message, $purchaserProfile);
            }
            if(Auth::user()->isPurchaser()){
                //message for carer
                $user = User::find($booking->carer_id);
                $email = $user->email;
                $text = view(config('settings.frontTheme') . '.emails.reject_booking')->with([
                    'purchaser' => $purchaserProfile, 'booking' => $booking, 'serviceUser' => $serviceUser, 'carer' =>
                        $carerProfile, 'sendTo' => 'carer'
                ])->render();

                $message = 'Hi ' . $carerProfile->like_name . '. ' . $purchaserProfile->first_name . ' has cancelled your booking ' . $appDateFrom . ' ' . $appTimeFrom . ' - ' . $appDateTo . ' ' . $appTimeTo . '. Please check your profile on the website https://holm.care/ for more details, and contact us if you need any help. The Holm Team';
                SmsTools::sendSmsToCarer($message, $carerProfile);
            }
            if(Auth::user()->isAdmin()){
			  $message = 'Hi ' . $purchaserProfile->like_name . '.  your booking for ' . $appDateFrom . ' ' . $appTimeFrom . ' - ' . $appDateTo . ' ' . $appTimeTo . ' has been cancelled. Please check your profile on the website https://holm.care/ for more details, and contact us if you need any help. The Holm Team';
			  $message = 'Hi ' . $carerProfile->like_name . '.  your booking for ' . $appDateFrom . ' ' . $appTimeFrom . ' - ' . $appDateTo . ' ' . $appTimeTo . ' has been cancelled. Please check your profile on the website https://holm.care/ for more details, and contact us if you need any help. The Holm Team';
               SmsTools::sendSmsToPurchaser($message, $purchaserProfile);
               SmsTools::sendSmsToCarer($message, $carerProfile);

			}else{
				
				 DB::table('mails')
                 ->insert(
                    [
                        'email' => $email,
                        'subject' => 'Rejected booking on HOLM',
                        'text' => $text,
                        'time_to_send' => Carbon::now(),
                        'status' => 'new'
                    ]);
				
			}

            return response(['status' => 'success']);
        }

        return response(['status' => 'uncancelableAppointments', 'message' => 'Appointments with less than 24 hours haven\'t been canceled. Please call 0161 706 0288 if need further assistance']);
    }

    public function completed(Booking $booking)
    {
    
      
        $user = Auth::user();
     if ($booking->has_active_appointments)
            return response(['status' => 'error']);

        if ($user->user_type_id == 3) {
            //Carer
            $booking->carer_status_id = self::COMPLETED;
            if ($booking->purchaser_status_id == self::COMPLETED) {
                BookingsMessage::create([
                    'booking_id' => $booking->id,
                    'type' => 'status_change',
                    'new_status' => 'completed',
                ]);
                $booking->status_id = self::COMPLETED;
                event(new BookingCompletedEvent($booking));
            }

        } else {
            //Purchaser
            $booking->purchaser_status_id = self::COMPLETED;
            $booking->status_id = self::COMPLETED;
            event(new BookingCompletedEvent($booking));
            BookingsMessage::create([
                'booking_id' => $booking->id,
                'type' => 'status_change',
                'new_status' => 'completed',
            ]);
        }

        if ($booking->appointments()->get()->first()->use_register_code == 1) {
            $purchaser = $booking->bookingPurchaser;
            $purchaser->use_register_code = 0;
            $purchaser->had_use_register_code = 0;
            $purchaser->save();
        }

        $booking->save();

        return response(['status' => 'success']);
    }

    public function create_message(Booking $booking, Request $request)
    {
		
        $user = Auth::user();
        $sender = ($user->user_type_id == 3 ? 'carer' : 'service_user');
        $server = ServiceUsersProfile::find($booking->service_user_id);
        $carer = CarersProfile::find($booking->carer_id);
        $purchaser = User::find($booking->purchaser_id);
        $p = PurchasersProfile::find($booking->purchaser_id);
        $carer_users = User::find($booking->carer_id);

        $BookingsMessage = BookingsMessage::create([
            'booking_id' => $booking->id,
            'sender' => $sender,
            'type' => 'message',
            'text' => $request->message,
        ]);


        $text = view(config('settings.frontTheme') . '.emails.new_message')->with([
            'server_users' => $server,
            'purchaser' => $p,
            'carer' => $carer,
            'booking' => $booking,
            'text' => $request->message,
            'sender' => $sender
        ])->render();



        DB::table('mails')
            ->insert(
                [
                    'email' => ($user->user_type_id == 3 ? $purchaser->email : $carer_users->email),
                    'subject' => 'You have a new message in your booking',
                    'text' => $text,
                    'time_to_send' => Carbon::now(),
                    'status' => 'new'
                ]);

        if($user->user_type_id == 3) {
            $message = 'Hi ' . $booking->bookingPurchaserProfile->like_name . ', you have a new message from ' . $booking->bookingCarerProfile->full_name . '. Please check your bookings on the website https://holm.care/';
			
			$sent = mail("poonam.durbule@smartdatainc.net"," check SMS purchaser ",$message);
		
			
            SmsTools::sendSmsToPurchaser($message, $booking->bookingPurchaserProfile);
        } else{
            $message = 'Hi ' . $booking->bookingCarerProfile->like_name . ', you have a new message from ' . $booking->bookingPurchaserProfile->full_name . '. Please check your bookings on the website https://holm.care/';
           
			$sent=mail("poonam.durbule@smartdatainc.net"," check SMS carer ",$message);

           
            SmsTools::sendSmsToCarer($message, $booking->bookingCarerProfile);
        }

        return redirect(url('/bookings/' . $booking->id . '/details#comments'));
    }

    public function leaveReviewPage(Booking $booking)
    {
        $this->template = config('settings.frontTheme') . '.templates.carerPrivateProfile';
        $this->title = 'Holm Care';

        $header = view(config('settings.frontTheme') . '.headers.baseHeader')->render();
        $footer = view(config('settings.frontTheme') . '.footers.baseFooter')->render();
        $modals = view(config('settings.frontTheme') . '.includes.modals')->render();

        $this->vars = array_add($this->vars, 'header', $header);
        $this->vars = array_add($this->vars, 'footer', $footer);
        $this->vars = array_add($this->vars, 'modals', $modals);

        $this->vars = array_add($this->vars, 'user', $this->user);
        $this->vars = array_add($this->vars, 'booking', $booking);

        $this->content = view(config('settings.frontTheme') . '.booking.leave_review')->with($this->vars)
            ->render();

        return $this->renderOutput();
    }

    public function createReview(Booking $booking, Request $request)
    {

        BookingOverview::create([
            'booking_id' => $booking->id,
            'punctuality' => $request->punctuality,
            'friendliness' => $request->friendliness,
            'communication' => $request->communication,
            'performance' => $request->performance,
            'comment' => $request->comment,
        ]);
        $server_users = ServiceUsersProfile::find($booking->service_user_id);
        $carer_users = CarersProfile::find($booking->carer_id);
        $text = view(config('settings.frontTheme') . '.emails.new_review')->with([
            'server_users' => $server_users,
            'carer_users' => $carer_users,
        ])->render();

        DB::table('mails')
            ->insert(
                [
                    'email' => 'nik@holm.care',
                    'subject' => 'You have a new review moderation',
                    'text' => $text,
                    'time_to_send' => Carbon::now(),
                    'status' => 'new'
                ]);
       /* DB::table('mails')
            ->insert(
                [
                    'email' => 'nataliabarladin@gmail.com',
                    'subject' => 'You have a new review moderation',
                    'text' => $text,
                    'time_to_send' => Carbon::now(),
                    'status' => 'new'
                ]); */
        return redirect()->back();
    }

    /**
     * @param Carbon $start_date
     * @param Carbon $end_date
     * @param int $step
     * @return array
     */
    private function generateDateRange(Carbon $start_date, Carbon $end_date, int $step = 1)
    {
        $dates = [];
        for ($date = $start_date; $date->lte($end_date); $date->addDays($step)) {
            $dates[] = $date->format('Y-m-d');
        }
        return $dates;
    }

    private function createBooking(BookingCreateRequest $data): Booking
    {
      
     
        $purchaser = Auth::user();
     
  
        $carer = User::find($data->carer_id);
      
        $liveInCarer = isset($data->live_in_carer)? 1 : 0 ;
	
        foreach ($data->bookings as $booking_item) {
			//echo "<pre>live_in_carer"; print_r($booking_item); echo "<pre>"; exit;
            //Creating booking
            $serviceUser = ServiceUsersProfile::find($data->service_user_id);
          	
           //echo "<pre> price: "; print_r($serviceUser); echo "<pre>";  exit();
            $booking = Booking::create([
                'purchaser_id' => $purchaser->id,
                'service_user_id' => $serviceUser->id,
                'carer_id' => $carer->id,
                'status_id' => 1,
                'carer_status_id' => 2,
                'purchaser_status_id' => 1,
                'live_in_carer' => $liveInCarer
//                'amount_for_purchaser' => $data->bookings->,
//                'amount_for_carer' =>
            ]);
       

            //Generating appointments
         
            $this->createAppointments($booking, $booking_item['appointments']);
  
            //Booking status for workroom
            BookingsMessage::create([
                'booking_id' => $booking->id,
                'type' => 'status_change',
                'new_status' => 'pending',
            ]);
            
            return $booking;
        }
    }

    private function createAppointments(Booking $booking, array $appointments): bool
    {
      
      $live_in_care_type 			=	0;
      $start_date                   =   '0000-00-00';
      $end_date                     =   '0000-00-00';
     
      //echo "<pre>"; print_r($appointments); echo "<pre>";exit;
       //if($end_date == '0000-00-00'){
        // $days = $this->generateDateRange(Carbon::parse(date_create_from_format('d/m/Y',$start_date)->format("Y-m-d")),Carbon::parse($end_date));
       //  } else {
      //   $days = $this->generateDateRange(Carbon::parse(date_create_from_format('d/m/Y',$start_date)->format("Y-m-d")),Carbon::parse(date_create_from_format('d/m/Y', $end_date)->format("Y-m-d"))); 
     //    }
       //echo "<pre>"; print_r($appointments); echo "<pre>";
        foreach ($appointments as $batch => $appointment_item) {
          $start_date = $appointment_item['date_start'];
          if(isset($appointment_item['date_end']))
         	 $end_date = $appointment_item['date_end'];
            isset($appointment_item['periodicity']) ? false : $appointment_item['periodicity'] = 'single';
            $days = [];
        switch (strtolower($appointment_item['periodicity'])) {
               case 'daily':
               // if($end_date == '0000-00-00'){
       //  $days = $this->generateDateRange(Carbon::parse(date_create_from_format('d/m/Y',$start_date)->format("Y-m-d")),Carbon::parse($end_date));
        // } else {
       //  $days = $this->generateDateRange(Carbon::parse(date_create_from_format('d/m/Y',$start_date)->format("Y-m-d")),Carbon::parse(date_create_from_format('d/m/Y', $end_date)->format("Y-m-d"))); 
       //  }
                    $days = $this->generateDateRange(Carbon::parse(date_create_from_format('d/m/Y', $appointment_item['date_start'])->format("Y-m-d")), Carbon::parse(date_create_from_format('d/m/Y', $appointment_item['date_end'])->format("Y-m-d")));
            //commented on 17/06/2019
                
                
                    break;
                case 'weekly':
               //   if($end_date == '0000-00-00'){
               //  $days = $this->generateDateRange(Carbon::parse(date_create_from_format('d/m/Y',$start_date)->format("Y-m-d")),Carbon::parse($end_date));
     //    } else {
       //  $days = $this->generateDateRange(Carbon::parse(date_create_from_format('d/m/Y',$start_date)->format("Y-m-d")),Carbon::parse(date_create_from_format('d/m/Y', $end_date)->format("Y-m-d")),7); 
         //}

             $days = $this->generateDateRange(Carbon::parse(date_create_from_format('d/m/Y', $appointment_item['date_start'])->format("Y-m-d")), Carbon::parse(date_create_from_format('d/m/Y', $appointment_item['date_end'])->format("Y-m-d")), 7);
                    break;
                case 'single':
            
              //  if($end_date == '0000-00-00'){
       //  $days = $this->generateDateRange(Carbon::parse(date_create_from_format('d/m/Y',$start_date)->format("Y-m-d")),Carbon::parse($end_date));
        // } else {
                   
       //  $days = $this->generateDateRange(Carbon::parse(date_create_from_format('d/m/Y',$start_date)->format("Y-m-d")),Carbon::parse(date_create_from_format('d/m/Y', $end_date)->format("Y-m-d"))); 
                 
                   //}
                   $days = $this->generateDateRange(Carbon::parse(date_create_from_format('d/m/Y', $appointment_item['date_start'])->format("Y-m-d")), Carbon::parse(date_create_from_format('d/m/Y', $appointment_item['date_start'])->format("Y-m-d")));
                    break;
                case 'live_in_carer':
           //     if($end_date == '0000-00-00'){
       //  $days = $this->generateDateRange(Carbon::parse(date_create_from_format('d/m/Y',$start_date)->format("Y-m-d")),Carbon::parse($start_date),7);
         //} else {
        // $days = $this->generateDateRange(Carbon::parse(date_create_from_format('d/m/Y',$start_date)->format("Y-m-d")),Carbon::parse(date_create_from_format('d/m/Y', $start_date)->format("Y-m-d")),7); 
       //  }
                    $days = $this->generateDateRange(Carbon::parse(date_create_from_format('d/m/Y', $appointment_item['date_start'])->format("Y-m-d")), Carbon::parse(date_create_from_format('d/m/Y', $appointment_item['date_end'])->format("Y-m-d")));
                    break;
            }
			$weekDaysArr 							=	[];
			$remainDyasArr 							=	[];
            //echo "No of Days : "; print_r(count($days));
			//echo "Days List : "; print_r($days); exit;
			if($appointment_item['periodicity'] == 'live_in_carer'){ //echo 11; exit;
              	$noOfWeek 							=	floor(count($days)/7);
              	$remaindays 						=	floor(count($days)%7);
                if(isset($noOfWeek) && $noOfWeek != ''){
					$noOfdays 						=	$noOfWeek*7;
					$weekDays 						= 	array_slice($days, 0, $noOfdays);
					$live_in_care_type 				=	'2';
					foreach($weekDays as $data){
                      	$weekDaysArr[] 				=	$data;
                    }
                }
              	if(isset($remaindays) && $remaindays !=''){
					$remainDays 					= 	array_slice($days, '-'.$remaindays, $remaindays,TRUE);
                	$live_in_care_type 				=	'1';
					foreach($remainDays as $data){
                      	$remainDyasArr[] 			=	$data;
                    }
                }
				//echo "<pre>"; print_r($weekDaysArr); echo "<pre>";exit;
            	if(is_array($weekDaysArr) && count($weekDaysArr)){
                	foreach($weekDaysArr as $weekDay){
                    	$appointment 				= 	$booking->appointments()->create([
                                                                'date_start' 			=> $weekDay,
                                                                'time_from' 			=> date("H.i", strtotime($appointment_item['time_from'])),
                                                                'time_to' 				=> date("H.i", strtotime($appointment_item['time_to'])),
                          										'periodicity' 			=> $appointment_item['periodicity'],
                                                                'status_id' 			=> 1,
                                                                'carer_status_id' 		=> 1,
                                                                'purchaser_status_id' 	=> 1,
                                                                'batch' 				=> $batch,
                                                                'use_register_code' 	=> 0,
                                                                'live_in_care_type' 	=> 2
                                                            ]);
                        //Attaching booking`s assistance_types
                        if (isset($appointment_item['assistance_types']))
                            $appointment->assistance_types()->attach($appointment_item['assistance_types']);
					}
				}
				if(is_array($remainDyasArr) && count($remainDyasArr)){
                	foreach($remainDyasArr as $remainDay){
                    	$appointment 				= 	$booking->appointments()->create([
                                                                'date_start' 			=> $remainDay,
                                                                'time_from' 			=> date("H.i", strtotime($appointment_item['time_from'])),
                                                                'time_to' 				=> date("H.i", strtotime($appointment_item['time_to'])),
                                                                'periodicity' 			=> $appointment_item['periodicity'],
                                                                'status_id' 			=> 1,
                                                                'carer_status_id' 		=> 1,
                                                                'purchaser_status_id' 	=> 1,
                                                                'batch' 				=> $batch,
                                                                'use_register_code' 	=> 0,
                                                                'live_in_care_type' 	=> 1
                                                            ]);
						//echo "<pre>"; print_r($appointment); echo "<pre>";exit;
                        //Attaching booking`s assistance_types
                        if (isset($appointment_item['assistance_types']))
                            $appointment->assistance_types()->attach($appointment_item['assistance_types']);
					}
				}
            }else{ //echo 12; exit;
              
            	foreach ($days as $day) {
                
                  if ($booking->use_register_code == 1) {
                      if ($day === reset($days)) { //echo 11; exit;
                          $appointment = $booking->appointments()->create([
                              'date_start' => $day,
                              'time_from' => date("H.i", strtotime($appointment_item['time_from'])),
                              'time_to' => date("H.i", strtotime($appointment_item['time_to'])),
                              'periodicity' => $appointment_item['periodicity'],
                              'status_id' => 1,
                              'carer_status_id' => 1,
                              'purchaser_status_id' => 1,
                              'batch' => $batch,
                              'use_register_code' => 1
                          ]);

                          //Attaching booking`s assistance_types
                          if (isset($appointment_item['assistance_types']))
                              $appointment->assistance_types()->attach($appointment_item['assistance_types']);
                      } else { 
                        
                          $appointment = $booking->appointments()->create([
                              'date_start' => $day,
                              'time_from' => date("H.i", strtotime($appointment_item['time_from'])),
                              'time_to' => date("H.i", strtotime($appointment_item['time_to'])),
                              'periodicity' => $appointment_item['periodicity'],
                              'status_id' => 1,
                              'carer_status_id' => 1,
                              'purchaser_status_id' => 1,
                              'batch' => $batch,
                              'use_register_code' => 0
                          ]);

                          //Attaching booking`s assistance_types
                          if (isset($appointment_item['assistance_types']))
                              $appointment->assistance_types()->attach($appointment_item['assistance_types']);
                      }
                  } else {
                      $appointment = $booking->appointments()->create([
                          'date_start' => $day,
                          'time_from' => date("H.i", strtotime($appointment_item['time_from'])),
                          'time_to' => date("H.i", strtotime($appointment_item['time_to'])),
                          'periodicity' => $appointment_item['periodicity'],
                          'status_id' => 1,
                          'carer_status_id' => 1,
                          'purchaser_status_id' => 1,
                          'batch' => $batch,
                          'use_register_code' => 0
                      ]);
                     
                      //Attaching booking`s assistance_types
                      if (isset($appointment_item['assistance_types']))
                          $appointment->assistance_types()->attach($appointment_item['assistance_types']);
                  }
              }
			}
			//echo 12; exit;
           /* foreach ($days as $day) {
              
                if ($booking->use_register_code == 1) {
                    if ($day === reset($days)) { //echo 11; exit;
                        $appointment = $booking->appointments()->create([
                            'date_start' => $day,
                            'time_from' => date("H.i", strtotime($appointment_item['time_from'])),
                            'time_to' => date("H.i", strtotime($appointment_item['time_to'])),
                            'periodicity' => $appointment_item['periodicity'],
                            'status_id' => 1,
                            'carer_status_id' => 1,
                            'purchaser_status_id' => 1,
                            'batch' => $batch,
                            'use_register_code' => 1
                        ]);

                        //Attaching booking`s assistance_types
                        if (isset($appointment_item['assistance_types']))
                            $appointment->assistance_types()->attach($appointment_item['assistance_types']);
                    } else {/// echo 22; exit;
                        $appointment = $booking->appointments()->create([
                            'date_start' => $day,
                            'time_from' => date("H.i", strtotime($appointment_item['time_from'])),
                            'time_to' => date("H.i", strtotime($appointment_item['time_to'])),
                            'periodicity' => $appointment_item['periodicity'],
                            'status_id' => 1,
                            'carer_status_id' => 1,
                            'purchaser_status_id' => 1,
                            'batch' => $batch,
                            'use_register_code' => 0
                        ]);

                        //Attaching booking`s assistance_types
                        if (isset($appointment_item['assistance_types']))
                            $appointment->assistance_types()->attach($appointment_item['assistance_types']);
                    }
                } else {
                    $appointment = $booking->appointments()->create([
                        'date_start' => $day,
                        'time_from' => date("H.i", strtotime($appointment_item['time_from'])),
                        'time_to' => date("H.i", strtotime($appointment_item['time_to'])),
                        'periodicity' => $appointment_item['periodicity'],
                        'status_id' => 1,
                        'carer_status_id' => 1,
                        'purchaser_status_id' => 1,
                        'batch' => $batch,
                        'use_register_code' => 0
                    ]);
                    //Attaching booking`s assistance_types
                    if (isset($appointment_item['assistance_types']))
                        $appointment->assistance_types()->attach($appointment_item['assistance_types']);
                }
            }*/
        }

        return true;
    }
}
