<?php

namespace App\Http\Controllers\Bookings;
use App\Appointment;
use App\AppointmentOverview;
use App\Booking;
use App\CarersProfile;
use App\DisputePayout;
use App\Events\AppointmentCompletedEvent;
use App\Http\Controllers\Controller;
use App\Http\Controllers\FrontController;
use App\Interfaces\Constants;
use App\PurchasersProfile;
use App\ServiceUsersProfile;
use App\User;
use Auth;
use SmsTools;
use DB;
use Carbon\Carbon;

use Illuminate\Http\Request;
use App\StripeCharge;
use App\StripeConnectedAccount; 
use PaymentTools;

class AppointmentsController extends FrontController implements Constants
{
    public function reject(Appointment $appointment){
        $user = Auth::user();
        $booking = $appointment->booking;
        $purchaserProfile = PurchasersProfile::find($booking->purchaser_id);
        $carerProfile = CarersProfile::find($booking->carer_id);
        $serviceUser = ServiceUsersProfile::find($booking->service_user_id);

        $purchaserEmail = User::find($purchaserProfile->id);
        $purchaserEmail = $purchaserEmail->email;
        $carerEmail = User::find($carerProfile->id);
        $carerEmail = $carerEmail->email;

        if ($user->user_type_id == 1) {
            $who_disputed = 'purchaser';
        } elseif ($user->user_type_id == 3) {
            $who_disputed = 'carer';
        }

        if($user->isCarer()){
            //$appointment->status_id = self::APPOINTMENT_STATUS_CANCELLED;
            $appointment->carer_status_id = self::APPOINTMENT_USER_STATUS_REJECTED;//dispute
            if($appointment->purchaser_status_id == self::APPOINTMENT_USER_STATUS_REJECTED){//dispute
                $appointment->status_id = self::APPOINTMENT_STATUS_CANCELLED;//Cancelled

                $purchaser = User::find($purchaserProfile->id);
                if ($appointment->use_register_code == 1) {
                    $appointment->use_register_code = 0;
                    $appointment->save();

                    $purchaser->use_register_code = 1;
                    $purchaser->had_use_register_code = 0;
                    $purchaser->save();
                }

            } elseif ($appointment->purchaser_status_id == self::APPOINTMENT_USER_STATUS_COMPLETED) {//completed
                $appointment->status_id = self::APPOINTMENT_STATUS_DISPUTE;//dispute
                DisputePayout::create([
                    'appointment_id' => $appointment->id,
                ]);
            } elseif ($appointment->cancelable) {
                $appointment->status_id = self::APPOINTMENT_STATUS_CANCELLED;

                $message = 'Sorry. ' . $appointment->booking->bookingCarerProfile->full_name . ' has cancelled your appointment for  ' . $appointment->formatted_date_start . ' ' . $appointment->formatted_time_from . '. Please check your account https://holm.care/ for more details. The Holm Team';
                SmsTools::sendSmsToServiceUser($message, $appointment->booking->bookingServiceUser);

                $message = 'Sorry. ' . $appointment->booking->bookingCarerProfile->full_name . ' has cancelled your appointment for  ' . $appointment->formatted_date_start . ' ' . $appointment->formatted_time_from . '. Please check your account https://holm.care/ for more details. The Holm Team';
                SmsTools::sendSmsToPurchaser($message, $appointment->booking->bookingPurchaserProfile);
            }

            $appointment->save();

            if ($appointment->status_id == self::APPOINTMENT_STATUS_DISPUTE) {
                $textToPurchaser = view(config('settings.frontTheme') . '.emails.appointment_rejected')->with([
                    'purchaser' => $purchaserProfile, 'booking' => $booking, 'appointment' => $appointment, 'serviceUser' => $serviceUser, 'carer' =>
                        $carerProfile, 'sendTo' => 'purchaser', 'user_like_name' => $purchaserProfile->like_name, 'whoDisputed' => $who_disputed
                ])->render();

                DB::table('mails')
                    ->insert(
                        [
                            'email' => $purchaserEmail,
                            'subject' => 'Disputing appointment on HOLM',
                            'text' => $textToPurchaser,
                            'time_to_send' => Carbon::now(),
                            'status' => 'new'
                        ]);

                $textToCarer = view(config('settings.frontTheme') . '.emails.appointment_rejected')->with([
                    'purchaser' => $purchaserProfile, 'booking' => $booking, 'appointment' => $appointment, 'serviceUser' => $serviceUser, 'carer' =>
                        $carerProfile, 'sendTo' => 'carer', 'user_like_name' => $carerProfile->like_name, 'whoDisputed' => $who_disputed
                ])->render();

                DB::table('mails')
                    ->insert(
                        [
                            'email' => $carerEmail,
                            'subject' => 'Disputing appointment on HOLM',
                            'text' => $textToCarer,
                            'time_to_send' => Carbon::now(),
                            'status' => 'new'
                        ]);
            }
        } else {
            //Purchaser
            $appointment->purchaser_status_id = self::APPOINTMENT_USER_STATUS_REJECTED;//dispute

            if($appointment->cancelable){
                //$appointment->status_id = self::APPOINTMENT_STATUS_CANCELLED;
                $message = 'Sorry. ' . $appointment->booking->bookingServiceUser->full_name . ' has cancelled your appointment for  ' . $appointment->formatted_date_start . ' ' . $appointment->formatted_time_from . '. Please check your account https://holm.care/ for more details. The Holm Team';
                SmsTools::sendSmsToCarer($message, $appointment->booking->bookingCarerProfile);
//            } elseif($appointment->carer_status_id == self::APPOINTMENT_USER_STATUS_NEW) {
//                $appointment->purchaser_status_id = self::APPOINTMENT_USER_STATUS_REJECTED;
//                $message = 'Sorry. '.$appointment->booking->bookingServiceUser->full_name.' has cancelled your appointment for  '.$appointment->formatted_date_start.' '.$appointment->formatted_time_from.'. Please check your account https://holm.care/ for more details. The Holm Team';
//                SmsTools::sendSmsToCarer($message, $appointment->booking->bookingCarerProfile);
            } elseif($appointment->carer_status_id == self::APPOINTMENT_USER_STATUS_COMPLETED) {
                $appointment->status_id = self::APPOINTMENT_STATUS_DISPUTE;
                DisputePayout::create([
                    'appointment_id' => $appointment->id,
                ]);
            } elseif ($appointment->carer_status_id == self::APPOINTMENT_USER_STATUS_REJECTED) {
                $appointment->status_id = self::APPOINTMENT_STATUS_CANCELLED;
                $purchaser = User::find($purchaserProfile->id);
                if ($appointment->use_register_code == 1) {
                    $appointment->use_register_code = 0;
                    $appointment->save();

                    $purchaser->use_register_code = 1;
                    $purchaser->had_use_register_code = 0;
                    $purchaser->save();
                }

                $message = 'Sorry. ' . $appointment->booking->bookingServiceUser->full_name . ' has cancelled your appointment for  ' . $appointment->formatted_date_start . ' ' . $appointment->formatted_time_from . '. Please check your account https://holm.care/ for more details. The Holm Team';
                SmsTools::sendSmsToCarer($message, $appointment->booking->bookingCarerProfile);
            }
            $appointment->save();

            if ($appointment->status_id == self::APPOINTMENT_STATUS_DISPUTE) {
                $textToPurchaser = view(config('settings.frontTheme') . '.emails.appointment_rejected')->with([
                    'purchaser' => $purchaserProfile, 'booking' => $booking, 'appointment' => $appointment, 'serviceUser' => $serviceUser, 'carer' =>
                        $carerProfile, 'sendTo' => 'purchaser', 'user_like_name' => $purchaserProfile->like_name, 'whoDisputed' => $who_disputed
                ])->render();

                DB::table('mails')
                    ->insert(
                        [
                            'email' => $purchaserEmail,
                            'subject' => 'Disputing appointment on HOLM',
                            'text' => $textToPurchaser,
                            'time_to_send' => Carbon::now(),
                            'status' => 'new'
                        ]);

                $textToCarer = view(config('settings.frontTheme') . '.emails.appointment_rejected')->with([
                    'purchaser' => $purchaserProfile, 'booking' => $booking, 'appointment' => $appointment, 'serviceUser' => $serviceUser, 'carer' =>
                        $carerProfile, 'sendTo' => 'carer', 'user_like_name' => $carerProfile->like_name, 'whoDisputed' => $who_disputed
                ])->render();

                DB::table('mails')
                    ->insert(
                        [
                            'email' => $carerEmail,
                            'subject' => 'Disputing appointment on HOLM',
                            'text' => $textToCarer,
                            'time_to_send' => Carbon::now(),
                            'status' => 'new'
                        ]);
            }
        }

        if($appointment->status_id == self::APPOINTMENT_STATUS_DISPUTE) {
            $textToAdmin = view(config('settings.frontTheme') . '.emails.appointment_rejected')->with([
                'purchaser' => $purchaserProfile, 'booking' => $booking, 'appointment' => $appointment, 'serviceUser' => $serviceUser, 'carer' =>
                    $carerProfile, 'sendTo' => 'admin', 'user_like_name' => 'Nik', 'whoDisputed' => $user
            ])->render();
            DB::table('mails')
                ->insert(
                    [
                        'email' => 'nik@holm.care',
                        'subject' => 'Disputing appointment on HOLM',
                        'text' => $textToAdmin,
                        'time_to_send' => Carbon::now(),
                        'status' => 'new'
                    ]);

           /* DB::table('mails')
                ->insert(
                    [
                        'email' => 'nataliabarladin@gmail.com',
                        'subject' => 'Disputing appointment on HOLM',
                        'text' => $textToAdmin,
                        'time_to_send' => Carbon::now(),
                        'status' => 'new'
                    ]);

            DB::table('mails')
                ->insert(
                    [
                        'email' => 'zineb.mustafaieva@gmail.com',
                        'subject' => 'Disputing appointment on HOLM',
                        'text' => $textToAdmin,
                        'time_to_send' => Carbon::now(),
                        'status' => 'new'
                    ]); */
        }
      
      
         if($appointment->carer_status_id != self::APPOINTMENT_USER_STATUS_COMPLETED) {
			
         ////Refund charge to purchaser 
		$sql = 'SELECT
			  SUM(a.price_for_purchaser) as total,  MAX(pp.id) as purchaser_id, MAX(pp.first_name) as first_name, MAX(pp.family_name) as family_name, a.booking_id
			FROM appointments a
			JOIN bookings b ON a.booking_id = b.id
			JOIN users p ON b.purchaser_id = p.id
			JOIN purchasers_profiles pp ON pp.id = p.id
			WHERE  a.payout = false AND pp.id = '.$booking->purchaser_id.'  AND a.booking_id = '.$booking->id.'
			GROUP BY a.booking_id';
		$res = DB::select($sql);
		$data = $res[0];
		
		$comment = 'Payment to Purchaser '.$data->first_name.' '.$data->family_name.' ('.$data->purchaser_id.') for appointment '.$appointment->id.' of booking '.$booking->id;
		$stripeCharge = StripeCharge::where('booking_id', $booking->id)->first();

		try {
            if($booking->payment_method == 'credit_card'){
                //Get stripe account of carer
                $stripeCharge = StripeCharge::where('booking_id', $booking->id)->first();
                $res = PaymentTools::createRefund($data->total*100, $stripeCharge->id, $booking->id, $comment);
            } else {
                $res = PaymentTools::createBonusRefund($data->total, $booking->id, $comment);
            }
        } catch (\Exception $ex) {
            return response($this->formatResponse('error', $ex->getMessage()));
        }
		

		//Mark certain appointments as with poyout
		Appointment::where('id', $appointment->id)
		->update(['payout' => 1]);
        $allAppsCancelled = true;

          if ($allAppsCancelled) {
              $booking->status_id = 4;
              $booking->save();
          }
        
		}

        return response(['status' => 'success']);
    }

    public function cancelled(Appointment $appointment){

        $user = Auth::user();
        $booking = $appointment->booking;
        //$email = $user->email;//ALEXX

        $purchaserProfile = PurchasersProfile::find($booking->purchaser_id);
        $carerProfile = CarersProfile::find($booking->carer_id);
        $serviceUser = ServiceUsersProfile::find($booking->service_user_id);

        if($user->isCarer()){
            //If Carer

            $receiver = DB::table('bookings')
                ->leftJoin('users', 'users.id', '=', 'bookings.purchaser_id')
                ->where('bookings.id', $booking->id)
                ->get();
            $email = $receiver[0]->email;

            try {
                $receiver2 = DB::table('bookings')
                    ->leftJoin('service_users_profiles', 'service_users_profiles.id', '=', 'bookings.service_user_id')
                    ->where('bookings.id', $booking->id)
                    ->get();
                $email2 = $receiver2[0]->email;
            } catch(\Exception $e){
                $email2 = false;
            }

            $appointment->status_id = self::APPOINTMENT_STATUS_CANCELLED;

            $purchaser = User::find($purchaserProfile->id);
            if ($appointment->use_register_code == 1) {
                $appointment->use_register_code = 0;
                $appointment->save();

                $purchaser->use_register_code = 1;
                $purchaser->had_use_register_code = 0;
                $purchaser->save();
            }

            if (!$appointment->cancelable) {
                $appointment->carer_status_id =  self::APPOINTMENT_USER_STATUS_REJECTED;
            }

            $message = 'Sorry. ' . $appointment->booking->bookingCarerProfile->full_name . ' has cancelled your appointment for  ' . $appointment->formatted_date_start . ' ' . $appointment->formatted_time_from . '. Please check your account https://holm.care/ for more details. The Holm Team';
            SmsTools::sendSmsToServiceUser($message, $appointment->booking->bookingServiceUser);

            $message = 'Sorry. ' . $appointment->booking->bookingCarerProfile->full_name . ' has cancelled your appointment for  ' . $appointment->formatted_date_start . ' ' . $appointment->formatted_time_from . '. Please check your account https://holm.care/ for more details. The Holm Team';
            SmsTools::sendSmsToPurchaser($message, $appointment->booking->bookingPurchaserProfile);

            //Отправляем письмо пурчейсеру
            $text = view(config('settings.frontTheme') . '.emails.appointment_cancelled')->with([
                'purchaser' => $purchaserProfile, 'booking' => $booking, 'appointment' => $appointment, 'serviceUser' => $serviceUser, 'carer' =>
                    $carerProfile, 'sendTo' => 'purchaser', 'user_like_name' => $purchaserProfile->first_name, 'user_name' => 'user_name'
            ])->render();

            DB::table('mails')
                ->insert(
                    [
                        'email' => $email,
                        'subject' => $carerProfile->first_name.' has cancelled an appointment on Holm',
                        'text' => $text,
                        'time_to_send' => Carbon::now(),
                        'status' => 'new'
                    ]);
            $text = view(config('settings.frontTheme') . '.emails.appointment_cancelled')->with([
                'purchaser' => $purchaserProfile, 'booking' => $booking, 'appointment' => $appointment, 'serviceUser' => $serviceUser, 'carer' =>
                    $carerProfile, 'sendTo' => 'purchaser', 'user_like_name' => $purchaserProfile->first_name, 'user_name' => 'user_name'
            ])->render();
            if($email2) {
                DB::table('mails')
                    ->insert(
                        [
                            'email' => $email2,
                            'subject' => $carerProfile->first_name.' has cancelled an appointment on Holm',
                            'text' => $text,
                            'time_to_send' => Carbon::now(),
                            'status' => 'new'
                        ]);
            }

        }
        elseif($user->isAdmin()){

          //Cancel Appointment By admin
			$appointment->status_id = self::APPOINTMENT_STATUS_CANCELLED;
			$purchaser = User::find($purchaserProfile->id);
			if ($appointment->use_register_code == 1) {
				$appointment->use_register_code = 0;
				$appointment->save();

				$purchaser->use_register_code = 1;
				$purchaser->had_use_register_code = 0;
				$purchaser->save();
			}

			$message = 'Your appointment for ' . $appointment->formatted_date_start . ' ' . $appointment->formatted_time_from . ' has been cancelled. Please check your account https://holm.care/ for more details. The Holm Team';
	
			SmsTools::sendSmsToCarer($message, $appointment->booking->bookingCarerProfile);
            SmsTools::sendSmsToServiceUser($message, $appointment->booking->bookingServiceUser);
            SmsTools::sendSmsToPurchaser($message, $appointment->booking->bookingPurchaserProfile);
            
       } 
       else {
            //Purchaser
            if ($appointment->cancelable) {
                $appointment->status_id = self::APPOINTMENT_STATUS_CANCELLED;

                $purchaser = User::find($purchaserProfile->id);
                if ($appointment->use_register_code == 1) {
                    $appointment->use_register_code = 0;
                    $appointment->save();

                    $purchaser->use_register_code = 1;
                    $purchaser->had_use_register_code = 0;
                    $purchaser->save();
                }

                $message = 'Sorry. ' . $appointment->booking->bookingServiceUser->full_name . ' has cancelled your appointment for  ' . $appointment->formatted_date_start . ' ' . $appointment->formatted_time_from . '. Please check your account https://holm.care/ for more details. The Holm Team';
                SmsTools::sendSmsToCarer($message, $appointment->booking->bookingCarerProfile);
            } elseif ($appointment->carer_status_id == self::APPOINTMENT_USER_STATUS_NEW) {
                $appointment->purchaser_status_id = self::APPOINTMENT_USER_STATUS_REJECTED;
//                $message = 'Sorry. ' . $appointment->booking->bookingServiceUser->full_name . ' has cancelled your appointment for  ' . $appointment->formatted_date_start . ' ' . $appointment->formatted_time_from . '. Please check your account https://holm.care/ for more details. The Holm Team';
//                SmsTools::sendSmsToCarer($message, $appointment->booking->bookingCarerProfile);
            } elseif($appointment->carer_status_id == self::APPOINTMENT_USER_STATUS_COMPLETED) {
                $appointment->status_id = self::APPOINTMENT_STATUS_DISPUTE;
                $appointment->purchaser_status_id = self::APPOINTMENT_USER_STATUS_REJECTED;
                DisputePayout::create([
                    'appointment_id' => $appointment->id,
                ]);
            } elseif ($appointment->carer_status_id == self::APPOINTMENT_USER_STATUS_REJECTED) {
                $appointment->purchaser_status_id = self::APPOINTMENT_USER_STATUS_REJECTED;
                $appointment->status_id = self::APPOINTMENT_STATUS_CANCELLED;

                $purchaser = User::find($purchaserProfile->id);
                if ($appointment->use_register_code == 1) {
                    $appointment->use_register_code = 0;
                    $appointment->save();

                    $purchaser->use_register_code = 1;
                    $purchaser->had_use_register_code = 0;
                    $purchaser->save();
                }

                $message = 'Sorry. ' . $appointment->booking->bookingServiceUser->full_name . ' has cancelled your appointment for  ' . $appointment->formatted_date_start . ' ' . $appointment->formatted_time_from . '. Please check your account https://holm.care/ for more details. The Holm Team';
                SmsTools::sendSmsToCarer($message, $appointment->booking->bookingCarerProfile);
            }



            $receiver = DB::table('bookings')
                ->leftJoin('users', 'users.id', '=', 'bookings.carer_id')
                ->where('bookings.id', $booking->id)
                ->get();
            $email = $receiver[0]->email;

            $text = view(config('settings.frontTheme') . '.emails.appointment_cancelled')->with([
                'purchaser' => $purchaserProfile, 'booking' => $booking, 'appointment' => $appointment, 'serviceUser' => $serviceUser, 'carer' =>
                    $carerProfile, 'sendTo' => 'carer', 'user_like_name' => $carerProfile->first_name
            ])->render();
            DB::table('mails')
                ->insert(
                    [
                        'email' => $email,
                        'subject' => $purchaserProfile->first_name.' has cancelled an appointment on Holm',
                        'text' => $text,
                        'time_to_send' => Carbon::now(),
                        'status' => 'new'
                    ]);
        }

        $appointment->save();

        $apps = Appointment::where('booking_id', $booking->id)->get();
        $allAppsCancelled = true;
        foreach ($apps as $app){
            if ($app->status_id != 5) {// 5 - cancelled
                $allAppsCancelled = false;
                break;
            }
        }
      
      	////Refund charge to purchaser 
			$sql = 'SELECT
                  SUM(a.price_for_purchaser) as total,  MAX(pp.id) as purchaser_id, MAX(pp.first_name) as first_name, MAX(pp.family_name) as family_name, a.booking_id
                FROM appointments a
                JOIN bookings b ON a.booking_id = b.id
                JOIN users p ON b.purchaser_id = p.id
                JOIN purchasers_profiles pp ON pp.id = p.id
                WHERE  a.payout = false AND pp.id = '.$booking->purchaser_id.' AND a.status_id = 5 AND a.booking_id = '.$booking->id.'
                GROUP BY a.booking_id';
			$res = DB::select($sql);
			$data = $res[0];
			
			$comment = 'Payment to Purchaser '.$data->first_name.' '.$data->family_name.' ('.$data->purchaser_id.') for appointment '.$appointment->id.' of booking '.$booking->id;
			$stripeCharge = StripeCharge::where('booking_id', $booking->id)->first();

            
            try {
				if($booking->payment_method == 'credit_card'){
					//Get stripe account of carer
					$stripeCharge = StripeCharge::where('booking_id', $booking->id)->first();
					$res = PaymentTools::createRefund($data->total*100, $stripeCharge->id, $booking->id, $comment);
				} else {
					$res = PaymentTools::createBonusRefund($data->total, $booking->id, $comment);
				}
			} catch (\Exception $ex) {
				return response($this->formatResponse('error', $ex->getMessage()));
			}
            
            //Mark certain appointments as with poyout
			Appointment::where('id', $appointment->id)
            ->update(['payout' => 1]);
        

        if ($allAppsCancelled) {
            $booking->status_id = 4;
            $booking->save();
        }

        return response(['status' => 'success']);
    }

    public function completed(Appointment $appointment)
    {
   
        $user = Auth::user();
        $booking = $appointment->booking;

        $purchaserProfile = PurchasersProfile::find($booking->purchaser_id);
        $carerProfile = CarersProfile::find($booking->carer_id);
        $serviceUser = ServiceUsersProfile::find($booking->service_user_id);

        $purchaser = User::find($purchaserProfile->id);
        $purchaserEmail = $purchaser->email;
        $carer = User::find($carerProfile->id);
        $carerEmail = $carer->email;
        if ($user->isCarer()) {
            //Carer
            if ($appointment->purchaser_status_id == self::APPOINTMENT_USER_STATUS_COMPLETED) {
				$appointment->carer_status_id = self::APPOINTMENT_USER_STATUS_COMPLETED;
                $appointment->status_id = self::APPOINTMENT_STATUS_COMPLETED;
                $appointment->save();

                $purchaser = User::find($purchaserProfile->id);
                if ($appointment->use_register_code == 1) {
                    $purchaser->use_register_code = 0;
                    $purchaser->had_use_register_code = 0;
                    $purchaser->save();
                }

                event(new AppointmentCompletedEvent($appointment));
            } elseif ($appointment->purchaser_status_id == self::APPOINTMENT_USER_STATUS_NEW) {
				$appointment->carer_status_id = self::APPOINTMENT_USER_STATUS_COMPLETED;
                $appointment->carer_status_date = Carbon::now();
                $appointment->status_id = self::APPOINTMENT_STATUS_IN_PROGRESS;  //chnage status to in progress to match automatic agreement condition.
                $appointment->save();
                
             } elseif ($appointment->purchaser_status_id == self::APPOINTMENT_USER_STATUS_REJECTED) {

                $appointment->status_id = self::APPOINTMENT_STATUS_DISPUTE;
                $appointment->carer_status_id = self::APPOINTMENT_USER_STATUS_COMPLETED;
                DisputePayout::create([
                    'appointment_id' => $appointment->id,
                ]);
                $appointment->save();
            }

            if ($appointment->status_id == self::APPOINTMENT_STATUS_DISPUTE) {
                $textToAdmin = view(config('settings.frontTheme') . '.emails.appointment_rejected')->with([
                    'purchaser' => $purchaserProfile, 'booking' => $booking, 'appointment' => $appointment, 'serviceUser' => $serviceUser, 'carer' =>
                        $carerProfile, 'sendTo' => 'admin', 'user_like_name' => 'Nik', 'whoDisputed' => $purchaser
                ])->render();
                DB::table('mails')
                    ->insert(
                        [
                            'email' => 'nik@holm.care',
                            'subject' => 'Disputing appointment on HOLM',
                            'text' => $textToAdmin,
                            'time_to_send' => Carbon::now(),
                            'status' => 'new'
                        ]);

             /*   DB::table('mails')
                    ->insert(
                        [
                            'email' => 'nataliabarladin@gmail.com',
                            'subject' => 'Disputing appointment on HOLM',
                            'text' => $textToAdmin,
                            'time_to_send' => Carbon::now(),
                            'status' => 'new'
                        ]);

                DB::table('mails')
                    ->insert(
                        [
                            'email' => 'zineb.mustafaieva@gmail.com',
                            'subject' => 'Disputing appointment on HOLM',
                            'text' => $textToAdmin,
                            'time_to_send' => Carbon::now(),
                            'status' => 'new'
                        ]); */

                $textToPurchaser = view(config('settings.frontTheme') . '.emails.appointment_rejected')->with([
                    'purchaser' => $purchaserProfile, 'booking' => $booking, 'appointment' => $appointment, 'serviceUser' => $serviceUser, 'carer' =>
                        $carerProfile, 'sendTo' => 'purchaser', 'user_like_name' => $purchaserProfile->like_name, 'whoDisputed' => 'purchaser'
                ])->render();

                DB::table('mails')
                    ->insert(
                        [
                            'email' => $purchaserEmail,
                            'subject' => 'Disputing appointment on HOLM',
                            'text' => $textToPurchaser,
                            'time_to_send' => Carbon::now(),
                            'status' => 'new'
                        ]);

                $textToCarer = view(config('settings.frontTheme') . '.emails.appointment_rejected')->with([
                    'purchaser' => $purchaserProfile, 'booking' => $booking, 'appointment' => $appointment, 'serviceUser' => $serviceUser, 'carer' =>
                        $carerProfile, 'sendTo' => 'carer', 'user_like_name' => $carerProfile->full_name, 'whoDisputed' => 'purchaser'
                ])->render();
                DB::table('mails')
                    ->insert(
                        [
                            'email' => $carerEmail,
                            'subject' => 'Disputing appointment on HOLM',
                            'text' => $textToCarer,
                            'time_to_send' => Carbon::now(),
                            'status' => 'new'
                        ]);
            }
        } else {
            if ($appointment->carer_status_id == self::APPOINTMENT_USER_STATUS_COMPLETED) {
                $appointment->status_id = self::APPOINTMENT_STATUS_COMPLETED;
                $appointment->purchaser_status_id = self::APPOINTMENT_USER_STATUS_COMPLETED;
                $appointment->save();

                if ($appointment->use_register_code == 1) {
                    $purchaser->use_register_code = 0;
                    $purchaser->had_use_register_code = 0;
                    $purchaser->save();
                }

                event(new AppointmentCompletedEvent($appointment));
            } elseif ($appointment->carer_status_id == self::APPOINTMENT_USER_STATUS_NEW) {
                $appointment->purchaser_status_id = self::APPOINTMENT_USER_STATUS_COMPLETED;
                $appointment->save();
            } elseif ($appointment->carer_status_id == self::APPOINTMENT_USER_STATUS_REJECTED) {
                $appointment->status_id = self::APPOINTMENT_STATUS_DISPUTE;
                $appointment->purchaser_status_id = self::APPOINTMENT_USER_STATUS_COMPLETED;
                DisputePayout::create([
                    'appointment_id' => $appointment->id,
                ]);
                $appointment->save();
            }

            if ($appointment->status_id == self::APPOINTMENT_STATUS_DISPUTE) {
                $textToAdmin = view(config('settings.frontTheme') . '.emails.appointment_rejected')->with([
                    'purchaser' => $purchaserProfile, 'booking' => $booking, 'appointment' => $appointment, 'serviceUser' => $serviceUser, 'carer' =>
                        $carerProfile, 'sendTo' => 'admin', 'user_like_name' => 'Nik', 'whoDisputed' => $carer
                ])->render();
                DB::table('mails')
                    ->insert(
                        [
                            'email' => 'nik@holm.care',
                            'subject' => 'Disputing appointment on HOLM',
                            'text' => $textToAdmin,
                            'time_to_send' => Carbon::now(),
                            'status' => 'new'
                        ]);

             /*   DB::table('mails')
                    ->insert(
                        [
                            'email' => 'nataliabarladin@gmail.com',
                            'subject' => 'Disputing appointment on HOLM',
                            'text' => $textToAdmin,
                            'time_to_send' => Carbon::now(),
                            'status' => 'new'
                        ]);

                DB::table('mails')
                    ->insert(
                        [
                            'email' => 'zineb.mustafaieva@gmail.com',
                            'subject' => 'Disputing appointment on HOLM',
                            'text' => $textToAdmin,
                            'time_to_send' => Carbon::now(),
                            'status' => 'new'
                        ]); */

                $textToPurchaser = view(config('settings.frontTheme') . '.emails.appointment_rejected')->with([
                    'purchaser' => $purchaserProfile, 'booking' => $booking, 'appointment' => $appointment, 'serviceUser' => $serviceUser, 'carer' =>
                        $carerProfile, 'sendTo' => 'purchaser', 'user_like_name' => $purchaserProfile->like_name, 'whoDisputed' => 'carer'
                ])->render();

                DB::table('mails')
                    ->insert(
                        [
                            'email' => $purchaserEmail,
                            'subject' => 'Disputing appointment on HOLM',
                            'text' => $textToPurchaser,
                            'time_to_send' => Carbon::now(),
                            'status' => 'new'
                        ]);

                $textToCarer = view(config('settings.frontTheme') . '.emails.appointment_rejected')->with([
                    'purchaser' => $purchaserProfile, 'booking' => $booking, 'appointment' => $appointment, 'serviceUser' => $serviceUser, 'carer' =>
                        $carerProfile, 'sendTo' => 'carer', 'user_like_name' => $carerProfile->full_name, 'whoDisputed' => 'carer'
                ])->render();
                DB::table('mails')
                    ->insert(
                        [
                            'email' => $carerEmail,
                            'subject' => 'Disputing appointment on HOLM',
                            'text' => $textToCarer,
                            'time_to_send' => Carbon::now(),
                            'status' => 'new'
                        ]);
            }
        }

        return response(['status' => 'success']);
    }

    public function leaveReviewPage(Appointment $appointment)
    {
        $this->template = config('settings.frontTheme') . '.templates.carerPrivateProfile';
        $this->title = 'Holm Care';

        $booking = Booking::find($appointment->booking_id);


        $header = view(config('settings.frontTheme') . '.headers.baseHeader')->render();
        $footer = view(config('settings.frontTheme') . '.footers.baseFooter')->render();
        $modals = view(config('settings.frontTheme') . '.includes.modals')->render();

        $this->vars = array_add($this->vars, 'booking', $booking);
        $this->vars = array_add($this->vars, 'header', $header);
        $this->vars = array_add($this->vars, 'footer', $footer);
        $this->vars = array_add($this->vars, 'modals', $modals);

        $this->vars = array_add($this->vars, 'user', $this->user);
        $this->vars = array_add($this->vars, 'appointment', $appointment);
        
         //check if already added review 
	     $res = DB::table('appointments_overviews')
	         ->where('appointment_id', $appointment->id)
	        ->count();

		$this->vars = array_add($this->vars, 'review', $res);
		
        $this->content = view(config('settings.frontTheme') . '.appointment.leave_review')->with($this->vars)
            ->render();

        return $this->renderOutput();
    }

    public function createReview(Appointment $appointment, Request $request)
    {
		
		//check if already added review 
	     $res = DB::table('appointments_overviews')
	         ->where('appointment_id', $appointment->id)
	        ->count();
		
		if($res){
		  return redirect()->back()->with('leavemessage', 'You have already added review for this appointment.');
		}
		
		
        AppointmentOverview::create([
            'appointment_id' => $appointment->id,
            'punctuality' => $request->punctuality,
            'friendliness' => $request->friendliness,
            'communication' => $request->communication,
            'performance' => $request->performance,
            'comment' => $request->comment,
        ]);
        $booking = Booking::find($appointment->booking_id);
        $server_users = ServiceUsersProfile::find($booking->service_user_id);
        $carer_users = CarersProfile::find($booking->carer_id);
        $text = view(config('settings.frontTheme') . '.emails.new_review')->with([
            'server_users' => $server_users,
            'carer_users' => $carer_users,
        ])->render();

        DB::table('mails')
            ->insert(
                [
                    'email' => 'nik@holm.care',
                    'subject' => 'You have a new review moderation',
                    'text' => $text,
                    'time_to_send' => Carbon::now(),
                    'status' => 'new'
                ]);
      /*  DB::table('mails')
            ->insert(
                [
                    'email' => 'nataliabarladin@gmail.com',
                    'subject' => 'You have a new review moderation',
                    'text' => $text,
                    'time_to_send' => Carbon::now(),
                    'status' => 'new'
                ]); */
        return redirect()->route('viewBookingDetails', [$appointment->booking_id]);
    }

    public function completeBookingIfAppointmentsCompleted($appointment_id){
        $appoint = Appointment::find($appointment_id);
        $booking_id = $appoint->booking_id;
        $appointments = Appointment::where('booking_id', $booking_id)->get();
        $bookingCompleted = true;
        $carerCompletedBooking = true;
        $purchaserCompletedBooking = true;
        foreach($appointments as $appointment){
            if($appointment->status_id != 4 && $appointment->status_id != 5) $bookingCompleted = false;
            if($appointment->carer_status_id != 2 && $carerCompletedBooking) $carerCompletedBooking = false;
            if($appointment->purchaser_status_id != 2 && $purchaserCompletedBooking) $purchaserCompletedBooking = false;
        }
        $booking = Booking::find($booking_id);
        if(Auth::user()->isCarer() && $carerCompletedBooking){
            $booking->carer_status_id = 7;//completed
            if($booking->purchaser_status_id == 7){
                $booking->status_id = 7;//completed
            }
        } elseif(!Auth::user()->isCarer() && $purchaserCompletedBooking){
            $booking->purchaser_status_id = 7;//completed
            if($booking->carer_status_id == 7){
                $booking->status_id = 7;//completed
            }
        }
        $booking->save();
        if($bookingCompleted) {
            return response(['status' => 'completed']);
        } else {
            return response(['status' => 'uncompleted']);
        }
    }
}
