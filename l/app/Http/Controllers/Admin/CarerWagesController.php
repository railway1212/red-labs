<?php

namespace App\Http\Controllers\Admin;

use App\CarersProfile;
use App\CarerWages;
use DB;
use Illuminate\Support\Facades\Config;
use Illuminate\Http\Request;
use App\Fees;

use Symfony\Component\HttpFoundation\JsonResponse;

class CarerWagesController extends AdminController
{
    public function __construct()
    {
        parent::__construct();
        $this->template = config('settings.theme') . '.templates.adminBase';
    }

    public function index(Request $request, CarersProfile $wages)
    {
        $filter = $request['userName'];
        $model = $wages;
        if(null !== $filter) {
            if(substr_count($filter, ' ') + 1 > 1) {
                $full_name = explode(' ', $filter);
                $data = $wages->select('*')->where('profiles_status_id','=',2)
                    ->where(function ($q) use ($full_name) {
                        $q->whereIn('first_name', $full_name);
                        $q->whereIn('family_name', $full_name);
                    })->paginate(Config::get('settings.AdminUserPagination'));
            } else {
                $data = $wages->select('*')->where('profiles_status_id','=',2)
                    ->where(function ($q) use ($filter) {
                        $q->where('id', $filter)->orWhere('first_name', 'like', $filter . '%')->orWhere('family_name', 'like', $filter . '%');
                    })->paginate(Config::get('settings.AdminUserPagination'));
            }
            $this->vars['carers'] = $data;
        } else {
            $data = $model::with('CarerWages')->select('*')->where('profiles_status_id','=',2)->paginate(Config::get('settings.AdminUserPagination'));
            $this->vars['carers'] = $data;
        }
        //$data = CarersProfile::with('CarerWages')->get();
        $this->content = view(config('settings.theme') . '.CarerWages')->with($this->vars)->render();

        return $this->renderOutput();
    }

    public function update(Request $request,CarerWages $wages)
    {
        //dd($request->all());
        $id = $request->get('carer_id');
        $rate = $request->get('hour_rate');
        $wages=$wages->firstOrNew(['carer_id'=>$id]);
        $wages->carer_id=$id;
        $wages->hour_rate=$rate;
        $wages->save();

        return  new JsonResponse(['status'=>'ok']);

    }

    public function filterCarerWages($carerId, $filter){
        if($filter == null) return;//Если фильтр не задан, не продолжаем
        $data = CarerWages::where('profiles_status_id',2)->
            where('id', $carerId)->
            where(function ($q) use ($filter) {
                $q->where('id', $filter)->orWhere('first_name', $filter)->orWhere('family_name', $filter);
            })->toSql();
        return $data;
        if (Request::ajax()) {
            return Response::json(View::make('carerWagesTableBody', array('data' => $data))->render());
        }
        return View::make('carerWagesTableBody', array('data' => $data));

    }
  
  	public function create(){
      	$carerProfileArr 						=	[];
      	$carerProfileArr[0]['id'] 			=	"";
      	$carerProfileArr[0]['name'] 			=	"Select Cares";
      	$carerProfile 							=	CarersProfile::with('user')
                                                      ->where('profiles_status_id','=',2)
                                                      ->get();
      if(is_object($carerProfile)){
      	foreach($carerProfile as $key => $cares){
        	$carerProfileArr[$key+1]['id'] 		=	$cares->id;
            $carerProfileArr[$key+1]['name'] 		=	$cares->first_name." ".$cares->family_name." (".$cares->like_name.")";
        }
      }
      
     	//echo "<pre>"; print_r($carerProfileArr); echo "</pre>"; exit;
      	$data 						= 	Fees::all();
        $this->vars['fees']			=	$data;
      	$this->vars['carersData']	=	$carerProfileArr;
    	$this->content = view(config('settings.theme') . '.CarerWagesAdd')->with($this->vars)->render();

        return $this->renderOutput();
    }
  	public function save(Request $request){
    	echo "<pre>"; print_r($request->all()); echo "<pre>"; exit;
    }
}
