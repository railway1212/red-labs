<?php

namespace App\Http\Controllers\Admin\Contact;
use App\Http\Controllers\Admin\AdminController;

use App\Contact;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use DB;


class ContactController extends AdminController
{
    private $trust;

    public function __construct(Contact $contact) {
        parent::__construct();
        $this->contact = $contact;

        $this->template = config('settings.theme').'.templates.adminBase';
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
      
        $this->title = 'Admin Contact';
        $contactInfo = Contact::orderBy('id', 'desc')->get();
        $contact = array();
        if(count($contactInfo) > 0){
            foreach($contactInfo as $contactData){
                    $contact[$contactData->data_key] = $contactData->data_val;
            }
        }

        $this->vars = array_add($this->vars,'contact',$contact);
        $this->content = view(config('settings.theme').'.contact.contact')->with($this->vars)->render();

        return $this->renderOutput();
    }

   

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $itemreq = $request;
        $download_file_name = "";
        if( $itemreq->hasFile('home_image')){ 
                $image = $itemreq->file('home_image'); 
                $image_path=base_path('public/image_Association');
                $fileName = $image->getClientOriginalName();
                $fileExtension = $image->getClientOriginalExtension();
                $download_file_name= time().'.'.$fileExtension;
                $image->move($image_path,$download_file_name);

                if($download_file_name){
                    DB::update('update contacts set data_val = ? where data_key = ?',[$download_file_name,'home_image']);
                }
                
        }
        $request 	=	Input::all();

        //dd($request);

            if(count($request)>0){
                foreach($request as $key => $val){ 
                    $findKey = DB::table('contacts')->where('data_key', $key)->first();
                    if($findKey && $key!='home_image' ){
                        DB::update('update contacts set data_val = ? where data_key = ?',[$val,$key]);
                    }else{
                        DB::table('contacts')->insert([
                               'data_key' => $key,
                               'data_val' => $val
                        ]);
                    }
                }
            }
          
            return redirect()->back(); 
    }

    
}
