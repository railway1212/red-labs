<?php

namespace App\Http\Controllers\Admin\Trust;
use App\Http\Controllers\Admin\AdminController;

use App\Trust;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;


class TrustController extends AdminController
{
    private $trust;

    public function __construct(Trust $trust) {
        parent::__construct();
        $this->trust = $trust;

        $this->template = config('settings.theme').'.templates.adminBase';
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->title = 'Admin Trusts';
        $trusts = Trust::orderBy('id', 'desc')->get();
        $this->vars = array_add($this->vars,'trusts',$trusts);
        $this->content = view(config('settings.theme').'.trust.trust')->with($this->vars)->render();

        return $this->renderOutput();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->title = 'Admin Trusts';
        $this->content = view(config('settings.theme') . '.trust.create')->with($this->vars)->render();

        return $this->renderOutput();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required'
         
        ]);

        $trust = new Trust;
        $trust->title = $request->title;
        $trust->url = $request->url;
        $trust->image_alt = $request->image_alt;
        $trust->status = 'published';
      	$itemreq = $request;
      	$download_file_name = "";
          	 if( $itemreq->hasFile('trustImage')){ 
                $image = $itemreq->file('trustImage'); 
                $image_path=base_path('public/image_Association');
                $fileName = $image->getClientOriginalName();
                $fileExtension = $image->getClientOriginalExtension();
                $download_file_name=time().'.'.$fileExtension;
                $image->move($image_path,$download_file_name);
                $trust->image_path = $download_file_name;
            }
        $trust->save();
        return redirect(route('trust.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($trustId)
    {
        $this->title = 'Admin Trusts';
        $trusts = $this->trust->findOrFail($trustId);
        $this->vars = array_add($this->vars,'trust',$trusts);
        $this->content = view(config('settings.theme').'.trust.edit')->with($this->vars)->render();
        return $this->renderOutput();

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
		if (isset($request->delete_trust_id)) {
            $trust = Trust::find($request->delete_trust_id);
            $trust->delete();
            return response(['status' => 'success']);
        }elseif($request->publish_trust_id){
			 $trust = Trust::find($request->publish_trust_id);
			 $trust->status = 'published';	
			 $trust->save();
            return response(['status' => 'success']);	
		}else{
           
      		$download_file_name = "";
            $trust = Trust::find($id);
            $trust->title = $request->title;
            $trust->url = $request->url;
            $trust->image_alt = $request->image_alt;
            //$trust->image_path = $request->image;
            $trust->status = 'published';
             $itemreq = $request;
          	 if( $itemreq->hasFile('trustImage')){ 
                $image = $itemreq->file('trustImage'); 
                $image_path=base_path('public/image_Association');
                $fileName = $image->getClientOriginalName();
                $fileExtension = $image->getClientOriginalExtension();
                $download_file_name=time().'.'.$fileExtension;
                $image->move($image_path,$download_file_name);
                $trust->image_path = $download_file_name;
            }
            $trust->save();
            return redirect(route('trust.index'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
