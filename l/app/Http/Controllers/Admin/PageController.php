<?php

namespace App\Http\Controllers\Admin\Page;

use App\Http\Controllers\Admin\AdminController;

use App\Page;
use App\Tag;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DB;
use App\Contact;

class PageController extends AdminController
{
    private $page;

    public function __construct(Page $page) {
        parent::__construct();
        $this->page = $page;

        $this->template = config('settings.theme').'.templates.adminBase';
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $this->title = 'Admin Pages';
        $pages = Page::orderBy('created_at', 'desc')->get();
        $this->vars = array_add($this->vars,'pages',$pages);

        $this->content = view(config('settings.theme').'.Page.page')->with($this->vars)->render();

        return $this->renderOutput();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->title = 'Admin Pages';
        $this->content = view(config('settings.theme') . '.Page.create')->with($this->vars)->render();

        return $this->renderOutput();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
		
		//~ dd($request->all());

        $this->validate($request, [
            'title' => 'required',
           // 'content' => 'required',
            'slug' => 'required',
        ]);

        $page = new Page;
        $page->title = $request->title;
        $page->content = $request->content;
        $page->slug = $request->slug;
        $page->meta_title = $request->meta_title;
        $page->meta_keyword = $request->meta_keywords;
        $page->meta_description = $request->meta_description;
        $page->status = 'published';
        $page->published_at = null;
        $page->save();
        return redirect(route('page.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($pageId)
    {
        $this->title = 'Admin Pages';
        $pages = $this->page->findOrFail($pageId);
        $this->vars = array_add($this->vars,'page',$pages);
        $contactInfo = Contact::orderBy('id', 'desc')->get();
        $contact = array();
        if(count($contactInfo) > 0){
            foreach($contactInfo as $contactData){
                    $contact[$contactData->data_key] = $contactData->data_val;
            }
        }
        $this->vars = array_add($this->vars,'contact',$contact);
        $this->content = view(config('settings.theme').'.Page.edit')->with($this->vars)->render();
        return $this->renderOutput();

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
		if (isset($request->delete_page_id)) {
            $page = Page::find($request->delete_page_id);
            $page->status = 'deleted';
            $page->save();
            return response(['status' => 'success']);
        } else {
		
            $page = Page::find($id);
            $page->title = $request->title;
            $page->content = $request->content;
            $page->slug = $request->slug;
            $page->meta_title = $request->meta_title;
			$page->meta_keyword = $request->meta_keywords;
			$page->meta_description = $request->meta_description;
            $page->status = 'published';
            $page->published_at= Carbon::now();
            if($request->extra_image || $request->extra_text){
                $count = 0;
                $extraContent = array();
                $blockImages = $request->extra_image;
                $blockImageAltTexts  = $request->image_alt;
                $blockContents  = $request->extra_text;
                $pageContent = $this->page->findOrFail($id);
                $oldExtraContent = unserialize($pageContent->extra_content);
                if($blockContents){
                    foreach($blockContents as $blockContentKey => $blockContentVal ){

                        if(isset($blockImages[$count])){
                            $blockImage = $blockImages[$blockContentKey];
                            $image_path=base_path('public/image_Association');
                            $fileName = $blockImage->getClientOriginalName();
                            $fileExtension = $blockImage->getClientOriginalExtension();
                            $downloadFileName = $fileName.time().'.'.$fileExtension;
                            $blockImage->move($image_path,$downloadFileName);

                        }else{
                            $downloadFileName = $oldExtraContent[$blockContentKey]['image'];
                        }
                        
                        if(!empty($blockContents[$blockContentKey])){
                            $extraContent[]=array(
                                'image' => $downloadFileName,
                                'content' => $blockContents[$blockContentKey],
                                'image_alt' => $blockImageAltTexts[$blockContentKey]
                            ); 
                        } 
                    $count++;
                    }
                }

                $page->extra_content = serialize($extraContent);
            }
            $page->save();

            $itemreq = $request;
            $download_file_name = "";
            if( $itemreq->hasFile('home_image')){ 
                    $image = $itemreq->file('home_image'); 
                    $image_path=base_path('public/image_Association');
                    $fileName = $image->getClientOriginalName();
                    $fileExtension = $image->getClientOriginalExtension();
                    $download_file_name= time().'.'.$fileExtension;
                    $image->move($image_path,$download_file_name);
    
                    if($download_file_name){
                        DB::update('update contacts set data_val = ? where data_key = ?',[$download_file_name,'home_image']);
                    }
                    
            }
            
            if($request->home_contact)
            {
                DB::update('update contacts set data_val = ? where data_key = ?',[$request->home_contact,'home_contact']);
            }
            if($request->our_mission)
            {
                DB::update('update contacts set data_val = ? where data_key = ?',[$request->our_mission,'our_mission']);
            }
            if($request->founder)
            {
                DB::update('update contacts set data_val = ? where data_key = ?',[$request->founder,'founder']);
            }
            
    


            return redirect(route('page.index'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

