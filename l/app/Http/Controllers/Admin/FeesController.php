<?php

namespace App\Http\Controllers\Admin;

use App\Fees;
use PaymentTools;
use DB;
use Illuminate\Http\Request;

class FeesController extends AdminController
{
    public function __construct()
    {
        parent::__construct();
        $this->template = config('settings.theme') . '.templates.adminBase';
    }

    public function index(){

        $data = Fees::all();
        $this->vars['fees']=$data;
        $this->content = view(config('settings.theme').'.fees')->with( $this->vars)->render();

        return $this->renderOutput();
    }

    public function update(Request $request){
        // echo "<pre>"; print_r($request->all()); echo "</pre>"; exit;
        if(isset($request['fees']) && is_array($request['fees']) && count($request['fees']) > 0){
            foreach ($request['fees'] as $key => $item) {
				if($item['type_carer'] == 'flat'){
                	$purchaser_rate 		=	$item['carer_rate']+$item['amount'];
                    $type_flat 				=	1;
                    $type_percent 			=	0;
                }else if ($item['type_carer'] == 'percent'){
                  	$percentageVal 			=	$item['carer_rate']*($item['amount']/100);
                	$purchaser_rate 		=	$item['carer_rate']+$percentageVal;
                  	$type_percent 			=	1;
					$type_flat 				=	0;
                }
              	//echo $purchaser_rate."+++".$key."<br>"; 
               $fees					=	Fees::find($item['id']);
               $fees->amount           	=   $item['amount'];
               $fees->carer_rate       	=   $item['carer_rate'];
               $fees->type_flat       	=   $type_flat;
               $fees->type_percent      =   $type_percent;
               $fees->purchaser_rate   	=   $purchaser_rate;
               $fees->save();
            }
        }
        $data = Fees::all();
        $this->vars['fees']=$data;
        $this->content = view(config('settings.theme').'.fees')->with( $this->vars)->render();

        return $this->renderOutput();
    }
}
