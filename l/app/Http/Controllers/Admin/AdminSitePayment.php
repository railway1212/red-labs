<?php

namespace App\Http\Controllers\Admin;

use App\Appointment;
use App\Booking;
use App\DisputePayment;
use App\DisputePayout;
use App\Http\Controllers\Admin\AdminController;
use App\PayoutToPurchaser;
use App\StripeCharge;
use App\StripeConnectedAccount;
use App\StripeRefund;
use App\StripeTransfer;
use App\Transaction;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use DB;
use PaymentTools;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Input;


class AdminSitePayment extends AdminController
{
    public function __construct()
    {
        parent::__construct();
        $this->template = config('settings.theme') . '.templates.adminBase';
    }

    public function getPayoutsToCarers(Request $request) {
       // echo "<pre>"; print_r($request->all()); echo "<pre>"; exit;
        $this->title = 'Admin | Booking Payouts To Carers';
        $transfers = StripeTransfer::all();
        $userName = $request->get('userName',false);
        $potentialPayouts = $this->getPotentialPayoutsForCarer($userName);
        $potentialPayouts = collect($potentialPayouts);
   
       //echo "<pre>"; print_r($userName); echo "<pre>"; exit;
        $this->vars['transfers'] = $transfers;
        if(is_numeric($request['userName'])) {
            $this->vars['transfers'] = $transfers->filter(function($item)use($request){
             // echo "<pre>"; print_r($item); echo "<pre>"; //exit;
				if(isset($item->booking_id) && $item->booking_id > 0){ 
					return ( $item->booking->carer_id === (int)$request['userName'] );
				}elseif(isset($item->bonus_id) && $item->bonus_id > 0){
					$bonusPayout = \App\BonusPayout::where('id', $item->bonus_id)->pluck('user_id')->toArray();
					return ( $bonusPayout[0] === (int)$request['userName'] );
				}

            });
        }elseif($request['userName']!=""){
			  $this->vars['transfers'] = $transfers->filter(function($item)use($request){
				if($item->bonus_id > 0){
					$bonusPayout = \App\BonusPayout::where('id', $item->bonus_id)->pluck('user_id')->toArray();
					$sql="SELECT * FROM carers_profiles where id = ".$bonusPayout[0];
					$bonusUser = DB::select($sql);
					
					return ( strpos(strtoupper($bonusUser[0]->first_name),strtoupper($request['userName']))!==false ||  strpos(strtoupper($bonusUser[0]->family_name),strtoupper($request['userName']))!==false );
				}	
            });
		}

        $all = collect();
        $all = $all->merge($potentialPayouts);
        $all = $all->merge($this->vars['transfers']);

        //dd($all);

//        dd($transfers);
//        $merged = $potentialPayouts->merge($transfers);
//        $merged = $potentialPayouts->push($transfers);

        $page = $request->get('page', 1);
        $perPage = 10;
        $start = ($page - 1) * $perPage;
        if ($page == 1) $start = 0;
        $count = count($all);
//        dd($potentialPayouts);
//        dd($count);

//        dd($merged);
//        dd($transfers);
//        $count = count($potentialPayouts);
//        dd($count);
        if ($count > 0)
            $pages = ceil($count / $perPage);
        else
            $pages = 0;
        $nextPage = $page + 1;
        $previousPage = $page - 1;
        // --------- pagination -----------
//        $potentialPayouts = array_slice($potentialPayouts, $start, $perPage);
        $all = $all->slice($start, $perPage);
        $this->vars = array_add($this->vars, 'nextPage', $nextPage);
        $this->vars = array_add($this->vars, 'previousPage', $previousPage);
        $this->vars = array_add($this->vars, 'count', $count);
        $this->vars = array_add($this->vars, 'curr_page', $page);
        $this->vars = array_add($this->vars, 'pages', $pages);

        $this->vars = array_add($this->vars, 'link', '/l/admin/carer-payout');
        $query = $request->all();
        if(key_exists('page',$query)) unset($query['page']);
        //dd($query,http_build_query($query));
        $this->vars = array_add($this->vars, 'queryString', http_build_query($query));
        $pagination = view(config('settings.theme') . '.pagination2')->with($this->vars)->render();
        $this->vars = array_add($this->vars, 'pagination', $pagination);
        $this->vars = array_add($this->vars, 'potentialPayouts', $potentialPayouts);
        $this->vars = array_add($this->vars, 'all', $all);
//        $this->vars['pagination'] = $pagination;
//        $this->vars['potentialPayouts'] = $potentialPayouts;

        $this->content = view(config('settings.theme').'.carerPayouts.carerPayouts')->with($this->vars)->render();

        return $this->renderOutput();
    }

    public function getPayoutsToPurchasers(Request $request) {
        $this->title = 'Admin | Booking Payouts To Purchasers';
        $userName = $request->get('userName',false);
        $payoutsToPurchasers = PayoutToPurchaser::all();
        if($userName)
        $payoutsToPurchasers = $payoutsToPurchasers->filter(function($item)use($userName){
            $name = $item->booking->bookingCarerProfile->full_name;
            if(strpos(strtoupper($name),strtoupper($userName))!==false)
                return true;
        });


        $potentialPayouts = $this->getPotentialPayoutsForPurchasers($userName);
        $this->vars['payoutsToPurchasers'] = $payoutsToPurchasers;
        $this->vars['potentialPayouts'] = $potentialPayouts;


        $this->content = view(config('settings.theme').'.purchaserPayouts.purchaserPayouts')->with($this->vars)->render();

        return $this->renderOutput();
    }

    public function getBookingTransactions(Request $request,Transaction $transaction){

        $this->title = 'Admin | Booking Transactions Management';
        $input = $request->all();
        //dd($input);
        $model = $transaction;
        $transactions = $model->select('*')->orderByDesc('id');
        
        
       

        //var_dump($input['userName']);
        if(isset($input['userName']) && !empty($input['userName'])){
		   	$userName = $input['userName'];
		   	
		   	$filterTransaction = Transaction::orderBy('id', 'desc')->get();
		   	$filterTransaction = $filterTransaction->filter(function($item) use ($userName) {
			  if(is_numeric($userName)){
				  return( $item->booking->bookingPurchaser->id == $userName ||
				    $item->booking->bookingCarer->id == $userName ||
				    $item->booking->id == $userName ||
				    $item->id == $userName
				  );
				  
			  }else{
				  
				return( (stripos($item->booking->bookingCarerProfile->full_name, $userName) !== false) ||
				    (stripos($item->booking->bookingPurchaserProfile->full_name, $userName) !== false)
				  );    
			  }	
			});
			
			
			$filterTransaction = $filterTransaction->pluck('id')->toArray();

			if(!empty($filterTransaction)){
				
				 $transactions = $transactions->whereIn('id',$filterTransaction);
			}
			

       }
        elseif(isset($input['daterange']) && isset($input['TransactionsSort'])){
            $date = explode(' - ',$input['daterange']);
            $transactions = $transactions->where('created_at','>=', Carbon::createFromFormat('d/m/y', $date[0])->startOfDay()->toDateTimeString())
                ->where('created_at','<=', Carbon::createFromFormat('d/m/y', $date[1])->endOfDay()->toDateTimeString())
                ->where('payment_method','=',$input['TransactionsSort']);
        }
        elseif(isset($input['daterange'])){
            $date = explode(' - ',$input['daterange']);
            $transactions = $transactions->where('created_at','>=', Carbon::createFromFormat('d/m/y', $date[0])->startOfDay()->toDateTimeString())
                ->where('created_at','<=', Carbon::createFromFormat('d/m/y', $date[1])->endOfDay()->toDateTimeString());
        }
        elseif(isset($input['TransactionsSort'])){
            $transactions = $transactions->where('payment_method','=',$input['TransactionsSort']);
        }
        

        $transactions = $transactions->paginate(10);

        $this->vars['transactions'] = $transactions;
        $this->vars['TransactionsSort'] = $request->get('TransactionsSort',null);

        $this->content = view(config('settings.theme') . '.bookingTransactions.bookingTransactions')->with($this->vars)->render();

        return $this->renderOutput();
    }

    private function getPotentialPayoutsForPurchasers($userName){
        $where ='';
        if($userName) $where =" and concat(first_name,' ',family_name) like '%".$userName."%'";

        $sql = 'SELECT
                  SUM(a.price_for_purchaser) as total,  MAX(pp.id) as purchaser_id, MAX(pp.first_name) as first_name, MAX(pp.family_name) as family_name, a.booking_id
                FROM appointments a
                JOIN bookings b ON a.booking_id = b.id
                JOIN users p ON b.purchaser_id = p.id
                JOIN purchasers_profiles pp ON pp.id = p.id
                WHERE  a.payout = false AND a.status_id = 5'.$where.'
                GROUP BY a.booking_id';
        $res = DB::select($sql);
        return $res;
    }

    private function getPotentialPayoutsForCarer($userName=false){
      	$where ='';
        if($userName) $where =" and concat(first_name,' ',family_name) like '%".$userName."%'";
    	if(is_numeric($userName)) $where =" and ( c.id like $userName OR a.id like $userName )";
        $sql = 'SELECT
                  SUM(a.price_for_carer) as total,  MAX(cp.id) as carer_id, MAX(cp.first_name) as first_name, MAX(cp.family_name) as family_name, a.booking_id, a.id
                FROM appointments a
                JOIN bookings b ON a.booking_id = b.id
                JOIN users c ON b.carer_id = c.id
                JOIN carers_profiles cp ON cp.id = c.id
                WHERE a.payout = false AND  a.status_id = 4  ' . $where . '
                GROUP BY a.id,a.booking_id'; 
     	$res = DB::select($sql);
       return $res;
    }

    public function makePayoutToPurchaser(Booking $booking){
        $sql = 'SELECT
                  SUM(a.price_for_purchaser) as total,  MAX(pp.id) as purchaser_id, MAX(pp.first_name) as first_name, MAX(pp.family_name) as family_name, a.booking_id
                FROM appointments a
                JOIN bookings b ON a.booking_id = b.id
                JOIN users p ON b.purchaser_id = p.id
                JOIN purchasers_profiles pp ON pp.id = p.id
                WHERE  a.payout = false AND pp.id = '.$booking->purchaser_id.' AND a.status_id = 5 AND a.booking_id = '.$booking->id.'
                GROUP BY a.booking_id';
        $res = DB::select($sql);
        $data = $res[0];
		$appointments = Appointment::where('booking_id', $booking->id)->where('status_id', 5)->where('payout', 0)->get();
        $appointmentsIds = implode(',', $appointments->pluck('id')->toArray());
        $comment = 'Payment to Purchaser '.$data->first_name.' '.$data->family_name.' ('.$data->purchaser_id.') for appointments '.$appointmentsIds.' in booking '.$booking->id;

        try {
            if($booking->payment_method == 'credit_card'){
                //Get stripe account of carer
                $stripeCharge = StripeCharge::where('booking_id', $booking->id)->first();
                $res = PaymentTools::createRefund($data->total*100, $stripeCharge->id, $booking->id, $comment);
            } else {
                $res = PaymentTools::createBonusRefund($data->total, $booking->id, $comment);
            }
        } catch (\Exception $ex) {
            return response($this->formatResponse('error', $ex->getMessage()));
        }

        //Mark certain appointments as with poyout
        Appointment::where('booking_id', $booking->id)
            ->where('booking_id', $booking->id)
            ->where('status_id', 5)
            ->where('payout', 0)
            ->update(['payout' => 1]);

        return response(['status' => 'success']);
    }

    /*public function makePayoutToCarer(Booking $booking){
		//echo "<pre>"; print_r($booking->all()); echo "<pre>"; exit;
        $sql = 'SELECT
                  SUM(a.price_for_carer) as total,  MAX(cp.id) as carer_id, MAX(cp.first_name) as first_name, MAX(cp.family_name) as family_name
                FROM appointments a
                JOIN bookings b ON a.booking_id = b.id
                JOIN users c ON b.carer_id = c.id
                JOIN carers_profiles cp ON cp.id = c.id
                WHERE  a.payout = false AND cp.id = '.$booking->carer_id.' AND a.status_id = 4
                GROUP BY a.booking_id';
        $res = DB::select($sql);
		//echo "<pre>"; print_r($res); echo "<pre>"; 
      
        $data = $res[0];

        //Get stripe account of carer
        $connectedAccount = StripeConnectedAccount::where('carer_id', $data->carer_id)->orderBy('created_at', 'desc')->first();
        
        //Transfer money to carer
        $appointments = Appointment::where('booking_id', $booking->id)->where('status_id', 4)->where('payout', 0)->get();
        $appointmentsIds = implode(',', $appointments->pluck('id')->toArray());
		//echo "<pre>Booking ID : "; print_r($appointmentsIds); echo "<pre>"; exit;exit;
        try {
            $res = PaymentTools::createTransfer($connectedAccount->id, $booking->id, $data->total * 100, 'Payment to Carer ' . $data->first_name . ' ' . $data->family_name . ' (' . $data->carer_id . ') for appointments ' . $appointmentsIds . ' in booking ' . $booking->id);
        } catch (\Exception $ex) {
            return response($this->formatResponse('error', $ex->getMessage()));
        }
        //Mark certain appointments as with poyout
        Appointment::where('booking_id', $booking->id)->where('status_id', 4)->where('payout', 0)->update(['payout' => 1]);

        return response(['status' => 'success']);
    } */
	public function makePayoutToCarer($appointment_id = 0){
          //echo "<pre>"; print_r($appointment_id); echo "<pre>"; exit;
          $sql = 'SELECT
                    SUM(a.price_for_carer) as total,  MAX(cp.id) as carer_id, MAX(cp.first_name) as first_name, MAX(cp.family_name) as family_name
                  FROM appointments a
                  JOIN bookings b ON a.booking_id = b.id
                  JOIN users c ON b.carer_id = c.id
                  JOIN carers_profiles cp ON cp.id = c.id
                  WHERE  a.payout = false AND a.status_id = 4 AND a.id = '.$appointment_id.'
                  GROUP BY a.booking_id';
          $res = DB::select($sql);
        // echo "<pre>"; print_r($res); echo "<pre>"; exit;

          $data = $res[0];
		  $booking 	=	Appointment::with('booking')
            				->where('id',$appointment_id)
            				->where('status_id', 4)
            				->where('payout', 0)
            				->first();
			//echo "<pre>"; print_r($booking); echo "<pre>"; exit;
          //Get stripe account of carer
          $connectedAccount = StripeConnectedAccount::where('carer_id', $data->carer_id)->orderBy('created_at', 'desc')->first();
			//echo "<pre>"; print_r($connectedAccount); echo "<pre>"; //exit;
          //Transfer money to carer
          $appointments = Appointment::where('id', $appointment_id)->where('status_id', 4)->where('payout', 0)->get();
      	//echo "<pre>"; print_r($appointments); echo "<pre>"; exit;
          //$appointmentsIds = implode(',', $appointments->pluck('id')->toArray());
          //echo "<pre>Booking ID : "; print_r($appointmentsIds); echo "<pre>"; exit;exit;
          try {
              $res = PaymentTools::createTransfer($connectedAccount->id, $appointment_id, $booking->booking_id, $data->total * 100, 'Payment to Carer ' . $data->first_name . ' ' . $data->family_name . ' (' . $data->carer_id . ') for appointments ' . $appointment_id . ' in booking ' . $booking->booking_id);
          } catch (\Exception $ex) {
              return response($this->formatResponse('error', $ex->getMessage()));
          }
          //Mark certain appointments as with poyout
          Appointment::where('id', $appointment_id)->where('status_id', 4)->where('payout', 0)->update(['payout' => 1]);

          return response(['status' => 'success']);
      }
	//export booking transaction data 
	public function exportTransactionToXls(Request $request,Transaction $transaction)
    {
        $input = Input::all();
        $model = $transaction;
        $transactions = $model->select('*')->orderByDesc('id');


        if(isset($input['userName']) && !empty($input['userName'])){
		   	$userName = $input['userName'];
		   	
		   	$filterTransaction = Transaction::orderBy('id', 'desc')->get();
		   	$filterTransaction = $filterTransaction->filter(function($item) use ($userName) {
			  if(is_numeric($userName)){
				  return( $item->booking->bookingPurchaser->id == $userName ||
				    $item->booking->bookingCarer->id == $userName ||
				    $item->booking->id == $userName ||
				    $item->id == $userName
				  );
				  
			  }else{
				  
				return( (stripos($item->booking->bookingCarerProfile->full_name, $userName) !== false) ||
				    (stripos($item->booking->bookingPurchaserProfile->full_name, $userName) !== false)
				  );    
			  }	
			});
			
			
			$filterTransaction = $filterTransaction->pluck('id')->toArray();

			if(!empty($filterTransaction)){
				
				 $transactions = $transactions->whereIn('id',$filterTransaction);
			}
			

       }
        elseif(isset($input['daterange']) && isset($input['TransactionsSort'])){
            $date = explode(' - ',$input['daterange']);
            $transactions = $transactions->where('created_at','>=', Carbon::createFromFormat('d/m/y', $date[0])->startOfDay()->toDateTimeString())
                ->where('created_at','<=', Carbon::createFromFormat('d/m/y', $date[1])->endOfDay()->toDateTimeString())
                ->where('payment_method','=',$input['TransactionsSort']);
        }
        elseif(isset($input['daterange'])){
            $date = explode(' - ',$input['daterange']);
            $transactions = $transactions->where('created_at','>=', Carbon::createFromFormat('d/m/y', $date[0])->startOfDay()->toDateTimeString())
                ->where('created_at','<=', Carbon::createFromFormat('d/m/y', $date[1])->endOfDay()->toDateTimeString());
        }
        elseif(isset($input['TransactionsSort'])){
            $transactions = $transactions->where('payment_method','=',$input['TransactionsSort']);
        }
        
        $transactions = $transactions->get();
       
        $data = $transactions;

		Excel::create('Booking Transactions', function($excel) use($data) {
            $excel->sheet('Booking Transactions', function($sheet) use ($data) {
                $sheet->loadView('HolmAdmin.bookingTransactionToXls')
                      ->with([
                          'transactions'       => $data
                      ]);
            });
        })->download('csv');
		
	}
	
	//export payment to purchasers data 
	public function exportPurchaserPaymentToXls(Request $request)
    {
		$request  =	Input::all();
        $userName = $request['userName'];
        $payoutsToPurchasers = PayoutToPurchaser::all();
        if($userName)
        $payoutsToPurchasers = $payoutsToPurchasers->filter(function($item)use($userName){
            $name = $item->booking->bookingCarerProfile->full_name;
            if(strpos(strtoupper($name),strtoupper($userName))!==false)
                return true;
        });
        
        $potentialPayouts = $this->getPotentialPayoutsForPurchasers($userName);
        $this->vars['payoutsToPurchasers'] = $payoutsToPurchasers;
        $this->vars['potentialPayouts'] = $potentialPayouts;
        $data = $this->vars;

		Excel::create('Purchaser Payment', function($excel) use($data) {
		$excel->sheet('Purchaser Payment', function($sheet) use ($data) {
			$sheet->loadView('HolmAdmin.purchaserPaymentToXls')
				  ->with([
					  'payoutsToPurchasers'       => $data['payoutsToPurchasers'],
					  'potentialPayouts'       =>  $data['potentialPayouts']
				  ]);
		});
		})->download('csv');
    }
	//export payment to carer data 
	public function exportCarerPaymentToXls(Request $request)
    {
		$request  =	Input::all();
        $transfers = StripeTransfer::all();
        $userName = $request['userName'];
        $potentialPayouts = $this->getPotentialPayoutsForCarer($userName);
        $potentialPayouts = collect($potentialPayouts);

        $this->vars['transfers'] = $transfers;
        if(is_numeric($request['userName'])) {
            $this->vars['transfers'] = $transfers->filter(function($item)use($request){

				if($item->booking_id > 0){
					return ( $item->booking->carer_id === (int)$request['userName'] );
				}elseif($item->bonus_id > 0){
					$bonusPayout = \App\BonusPayout::where('id', $item->bonus_id)->pluck('user_id')->toArray();
					return ( $bonusPayout[0] === (int)$request['userName'] );
				}

            });
        }elseif($request['userName']!=""){
			  $this->vars['transfers'] = $transfers->filter(function($item)use($request){
				if($item->bonus_id > 0){
					$bonusPayout = \App\BonusPayout::where('id', $item->bonus_id)->pluck('user_id')->toArray();
					$sql="SELECT * FROM carers_profiles where id = ".$bonusPayout[0];
					$bonusUser = DB::select($sql);
					
					return ( strpos(strtoupper($bonusUser[0]->first_name),strtoupper($request['userName']))!==false ||  strpos(strtoupper($bonusUser[0]->family_name),strtoupper($request['userName']))!==false );
				}	
            });
			
		}
        $all = collect();
        $all = $all->merge($potentialPayouts);
        $all = $all->merge($this->vars['transfers']);

        $this->vars = array_add($this->vars, 'potentialPayouts', $potentialPayouts);
        $this->vars = array_add($this->vars, 'all', $all);
        
        $data = $this->vars;

		Excel::create('Carer Payment', function($excel) use($data) {
		$excel->sheet('Carer Payment', function($sheet) use ($data) {
			$sheet->loadView('HolmAdmin.carerPaymentToXls')
				  ->with([
					  'potentialPayouts'       => $data['potentialPayouts'],
					  'all'                    =>  $data['all']
				  ]);
		});
		})->download('csv');
    }

}
