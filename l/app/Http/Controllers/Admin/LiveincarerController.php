<?php

namespace App\Http\Controllers\Admin;

use App\LiveCarer;
use PaymentTools;
use DB;
use Illuminate\Http\Request;

class LiveincarerController extends AdminController
{
    public function __construct()
    {
        parent::__construct();
        $this->template = config('settings.theme') . '.templates.adminBase';
    }

    public function index(){

        $data = LiveCarer::all();

        // dd($data);
        // dd($data);
        $this->vars['livecarers']=$data;
        $this->content = view(config('settings.theme').'.liveincarers')->with( $this->vars)->render();

        return $this->renderOutput(); 

    }

    public function update(Request $request){
        // dd($request['fees']);
        if(isset($request['livecarers']) && is_array($request['livecarers']) && count($request['livecarers']) > 0){
            foreach ($request['livecarers'] as $key => $item) {
                $livecarers=LiveCarer::find($item['id']);
                $livecarers->amount           =   $item['amount'];
                $livecarers->carer_rate       =   $item['carer_rate'];   
                $livecarers->purchaser_rate   =   $item['carer_rate']+$item['amount'];
                $livecarers->save();
            }
        }
        $data = LiveCarer::all();
        $this->vars['livecarers']=$data;
        $this->content = view(config('settings.theme').'.liveincarers')->with( $this->vars)->render();

        return $this->renderOutput();
    }
}

