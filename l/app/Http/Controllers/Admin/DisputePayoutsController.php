<?php

namespace App\Http\Controllers\Admin;

use App\Appointment;
use App\DisputePayout;
use App\Http\Controllers\Admin\AdminController;
use App\StripeCharge;
use App\StripeConnectedAccount;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use PaymentTools;
use Illuminate\Support\Facades\DB;
use App\Events\AppointmentCompletedEvent;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Input;


class DisputePayoutsController extends AdminController
{

    public function __construct()
    {
        parent::__construct();
        $this->template = config('settings.theme') . '.templates.adminBase';
    }

    public function index(Request $request) {
		
        $disputePayouts = DisputePayout::orderBy('created_at', 'desc')->get();
        $paidTo = false;
        if($request['whoPaid'] == '1') $paidTo = 'carer';
        if($request['whoPaid'] == '2') $paidTo = 'purchaser';
        if(isset($request['userName']) && $request['userName'] !== null) {
                $this->vars['disputePayouts'] = is_numeric($request['userName'])
                    ? $disputePayouts->filter(function($item) use ($request, $paidTo) {
                        if($paidTo){
                            return (($item->appointment->booking->bookingCarer->id === (int)$request['userName'] ||
                                    $item->appointment->booking->bookingPurchaser->id === (int)$request['userName'] ||
                                $item->appointment->id === (int)$request['userName'] ||
                                $item->appointment->booking->id === (int)$request['userName'] ) &&
                                ($item->status == $paidTo));
                        } else{
                        return (
                            $item->appointment->booking->bookingCarer->id === (int)$request['userName'] ||
                            $item->appointment->booking->bookingPurchaser->id === (int)$request['userName'] ||
                            $item->appointment->id === (int)$request['userName'] ||
                            $item->appointment->booking->id === (int)$request['userName']
                        );}
                    })
                    : $disputePayouts->filter(function($item) use ($request, $paidTo) {
						
                        if($paidTo){
                            return ((stripos($item->appointment->booking->bookingCarer->full_name, $request['userName']) !== false) ||
								(stripos($item->appointment->booking->bookingPurchaser->full_name, $request['userName']) !== false) ||
                                (stripos($item->appointment->booking->transaction->id, $request['userName']) !== false)) &&
                            ($item->status == $paidTo);
                        } else {

                            if (isset($item->appointment->booking->transaction->id)) {
								
                                return (stripos(
                                        $item->appointment->booking->bookingCarer->full_name, $request['userName']) !== false ||
                                        stripos(
                                        $item->appointment->booking->bookingPurchaser->full_name, $request['userName']) !== false ||
  
                                    stripos($item->appointment->booking->transaction->id, $request['userName']) !== false);
                            } else {
                                return (stripos($item->appointment->booking->bookingCarer->full_name, $request['userName']) !== false ||
										 stripos($item->appointment->booking->bookingPurchaser->full_name, $request['userName']) !== false
                                       );
                            }

                        }
                    });
        } else {
            if($paidTo) {
                $disputePayouts = DisputePayout::where('status', '=', $paidTo)->get();
            }
            $this->vars['disputePayouts'] = $disputePayouts;
        }

        $this->content = view(config('settings.theme').'.disputePayouts.disputePayouts')->with($this->vars)->render();

        return $this->renderOutput();
    }

    public function setDetailsOfDispute(Request $request) {
        $disputePayout = DisputePayout::find($request->dispute_payout_id);
        $disputePayout->details_of_dispute = $request->details_of_dispute;
        $disputePayout->save();
    }

    public function detailsOfDisputeResolution(Request $request) {
        $disputePayout = DisputePayout::find($request->dispute_payout_id);
        $disputePayout->details_of_dispute_resolution = $request->details_of_dispute_resolution;
        $disputePayout->save();
    }

    public function payoutToCarer(Request $request){
        $disputePayout = DisputePayout::find($request->dispute_payout_id);
        $appointment = $disputePayout->appointment;
        $booking = $appointment->booking;
        $purchaser = $booking->bookingPurchaserProfile;
        $purchaserEmail = $booking->bookingPurchaser;
        $carer = $booking->bookingCarerProfile;
        $carerEmail = $booking->bookingCarer;
        $stripeConnectedAccount = StripeConnectedAccount::where('carer_id', $booking->carer_id)->first();
        $comment = 'Payment to Carer '.$booking->bookingCarerProfile->full_name.' ('.$booking->carer_id.') for appointment '.$appointment->id.' in booking '.$booking->id.' (dispute resolving)';
        try {
            $res = PaymentTools::createTransfer($stripeConnectedAccount->id, $booking->id, $appointment->price_for_carer*100, $comment);
        } catch (\Exception $ex) {
            return response($this->formatResponse('error', $ex->getMessage()));
        }
        $disputePayout->status = 'carer';
        $disputePayout->save();

        $appointment->payout = true;
        $appointment->status_id = 4;
        $appointment->save();

        $purchaser1 = User::find($purchaserEmail->id);
        if ($appointment->use_register_code == 1) {
            $purchaser1->use_register_code = 0;
            $purchaser1->had_use_register_code = 0;
            $purchaser1->save();
        }

        $appointments = $booking->appointments()->get();
        $status_cancel = true;

        if (!$booking->has_active_appointments) {
            foreach ($appointments as $app) {
                if ($app->status_id != 5) {
                    $status_cancel = false;
                    break;
                }
            }

            if (!$status_cancel) {
                $booking->status_id = 7;
            } else {
                $booking->status_id = 4;
            }
        }

        $booking->save();

        //email to purchaser
        $payment = number_format((float)$appointment->price_for_purchaser, 2, '.', '') ;
        $text = view(config('settings.frontTheme') . '.emails.appointment_resolved')->with([
            'purchaser' => $purchaser, 'booking' => $booking, 'appointment' => $appointment,  'carer' =>
                $carer, 'payment' => $payment, 'sendTo' => 'purchaser', 'payoutTo' => 'carer'
        ])->render();

        DB::table('mails')
            ->insert([
                'email' => $purchaserEmail->email,
                'subject' => 'Holm dispute resolution',
                'text' => $text,
                'time_to_send' => Carbon::now(),
                'status' => 'new'
            ]);

        DB::table('mails')
            ->insert([
                'email' => 'nik@holm.care',
                'subject' => 'Holm dispute resolution',
                'text' => $text,
                'time_to_send' => Carbon::now(),
                'status' => 'new'
            ]);

        event(new AppointmentCompletedEvent($appointment));
        //email to carer
        $payment1 = number_format((float)$appointment->price_for_carer, 2, '.', '') ;
        $text1 = view(config('settings.frontTheme') . '.emails.appointment_resolved')->with([
            'purchaser' => $purchaser, 'booking' => $booking, 'appointment' => $appointment, 'carer' =>
                $carer, 'payment' => $payment1, 'sendTo' => 'carer', 'payoutTo' => 'carer'
        ])->render();

        DB::table('mails')
            ->insert([
                'email' => $carerEmail->email,
                'subject' => 'Holm dispute resolution',
                'text' => $text1,
                'time_to_send' => Carbon::now(),
                'status' => 'new'
            ]);

        DB::table('mails')
            ->insert([
                'email' => 'nik@holm.care',
                'subject' => 'Holm dispute resolution',
                'text' => $text1,
                'time_to_send' => Carbon::now(),
                'status' => 'new'
            ]);

        return response(['status' => 'success']);
    }

    public function payoutToPurchaser(Request $request)
    {
        $disputePayout = DisputePayout::find($request->dispute_payout_id);
        $appointment = $disputePayout->appointment;
        $booking = $appointment->booking;

        $purchaser = $booking->bookingPurchaserProfile;
        $purchaserEmail = $booking->bookingPurchaser;
        $carer = $booking->bookingCarerProfile;
        $carerEmail = $booking->bookingCarer;

        $comment = 'Payment to Purchaser '.$booking->bookingCarerProfile->full_name.' ('.$booking->carer_id.') for appointment '.$appointment->id.' in booking '.$booking->id.' (dispute resolving)';

        if ($appointment->price_for_purchaser != 0) {
            try {
                if ($booking->payment_method == 'credit_card') {
                    $stripeCharge = StripeCharge::where('booking_id', $booking->id)->first();
                    $res = PaymentTools::createRefund($appointment->price_for_purchaser * 100, $stripeCharge->id, $booking->id, $comment);
                } else {
                    $res = PaymentTools::createBonusRefund($appointment->price_for_purchaser, $booking->id, $comment);
                }
            } catch (\Exception $ex) {
                return response($this->formatResponse('error', $ex->getMessage()));
            }
        }

        $disputePayout->status = 'purchaser';
        $disputePayout->save();

        $appointment->payout = true;
        $appointment->status_id = 5;
        $appointment->save();

        $purchaser1 = User::find($purchaserEmail->id);
        if ($appointment->use_register_code == 1) {
            $purchaser1->use_register_code = 1;
            $purchaser1->had_use_register_code = 0;
            $purchaser1->save();
        }

        $appointments = $booking->appointments()->get();
        $status_cancel = true;

        if (!$booking->has_active_appointments) {
            foreach ($appointments as $app) {
                if ($app->status_id != 5) {
                    $status_cancel = false;
                    break;
                }
            }

            if (!$status_cancel) {
                $booking->status_id = 7;
            } else {
                $booking->status_id = 4;
            }
        }

        $booking->save();

        //email to carer
        $payment = number_format((float)$appointment->price_for_carer, 2, '.', '');
        $text = view(config('settings.frontTheme') . '.emails.appointment_resolved')->with([
            'purchaser' => $purchaser, 'booking' => $booking, 'appointment' => $appointment,  'carer' =>
                $carer, 'payment' => $payment, 'sendTo' => 'carer', 'payoutTo' => 'purchaser'
        ])->render();

        DB::table('mails')
            ->insert([
                'email' => $carerEmail->email,
                'subject' => 'Holm dispute resolution',
                'text' => $text,
                'time_to_send' => Carbon::now(),
                'status' => 'new'
            ]);

        DB::table('mails')
            ->insert([
                'email' => 'nik@holm.care',
                'subject' => 'Holm dispute resolution',
                'text' => $text,
                'time_to_send' => Carbon::now(),
                'status' => 'new'
            ]);

        //email to purchaser
        $payment1 = number_format((float)$appointment->price_for_purchaser, 2, '.', '') ;
        $text1 = view(config('settings.frontTheme') . '.emails.appointment_resolved')->with([
            'purchaser' => $purchaser, 'booking' => $booking, 'appointment' => $appointment, 'carer' =>
                $carer, 'payment' => $payment1, 'sendTo' => 'purchaser', 'payoutTo' => 'purchaser'
        ])->render();

        DB::table('mails')
            ->insert([
                'email' => $purchaserEmail->email,
                'subject' => 'Holm dispute resolution',
                'text' => $text1,
                'time_to_send' => Carbon::now(),
                'status' => 'new'
            ]);

        DB::table('mails')
            ->insert([
                'email' => 'nik@holm.care',
                'subject' => 'Holm dispute resolution',
                'text' => $text1,
                'time_to_send' => Carbon::now(),
                'status' => 'new'
            ]);

        return response(['status' => 'success']);
    }
        //export data 
    public function  exportDisputePayoutToXls(Request $request)
    {
		$request  =	Input::all();
		
		$disputePayouts = DisputePayout::orderBy('created_at', 'desc')->get();
		$paidTo = false;
		if($request['whoPaid'] == '1') $paidTo = 'carer';
		if($request['whoPaid'] == '2') $paidTo = 'purchaser';
		if(isset($request['userName']) && $request['userName'] !== null) {
				$this->vars['disputePayouts'] = is_numeric($request['userName'])
					? $disputePayouts->filter(function($item) use ($request, $paidTo) {
						if($paidTo){
							return (($item->appointment->booking->bookingCarer->id === (int)$request['userName'] ||
									$item->appointment->booking->bookingPurchaser->id === (int)$request['userName'] ||
								$item->appointment->id === (int)$request['userName'] ||
								$item->appointment->booking->id === (int)$request['userName'] ) &&
								($item->status == $paidTo));
						} else{
						return (
							$item->appointment->booking->bookingCarer->id === (int)$request['userName'] ||
							$item->appointment->booking->bookingPurchaser->id === (int)$request['userName'] ||
							$item->appointment->id === (int)$request['userName'] ||
							$item->appointment->booking->id === (int)$request['userName']
						);}
					})
					: $disputePayouts->filter(function($item) use ($request, $paidTo) {
						if($paidTo){
							return ((stripos($item->appointment->booking->bookingCarer->full_name, $request['userName']) !== false) ||
									(stripos($item->appointment->booking->bookingPurchaser->full_name, $request['userName']) !== false) ||
								(stripos($item->appointment->booking->transaction->id, $request['userName']) !== false)) &&
							($item->status == $paidTo);
						} else {
							if (isset($item->appointment->booking->transaction->id)) {
								return (stripos(
										$item->appointment->booking->bookingCarer->full_name, $request['userName']) !== false ||
										(stripos($item->appointment->booking->bookingPurchaser->full_name, $request['userName']) !== false) ||
									stripos($item->appointment->booking->transaction->id, $request['userName']) !== false);
							} else {
								return (stripos($item->appointment->booking->bookingCarer->full_name, $request['userName']) !== false || (stripos($item->appointment->booking->bookingPurchaser->full_name, $request['userName']) !== false));
							}

						}
					});
		} else {
			if($paidTo) {
				$disputePayouts = DisputePayout::where('status', '=', $paidTo)->get();
			}
			$this->vars['disputePayouts'] = $disputePayouts;
		}
		
		$data = $this->vars;
		
		Excel::create('Dispute Payouts', function($excel) use($data) {
		$excel->sheet('Dispute Payouts', function($sheet) use ($data) {
			$sheet->loadView('HolmAdmin.disputePayoutToXls')
				  ->with([
					  'disputePayouts'       => $data['disputePayouts']
				  ]);
		});
		})->download('csv');	
	}
	
	//export statistic for accountant
	 public function exportStaticticsToXls()
    {
		
		$input = Input::all();
		$disputePayouts = DisputePayout::orderBy('created_at', 'desc')->get();

		 if (isset($input['dateFrom']) && $input['dateFrom'] > '') {
			$disputePayouts = $disputePayouts->where('created_at','>=', Carbon::createFromFormat('d-m-Y', $input['dateFrom'])->startOfDay()->toDateTimeString());
		} 

		if (isset($input['dateTo']) && $input['dateTo'] > '') {
			$disputePayouts = $disputePayouts->where('created_at','<=', Carbon::createFromFormat('d-m-Y', $input['dateTo'])->endOfDay()->toDateTimeString());
		}

		$disputePayouts = $disputePayouts->where('status', '!=', 'pending');
         
       //~ if(isset($input['daterange']) && !empty($input['daterange'])){
            //~ $date = explode(' - ',$input['daterange']);
            //~ $disputePayouts = $disputePayouts->where('created_at','>=', Carbon::createFromFormat('d/m/y', $date[0])->startOfDay()->toDateTimeString())
                //~ ->where('created_at','<=', Carbon::createFromFormat('d/m/y', $date[1])->endOfDay()->toDateTimeString())
                //~ ->where('status', '!=', 'pending');
        //~ }
        
        
        
        
        $this->vars['disputePayouts'] = $disputePayouts;
        $data = $this->vars;

		Excel::create('Statistic', function($excel) use($data) {
            $excel->sheet('Statistic', function($sheet) use ($data) {
                $sheet->loadView('HolmAdmin.statisticsUserToXls')
                      ->with([
                          'disputePayouts'          => $data['disputePayouts'],
                      ]);
            });
        })->download('csv');
	}
}
