<?php

namespace App\Http\Controllers\Admin\Faq;
use App\Http\Controllers\Admin\AdminController;

use App\Faq;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class FaqController extends AdminController
{
    private $faq;

    public function __construct(Faq $faq) {
        parent::__construct();
        $this->faq = $faq;

        $this->template = config('settings.theme').'.templates.adminBase';
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
		
	

        $this->title = 'Admin Faqs';
        $faqs = Faq::orderBy('faq_order', 'asc')->get();
        $this->vars = array_add($this->vars,'faqs',$faqs);
        $this->content = view(config('settings.theme').'.faq.faq')->with($this->vars)->render();

        return $this->renderOutput();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->title = 'Admin FAQs';
        $this->content = view(config('settings.theme') . '.faq.create')->with($this->vars)->render();

        return $this->renderOutput();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
		
	

        $this->validate($request, [
            'title' => 'required'
         
        ]);

        $faq = new Faq;
        $faq->title = $request->title;
        $faq->content = $request->content;
        $faq->faq_order = $request->faq_order;
        $faq->status = 'published';
        $faq->save();
        return redirect(route('faq.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($faqId)
    {
        $this->title = 'Admin FAQs';
        $faqs = $this->faq->findOrFail($faqId);
        $this->vars = array_add($this->vars,'faq',$faqs);
        $this->content = view(config('settings.theme').'.faq.edit')->with($this->vars)->render();
        return $this->renderOutput();

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

		
		if (isset($request->delete_faq_id)) {
            $faq = Faq::find($request->delete_faq_id);
            $faq->delete();
            return response(['status' => 'success']);
        }elseif($request->publish_faq_id){
			 $faq = Faq::find($request->publish_faq_id);
			 $faq->status = 'published';	
			 $faq->save();
            return response(['status' => 'success']);	
		}else {
			
            $faq = faq::find($id);
            $faq->title = $request->title;
            $faq->content = $request->content;
            $faq->faq_order = $request->faq_order;
            $faq->status = 'published';
            $faq->save();

            return redirect(route('faq.index'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
