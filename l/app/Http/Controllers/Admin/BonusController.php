<?php

namespace App\Http\Controllers\Admin;


use App\BonusPayout;
use App\BonusTransaction;
use App\CarersProfile;
use App\PurchasersProfile;
use App\StripeConnectedAccount;
use App\User;
use Illuminate\Http\Request;
use PaymentTools;
use DB;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Input;
use Auth;

class BonusController extends AdminController
{

    public function __construct()
    {
        parent::__construct();
        $this->template = config('settings.theme') . '.templates.adminBase';
    }

    public function getBonusesCarer(Request $request)
    {
    
            $res = DB::table('bonus_payouts')
                ->join('users', 'bonus_payouts.user_id', '=', 'users.id')
                ->select('bonus_payouts.id')
                ->where('users.user_type_id', 3)
               // ->where('bonus_payouts.is_primary', 1)
                ->get();
    

        $res = $res->pluck('id')->toArray();
        $bonusPayouts = BonusPayout::find($res);
       $userName = trim($request->get('userName',false));
     
        if($userName){
             $bonusPayouts = $bonusPayouts->filter(function($item)use($userName){
                if(is_numeric($userName)){
                    if($item->user->id === (int)$userName || strpos($item->user->referral_code, $userName) !== false) {return true;}
                } else {
					$referralUser = $this->referral_search_user($item->id) ;
                    if (strpos($item->user->referral_code, $userName) !== false ||
						strpos(strtolower($referralUser), strtolower($userName)) !== false ||
                        strpos(strtolower($item->user->full_name), strtolower($userName)) !== false)
                        return true;
                }
            });
        }
        $users = User::all();

        $this->vars['users'] = $users;
        $this->vars['bonusPayouts'] = $bonusPayouts->sortByDesc('id');
        $this->vars['profiles'] = $this->getTotalProfilesForCodeBenefRow($bonusPayouts);
		$this->content = view(config('settings.theme').'.bonusesCarers')->with($this->vars)->render();

        return $this->renderOutput();
    }

    public function getBonusesPurchaser(Request $request) {
     
        $res = DB::table('bonus_payouts')
            ->join('users', 'bonus_payouts.user_id', '=', 'users.id')
            ->select('bonus_payouts.id')
            ->where('users.user_type_id', 1)
            ->get();
        $res = $res->pluck('id')->toArray();
        $bonusPayouts = BonusPayout::find($res);

        $userName = $request->get('userName',false);

        if($userName){
            $bonusPayouts = $bonusPayouts->filter(function($item)use($userName){
                if(is_numeric($userName)){
                    if ($item->user->id === (int)$userName || $this->user_referral_code($item->id) === $userName) {
                        return true;
                    }
                } else {

					$referralUser = $this->referral_search_user($item->id) ;
                    if($this->user_referral_code($item->id) === $userName ||
                        strpos(strtolower($item->user->full_name), strtolower($userName)) !== false ||
                        strpos(strtolower($referralUser), strtolower($userName)) !== false) {
                        return true;
                    }
                    
                    
                }
            });
        }

        $users = User::all();

        $this->vars['users'] = $users;
        $this->vars['profiles'] = $this->getTotalProfilesForCodeBenefRow($bonusPayouts);
        $this->vars['bonusPayouts'] = $bonusPayouts->sortByDesc('id');
//        dd($this->vars['profiles']);


        $this->content = view(config('settings.theme').'.bonusesPurchasers')->with($this->vars)->render();

        return $this->renderOutput();
    }
    
	//get refderal search user data  
	   public function referral_search_user($id){
		   $bonusres = DB::table('bonus_payouts')
			->join('users', 'bonus_payouts.user_id', '=', 'users.id')
			->select('bonus_payouts.referral_user_id')
			->where('bonus_payouts.id', $id)
			->get();
		   $userId = $bonusres->pluck('referral_user_id')->toArray();
		   $referral_user_name = "";
		   $referral_user = User::where('id', $userId[0])->first();
         //if_isset condition written on 26 june 2019
         	if(isset($referral_user))
			switch ($referral_user->user_type_id) {
				case 1:
					$referral_user_name = PurchasersProfile::find($referral_user->id);
                
					return $referral_user_name->first_name . ' ' . $referral_user_name->family_name;
					break;
				case 3:
					$referral_user_name = CarersProfile::find($referral_user->id);
					return $referral_user_name->first_name . ' ' . $referral_user_name->family_name;
					break;
			}
         
		   return  $referral_user_name; 
		   
	   }

	// get referal code of user
	public static function user_referral_code($id){
		$bonusRecord = DB::table('bonus_payouts')
		    ->select('bonus_payouts.is_primary')
			->where('bonus_payouts.id', $id)
			->get();
		
		$bonus =$bonusRecord->pluck('is_primary')->toArray();
       
		if($bonus[0] != 0){
			$referralBonusRecord = DB::table('bonus_payouts')
		    ->select('bonus_payouts.user_id')
			->where('bonus_payouts.id', $bonus[0])
			->get();
			$referralBonus = $referralBonusRecord->pluck('user_id')->toArray();	
		}else{
		   	$referralBonusRecord = DB::table('bonus_payouts')
		    ->select('bonus_payouts.referral_user_id')
			->where('bonus_payouts.id', $id)
			->get();		
      
			$referralBonus = $referralBonusRecord->pluck('referral_user_id')->toArray();	
		}	
   if(isset($referralBonus[0])){
   $referral_user = User::where('id', $referralBonus[0])->first();
     
			$referral_code = $referral_user->own_referral_code; 
	  
	 }else{
     $referral_code = '';
   }
      
      return  $referral_code ;
			
	  
	}


    public function payoutBonus(Request $request) {
        $bonusPayout = BonusPayout::findOrFail($request->bonus_id);
      	$user = $bonusPayout->user;
    
        if($user->user_type_id == 1){
            //Purchaser
            BonusTransaction::create([
               'user_id' => $user->id,
               'amount' => $bonusPayout->amount,
            ]);
        } elseif($user->user_type_id == 3){
          
            //Carer
            $stripeConnectedAccount = StripeConnectedAccount::where('carer_id', $user->id)->get()->first();
           
         if(isset($stripeConnectedAccount))
            try {
                PaymentTools::createTransfer($stripeConnectedAccount->id, 0, 0, $bonusPayout->amount * 100, 'Bonus payout paid to '.$user->userCarerProfile->full_name,$request->bonus_id);
            } catch (\Exception $ex) {
                return response($this->formatResponse('error', $ex->getMessage()));
            }
        }
        $bonusPayout->payout = true;
        $bonusPayout->save();
		return response(['status' => 'success']);
    }

    public function cancelBonus(Request $request) {
        $bonusPayout = BonusPayout::findOrFail($request->bonus_id);
        $bonusPayout->delete();
       return response(['status' => 'success']);
    }

    private function getTotalProfilesForCodeBenefRow($bonusPayouts)
    {
        $bonusWithReferral = $bonusPayouts->filter(function($item) {
            return $item->user->referral_code;
        });
        $userIds = $bonusWithReferral->pluck('referral_user_id')->toArray();

        $purchasersProfiles = PurchasersProfile::find($userIds);
        $carersProfiles = CarersProfile::find($userIds);
        return $purchasersProfiles->merge($carersProfiles);
    }
    
    //export bonus carer
     public function exportBonusCarersToXls(Request $request)
    {
	
		$request 	=	Input::all();
		$res = DB::table('bonus_payouts')
			->join('users', 'bonus_payouts.user_id', '=', 'users.id')
			->select('bonus_payouts.id')
			->where('users.user_type_id', 3)
			->get();

        $res = $res->pluck('id')->toArray();
        $bonusPayouts = BonusPayout::find($res);
        $userName = $request['userName'];
        if($userName){
             $bonusPayouts = $bonusPayouts->filter(function($item)use($userName){
                if(is_numeric($userName)){
                    if($item->user->id === (int)$userName || strpos($item->user->referral_code, $userName) !== false) {return true;}
                } else {
					$referralUser = $this->referral_search_user($item->id) ;
                    if (strpos($item->user->referral_code, $userName) !== false ||
						strpos(strtolower($referralUser), strtolower($userName)) !== false ||
                        strpos(strtolower($item->user->full_name), strtolower($userName)) !== false)
                        return true;
                }
            });
        }
        $users = User::all();

        $this->vars['users'] = $users;
        $this->vars['bonusPayouts'] = $bonusPayouts->sortByDesc('id');
        $this->vars['profiles'] = $this->getTotalProfilesForCodeBenefRow($bonusPayouts);
		$data = $this->vars;
		
		Excel::create('Bonus Carer', function($excel) use($data) {
		$excel->sheet('Bonus Carer', function($sheet) use ($data) {
			$sheet->loadView('HolmAdmin.bonusCarerToXls')
				  ->with([
					  'users'              => $data['users'],
					  'bonusPayouts'       => $data['bonusPayouts'],
					  'profiles'           => $data['profiles'],
				  ]);
		});
		})->download('csv');
       
    }
    //export bonus purchaser
     public function exportBonusPurchaserToXls(Request $request)
    {

		$request 	=	Input::all();
		$res = DB::table('bonus_payouts')
		->join('users', 'bonus_payouts.user_id', '=', 'users.id')
		->select('bonus_payouts.id')
		->where('users.user_type_id', 1)
		->get();

        $res = $res->pluck('id')->toArray();

        $bonusPayouts = BonusPayout::find($res);
        $userName = $request['userName'];

        if($userName){
            $bonusPayouts = $bonusPayouts->filter(function($item)use($userName){
                if(is_numeric($userName)){
                    if ($item->user->id === (int)$userName || $this->user_referral_code($item->id)  === (int)$userName) {
                        return true;
                    }
                } else {
				
					$referralUser = $this->referral_search_user($item->id) ;
                    if($this->user_referral_code($item->id) === $userName ||
                        strpos(strtolower($item->user->full_name), strtolower($userName)) !== false ||
                        strpos(strtolower($referralUser), strtolower($userName)) !== false) {
                        return true;
                    }
                    
                    
                }
            });
        }
        
        $users = User::all();

        $this->vars['users'] = $users;
        $this->vars['profiles'] = $this->getTotalProfilesForCodeBenefRow($bonusPayouts);
        $this->vars['bonusPayouts'] = $bonusPayouts->sortByDesc('id');
		$data = $this->vars;

		Excel::create('Bonus Purchaser', function($excel) use($data) {
		$excel->sheet('Bonus Purchaser', function($sheet) use ($data) {
			$sheet->loadView('HolmAdmin.bonusPurchaserToXls')
				  ->with([
					  'users'              => $data['users'],
					  'bonusPayouts'       => $data['bonusPayouts'],
					  'profiles'           => $data['profiles'],
				  ]);
		});
		})->download('csv');
    }
  
     //Credit Wallet 
    public function CreditWallet(Request $request){
	  $amount   =   $request['amount'];
	  $userId = $request['userId'];
	  BonusTransaction::create([
               'user_id' => $userId,
               'amount' => $amount,
               'comment' => 'wallet credit by admin'
            ]);
	 
	 return response(['status' => 'success']);
	}
}
