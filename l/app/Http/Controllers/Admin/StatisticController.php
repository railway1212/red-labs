<?php

namespace App\Http\Controllers\Admin;

use Carbon\Carbon;
use DB;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Input;

class StatisticController extends AdminController
{
    public function __construct()
    {
        parent::__construct();
        $this->template = config('settings.theme') . '.templates.adminBase';
    }

    public function index(){
		
		$input = Input::all();
        $filter = FALSE;
		$dateFrom = $dateTo ="";
        if (isset($input['dateFrom']) && $input['dateFrom'] > '') {
            $input['dateFrom'] .= " 00:00:00";
             $dateFrom = strtotime($input['dateFrom']);
            $dateFrom = date("Y-m-d H:i:s", $dateFrom);
             $filter = TRUE; 
        } else {
            $dateFrom = FALSE;
        }

        if (isset($input['dateTo']) && $input['dateTo'] > '') {
            $input['dateTo'] .= " 00:00:00";
            $dateTo = strtotime($input['dateTo']);
            $dateTo = date("Y-m-d H:i:s", $dateTo);
            $filter = TRUE; 
        } else {
            $dateTo = FALSE;
        }
		
		$bookingsStatisticFilter = $bookingsStatisticTotalFilter = $userAmountFilter = $appointmentFilter = $monthFilter = $genderFilter = $ageServiceFilter =  "";
		
		if (!empty($dateFrom) && !empty($dateTo)) {
		  $bookingsStatisticFilter = "AND b.created_at >= '".$dateFrom."' AND b.created_at <= '".$dateTo."'";
		  $bookingsStatisticTotalFilter ="AND b.created_at >= '".$dateFrom."' AND b.created_at <= '".$dateTo."'";
		  $purchasersAmountFilter ="AND p.created_at >= '".$dateFrom."' AND p.created_at <= '".$dateTo."'";
		  $userAmountFilter ="AND p.created_at >= '".$dateFrom."' AND p.created_at <= '".$dateTo."'";
		  $appointmentFilter ="AND a.date_start >='".$dateFrom."' AND a.date_start <='".$dateTo."'"; 
		  $monthFilter =" t.created_at >= '".$dateFrom."' AND t.created_at <= '".$dateTo."'";
          //$genderFilter = " AND DATE('created_at') = '".date('Y-m-d', strtotime($dateTo))."'";
          $genderFilter =" AND  ( created_at <= '".$dateTo."' )";
          $ageServiceFilter = " AND  ( purchasers_profiles.created_at <= '".$dateTo."' )";
		  
		
        }elseif ( !empty($dateFrom)) {
           $bookingsStatisticFilter = "AND b.created_at >= '".$dateFrom."'";
           $bookingsStatisticTotalFilter = "AND b.created_at >= '".$dateFrom."'";
           $userAmountFilter = "AND p.created_at >= '".$dateFrom."'";
           $appointmentFilter = "AND a.date_start >= '".$dateFrom."'";
           $monthFilter = " t.created_at >= '".$dateFrom."'";
          // $genderFilter = " AND DATE('created_at') = '".$dateFrom."' ";
 		   $genderFilter =" AND  created_at <= '".$dateFrom."'";
           $ageServiceFilter = " AND purchasers_profiles.created_at <= '".$dateFrom."'";

        }elseif (!empty($dateTo)) {
           $bookingsStatisticFilter = "AND b.created_at <= '".$dateTo."'";
           $bookingsStatisticTotalFilter = "AND b.created_at <= '".$dateTo."'";
           $userAmountFilter = "AND p.created_at <= '".$dateTo."'";
           $appointmentFilter = "AND a.date_start <= '".$dateTo."'";
           $monthFilter = " t.created_at <= '".$dateTo."'";
           //$genderFilter = " AND DATE('created_at') = '".$dateTo."' ";
           $genderFilter =" AND  created_at <= '".$dateTo."'";
           $ageServiceFilter = " AND purchasers_profiles.created_at <= '".$dateTo."'";
        }
		
		
		
        //Bookings
        $bookingsStatistic = DB::select('SELECT
              SUM(a.price) as price,
              COUNT(a.price) as amount,
              b.status_id
            FROM bookings b
              JOIN (SELECT booking_id, SUM(price_for_purchaser) as price FROM appointments GROUP BY booking_id) AS a ON a.booking_id = b.id
            WHERE b.status_id  IN (2, 5, 7) '.$bookingsStatisticFilter.'
            GROUP BY b.status_id
            ORDER BY b.status_id');
        $this->vars['bookingsStatistic'] = $bookingsStatistic;

        $bookingsStatisticTotal = DB::select('SELECT
              SUM(a.price) as price,
              COUNT(a.price) as amount
            FROM bookings b
              JOIN (SELECT booking_id, SUM(price_for_purchaser) as price FROM appointments GROUP BY booking_id) AS a ON a.booking_id = b.id
            WHERE b.status_id  IN (2, 5, 7) '.$bookingsStatisticTotalFilter);
        $this->vars['bookingsStatisticTotal'] = $bookingsStatisticTotal;

        //User types
        $usersStatistic['purchasers_amount'] = DB::select("SELECT COUNT(p.id) as count
            FROM  purchasers_profiles p WHERE p.registration_status = 'completed' ".$userAmountFilter)[0]->count;
        $usersStatistic['carers_amount'] = DB::select("SELECT COUNT(p.id) as count
             FROM carers_profiles p WHERE  p.registration_status = 'completed' ".$userAmountFilter)[0]->count;
        $usersStatistic['service_users_profiles'] = DB::select("SELECT COUNT(p.id) as count
             FROM service_users_profiles p WHERE  p.registration_status = 'completed' ".$userAmountFilter)[0]->count;
        $usersStatistic['incomplete_purchasers_amount'] = DB::select("SELECT COUNT(p.id) as count
            FROM  purchasers_profiles p WHERE p.registration_status != 'completed' ".$userAmountFilter)[0]->count;
        $usersStatistic['incomplete_carers_amount'] = DB::select("SELECT COUNT(p.id) as count
             FROM carers_profiles p WHERE  p.registration_status != 'completed' ".$userAmountFilter)[0]->count;
        $usersStatistic['incomplete_service_users_profiles'] = DB::select("SELECT COUNT(p.id) as count
             FROM service_users_profiles p WHERE  p.registration_status != 'completed' ".$userAmountFilter)[0]->count;
        $this->vars['usersStatistic'] = $usersStatistic;

        //Most active carers
        //AND UNIX_TIMESTAMP(NOW()) - UNIX_TIMESTAMP(a.date_start) <= 604800
        //AND UNIX_TIMESTAMP(NOW()) - UNIX_TIMESTAMP(a.date_start) <= 2592000
        $mostActiveCarers = DB::select('SELECT u.id, p.first_name, p.family_name,
                              (
                                SELECT COUNT(a.id)
                                FROM appointments a
                                JOIN bookings b ON a.booking_id = b.id
                                JOIN carers_profiles p ON b.carer_id = p.id
                                WHERE p.id = u.id AND a.status_id = 4  '.$appointmentFilter.'
                              ) as appointments_per_last_week,
                              (
                                SELECT COUNT(a.id)
                                FROM appointments a
                                  JOIN bookings b ON a.booking_id = b.id
                                  JOIN carers_profiles p ON b.carer_id = p.id
                                WHERE p.id = u.id AND a.status_id = 4  '.$appointmentFilter.'
                              ) as appointments_per_last_month
                              FROM users u
                              JOIN carers_profiles p ON p.id = u.id WHERE u.user_type_id = 3 '.$userAmountFilter.'
                            ORDER BY appointments_per_last_week DESC LIMIT 5');
        $this->vars['mostActiveCarers'] = $mostActiveCarers;
     
        

        //Most active purchasers
        //AND UNIX_TIMESTAMP(NOW()) - UNIX_TIMESTAMP(a.date_start) <= 604800
        //AND UNIX_TIMESTAMP(NOW()) - UNIX_TIMESTAMP(a.date_start) <= 2592000
        $mostActivePurchasers = DB::select('SELECT u.id, p.first_name, p.family_name,
                          (
                            SELECT COUNT(a.id)
                            FROM appointments a
                              JOIN bookings b ON a.booking_id = b.id
                              JOIN purchasers_profiles p ON b.purchaser_id = p.id
                            WHERE p.id = u.id AND a.status_id = 4  '.$appointmentFilter.'
                          ) as appointments_per_last_week,
                          (
                            SELECT COUNT(a.id)
                            FROM appointments a
                              JOIN bookings b ON a.booking_id = b.id
                              JOIN purchasers_profiles p ON b.purchaser_id = p.id
                            WHERE p.id = u.id AND a.status_id = 4  '.$appointmentFilter.'
                          ) as appointments_per_last_month
                        FROM users u
                          JOIN purchasers_profiles p ON p.id = u.id  WHERE u.user_type_id = 1 '.$userAmountFilter.'
                        ORDER BY appointments_per_last_week DESC LIMIT 5;');
        $this->vars['mostActivePurchasers'] = $mostActivePurchasers;

        //Income
        $date = Carbon::now();
        $nextSunday = $date->endOfWeek();
        $incomeStatistic['week'] = DB::select('SELECT
                      (
                        SELECT SUM(t.amount)
                        FROM transactions t
                        WHERE UNIX_TIMESTAMP(t.created_at)  BETWEEN  '.$nextSunday->timestamp.' - 1209600 AND '.$nextSunday->timestamp.' - 604800
                      ) as `last`,
                      (
                        SELECT SUM(t.amount)
                        FROM transactions t
                        WHERE UNIX_TIMESTAMP(t.created_at) > '.$nextSunday->timestamp.' - 604800
                      ) as `current`')[0];
       $endOfMonth = $date->endOfMonth();
       
     
       
       
        $incomeStatistic['month'] = DB::select('SELECT
                      (
                        SELECT SUM(t.amount)
                        FROM transactions t
                        WHERE UNIX_TIMESTAMP(t.created_at)  BETWEEN  '.$endOfMonth->timestamp.' - 5184000 AND '.$endOfMonth->timestamp.' - 2592000
                      ) as `last`,
                      (
                        SELECT SUM(t.amount)
                        FROM transactions t
                        WHERE UNIX_TIMESTAMP(t.created_at) > '.$nextSunday->timestamp.' - 2592000
                      ) as `current`')[0];
                      
          if($filter == TRUE){             
			//~ $incomeStatistic['month'] = DB::select('SELECT
                      //~ (
                        //~ SELECT COUNT(t.id)
                        //~ FROM transactions t
                        //~ WHERE '.$monthFilter.'
                      //~ ) as `last`,
                      //~ (
                        //~ SELECT COUNT(t.id)
                        //~ FROM transactions t
                        //~ WHERE UNIX_TIMESTAMP(t.created_at) > '.$nextSunday->timestamp.' - 2592000
                      //~ ) as `current`')[0];     
                      
          }            
        $this->vars['incomeStatistic'] = $incomeStatistic;                

        //Transactions amount
        $date = Carbon::now();
        $nextSunday = $date->endOfWeek();
        $transactionsStatistic['week'] = DB::select('SELECT
                      (
                        SELECT COUNT(t.id)
                        FROM transactions t
                        WHERE UNIX_TIMESTAMP(t.created_at)  BETWEEN  '.$nextSunday->timestamp.' - 1209600 AND '.$nextSunday->timestamp.' - 604800
                      ) as `last`,
                      (
                        SELECT COUNT(t.id)
                        FROM transactions t
                        WHERE UNIX_TIMESTAMP(t.created_at) > '.$nextSunday->timestamp.' - 604800
                      ) as `current`')[0];
        $endOfMonth = $date->endOfMonth();
        $transactionsStatistic['month'] = DB::select('SELECT
                      (
                        SELECT COUNT(t.id)
                        FROM transactions t
                        WHERE UNIX_TIMESTAMP(t.created_at)  BETWEEN  '.$endOfMonth->timestamp.' - 5184000 AND '.$endOfMonth->timestamp.' - 2592000
                      ) as `last`,
                      (
                        SELECT COUNT(t.id)
                        FROM transactions t
                        WHERE UNIX_TIMESTAMP(t.created_at) > '.$nextSunday->timestamp.' - 2592000
                      ) as `current`')[0];
                      
         if($filter == TRUE){                            
         $transactionsStatistic['month'] =DB::select('SELECT
                      (
                        SELECT COUNT(t.id)
                        FROM transactions t
                        WHERE '.$monthFilter.'
                      ) as `last`,
                      (
                        SELECT COUNT(t.id)
                        FROM transactions t
                        WHERE UNIX_TIMESTAMP(t.created_at) > '.$nextSunday->timestamp.' - 2592000
                      ) as `current`')[0];        
         }                       
                      
        $this->vars['transactionsStatistic'] = $transactionsStatistic;

        //New carers
        $date = Carbon::now();
        $nextSunday = $date->endOfWeek();
        $newCarersStatistic['week'] = DB::select('SELECT
                      (
                        SELECT COUNT(t.id)
                        FROM carers_profiles t
                        WHERE UNIX_TIMESTAMP(t.created_at)  BETWEEN  '.$nextSunday->timestamp.' - 1209600 AND '.$nextSunday->timestamp.' - 604800
                      ) as `last`,
                      (
                        SELECT COUNT(t.id)
                        FROM carers_profiles t
                        WHERE UNIX_TIMESTAMP(t.created_at) > '.$nextSunday->timestamp.' - 604800
                      ) as `current`')[0];
        $endOfMonth = $date->endOfMonth();
        $newCarersStatistic['month'] = DB::select('SELECT
                      (
                        SELECT COUNT(t.id)
                        FROM carers_profiles t
                        WHERE UNIX_TIMESTAMP(t.created_at)  BETWEEN  '.$endOfMonth->timestamp.' - 5184000 AND '.$endOfMonth->timestamp.' - 2592000
                      ) as `last`,
                      (
                        SELECT COUNT(t.id)
                        FROM carers_profiles t
                        WHERE UNIX_TIMESTAMP(t.created_at) > '.$nextSunday->timestamp.' - 2592000
                      ) as `current`')[0];
                      
			if($filter == TRUE){ 
				$newCarersStatistic['month'] = DB::select('SELECT
                      (
                        SELECT COUNT(t.id)
                        FROM carers_profiles t
                        WHERE '.$monthFilter.'
                      ) as `last`,
                      (
                        SELECT COUNT(t.id)
                        FROM carers_profiles t
                        WHERE UNIX_TIMESTAMP(t.created_at) > '.$nextSunday->timestamp.' - 2592000
                      ) as `current`')[0]; 
		   }               
                      
                      
        $this->vars['newCarersStatistic'] = $newCarersStatistic;

        //New purchasers
        $date = Carbon::now();
        $nextSunday = $date->endOfWeek();
        $newPurchaserStatistic['week'] = DB::select('SELECT
                      (
                        SELECT COUNT(t.id)
                        FROM purchasers_profiles t
                        WHERE UNIX_TIMESTAMP(t.created_at)  BETWEEN  '.$nextSunday->timestamp.' - 1209600 AND '.$nextSunday->timestamp.' - 604800
                      ) as `last`,
                      (
                        SELECT COUNT(t.id)
                        FROM purchasers_profiles t
                        WHERE UNIX_TIMESTAMP(t.created_at) > '.$nextSunday->timestamp.' - 604800
                      ) as `current`')[0];
        $endOfMonth = $date->endOfMonth();
        $newPurchaserStatistic['month'] = DB::select('SELECT
                      (
                        SELECT COUNT(t.id)
                        FROM purchasers_profiles t
                        WHERE UNIX_TIMESTAMP(t.created_at)  BETWEEN  '.$endOfMonth->timestamp.' - 5184000 AND '.$endOfMonth->timestamp.' - 2592000
                      ) as `last`,
                      (
                        SELECT COUNT(t.id)
                        FROM purchasers_profiles t
                        WHERE UNIX_TIMESTAMP(t.created_at) > '.$nextSunday->timestamp.' - 2592000
                      ) as `current`')[0];
                      
			if($filter == TRUE){ 
				$newPurchaserStatistic['month'] = DB::select('SELECT
                      (
                        SELECT COUNT(t.id)
                        FROM purchasers_profiles t
                        WHERE '.$monthFilter.'
                      ) as `last`,
                      (
                        SELECT COUNT(t.id)
                        FROM purchasers_profiles t
                        WHERE UNIX_TIMESTAMP(t.created_at) > '.$nextSunday->timestamp.' - 2592000
                      ) as `current`')[0];    
		   }                                
                      
        $this->vars['newPurchaserStatistic'] = $newPurchaserStatistic;


        //Age
        $ageStatistic['purchasers'] = DB::select('SELECT
          (SELECT COUNT(id) FROM purchasers_profiles WHERE YEAR(NOW()) - YEAR(DoB) <= 19 '.$genderFilter.') as `19`,
          (SELECT COUNT(id) FROM purchasers_profiles WHERE YEAR(NOW()) - YEAR(DoB) BETWEEN 20 AND 39 '.$genderFilter.') as 20_39,
          (SELECT COUNT(id) FROM purchasers_profiles WHERE YEAR(NOW()) - YEAR(DoB) BETWEEN 40 AND 59 '.$genderFilter.') as 40_59,
          (SELECT COUNT(id) FROM purchasers_profiles WHERE YEAR(NOW()) - YEAR(DoB) BETWEEN 60 AND 79 '.$genderFilter.') as 60_79,
          (SELECT COUNT(id) FROM purchasers_profiles WHERE YEAR(NOW()) - YEAR(DoB)  >= 80 '.$genderFilter.') as `80` ')[0];
        $ageStatistic['service_users'] = DB::select('SELECT
          (SELECT COUNT(service_users_profiles.id) FROM service_users_profiles JOIN purchasers_profiles ON purchasers_profiles.id=service_users_profiles.purchaser_id WHERE YEAR(NOW()) - YEAR(service_users_profiles.DoB) <= 19 '.$ageServiceFilter.') as `19`,
          (SELECT COUNT(service_users_profiles.id) FROM service_users_profiles JOIN purchasers_profiles ON purchasers_profiles.id=service_users_profiles.purchaser_id WHERE YEAR(NOW()) - YEAR(service_users_profiles.DoB) BETWEEN 20 AND 39 '.$ageServiceFilter.') as 20_39,
          (SELECT COUNT(service_users_profiles.id) FROM service_users_profiles JOIN purchasers_profiles ON purchasers_profiles.id=service_users_profiles.purchaser_id WHERE YEAR(NOW()) - YEAR(service_users_profiles.DoB) BETWEEN 40 AND 59 '.$ageServiceFilter.') as 40_59,
          (SELECT COUNT(service_users_profiles.id) FROM service_users_profiles JOIN purchasers_profiles ON purchasers_profiles.id=service_users_profiles.purchaser_id WHERE YEAR(NOW()) - YEAR(service_users_profiles.DoB) BETWEEN 60 AND 79 '.$ageServiceFilter.') as 60_79,
          (SELECT COUNT(service_users_profiles.id) FROM service_users_profiles JOIN purchasers_profiles ON purchasers_profiles.id=service_users_profiles.purchaser_id WHERE YEAR(NOW()) - YEAR(service_users_profiles.DoB)  >= 80 '.$ageServiceFilter.') as `80` ')[0];
        $ageStatistic['carers'] = DB::select('SELECT
          (SELECT COUNT(id) FROM carers_profiles WHERE YEAR(NOW()) - YEAR(DoB) <= 19 '.$genderFilter.') as `19`,
          (SELECT COUNT(id) FROM carers_profiles WHERE YEAR(NOW()) - YEAR(DoB) BETWEEN 20 AND 39 '.$genderFilter.') as 20_39,
          (SELECT COUNT(id) FROM carers_profiles WHERE YEAR(NOW()) - YEAR(DoB) BETWEEN 40 AND 59 '.$genderFilter.') as 40_59,
          (SELECT COUNT(id) FROM carers_profiles WHERE YEAR(NOW()) - YEAR(DoB) BETWEEN 60 AND 79 '.$genderFilter.') as 60_79,
          (SELECT COUNT(id) FROM carers_profiles WHERE YEAR(NOW()) - YEAR(DoB)  >= 80 '.$genderFilter.') as `80` ')[0];
        $this->vars['ageStatistic'] = $ageStatistic;

        //Gender
        $genderStatistic['purchasers'] = DB::select('SELECT
          (SELECT COUNT(id) FROM purchasers_profiles WHERE LOWER(gender) = "male" ' .$genderFilter.') as male,
          (SELECT COUNT(id) FROM purchasers_profiles WHERE LOWER(gender) = "female" ' .$genderFilter.') as female')[0];
        $genderStatistic['service_users'] = DB::select('SELECT
          (SELECT COUNT(service_users_profiles.id) FROM service_users_profiles JOIN purchasers_profiles ON purchasers_profiles.id=service_users_profiles.purchaser_id WHERE LOWER(service_users_profiles.gender) = "male" ' .$ageServiceFilter.' ) as male,
          (SELECT COUNT(service_users_profiles.id) FROM service_users_profiles JOIN purchasers_profiles ON purchasers_profiles.id=service_users_profiles.purchaser_id WHERE LOWER(service_users_profiles.gender) = "female" ' .$ageServiceFilter.') as female')[0];
        $genderStatistic['carers'] = DB::select('SELECT
          (SELECT COUNT(id) FROM carers_profiles WHERE LOWER(gender) = "male" '.$genderFilter.' ) as male,
          (SELECT COUNT(id) FROM carers_profiles WHERE LOWER(gender) = "female" '.$genderFilter.') as female')[0];
        $this->vars['genderStatistic'] = $genderStatistic;

        //chart
        $dataForTransactionsChart = DB::select('SELECT COUNT(id) as amount, DATE_FORMAT(created_at, \'%M, %Y\') as month FROM transactions GROUP BY month ORDER BY month');
        $this->vars['dataForTransactionsChart'] = $dataForTransactionsChart;

        $this->content = view(config('settings.theme').'.statistic')->with($this->vars)->render();

        return $this->renderOutput();
    }


	//export data in csv 
    public function exportStaticticsToXls()
    {
      
        $input 	= Input::all();
        $filter = FALSE;
		$dateFrom = $dateTo ="";
        if (isset($input['dateFrom']) && $input['dateFrom'] > '') {
            $input['dateFrom'] .= " 00:00:00";
             $dateFrom = strtotime($input['dateFrom']);
            $dateFrom = date("Y-m-d H:i:s", $dateFrom);
             $filter = TRUE; 
        } else {
            $dateFrom = FALSE;
        }

        if (isset($input['dateTo']) && $input['dateTo'] > '') {
            $input['dateTo'] .= " 00:00:00";
            $dateTo = strtotime($input['dateTo']);
            $dateTo = date("Y-m-d H:i:s", $dateTo);
            $filter = TRUE; 
        } else {
            $dateTo = FALSE;
        }
		
		$bookingsStatisticFilter = $bookingsStatisticTotalFilter = $userAmountFilter = $monthFilter = $appointmentFilter = $genderFilter = $ageServiceFilter =  "" ;
		$this->vars['dateRange'] ="Month";
		$statistic_title='Statistic';
		
		if (!empty($dateFrom) && !empty($dateTo)) {
		  $bookingsStatisticFilter = "AND b.created_at >= '".$dateFrom."' AND b.created_at <= '".$dateTo."'";
		  $bookingsStatisticTotalFilter ="AND b.created_at >= '".$dateFrom."' AND b.created_at <= '".$dateTo."'";
		  $purchasersAmountFilter ="AND p.created_at >= '".$dateFrom."' AND p.created_at <= '".$dateTo."'";
		  $userAmountFilter ="AND p.created_at >= '".$dateFrom."' AND p.created_at <= '".$dateTo."'";
		  $appointmentFilter ="AND a.date_start >='".$dateFrom."' AND a.date_start <='".$dateTo."'"; 
		  $monthFilter ="t.created_at >= '".$dateFrom."' AND t.created_at <= '".$dateTo."'";
		  $this->vars['dateRange'] = date("d/m/Y", strtotime($dateFrom))." - ".date("d/m/Y", strtotime($dateTo)); 
		  $statistic_title ='Statistic_('.date("d/m/Y", strtotime($dateFrom))."-".date("d/m/Y", strtotime($dateTo)).')';
		 // $genderFilter = " AND DATE('created_at') = '".date('Y-m-d', strtotime($dateTo))."'";
           $genderFilter =" AND  (created_at <= '".$dateTo."' )";
           $ageServiceFilter = " AND  ( purchasers_profiles.created_at <= '".$dateTo."' )";
		  
        }elseif ( !empty($dateFrom)) {
           $bookingsStatisticFilter = "AND b.created_at >= '".$dateFrom."'";
           $bookingsStatisticTotalFilter = "AND b.created_at >= '".$dateFrom."'";
           $userAmountFilter = "AND p.created_at >= '".$dateFrom."'";
           $monthFilter = "t.created_at >= '".$dateFrom."'";
           $appointmentFilter = "AND a.date_start >= '".$dateFrom."'";
           $this->vars['dateRange'] = 'From '.date("d/m/Y", strtotime($dateFrom)); 
           $statistic_title='Statistic_From('.date("d/m/Y", strtotime($dateFrom)).')';
          // $genderFilter = " AND DATE('created_at') = '".$dateFrom."' ";
          // $ageServiceFilter = " AND DATE('purchasers_profiles.created_at') = '".date('Y-m-d', strtotime($dateFrom))."'";
          
           $genderFilter =" AND   created_at <= '".$dateFrom."' ";
           $ageServiceFilter = " AND  purchasers_profiles.created_at <= '".$dateFrom."'";

        }elseif (!empty($dateTo)) {
           $bookingsStatisticFilter = "AND b.created_at <= '".$dateTo."'";
           $bookingsStatisticTotalFilter = "AND b.created_at <= '".$dateTo."'";
           $userAmountFilter = "AND p.created_at <= '".$dateTo."'";
           $appointmentFilter = "AND a.date_start <= '".$dateTo."'";
           $monthFilter = "t.created_at <= '".$dateTo."'";
           $this->vars['dateRange'] = 'Till '.date("d/m/Y", strtotime($dateTo)); 
           $statistic_title ='Statistic_Till('.date("d/m/Y", strtotime($dateTo)).')';
          // $genderFilter = " AND DATE('created_at') = '".$dateTo."' ";
           //$ageServiceFilter = " AND DATE('purchasers_profiles.created_at') = '".date('Y-m-d', strtotime($dateTo))."'";
          
           $genderFilter =" AND created_at <= '".$dateTo."' ";
           $ageServiceFilter = " AND  purchasers_profiles.created_at <= '".$dateTo."'";
        }
      
    

        $bookingsStatistic = DB::select('SELECT
              SUM(a.price) as price,
              COUNT(a.price) as amount,
              b.status_id
            FROM bookings b
              JOIN (SELECT booking_id, SUM(price_for_purchaser) as price FROM appointments GROUP BY booking_id) AS a ON a.booking_id = b.id
            WHERE b.status_id  IN (2, 5, 7) '.$bookingsStatisticFilter.'
            GROUP BY b.status_id
            ORDER BY b.status_id');
        $this->vars['bookingsStatistic'] = $bookingsStatistic;

        $bookingsStatisticTotal = DB::select('SELECT
              SUM(a.price) as price,
              COUNT(a.price) as amount
            FROM bookings b
              JOIN (SELECT booking_id, SUM(price_for_purchaser) as price FROM appointments GROUP BY booking_id) AS a ON a.booking_id = b.id
            WHERE b.status_id  IN (2, 5, 7) '.$bookingsStatisticTotalFilter);
        $this->vars['bookingsStatisticTotal'] = $bookingsStatisticTotal;

        //User types
        $usersStatistic['purchasers_amount'] = DB::select("SELECT COUNT(p.id) as count
            FROM  purchasers_profiles p WHERE p.registration_status = 'completed' ".$userAmountFilter)[0]->count;
        $usersStatistic['carers_amount'] = DB::select("SELECT COUNT(p.id) as count
             FROM carers_profiles p WHERE  p.registration_status = 'completed' ".$userAmountFilter)[0]->count;
        $usersStatistic['service_users_profiles'] = DB::select("SELECT COUNT(p.id) as count
             FROM service_users_profiles p WHERE  p.registration_status = 'completed' ".$userAmountFilter)[0]->count;
        $usersStatistic['incomplete_purchasers_amount'] = DB::select("SELECT COUNT(p.id) as count
            FROM  purchasers_profiles p WHERE p.registration_status != 'completed' ".$userAmountFilter)[0]->count;
        $usersStatistic['incomplete_carers_amount'] = DB::select("SELECT COUNT(p.id) as count
             FROM carers_profiles p WHERE  p.registration_status != 'completed' ".$userAmountFilter)[0]->count;
        $usersStatistic['incomplete_service_users_profiles'] = DB::select("SELECT COUNT(p.id) as count
             FROM service_users_profiles p WHERE  p.registration_status != 'completed' ".$userAmountFilter)[0]->count;
        $this->vars['usersStatistic'] = $usersStatistic;

       //Most active carers
        //AND UNIX_TIMESTAMP(NOW()) - UNIX_TIMESTAMP(a.date_start) <= 604800
        //AND UNIX_TIMESTAMP(NOW()) - UNIX_TIMESTAMP(a.date_start) <= 2592000
        $mostActiveCarers = DB::select('SELECT u.id, p.first_name, p.family_name,
                              (
                                SELECT COUNT(a.id)
                                FROM appointments a
                                JOIN bookings b ON a.booking_id = b.id
                                JOIN carers_profiles p ON b.carer_id = p.id
                                WHERE p.id = u.id AND a.status_id = 4  '.$appointmentFilter.'
                              ) as appointments_per_last_week,
                              (
                                SELECT COUNT(a.id)
                                FROM appointments a
                                  JOIN bookings b ON a.booking_id = b.id
                                  JOIN carers_profiles p ON b.carer_id = p.id
                                WHERE p.id = u.id AND a.status_id = 4  '.$appointmentFilter.'
                              ) as appointments_per_last_month
                              FROM users u
                              JOIN carers_profiles p ON p.id = u.id WHERE u.user_type_id = 3 '.$userAmountFilter.'
                            ORDER BY appointments_per_last_week DESC LIMIT 5');
        $this->vars['mostActiveCarers'] = $mostActiveCarers;
     
        

        //Most active purchasers
        //AND UNIX_TIMESTAMP(NOW()) - UNIX_TIMESTAMP(a.date_start) <= 604800
        //AND UNIX_TIMESTAMP(NOW()) - UNIX_TIMESTAMP(a.date_start) <= 2592000
        $mostActivePurchasers = DB::select('SELECT u.id, p.first_name, p.family_name,
                          (
                            SELECT COUNT(a.id)
                            FROM appointments a
                              JOIN bookings b ON a.booking_id = b.id
                              JOIN purchasers_profiles p ON b.purchaser_id = p.id
                            WHERE p.id = u.id AND a.status_id = 4  '.$appointmentFilter.'
                          ) as appointments_per_last_week,
                          (
                            SELECT COUNT(a.id)
                            FROM appointments a
                              JOIN bookings b ON a.booking_id = b.id
                              JOIN purchasers_profiles p ON b.purchaser_id = p.id
                            WHERE p.id = u.id AND a.status_id = 4  '.$appointmentFilter.'
                          ) as appointments_per_last_month
                        FROM users u
                          JOIN purchasers_profiles p ON p.id = u.id  WHERE u.user_type_id = 1 '.$userAmountFilter.'
                        ORDER BY appointments_per_last_week DESC LIMIT 5;');
        $this->vars['mostActivePurchasers'] = $mostActivePurchasers;


        //Income
        $date = Carbon::now();
        $nextSunday = $date->endOfWeek();
        $incomeStatistic['week'] = DB::select('SELECT
                      (
                        SELECT SUM(t.amount)
                        FROM transactions t
                        WHERE UNIX_TIMESTAMP(t.created_at)  BETWEEN  '.$nextSunday->timestamp.' - 1209600 AND '.$nextSunday->timestamp.' - 604800
                      ) as `last`,
                      (
                        SELECT SUM(t.amount)
                        FROM transactions t
                        WHERE UNIX_TIMESTAMP(t.created_at) > '.$nextSunday->timestamp.' - 604800
                      ) as `current`')[0];
        $endOfMonth = $date->endOfMonth();

        $incomeStatistic['month'] = DB::select('SELECT
                      (
                        SELECT SUM(t.amount)
                        FROM transactions t
                        WHERE UNIX_TIMESTAMP(t.created_at)  BETWEEN  '.$endOfMonth->timestamp.' - 5184000 AND '.$endOfMonth->timestamp.' - 2592000
                      ) as `last`,
                      (
                        SELECT SUM(t.amount)
                        FROM transactions t
                        WHERE UNIX_TIMESTAMP(t.created_at) > '.$nextSunday->timestamp.' - 2592000
                      ) as `current`')[0];
        
        if($filter == TRUE){             
			$incomeStatistic['month'] = DB::select('SELECT
						  (
							SELECT SUM(t.amount)
							FROM transactions t
							WHERE '.$monthFilter.'  
						  ) as `last`')[0];
                      
          }            
        $this->vars['incomeStatistic'] = $incomeStatistic;

        //Transactions amount
        $date = Carbon::now();
        $nextSunday = $date->endOfWeek();
        $transactionsStatistic['week'] = DB::select('SELECT
                      (
                        SELECT COUNT(t.id)
                        FROM transactions t
                        WHERE UNIX_TIMESTAMP(t.created_at)  BETWEEN  '.$nextSunday->timestamp.' - 1209600 AND '.$nextSunday->timestamp.' - 604800
                      ) as `last`,
                      (
                        SELECT COUNT(t.id)
                        FROM transactions t
                        WHERE UNIX_TIMESTAMP(t.created_at) > '.$nextSunday->timestamp.' - 604800
                      ) as `current`')[0];
        $endOfMonth = $date->endOfMonth();
        $transactionsStatistic['month'] = DB::select('SELECT
                      (
                        SELECT COUNT(t.id)
                        FROM transactions t
                        WHERE UNIX_TIMESTAMP(t.created_at)  BETWEEN  '.$endOfMonth->timestamp.' - 5184000 AND '.$endOfMonth->timestamp.' - 2592000
                      ) as `last`,
                      (
                        SELECT COUNT(t.id)
                        FROM transactions t
                        WHERE UNIX_TIMESTAMP(t.created_at) > '.$nextSunday->timestamp.' - 2592000
                      ) as `current`')[0];
                      
         if($filter == TRUE){                            
        $transactionsStatistic['month'] = DB::select('SELECT
                      (
                        SELECT COUNT(t.id)
                        FROM transactions t
                        WHERE '.$monthFilter.'  
                      ) as `last`')[0];
                      
          }            
        $this->vars['transactionsStatistic'] = $transactionsStatistic;

        //New carers
        $date = Carbon::now();
        $nextSunday = $date->endOfWeek();
        $newCarersStatistic['week'] = DB::select('SELECT
                      (
                        SELECT COUNT(t.id)
                        FROM carers_profiles t
                        WHERE UNIX_TIMESTAMP(t.created_at)  BETWEEN  '.$nextSunday->timestamp.' - 1209600 AND '.$nextSunday->timestamp.' - 604800
                      ) as `last`,
                      (
                        SELECT COUNT(t.id)
                        FROM carers_profiles t
                        WHERE UNIX_TIMESTAMP(t.created_at) > '.$nextSunday->timestamp.' - 604800
                      ) as `current`')[0];
        $endOfMonth = $date->endOfMonth();
        $newCarersStatistic['month'] = DB::select('SELECT
                      (
                        SELECT COUNT(t.id)
                        FROM carers_profiles t
                        WHERE UNIX_TIMESTAMP(t.created_at)  BETWEEN  '.$endOfMonth->timestamp.' - 5184000 AND '.$endOfMonth->timestamp.' - 2592000
                      ) as `last`,
                      (
                        SELECT COUNT(t.id)
                        FROM carers_profiles t
                        WHERE UNIX_TIMESTAMP(t.created_at) > '.$nextSunday->timestamp.' - 2592000
                      ) as `current`')[0];
                      
                      
       if($filter == TRUE){ 
		    $newCarersStatistic['month'] = DB::select('SELECT
                      (
                        SELECT COUNT(t.id)
                        FROM carers_profiles t
                        WHERE '.$monthFilter.' 
                      ) as `last`')[0];
		   
	   }               
                      
        $this->vars['newCarersStatistic'] = $newCarersStatistic;

        //New purchasers
        $date = Carbon::now();
        $nextSunday = $date->endOfWeek();
        $newPurchaserStatistic['week'] = DB::select('SELECT
                      (
                        SELECT COUNT(t.id)
                        FROM purchasers_profiles t
                        WHERE UNIX_TIMESTAMP(t.created_at)  BETWEEN  '.$nextSunday->timestamp.' - 1209600 AND '.$nextSunday->timestamp.' - 604800
                      ) as `last`,
                      (
                        SELECT COUNT(t.id)
                        FROM purchasers_profiles t
                        WHERE UNIX_TIMESTAMP(t.created_at) > '.$nextSunday->timestamp.' - 604800
                      ) as `current`')[0];
        $endOfMonth = $date->endOfMonth();
        $newPurchaserStatistic['month'] = DB::select('SELECT
                      (
                        SELECT COUNT(t.id)
                        FROM purchasers_profiles t
                        WHERE UNIX_TIMESTAMP(t.created_at)  BETWEEN  '.$endOfMonth->timestamp.' - 5184000 AND '.$endOfMonth->timestamp.' - 2592000
                      ) as `last`,
                      (
                        SELECT COUNT(t.id)
                        FROM purchasers_profiles t
                        WHERE UNIX_TIMESTAMP(t.created_at) > '.$nextSunday->timestamp.' - 2592000
                      ) as `current`')[0];
                      
           if($filter == TRUE){ 
			    $newPurchaserStatistic['month'] = DB::select('SELECT
                      (
                        SELECT COUNT(t.id)
                        FROM purchasers_profiles t
                        WHERE '.$monthFilter.'
                      ) as `last`')[0];      
                      
		   }            
           
        $this->vars['newPurchaserStatistic'] = $newPurchaserStatistic;


        //Age
        $ageStatistic['purchasers'] = DB::select('SELECT
          (SELECT COUNT(id) FROM purchasers_profiles WHERE YEAR(NOW()) - YEAR(DoB) <= 19 '.$genderFilter.') as `19`,
          (SELECT COUNT(id) FROM purchasers_profiles WHERE YEAR(NOW()) - YEAR(DoB) BETWEEN 20 AND 39 '.$genderFilter.') as 20_39,
          (SELECT COUNT(id) FROM purchasers_profiles WHERE YEAR(NOW()) - YEAR(DoB) BETWEEN 40 AND 59 '.$genderFilter.') as 40_59,
          (SELECT COUNT(id) FROM purchasers_profiles WHERE YEAR(NOW()) - YEAR(DoB) BETWEEN 60 AND 79 '.$genderFilter.') as 60_79,
          (SELECT COUNT(id) FROM purchasers_profiles WHERE YEAR(NOW()) - YEAR(DoB)  >= 80 '.$genderFilter.') as `80` ')[0];
        $ageStatistic['service_users'] = DB::select('SELECT
          (SELECT COUNT(service_users_profiles.id) FROM service_users_profiles JOIN purchasers_profiles ON purchasers_profiles.id=service_users_profiles.purchaser_id WHERE YEAR(NOW()) - YEAR(service_users_profiles.DoB) <= 19 '.$ageServiceFilter.') as `19`,
          (SELECT COUNT(service_users_profiles.id) FROM service_users_profiles JOIN purchasers_profiles ON purchasers_profiles.id=service_users_profiles.purchaser_id WHERE YEAR(NOW()) - YEAR(service_users_profiles.DoB) BETWEEN 20 AND 39 '.$ageServiceFilter.') as 20_39,
          (SELECT COUNT(service_users_profiles.id) FROM service_users_profiles JOIN purchasers_profiles ON purchasers_profiles.id=service_users_profiles.purchaser_id WHERE YEAR(NOW()) - YEAR(service_users_profiles.DoB) BETWEEN 40 AND 59 '.$ageServiceFilter.') as 40_59,
          (SELECT COUNT(service_users_profiles.id) FROM service_users_profiles JOIN purchasers_profiles ON purchasers_profiles.id=service_users_profiles.purchaser_id WHERE YEAR(NOW()) - YEAR(service_users_profiles.DoB) BETWEEN 60 AND 79 '.$ageServiceFilter.') as 60_79,
          (SELECT COUNT(service_users_profiles.id) FROM service_users_profiles JOIN purchasers_profiles ON purchasers_profiles.id=service_users_profiles.purchaser_id WHERE YEAR(NOW()) - YEAR(service_users_profiles.DoB)  >= 80 '.$ageServiceFilter.') as `80` ')[0];
        $ageStatistic['carers'] = DB::select('SELECT
          (SELECT COUNT(id) FROM carers_profiles WHERE YEAR(NOW()) - YEAR(DoB) <= 19 '.$genderFilter.') as `19`,
          (SELECT COUNT(id) FROM carers_profiles WHERE YEAR(NOW()) - YEAR(DoB) BETWEEN 20 AND 39 '.$genderFilter.') as 20_39,
          (SELECT COUNT(id) FROM carers_profiles WHERE YEAR(NOW()) - YEAR(DoB) BETWEEN 40 AND 59 '.$genderFilter.') as 40_59,
          (SELECT COUNT(id) FROM carers_profiles WHERE YEAR(NOW()) - YEAR(DoB) BETWEEN 60 AND 79 '.$genderFilter.') as 60_79,
          (SELECT COUNT(id) FROM carers_profiles WHERE YEAR(NOW()) - YEAR(DoB)  >= 80 '.$genderFilter.') as `80` ')[0];
        $this->vars['ageStatistic'] = $ageStatistic;

        //Gender
        $genderStatistic['purchasers'] = DB::select('SELECT
          (SELECT COUNT(id) FROM purchasers_profiles WHERE LOWER(gender) = "male" ' .$genderFilter.') as male,
          (SELECT COUNT(id) FROM purchasers_profiles WHERE LOWER(gender) = "female" ' .$genderFilter.') as female')[0];
      
     
      
      
        $genderStatistic['service_users'] = DB::select('SELECT
          (SELECT COUNT(service_users_profiles.id) FROM service_users_profiles JOIN purchasers_profiles ON purchasers_profiles.id=service_users_profiles.purchaser_id WHERE LOWER(service_users_profiles.gender) = "male" ' .$ageServiceFilter.' ) as male,
          (SELECT COUNT(service_users_profiles.id) FROM service_users_profiles JOIN purchasers_profiles ON purchasers_profiles.id=service_users_profiles.purchaser_id WHERE LOWER(service_users_profiles.gender) = "female" ' .$ageServiceFilter.') as female')[0];
        $genderStatistic['carers'] = DB::select('SELECT
          (SELECT COUNT(id) FROM carers_profiles WHERE LOWER(gender) = "male" '.$genderFilter.' ) as male,
          (SELECT COUNT(id) FROM carers_profiles WHERE LOWER(gender) = "female" '.$genderFilter.') as female')[0];
        $this->vars['genderStatistic'] = $genderStatistic;

        //chart
        $dataForTransactionsChart = DB::select('SELECT COUNT(id) as amount, DATE_FORMAT(created_at, \'%M, %Y\') as month FROM transactions GROUP BY month ORDER BY month');
        $this->vars['dataForTransactionsChart'] = $dataForTransactionsChart;
        $data = $this->vars;

        Excel::create( 'Statistic' , function($excel) use($data) {
            $excel->sheet('Statistic' , function($sheet) use ($data) {
                $sheet->loadView('HolmAdmin.statisticsToXls')
                      ->with([
                          'bookingsStatistic'       => $data['bookingsStatistic'],
                          'bookingsStatisticTotal'  => $data['bookingsStatisticTotal'],
                          'usersStatistic'          => $data['usersStatistic'],
                          'mostActiveCarers'        => $data['mostActiveCarers'],
                          'mostActivePurchasers'    => $data['mostActivePurchasers'],
                          'incomeStatistic'         => $data['incomeStatistic'],
                          'transactionsStatistic'   => $data['transactionsStatistic'],
                          'newCarersStatistic'      => $data['newCarersStatistic'],
                          'newPurchaserStatistic'   => $data['newPurchaserStatistic'],
                          'genderStatistic'         => $data['genderStatistic'],
                          'ageStatistic'            => $data['ageStatistic'],
                          'daterange'               => $data['dateRange'],
                      ]);
                      
            });
        })->download('xlsx');
    }
}
