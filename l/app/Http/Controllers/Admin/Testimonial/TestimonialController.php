<?php

namespace App\Http\Controllers\Admin\Testimonial;
use App\Http\Controllers\Admin\AdminController;

use App\Testimonial;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Page;


class TestimonialController extends AdminController
{
    private $testimonial;

    public function __construct(Testimonial $testimonial) {
        parent::__construct();
        $this->testimonial = $testimonial;


        $this->template = config('settings.theme').'.templates.adminBase';
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->title = 'Admin Testimonial';
        $testimonials = Testimonial::orderBy('id', 'desc')->get();
        $this->vars = array_add($this->vars,'testimonials',$testimonials);

        $this->content = view(config('settings.theme').'.testimonial.testimonial')->with($this->vars)->render();
        return $this->renderOutput();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->title = 'Admin Testimonial';
        $this->content = view(config('settings.theme') . '.testimonial.create')->with($this->vars)->render();

        return $this->renderOutput();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'client_name' => 'required'
         
        ]);

        $testimonial = new Testimonial;
        $testimonial->client_name = $request->client_name;
        $testimonial->page_id = $request->page_id;
        $testimonial->description = $request->description;
        $testimonial->client_info = $request->client_info;

      	$itemreq = $request;
      	$download_file_name = "";
          	 if( $itemreq->hasFile('image')){ 
                $image = $itemreq->file('image'); 
                $image_path=base_path('public/image_Association');
                $fileName = $image->getClientOriginalName();
                $fileExtension = $image->getClientOriginalExtension();
                $download_file_name=time().'.'.$fileExtension;
                $image->move($image_path,$download_file_name);
                $testimonial->image_name = $download_file_name;
            }
        $testimonial->save();
        return redirect(route('testimonial.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($testimonialId)
    {
        $this->title = 'Admin Testimonial';
        $testimonial = $this->testimonial->findOrFail($testimonialId);
        $this->vars = array_add($this->vars,'testimonial',$testimonial);
        $this->content = view(config('settings.theme').'.testimonial.edit')->with($this->vars)->render();
        return $this->renderOutput();

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
            
		if (isset($request->delete_testimonial_id)) {
            $testimonial = Testimonial::find($request->delete_testimonial_id);
            $testimonial->delete();
            return response(['status' => 'success']);
        }else{
           
      		$download_file_name = "";
            $testimonial = Testimonial::find($id);
            $testimonial->client_name = $request->client_name;
            $testimonial->page_id = $request->page_id;
            $testimonial->description = $request->description;
            $testimonial->client_info = $request->client_info;
          
        
             $itemreq = $request;
          	 if( $itemreq->hasFile('image')){ 
                $image = $itemreq->file('image'); 
                $image_path=base_path('public/image_Association');
                $fileName = $image->getClientOriginalName();
                $fileExtension = $image->getClientOriginalExtension();
                $download_file_name=time().'.'.$fileExtension;
                $image->move($image_path,$download_file_name);
                $testimonial->image_name = $download_file_name;
            }
            $testimonial->updated_at = Carbon::now();
            $testimonial->save();
            return redirect(route('testimonial.index'));
        }
    }

    
}