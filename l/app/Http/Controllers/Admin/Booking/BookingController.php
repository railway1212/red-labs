<?php

namespace App\Http\Controllers\Admin\Booking;


use App\BookingStatus;
use App\Booking;
use App\User;
use App\Http\Controllers\Admin\AdminController;
use DB ; 

use App\Http\Controllers\Admin\Repo\Models\Booking\AdminBookings;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Input;

use Illuminate\Http\Request;


class BookingController extends AdminController
{
    private $booking;

    public function __construct(AdminBookings $booking)
    {
        parent::__construct();
        $this->booking = $booking;

        $this->template = config('settings.theme') . '.templates.adminBase';
    }

    public function index(Request $request)
    {

        $input = $request->all();
        $filter = FALSE;
        //$orderDirection = 'desc';
        //Проверка, указан ли фильтр менюшки
        if (isset($input['filter']) && $input['filter'] != 0){
            $filter_status = $input['filter'];
        } else {
            $filter_status = FALSE;
        }

        if (isset($input['dateFrom']) && $input['dateFrom'] > '') {
            //Fix add Time in string
            $input['dateFrom'] .= " 00:00:00";
            $dateFrom = strtotime($input['dateFrom']);
            //$dateFrom = date("Y-m-d H:i:s", $timestamp);
        } else {
            $dateFrom = FALSE;
        }

        if (isset($input['dateTo']) && $input['dateTo'] > '') {
            //Fix add Time in string
            $input['dateTo'] .= " 00:00:00";
            $dateTo = strtotime($input['dateTo']);
           // $dateTo = date("Y-m-d H:i:s", $timestamp);
        } else {
            $dateTo = FALSE;
        }

        //Проверка указан ли имя пользователя
        if (isset($input['userName'])) {
            $filter = ['first_name', '=', $input['userName']];
        } else {
            $filter = FALSE;
        }

        $this->title = 'Admin Bookings Details';

//      $bookings = $this->booking->get('*', FALSE, true,  $filter, ['id','desc']);
        $bookings = Booking::with('bookingServiceUser')->with('bookingCarerProfile')->with('bookingPurchaserProfile')->orderBy('updated_at', 'desc')->get();


        $userName = (isset($input['userName'])) ? $input['userName'] : false;
        if (!empty($userName))
            $bookings = $bookings->filter(function ($item) use ($userName) {
                $appointmentsIds = array_pluck($item->appointments, 'id');
                if (strpos(strtolower($item->bookingServiceUser->first_name), strtolower($userName)) !== false
                    || strpos(strtolower($item->bookingCarerProfile->first_name), strtolower($userName)) !== false
                    || strpos(strtolower($item->bookingPurchaserProfile->first_name), strtolower($userName)) !== false
                    || strpos(strtolower($item->bookingServiceUser->family_name), strtolower($userName)) !== false
                    || strpos(strtolower($item->bookingCarerProfile->family_name), strtolower($userName)) !== false
                    || strpos(strtolower($item->bookingPurchaserProfile->family_name), strtolower($userName)) !== false
                    || $item->id == (int)$userName
                    || $item->bookingServiceUser->id == (int)$userName
                    || $item->bookingCarerProfile->id == (int)$userName
                    || $item->bookingPurchaserProfile->id == (int)$userName
                    || in_array((int)$userName, $appointmentsIds)
                    )
                    return true;
            });

        if ($filter_status !== FALSE) {
            $bookings = $bookings->filter(function ($item) use ($filter_status) {
                if ($item->status_id == $filter_status)
                    return true;
            });
        }

        if ($dateFrom !== FALSE) {
            $bookings = $bookings->filter(function ($item) use ($dateFrom) {
               // var_dump($item->created_at);echo "<br>";
                if (strtotime((string)$item->created_at) >= $dateFrom)
                    return true;
            });
        }

        if ($dateTo !== FALSE) {
            $bookings = $bookings->filter(function ($item) use ($dateTo) {
                if (strtotime((string)$item->updated_at) <= $dateTo)
                    return true;
            });
        }


        $page = $request->get('page', 1);
        $perPage = 9;
        $start = ($page - 1) * $perPage;
        if ($page == 1) $start = 0;
        $count = count($bookings);
        if ($count > 0)
            $pages = ceil($count / $perPage);
        else
            $pages = 0;
        $nextPage = $page + 1;
        $previousPage = $page - 1;
        // --------- pagination -----------
        $bookings = $bookings->slice($start, $perPage);
        $this->vars = array_add($this->vars, 'nextPage', $nextPage);
        $this->vars = array_add($this->vars, 'previousPage', $previousPage);
        $this->vars = array_add($this->vars, 'count', $count);
        $this->vars = array_add($this->vars, 'curr_page', $page);
        $this->vars = array_add($this->vars, 'pages', $pages);

        $this->vars = array_add($this->vars, 'link', '/l/admin/booking');
        $query = $request->all();
        if(key_exists('page',$query)) unset($query['page']);
        $this->vars = array_add($this->vars, 'queryString', $query);
        $pagination = view(config('settings.theme') . '.pagination2')->with($this->vars)->render();
        $this->vars = array_add($this->vars, 'pagination', $pagination);
        // --------------------------------
        $this->vars = array_add($this->vars, 'bookings', $bookings);

        $bookingStatuses = BookingStatus::all()->pluck('name', 'id')->toArray();
        $this->vars = array_add($this->vars, 'bookingStatuses', $bookingStatuses);

        $this->content = view(config('settings.theme') . '.bookingsDetails.bookingsDetails')->with($this->vars)->render();

        return $this->renderOutput();
    }
    
    
    //export data 
    public function exportBookingToXls(Request $request)
    {
		
		
		$input 	= Input::all();
        $filter = FALSE;
   
        //Проверка, указан ли фильтр менюшки
        if (isset($input['filter']) && $input['filter'] != 0){
            $filter_status = $input['filter'];
        } else {
            $filter_status = FALSE;
        }

        if (isset($input['dateFrom']) && $input['dateFrom'] > '') {
            //Fix add Time in string
            $input['dateFrom'] .= " 00:00:00";
            $dateFrom = strtotime($input['dateFrom']);
            //$dateFrom = date("Y-m-d H:i:s", $timestamp);
        } else {
            $dateFrom = FALSE;
        }

        if (isset($input['dateTo']) && $input['dateTo'] > '') {
            //Fix add Time in string
            $input['dateTo'] .= " 00:00:00";
            $dateTo = strtotime($input['dateTo']);
           // $dateTo = date("Y-m-d H:i:s", $timestamp);
        } else {
            $dateTo = FALSE;
        }

        //Проверка указан ли имя пользователя
        if (isset($input['userName'])) {
            $filter = ['first_name', '=', $input['userName']];
        } else {
            $filter = FALSE;
        }


	   $bookings = Booking::with('bookingServiceUser')->with('bookingCarerProfile')->with('bookingPurchaserProfile')->orderBy('updated_at', 'desc')->get();


        $userName = (isset($input['userName'])) ? $input['userName'] : false;
        if (!empty($userName))
            $bookings = $bookings->filter(function ($item) use ($userName) {
                $appointmentsIds = array_pluck($item->appointments, 'id');
                if (strpos(strtolower($item->bookingServiceUser->first_name), strtolower($userName)) !== false
                    || strpos(strtolower($item->bookingCarerProfile->first_name), strtolower($userName)) !== false
                    || strpos(strtolower($item->bookingPurchaserProfile->first_name), strtolower($userName)) !== false
                    || strpos(strtolower($item->bookingServiceUser->family_name), strtolower($userName)) !== false
                    || strpos(strtolower($item->bookingCarerProfile->family_name), strtolower($userName)) !== false
                    || strpos(strtolower($item->bookingPurchaserProfile->family_name), strtolower($userName)) !== false
                    || $item->id == (int)$userName
                    || $item->bookingServiceUser->id == (int)$userName
                    || $item->bookingCarerProfile->id == (int)$userName
                    || $item->bookingPurchaserProfile->id == (int)$userName
                    || in_array((int)$userName, $appointmentsIds)
                    )
                    return true;
            });

        if ($filter_status !== FALSE) {
            $bookings = $bookings->filter(function ($item) use ($filter_status) {
                if ($item->status_id == $filter_status)
                    return true;
            });
        }

        if ($dateFrom !== FALSE) {
            $bookings = $bookings->filter(function ($item) use ($dateFrom) {
               // var_dump($item->created_at);echo "<br>";
                if (strtotime((string)$item->created_at) >= $dateFrom)
                    return true;
            });
        }

        if ($dateTo !== FALSE) {
            $bookings = $bookings->filter(function ($item) use ($dateTo) {
                if (strtotime((string)$item->updated_at) <= $dateTo)
                    return true;
            });
        }
        $data = $bookings;
		
		Excel::create('Booking', function($excel) use($data) {
            $excel->sheet('Booking', function($sheet) use ($data) {
                $sheet->loadView('HolmAdmin.bookingToXls')
                      ->with([
                          'bookings'       => $data
                      ]);
            });
        })->download('csv');

	}
	
	  //export Accountant data
    public function exportAccountantReportToXls(Request $request){
		$input 	= Input::all();
        $filter = FALSE;
		$dateFrom = $dateTo ="";
        //Проверка, указан ли фильтр менюшки
       

        if (isset($input['dateFrom']) && $input['dateFrom'] > '') {
            $input['dateFrom'] .= " 00:00:00";
             $dateFrom = strtotime($input['dateFrom']);
            $dateFrom = date("Y-m-d H:i:s", $dateFrom);
             $filter = TRUE; 
        } else {
            $dateFrom = FALSE;
        }

        if (isset($input['dateTo']) && $input['dateTo'] > '') {
            $input['dateTo'] .= " 00:00:00";
            $dateTo = strtotime($input['dateTo']);
            $dateTo = date("Y-m-d H:i:s", $dateTo);
            $filter = TRUE; 
        } else {
            $dateTo = FALSE;
        }
		

		$bookings = DB::select("select bookings.id,bookings.created_at,bookings.purchaser_id,bookings.carer_id,stripe_charges.id as trans_id,stripe_charges.created_at as tr_created,stripe_charges.amount as amount, 'purchaser' as status  from bookings inner join stripe_charges on stripe_charges.booking_id = bookings.id 
		union 
		select bookings.id ,bookings.created_at,bookings.purchaser_id,bookings.carer_id,transactions.id as trans_id,transactions.created_at as tr_created, transactions.amount as amount, 'purchaser' as status from bookings inner join transactions  on transactions.booking_id = bookings.id where  transactions.payment_method='bonus'
		union
		select bookings.id,bookings.created_at,bookings.purchaser_id,bookings.carer_id,stripe_transfers.id as trans_id ,stripe_transfers.created_at as tr_created, stripe_transfers.amount as amount,'carer' as status from bookings inner join stripe_transfers on stripe_transfers.booking_id = bookings.id ORDER BY tr_created  DESC
		");
		
        if (!empty($dateFrom) && !empty($dateTo)) {
			 $dateText1 = "stripe_charges.created_at >= '".$dateFrom."' AND stripe_charges.created_at <= '".$dateTo."'";
			 $dateText2 = "transactions.created_at >= '".$dateFrom."' AND transactions.created_at <= '".$dateTo."'";
			 $dateText3 = "stripe_transfers.created_at >= '".$dateFrom."' AND stripe_transfers.created_at <= '".$dateTo."'";

        }elseif ( !empty($dateFrom)) {
           $dateText1 = "stripe_charges.created_at >= '".$dateFrom."'";
           $dateText2 = "transactions.created_at >= '".$dateFrom."'";
           $dateText3 = "stripe_transfers.created_at >= '".$dateFrom."'";
           
        }elseif (!empty($dateTo)) {
           $dateText1 = "stripe_charges.created_at <= '".$dateTo."'";
           $dateText2 = "transactions.created_at <= '".$dateTo."'";
           $dateText3 = "stripe_transfers.created_at <= '".$dateTo."'";

        }

        if($filter== TRUE){
		$bookings = DB::select("(select bookings.id,bookings.created_at,bookings.purchaser_id,bookings.carer_id,stripe_charges.id as trans_id,stripe_charges.created_at as tr_created,stripe_charges.amount as amount, 'purchaser' as status  from bookings inner join stripe_charges on stripe_charges.booking_id = bookings.id where ".$dateText1.")
		UNION  
		(select bookings.id ,bookings.created_at,bookings.purchaser_id,bookings.carer_id,transactions.id as trans_id,transactions.created_at as tr_created, transactions.amount as amount, 'purchaser' as status from bookings inner join transactions  on transactions.booking_id = bookings.id where  transactions.payment_method='bonus' AND ".$dateText2.") 
		UNION 
		(select bookings.id,bookings.created_at,bookings.purchaser_id,bookings.carer_id,stripe_transfers.id as trans_id ,stripe_transfers.created_at as tr_created, stripe_transfers.amount as amount,'carer' as status from bookings inner join stripe_transfers on stripe_transfers.booking_id = bookings.id  where ".$dateText3." )  ORDER BY tr_created  DESC
		");
	
		}

        $data = $bookings;

		Excel::create('Accountant Report', function($excel) use($data) {
            $excel->sheet('Accountant Report', function($sheet) use ($data) {
                $sheet->loadView('HolmAdmin.accountantReportToXls')
                      ->with([
                          'bookings'   => $data
                      ]);
            });
        })->download('csv');
	}
	
	//get user name 
	public static function getDisplayFieldName($id = 0, $table_name = '', $fieldArr = array()) { 
		$response = ''; $dbResponseObj = '';
		if ((int) $id) { 
			$responseObj = DB::table("$table_name") ->where("$table_name.id", '=', $id); 
			if (is_array($fieldArr) && count($fieldArr) > 0) { 
				foreach ($fieldArr as $colKey => $cloName) { 
					$selectArr[] = "$table_name.$cloName"; 
				} 
				$responseObj->select($selectArr); 
			} $dbResponseObj = $responseObj->first(); 
		} 
		if (is_array($fieldArr) && count($fieldArr) > 0) { 
			foreach ($fieldArr as $colKey => $cloName) { 
				if (isset($dbResponseObj->$cloName) && $dbResponseObj->$cloName != '') { 
					$response .= ' '.$dbResponseObj->$cloName; 
				}
			} 
		} 
		return $response; 
	}
	
	//get amount
	public static function getDisplayAmount($amount){ 
		 $amountLen = strlen((string)$amount);
		 if(strpos($amount,'.') !== false)
		 { 
			$is_int = 0;
		 }else{ 
			$is_int = 1 ;
		 }
		 if($amountLen > 3 && $is_int){
			 $amount =  $amount/100;  
		 }
			 $amount = number_format($amount, 2,'.','');
		  
		 return '£'.$amount;
	}
    
}
