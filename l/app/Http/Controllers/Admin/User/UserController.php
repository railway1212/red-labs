<?php

namespace App\Http\Controllers\Admin\User;

use App\Booking;
use App\CarersProfile;
use App\Helpers\Facades\PaymentTools;
use App\Http\Controllers\Admin\AdminController;
use App\Http\Controllers\Admin\Repo\Models\User\AdminUsers;
use App\MailError;
use App\PurchasersProfile;
use App\ServiceUsersProfile;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Swift_TransportException;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Input;
use App\Document;
use App\ServiceType;
use App\AssistanceType;
use App\WorkingTime;
use App\Language;
use App\StripeTransfer;
use App\BonusPayout;

//use App\Http\Controllers\Controller;

class UserController extends AdminController
{
    private $siteUsers;

    public function __construct(AdminUsers $siteUsers)
    {
        parent::__construct();
        $this->siteUsers = $siteUsers;

        $this->template = config('settings.theme') . '.templates.adminBase';
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request,$page = 1)
    {
     
        $this->title = 'Admin Profiles Management';
        $profileType = $this->siteUsers->getProfileType();
        $statusType = $this->siteUsers->getStatusType();
        $totalsByUserType = $this->siteUsers->getTotalsByUserType();
        $totals = $this->siteUsers->getTotals($totalsByUserType);
		$request->has('profileType')? $profileTypeFilter = $request->get('profileType') : $profileTypeFilter = null;
        $request->has('statusType')? $statusTypeFilter  = $request->get('statusType') : $statusTypeFilter = null;
        $request->has('userName')? $userNameFilter = $request->get('userName') : $userNameFilter = null;
        $userList = $this->siteUsers->getUserList($profileTypeFilter,$statusTypeFilter,$userNameFilter);
  		
      	//$test = $this->user['completed_appointments_hours'];
       //dd($test);
  		$page = $request->get('page',1);
        $perPage = 9;
        $start = ($page - 1) * $perPage;
        if($page==1) $start = 0;
        $count = count($userList);
        if($count>0)
            $pages = ceil($count/$perPage);
        else
            $pages=0;
        $nextPage=$page+1;
        $previousPage = $page-1;
      
        $userList = $userList->slice($start,$perPage);
       
        $this->vars = array_add($this->vars, 'profileType', $profileType);
        $this->vars = array_add($this->vars, 'profileTypeFilter', $profileTypeFilter);
        $this->vars = array_add($this->vars, 'statusTypeFilter', $statusTypeFilter);
        $this->vars = array_add($this->vars, 'userName', $userNameFilter);
        $this->vars = array_add($this->vars, 'nextPage', $nextPage);
        $this->vars = array_add($this->vars, 'previousPage', $previousPage);
        $this->vars = array_add($this->vars, 'count', $count);
        $this->vars = array_add($this->vars, 'curr_page', $page);
        $this->vars = array_add($this->vars, 'pages', $pages);
        $this->vars = array_add($this->vars, 'statusType', $statusType);
        $this->vars = array_add($this->vars, 'totals', $totals);
        $this->vars = array_add($this->vars, 'totalsByUserType', $totalsByUserType);
        $this->vars = array_add($this->vars, 'userList', $userList);
        $this->vars = array_add($this->vars, 'link', '/l/admin/user');
        $this->vars = array_add($this->vars, 'request', $request);
        $pagination = view(config('settings.theme') . '.pagination2')->with($this->vars)->render();
        $this->vars = array_add($this->vars, 'pagination', $pagination);
        $this->content = view(config('settings.theme') . '.profilesManagement.profilesManagement')->with($this->vars)->render();
     
        return $this->renderOutput();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
      
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        switch ($request->get('user_type')) {
            case 'carer':
                $profile = CarersProfile::findOrfail($id);
                break;
            case 'service':
                $profile = ServiceUsersProfile::findOrfail($id);
                break;
            case 'purchaser':
                $profile = PurchasersProfile::findOrfail($id);
                break;
        }

        $previous = $profile->profiles_status_id;
//        $allCarersInBookings = Booking::groupBy('carer_id')->pluck('carer_id')->toArray();
        if ($profile) {
            switch ($request->get('action')) {
                case 'accept':
                    if ($request->get('user_type') == 'carer') {
                        if ($profile->profiles_status_id !== 5 && $profile->profiles_status_id == 1) {
                            $profile->profiles_status_id = 2;
                        } else {
                            $profile->profiles_status_id = 5;
                        }
                    } else {
                        $profile->profiles_status_id = 2;
                    }
                    break;
                case 'reject':
                    $profile->profiles_status_id = 3;
                    break;
                case 'block':
                    $profile->profiles_status_id = 5;
                    break;
                case 'recover':
                    $profile->profiles_status_id = 1;
            }
        } else {

        }

        $profile->update();

        if ($profile) {
            switch ($request->get('action')) {
                case 'accept':{
                    $user = Auth::user();
                    //Creation connected accounts in stripe on sign up
                    $data=[];
                    $data["email"] =$profile->email;
                    $data["legal_entity"]["address"]["city"] = $profile->town;
                    $data["legal_entity"]["address"]["line1"] = $profile->address_line1;
                    $data["legal_entity"]["address"]["postal_code"] = $profile->postcode;

                    $data["legal_entity"]["dob"]['day']=date('d',strtotime($profile->DoB));
                    $data["legal_entity"]["dob"]['month']=date('m',strtotime($profile->DoB));
                    $data["legal_entity"]["dob"]['year']=date('Y',strtotime($profile->DoB));
                    $data["legal_entity"]['first_name']=$profile->first_name;
                    $data["legal_entity"]["last_name"]=$profile->family_name;

					 //Delete all connected account of user
					DB::table('stripe_connected_accounts')->where('carer_id', '=', $profile->id)->delete();

                    if ($request->user_type !== 'service' && !isset($request->unblock)) {
                            $accountNumber =  $profile->account_number;
                          
                         if(!empty($accountNumber)){
                              PaymentTools::createConnectedAccount($data, $profile->id);
                          }
                    }

                    if ($previous == 5) {
                        if ($request->user_type == 'service') {
                            if ($profile->deleted == 'Yes') {
                                $profile->deleted = null;
                                $profile->update();
                            }

                            $purchaser = $profile->purchaser_id;
                            $user = User::findOrFail($purchaser);

                            $text = view(config('settings.frontTheme') . '.emails.unblock_account_by_admin')->with([
                                'user' => $user,
                                'like_name' => $profile->like_name
                            ])->render();

                            DB::table('mails')
                                ->insert(
                                    [
                                        'email' => $user->email,
                                        'subject' => 'Unblock account by admin',
                                        'text' => $text,
                                        'time_to_send' => Carbon::now(),
                                        'status' => 'new'
                                    ]);
                        } else {
                            $user = User::findOrFail($profile->id);

                            $text = view(config('settings.frontTheme') . '.emails.unblock_account_by_admin')->with([
                                'user' => $user,
                                'like_name' => $profile->like_name
                            ])->render();

                            DB::table('mails')
                                ->insert(
                                    [
                                        'email' => $user->email,
                                        'subject' => 'Unblock account by admin',
                                        'text' => $text,
                                        'time_to_send' => Carbon::now(),
                                        'status' => 'new'
                                    ]);
                            $profile->profiles_status_id = 1;
                            $profile->update();
                        }
                    }

                    if ($request->get('user_type') == 'carer' && $previous == 1) {
                        $user = User::findOrFail($profile->id);

                        $text = view(config('settings.frontTheme') . '.emails.confirm_account_by_admin')->with([
                            'user' => $user,

                        ])->render();

                        DB::table('mails')
                            ->insert(
                                [
                                    'email' => $user->email,
                                    'subject' => 'HOLM account confirmation',
                                    'text' => $text,
                                    'time_to_send' => Carbon::now(),
                                    'status' => 'new'
                                ]);
                    }
                }
                    break;
                case 'reject': {
                   // $user = User::findOrFail($profile->id);
                  
                  if($request->get('user_type')=='service'){
						
						$user = User::findOrFail($profile->purchaser_id);
						
					}else{
					
						$user = User::findOrFail($profile->id);
				    }

                    $text = view(config('settings.frontTheme') . '.emails.reject_account_by_admin')->with([
                        'user' => $user,
                        'like_name'=>$profile->like_name
                    ])->render();

                    DB::table('mails')
                        ->insert(
                            [
                                'email' =>$user->email,
                                'subject' =>'Reject account by admin',
                                'text' =>$text,
                                'time_to_send' => Carbon::now(),
                                'status'=>'new'
                            ]);

                }
                    break;
                case 'block':{
                    if ($request->user_type == 'service') {
                        $purchaser = $profile->purchaser_id;
                        $user = User::findOrFail($purchaser);
                    } else {
                        $user = User::findOrFail($profile->id);
                    }

                    $text = view(config('settings.frontTheme') . '.emails.block_account_by_admin')->with([
                        'user' => $user,
                        'like_name'=>$profile->like_name
                    ])->render();

                    DB::table('mails')
                        ->insert(
                            [
                                'email' =>$user->email,
                                'subject' =>'Block account by admin',
                                'text' =>$text,
                                'time_to_send' => Carbon::now(),
                                'status'=>'new'
                            ]);

                }
                    break;
            }
        } else {
//            dd($request->all());
        }

//        if ($request->get('user_type') == 'carer' && $request->get('action') == 'accept' && $previous == 1) {
//
//            $user = User::findOrFail($profile->id);
//
//            $profile->profiles_status_id = 2;
//            $profile->update();
//
//
//            $text = view(config('settings.frontTheme') . '.emails.confirm_account_by_admin')->with([
//                'user' => $user,
//
//            ])->render();
//
//            DB::table('mails')
//                ->insert(
//                    [
//                        'email' =>$user->email,
//                        'subject' =>'HOLM account confirmation',
//                        'text' =>$text,
//                        'time_to_send' => Carbon::now(),
//                        'status'=>'new'
//                    ]);
//
//
//        }


        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getCarerImage(Request $request,$id){

        $user = User::with('userCarerProfile')->with('documents')->find($id);
        $userCarerReferences=CarersProfile::with('CarerReferences')->find($id)->CarerReferences;

        $userProfileList = collect();
        $userProfileList->push($userCarerReferences);
        $userProfileList->push($user);
//        dd($userProfileList);
        $text = $this->content = view(config('settings.theme').'.profilesManagement.CarerAnswers')->with([
            'user' => $userProfileList,
        ])->render();
//        echo $text; die;
        $data = ['ansver'=>true,'form'=>$text];
        return response()->json($data);
    }
  
   ///IP monitoring
    public function ipMonitoring(Request $request){

		$input = Input::all();
		$where = "";
		if($input){
			$dateFrom = date('Y-m-d',strtotime($input['dateFrom']));
			$dateTo = date('Y-m-d',strtotime($input['dateTo']));
			if(!empty($input['dateFrom']) && !empty($input['dateTo'])){
				
					$where = "where DATE(created_at) >= '".$dateFrom."' AND DATE(created_at) <= '".$dateTo."'";
				
			}elseif(!empty($input['dateFrom'])){
				   $where = "where DATE(created_at) >= '".$dateFrom."'";
			}elseif(!empty($input['dateTo'])){
				   $where = "where DATE(created_at) <= '".$dateTo."'";
			}
	    }

		$sql ="SELECT * from ip_monitoring ".$where." ORDER BY id DESC";
		$items = DB::select($sql);

		$this->vars = array_add($this->vars, 'items', $items);
		$this->content = view(config('settings.theme').'.ipMonitoring')->with( $this->vars)->render();
        return $this->renderOutput();
		
	}
  //GDPR Details
    
    public function UserGdpr(Request $request){
		
		$profileTypeFilter = $statusTypeFilter = null ;
		$request->has('userName')? $userNameFilter = $request->get('userName') : $userNameFilter = null;
		$userFilter = $this->siteUsers->getUserList($profileTypeFilter,$statusTypeFilter,$userNameFilter);
		$user = $general = $documents = $typeServices = "";
		
		// filter for multiple options
		if(count($userFilter) > 1){
			
			$this->vars = array_add($this->vars, 'user', $userFilter); 
			
		}else{
	
			if(!empty($userNameFilter)){
				foreach($userFilter as $key => $user){
					if($key == 0){
						$user = $user;
					}
				}
		     }
		 }    


        if(!empty($user)){
			//user data 
			$general = User::where('id',$user->id)->first();    
			$purchaser=PurchasersProfile::find($user->id);
			$carer = CarersProfile::find($user->id);
			$service_user=ServiceUsersProfile::find($user->id);
			$documents = Document::where('user_id', $user->id)->get();
			$mails = DB::table('mails')->where('email',$user->email)->get();


			// carer
			if($carer){

				$carer_languages = $carer->Languages;
				$carer_service = $carer->ServicesTypes;
				$carer_assistance=$carer->AssistantsTypes;
				$carer_references=$carer->CarerReferences;
				$availibility = $carer->WorkingTimes()->get();
                $carer_booking=$carer->bookings;
                $profileImage = $carer->img_url;

				$transfers = StripeTransfer::all();
				$userName = $request->get('userName',false);
				$potentialPayouts = $this->getPotentialPayoutsForCarer($userName);
				$potentialPayouts = collect($potentialPayouts);

				$this->vars['transfers'] = $transfers;
				if(is_numeric($request['userName'])) {
					$this->vars['transfers'] = $transfers->filter(function($item)use($request){

						if($item->booking_id > 0){
							return ( $item->booking->carer_id === (int)$request['userName'] );
						}elseif($item->bonus_id > 0){
							$bonusPayout = \App\BonusPayout::where('id', $item->bonus_id)->pluck('user_id')->toArray();
							return ( $bonusPayout[0] === (int)$request['userName'] );
						}

					});
				}
				elseif($request['userName']!=""){
					  $this->vars['transfers'] = $transfers->filter(function($item)use($request){
						if($item->bonus_id > 0){
							$bonusPayout = \App\BonusPayout::where('id', $item->bonus_id)->pluck('user_id')->toArray();
							$sql="SELECT * FROM carers_profiles where id = ".$bonusPayout[0];
							$bonusUser = DB::select($sql);
							
							return ( strpos(strtoupper($bonusUser[0]->first_name),strtoupper($request['userName']))!==false ||  strpos(strtoupper($bonusUser[0]->family_name),strtoupper($request['userName']))!==false );
						}   
					});
				}

				$all = collect();
				$all = $all->merge($potentialPayouts);
				$all = $all->merge($this->vars['transfers']);

                /*echo '<pre>';
                print_r($this->vars['transfers']);
                echo '</pre>';*/
               
               


                $cv = Document::where('user_id', $user->id)->where('type','ADDITIONAL_DOCUMENTS_CV')->first();

				$this->vars = array_add($this->vars, 'potentialPayouts', $potentialPayouts);
                $this->vars = array_add($this->vars, 'all', $all);
                $this->vars = array_add($this->vars, 'transfersCarer', $transfers);
				$this->vars = array_add($this->vars,'search_user_type','carer');
				$this->vars = array_add($this->vars, 'cp', $carer);
				$this->vars = array_add($this->vars, 'carer_references', $carer_references);
				$this->vars = array_add($this->vars, 'carer_languages',$carer_languages);
				$this->vars = array_add($this->vars, 'carer_service',$carer_service);
				$this->vars = array_add($this->vars, 'carer_assistance',$carer_assistance);
				$this->vars = array_add($this->vars, 'availibility', $availibility);
				$this->vars = array_add($this->vars, 'carer_booking', $carer_booking);
                $this->vars = array_add($this->vars, 'cv', $cv);
			}

			//purchaser details
			elseif($purchaser){
				$selected_user = User::find($user->id);
				$credit_card=isset($selected_user->credit_cards) ? $selected_user->credit_cards : [];
				$service_user = $purchaser->serviceUsers;
				$purchaser_booking=$purchaser->bookings;
                $purchaser_transaction=$purchaser->transaction;
                $profileImage = $purchaser->img_url;

				$this->vars = array_add($this->vars, 'credit_card',$credit_card); 
				$this->vars = array_add($this->vars, 'service_user',$service_user);
				$this->vars = array_add($this->vars, 'purchaser_booking',$purchaser_booking);
				$this->vars = array_add($this->vars, 'purchaser_transaction',$purchaser_transaction); 
			}
			
			//Service User
			elseif($service_user){
				
				$service_user_language=$service_user->Languages()->get();
				$service_user_condition=$service_user->ServiceUserConditions()->get();
				$care_type=$service_user->AssistantsTypes()->get()->sortBy('id');
				$service_type=$service_user->ServicesTypes()->get()->sortBy('id');
				
				$servicePerchaser = PurchasersProfile::find($service_user['purchaser_id']);
				$servicePerchaserBooking = $servicePerchaser->bookings;
				$servicePerchaserTransactions = $servicePerchaser->transaction;
				$servicePerchaserEmail = DB::table('users')->select('email')
				                                          ->where('id',$service_user['purchaser_id'])
                                                          ->first();	
                $profileImage = $service_user->img_url;                                            
														  
				$mails = DB::table('mails')->where('email',$servicePerchaserEmail->email)->get();										  
				
				

				$this->vars = array_add($this->vars, 'service_user_language', $service_user_language);    
				$this->vars = array_add($this->vars, 'service_user_condition', $service_user_condition);
				$this->vars = array_add($this->vars, 'care_type', $care_type);
				$this->vars = array_add($this->vars, 'service_type', $service_type);
				$this->vars = array_add($this->vars, 'servicePerchaserBooking', $servicePerchaserBooking);
				$this->vars = array_add($this->vars, 'servicePerchaserTransaction', $servicePerchaserTransactions);
				
			}
			

				$this->vars = array_add($this->vars, 'general', $general);
				$this->vars = array_add($this->vars, 'documents', $documents);
				$this->vars = array_add($this->vars, 'typeServices', $typeServices);
                $this->vars = array_add($this->vars, 'mails', $mails); 
                $this->vars = array_add($this->vars, 'profileImage', $profileImage);
        
         }



        $this->vars = array_add($this->vars, 'user', $user);
		$this->content = view(config('settings.theme').'.gdpr.gdpr')->with( $this->vars)->render();
					
        return $this->renderOutput();
		
	}
	
  //export GDPR in csv file 
	public function exportGdprToXls(Request $request){
		
		$profileTypeFilter = $statusTypeFilter = null ;
		$request->has('userName')? $userNameFilter = $request->get('userName') : $userNameFilter = null;
		$userFilter = $this->siteUsers->getUserList($profileTypeFilter,$statusTypeFilter,$userNameFilter);
		$user = $general = $documents = $typeServices = "";
		
		
		if(!empty($userNameFilter)){
			foreach($userFilter as $key => $user){
				if($key == 0){
					$user = $user;
				}
			}
		 }else{
			 return redirect()->back();
		 }
		    


        if(!empty($user)){
            
			//user data 
			$general = User::where('id',$user->id)->first();    
			$purchaser=PurchasersProfile::find($user->id);
			$carer = CarersProfile::find($user->id);
			$service_user=ServiceUsersProfile::find($user->id);
			$documents = Document::where('user_id', $user->id)->get();
			$mails = DB::table('mails')->where('email',$user->email)->get();

			// carer
			if($carer){

				$carer_languages = $carer->Languages;
				$carer_service = $carer->ServicesTypes;
				$carer_assistance=$carer->AssistantsTypes;
				$carer_references=$carer->CarerReferences;
				$availibility = $carer->WorkingTimes()->get();
                $carer_booking=$carer->bookings;
                $bonusUserId = $user->id;
                $profileImage = $carer->img_url;

				$transfers = StripeTransfer::all();
				$userName = $request->get('userName',false);
				$potentialPayouts = $this->getPotentialPayoutsForCarer($userName);
				$potentialPayouts = collect($potentialPayouts);

				$this->vars['transfers'] = $transfers;
				if(is_numeric($request['userName'])) {
					$this->vars['transfers'] = $transfers->filter(function($item)use($request){

						if($item->booking_id > 0){
							return ( $item->booking->carer_id === (int)$request['userName'] );
						}elseif($item->bonus_id > 0){
							$bonusPayout = \App\BonusPayout::where('id', $item->bonus_id)->pluck('user_id')->toArray();
							return ( $bonusPayout[0] === (int)$request['userName'] );
						}

					});
				}
				elseif($request['userName']!=""){
					  $this->vars['transfers'] = $transfers->filter(function($item)use($request){
						if($item->bonus_id > 0){
							$bonusPayout = \App\BonusPayout::where('id', $item->bonus_id)->pluck('user_id')->toArray();
							$sql="SELECT * FROM carers_profiles where id = ".$bonusPayout[0];
							$bonusUser = DB::select($sql);
							
							return ( strpos(strtoupper($bonusUser[0]->first_name),strtoupper($request['userName']))!==false ||  strpos(strtoupper($bonusUser[0]->family_name),strtoupper($request['userName']))!==false );
						}   
					});
				}

				$all = collect();
				$all = $all->merge($potentialPayouts);
                $all = $all->merge($this->vars['transfers']);

                $cv = Document::where('user_id', $user->id)->where('type','ADDITIONAL_DOCUMENTS_CV')->first();

                 $xlsData ['potentialPayouts'] = $potentialPayouts;
                 $xlsData ['all'] = $all;
				 $xlsData ['transfers'] = $transfers;
				 $xlsData ['search_user_type'] ='carer';
				 $xlsData ['cp'] = $carer;
				 $xlsData ['carer_references'] = $carer_references;
				 $xlsData ['carer_languages'] = $carer_languages;
				 $xlsData ['carer_service'] = $carer_service;
				 $xlsData ['carer_assistance'] = $carer_assistance;
				 $xlsData ['availibility'] = $availibility;
				 $xlsData ['carer_booking'] = $carer_booking;
                 $xlsData ['cv'] = $cv;
			}

			//purchaser details
			elseif($purchaser){
				$selected_user=User::find($user->id);
				$credit_card=isset($selected_user->credit_cards) ? $selected_user->credit_cards : [];
				$service_user=$purchaser->serviceUsers;
				$purchaser_booking=$purchaser->bookings;
                $purchaser_transaction=$purchaser->transaction;
                $bonusUserId = $user->id;
                $profileImage = $purchaser->img_url;

				$xlsData ['credit_card'] = $credit_card;
				$xlsData ['service_users'] = $service_user;
				$xlsData ['purchaser_booking'] = $purchaser_booking;
                $xlsData ['purchaser_transaction'] = $purchaser_transaction;

			}
			
			//Service User
			elseif($service_user){
				
				$service_user_language=$service_user->Languages()->get();
				$service_user_condition=$service_user->ServiceUserConditions()->get();
				$care_type=$service_user->AssistantsTypes()->get()->sortBy('id');
				$service_type=$service_user->ServicesTypes()->get()->sortBy('id');
				
				$servicePerchaser = PurchasersProfile::find($service_user['purchaser_id']);
				$servicePerchaserBooking = $servicePerchaser->bookings;
				$servicePerchaserTransactions = $servicePerchaser->transaction;
				$servicePerchaserEmail = DB::table('users')->select('email')
				                                          ->where('id',$service_user['purchaser_id'])
														  ->first();	
														  
				$mails = DB::table('mails')->where('email',$servicePerchaserEmail->email)->get();										  
                $bonusUserId = $service_user['purchaser_id'];
                $profileImage = $service_user->img_url;
				
				$xlsData ['service_user_language'] = $service_user_language;
				$xlsData ['service_user_condition'] = $service_user_condition;
				$xlsData ['care_type'] = $care_type;
				$xlsData ['service_type'] = $service_type;
				$xlsData ['servicePerchaserBooking'] = $servicePerchaserBooking;
				$xlsData ['servicePerchaserTransaction'] = $servicePerchaserTransactions;

            }
            
                    //Bonus
                    $res = DB::table('bonus_payouts')
                    ->join('users', 'bonus_payouts.user_id', '=', 'users.id')
                    ->select('bonus_payouts.id')
                    ->where('users.id', $bonusUserId)
                    ->get();

                    $res = $res->pluck('id')->toArray();
                    $bonusPayouts = BonusPayout::find($res);
                    $userName = trim($bonusUserId);
                    if($userName){
                        $bonusPayouts = $bonusPayouts->filter(function($item)use($userName){
                            if(is_numeric($userName)){
                                if($item->user->id === (int)$userName || strpos($item->user->referral_code, $userName) !== false) {return true;}
                            } 
                        });
                    }
                $bonusPayouts = $bonusPayouts->sortByDesc('id');
                $users = User::all();

                $xlsData ['bonusPayouts'] = $bonusPayouts;
                $xlsData ['users'] = $users;    

			
				$xlsData ['general'] = $general;
				$xlsData ['documents'] = $documents;
				$xlsData ['typeServices'] = $typeServices;
                $xlsData ['mails'] = $mails;
                $xlsData ['profileImage'] = $profileImage;

         }
        $xlsData ['user'] = $user; 
      
	
	
   $data = $xlsData;
  	 
   Excel::create( 'GDPR' , function($excel) use($data) {


	$excel->sheet('Personal details' , function($sheet) use ($data) {
		$sheet->loadView('HolmAdmin.gdpr.gdprToXls')
                      ->with( $data );
                      $sheet->setSize('A1:B999', 80);
                      $sheet->getStyle('A1:A999')->getAlignment()->setWrapText(true); 

        });

    $excel->sheet('Booking details' , function($sheet) use ($data) {
        $sheet->loadView('HolmAdmin.gdpr.gdprToxlsBooking')
                        ->with( $data );
        });

    $excel->sheet('Transactions' , function($sheet) use ($data) {
        $sheet->loadView('HolmAdmin.gdpr.gdprToxlsTransaction')
                        ->with( $data );
        });  
        
    $excel->sheet('Messages' , function($sheet) use ($data) {
    $sheet->loadView('HolmAdmin.gdpr.GdprMails')
                    ->with( $data );
        });    
    
    $excel->sheet('Bonuses' , function($sheet) use ($data) {
        $sheet->loadView('HolmAdmin.gdpr.gdprToXlsBonuses')
                        ->with( $data );
            });
    
    $excel->sheet('Images' , function($sheet) use ($data) {
        $sheet->loadView('HolmAdmin.gdpr.gdprToXlsDocuments')
                        ->with( $data );
                        $sheet->setSize('A1:B999', 50);
                        $sheet->getStyle('A1:A999')->getAlignment()->setWrapText(true); 
            });        

   })->download('xlsx');
	
}
///get potential payout
    private function getPotentialPayoutsForCarer($userName=false){
        $where ='';
        if($userName) $where =" and concat(first_name,' ',family_name) like '%".$userName."%'";
    
        if(is_numeric($userName)) $where =" and ( c.id like $userName OR a.id like $userName )";
        
        $sql = 'SELECT
                  SUM(a.price_for_carer) as total,  MAX(cp.id) as carer_id, MAX(cp.first_name) as first_name, MAX(cp.family_name) as family_name, a.booking_id, a.id
                FROM appointments a
                JOIN bookings b ON a.booking_id = b.id
                JOIN users c ON b.carer_id = c.id
                JOIN carers_profiles cp ON cp.id = c.id
                WHERE  a.payout = false AND a.status_id = 4  ' . $where . '
                GROUP BY a.id,a.booking_id';   
        $res = DB::select($sql);
        return $res;
    }  
  
}
