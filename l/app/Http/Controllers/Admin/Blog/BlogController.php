<?php

namespace App\Http\Controllers\Admin\Blog;

use App\Http\Controllers\Admin\AdminController;
#use App\Http\Controllers\Controller;
use App\Blog;
use App\Tag;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class BlogController extends AdminController
{
    private $blog;

    public function __construct(Blog $blog) {
        parent::__construct();
        $this->blog = $blog;

        $this->template = config('settings.theme').'.templates.adminBase';
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $this->title = 'Admin Blogs';
        $blogs = Blog::orderBy('created_at', 'desc')->paginate(5);
        $this->vars = array_add($this->vars,'blogs',$blogs);
       // dd($this->vars);

        $this->content = view(config('settings.theme').'.blog.blog')->with($this->vars)->render();

        return $this->renderOutput();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->title = 'Admin Blogs';
//        $tags = Tag::all();
//        $this->vars = array_add($this->vars, 'tags', $tags);
        $this->content = view(config('settings.theme') . '.blog.create')->with($this->vars)->render();

        return $this->renderOutput();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request->all());
        $this->validate($request, [
            'title' => 'required',
            'body' => 'required',
            'slug' => 'required',
        ]);
//        if ($request->hasFile('image')) {
//            $imageName = $request->image->store('public');
//        }else{
//            return 'No';
//        }
        $blog = new Blog;
        $blog->title = $request->title;
        $blog->body = $request->body;
        $blog->slug = $this->seoUrl($request->slug);
        $blog->tags = $request->tags;
        $blog->meta_title = $request->meta_title;
        $blog->meta_keywords = $request->meta_keywords;
        $blog->meta_description = $request->meta_description;
        $blog->user_id = Auth::user()->id;
        $blog->status = 'not published';
        $blog->published_at = null;
        $blog->save();
//        $blog->tags()->sync($request->tags);
        return redirect(route('blog.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($blogId)
    {
        $this->title = 'Admin Blogs';
        $blogs = $this->blog->findOrFail($blogId);
//        $tags = Tag::all();
        $this->vars = array_add($this->vars,'blog',$blogs);
//        $this->vars = array_add($this->vars, 'tags', $tags);
        $this->content = view(config('settings.theme').'.blog.edit')->with($this->vars)->render();

        return $this->renderOutput();

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        if (isset($request->delete_blog_id)) {
            $blog = Blog::find($request->delete_blog_id);
            $blog->status = 'deleted';
            $blog->save();
            return response(['status' => 'success']);
        } elseif (isset($request->publish_blog_id)) {
            $blog = Blog::find($request->publish_blog_id);
            $blog->status = 'published';
            $blog->published_at = Carbon::now();
            $blog->save();
            return response(['status' => 'success']);
        } else {
            $this->validate($request, [
                'title' => 'required',
                'body' => 'required',
                'slug' => 'required',
            ]);

            $blog = Blog::find($id);
            $blog->title = $request->title;
            $blog->body = $request->body;
            $blog->slug = $this->seoUrl($request->slug);
            $blog->tags = $request->tags;
            $blog->meta_title = $request->meta_title;
			$blog->meta_keywords = $request->meta_keywords;
			$blog->meta_description = $request->meta_description;
            $blog->user_id = Auth::user()->id;
            $blog->status = $blog->status;
            if ($blog->published_at == null) {
                $blog->published_at = null;
            }
//            $blog->tags()->sync($request->tags);
            $blog->save();

            return redirect(route('blog.index'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
  
  	 //SEO URL
    private function seoUrl($string) {
        //Lower case everything
        $string = strtolower($string);
        //Make alphanumeric (removes all other characters)
        $string = preg_replace("/[^a-z0-9_\s-]/", "", $string);
        //Clean up multiple dashes or whitespaces
        $string = preg_replace("/[\s-]+/", " ", $string);
        //Convert whitespaces and underscore to dash
        $string = preg_replace("/[\s_]/", "-", $string);
        return $string;
    }
}
