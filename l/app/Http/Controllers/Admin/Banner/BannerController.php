<?php

namespace App\Http\Controllers\Admin\Banner;
use App\Http\Controllers\Admin\AdminController;

use App\Banner;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;


class BannerController extends AdminController
{
    private $banner;

    public function __construct(Banner $banner) {
        parent::__construct();
        $this->banner = $banner;

        $this->template = config('settings.theme').'.templates.adminBase';
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
//echo SITE_URL; exit;        
                $this->title = 'Admin Banner';
        $banners = Banner::orderBy('id', 'desc')->get();
        $this->vars = array_add($this->vars,'banners',$banners);
        $this->content = view(config('settings.theme').'.banner.banner')->with($this->vars)->render();

        return $this->renderOutput();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->title = 'Admin Banners';
        $this->content = view(config('settings.theme') . '.banner.create')->with($this->vars)->render();

        return $this->renderOutput();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'page_id' => 'required'
         
        ]);

        $banner = new Banner;
        $banner->page_id = $request->page_id;
        $bannerText = serialize($request->text);
        $banner->text = $bannerText;
        $banner->image_alt = $request->image_alt;

      	$itemreq = $request;
      	$download_file_name = "";
          	 if( $itemreq->hasFile('image')){ 
                $image = $itemreq->file('image'); 
                $image_path=base_path('public/image_Association');
                $fileName = $image->getClientOriginalName();
                $fileExtension = $image->getClientOriginalExtension();
                $download_file_name=time().'.'.$fileExtension;
                $image->move($image_path,$download_file_name);
                $banner->image_name = $download_file_name;
            }
      

      
        $banner->save();
        return redirect(route('banner.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($bannerId)
    {
        $this->title = 'Admin Banner';
        $banner = $this->banner->findOrFail($bannerId);
        $this->vars = array_add($this->vars,'banner',$banner);
        $this->content = view(config('settings.theme').'.banner.edit')->with($this->vars)->render();
        return $this->renderOutput();

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
		if (isset($request->delete_banner_id)) {
            $banner = Banner::find($request->delete_banner_id);
            $banner->delete();
            return response(['status' => 'success']);
        }else{

            //dd($request->text);
           
      		$download_file_name = "";
            $banner = Banner::find($id);
            $banner->page_id = $request->page_id;
            $bannerText = serialize($request->text);
            $banner->text = $bannerText;
            $banner->image_alt = $request->image_alt;

             $itemreq = $request;
          	 if( $itemreq->hasFile('image')){ 
                $image = $itemreq->file('image'); 
                $image_path=base_path('public/image_Association');
                $fileName = $image->getClientOriginalName();
                $fileExtension = $image->getClientOriginalExtension();
                $download_file_name=time().'.'.$fileExtension;
                $image->move($image_path,$download_file_name);
                $banner->image_name = $download_file_name;
            }
            $banner->updated_at = Carbon::now();
          
    
            $banner->save();
            return redirect(route('banner.index'));
        }
    }

  
}
