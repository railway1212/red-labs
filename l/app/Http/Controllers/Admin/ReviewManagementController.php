<?php

namespace App\Http\Controllers\Admin;


use App\Appointment;
use App\AppointmentOverview;
use App\Booking;
use Illuminate\Http\Request;
use DB;


class ReviewManagementController extends AdminController
{

    public function __construct()
    {
        parent::__construct();
        $this->template = config('settings.theme') . '.templates.adminBase';
    }

    public function edit(Request $request, $id){
        $input = $request->all();
        if(isset($input['comment'])){
            $review = AppointmentOverview::find($id);
            $review->comment = $input['comment'];
            $review->punctuality = $input['punctuality'];
            $review->friendliness = $input['friendliness'];
            $review->communication = $input['communication'];
            $review->performance = $input['performance'];
            $review->save();
           return redirect()->route('ReviewManagement');
        }

        $review = AppointmentOverview::find($id);
        $this->vars['reviews']=$review;
        $this->content = view(config('settings.theme').'.review_management_edit')->with( $this->vars)->render();

        return $this->renderOutput();
    }

    public function index(Request $request){
//dd($request); exit;
        $this->title = 'Holm Admin | Review Management';
        $input = $request->all();
        if(isset($input['method'])&&$input['id']){
            switch ($input['method']){
                case 'confirm':
                    $review = AppointmentOverview::find($input['id']);
                    if(isset($review->id)){
                        $review->accept = 1;
                        $review->save();
                    }
                    break;
                case 'delete':
                    $review = AppointmentOverview::find($input['id']);
                    if(isset($review->id)){
                        $review->delete();
                    }
                    break;
            }

        }
		$dataObj 					= 	AppointmentOverview::where('accept','=',0);
        if(isset($request->appointment_id) && $request->appointment_id != ''){
			$dataObj 				=	$dataObj->where('appointment_id','LIKE','%'.$request->appointment_id.'%');
        }
        $data 						=	$dataObj->orderby('id','desc')->paginate(10);
        $this->vars['reviews']		=	$data;
        $this->content = view(config('settings.theme').'.review_management')->with( $this->vars)->render();

        return $this->renderOutput();
    }
	public function reviewed(Request $request){
		
        $this->title = 'Holm Admin | Review Management';
        $input = $request->all();
        if(isset($input['method'])&&$input['id']){
            switch ($input['method']){
                case 'confirm':
                    $review = AppointmentOverview::find($input['id']);
                    if(isset($review->id)){
                        $review->accept = 1;
                        $review->save();
                    }
                    break;
                case 'delete':
                    $review = AppointmentOverview::find($input['id']);
                    if(isset($review->id)){
                        $review->delete();
                    }
                    break;
            }

        }
		$dataObj 					= AppointmentOverview::where('accept','=',1);
		
        if(isset($request->appointment_id) && $request->appointment_id != ''){
			$dataObj 				=	$dataObj->where('appointment_id','LIKE','%'.$request->appointment_id.'%');
        }
        $acceptedData 		=	$dataObj->orderby('id','desc')->paginate(10);
      	$acceptedDataCnt 	= 	$dataObj->count();
      	//$acceptedData 		= AppointmentOverview::where('accept','=',1)->paginate(8);
      	$this->vars['allReviews']	=	$acceptedData;
      	$this->vars['allReviewCnt'] =	$acceptedDataCnt;
        $this->content = view(config('settings.theme').'.reviewed_management')->with( $this->vars)->render();

        return $this->renderOutput();
    }
  
  	public function reviewed_edit(Request $request, $id){
    	$input = $request->all();
        if(isset($input['comment'])){
            $review = AppointmentOverview::find($id);
            $review->comment = $input['comment'];
            $review->punctuality = $input['punctuality'];
            $review->friendliness = $input['friendliness'];
            $review->communication = $input['communication'];
            $review->performance = $input['performance'];
            $review->save();
           return redirect()->route('ReviewedManagement');
        }

        $review = AppointmentOverview::find($id);
        $this->vars['reviews']=$review;
        $this->content = view(config('settings.theme').'.reviewed_management_edit')->with( $this->vars)->render();

        return $this->renderOutput();
    }
}
