<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Password;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }


    /**
     * Send a reset link to the given user.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function sendResetLinkEmail(Request $request)
    {
        $this->validateEmail($request);

        // We will send the password reset link to this user. Once we have attempted
        // to send the link, we will examine the response then see the message we
        // need to show to the user. Finally, we'll send out a proper response.
        $email = $request->get('email');

        $user = User::with('userPurchaserProfile')->with('userCarerProfile')->where('email', '=', $email)->get();
        if(!count($user)){
            return redirect()->back()->withErrors(
                ['email_not_exist' => 'WTF??']
            );
        }
        $user=$user->shift();
        $resquests = ['email' => $request->get('email')];      
        $request->session()->put('email', $request->get('email'));
            switch ($user->user_type_id) {
                case 1: $name='Purchaser';break;
                case 2: $name='Service user';break;
                case 3: $name='Carer';break;
                case 4: $name='Admin';break;
            }
        $request->session()->put('name', $name); 
        $response = $this->broker()->sendResetLink(
            $resquests
        );
        
        return $response == Password::RESET_LINK_SENT
            ? $this->sendResetLinkResponse($response)
            : $this->sendResetLinkFailedResponse($request, $response);
    }

}