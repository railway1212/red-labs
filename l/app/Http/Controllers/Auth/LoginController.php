<?php

namespace App\Http\Controllers\Auth;

use App\CarersProfile;
use App\Http\Controllers\Controller;
use App\PurchasersProfile;
use App\ServiceUsersProfile;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\DB;
use App\Events\LogSuccessfulLogin;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    //use AuthenticatesUsers;

    use AuthenticatesUsers {
        logout as performLogout;
    }

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    //protected $redirectTo = '/home';

    public function logout(Request $request)
    {
      //echo "<pre>"; print_r($request->all()); echo "<pre>"; exit;
       $return_url = config('settings.WP_URL');
        
        if (isset($_COOKIE['api_token'])) {
            unset($_COOKIE['api_token']);
            unset($_COOKIE['userid']);
            setcookie('api_token', null,  time() - 3600, '/');
            setcookie('userid', null,  time() - 3600, '/');
            setcookie('holmdata', null,  time() - 3600, '/');
            setcookie('family_name', null,  time() - 3600, '/');
            setcookie('service_user', null,  time() - 3600, '/');
            setcookie('user_type', null,  time() - 3600, '/');
            setcookie('first_name', null,  time() - 3600, '/');
            setcookie('image_name', null,  time() - 3600, '/');
            setcookie('ckeck_cookie', null,  time() - 3600, '/');
            setcookie('laravel_session', null,  time() - 3600, '/');
        } 
         $this->performLogout($request);
        Auth::logout();
        setcookie('laravel_session', null,  time() - 3600, '/');
        header('Location: '.$return_url);
        
        exit;
    }

    protected function redirectTo()
    {
        //user_type_id
        // 1 - purchaser
        // 2 - service user ???
        // 3 - carer
        // 4 - admin
        if( Cookie::get('invite')) {
            Cookie::forget('invite');
            return '/invite/refer-users';
        }
        if( Cookie::get('bookingFilter')) {
            Cookie::forget('bookingFilter');
            return '/carer-settings/booking/new/';
        }
        if( Cookie::get('bookingFilterCanceled')) {
            Cookie::forget('bookingFilterCanceled');
            return '/carer-settings/booking/canceled/';
        }
        if( Cookie::get('purchaserbookingFilterCanceled')) {
            Cookie::queue(Cookie::forget('purchaserbookingFilterCanceled'));
            return '/purchaser-settings/booking/canceled/';
        }

        if( Cookie::get('bookingFilterPurchaser')) {
            Cookie::queue(Cookie::forget('bookingFilter'));
            return '/purchaser-settings/booking/pending/';
        }
        if( Cookie::get('referUserProfilePublic')) {
            $url = route('ServiceUserProfilePublic',['serviceUserProfile'=> Cookie::get('referUserProfilePublic')]);
            Cookie::queue(Cookie::forget('referUserProfilePublic'));
            return $url;
        }
        if( Cookie::get('adminPanelEnter')) {
            $url =  Cookie::get('adminPanelEnter');
            $cookie = Cookie::forget('adminPanelEnter');
            return $url;
        }
        
        if( Cookie::get('bookingView') && Cookie::get('bookingViewPlace')) {
            $id = Cookie::get('bookingView');
            $place = Cookie::get('bookingViewPlace');
            Cookie::forget('bookingView');
            Cookie::forget('bookingViewPlace');
            return '/bookings/'.$id.'/details#'.$place;
        }

        if( Cookie::get('bookingDetailsOrPayment')){
            $uri = Cookie::get('bookingDetailsOrPayment');
            Cookie::forget('bookingDetailsOrPayment');
            return $uri;
        }

        if( Cookie::get('CarerRegistration')) {
            Cookie::forget('refer');
            return '/carer-registration/';
        }

        $user = Auth::user();
		event(new LogSuccessfulLogin($user));

        switch ($user->user_type_id) {
            case 1: {
                $purchaserProfile =  PurchasersProfile::findOrFail($user->id);
                $purchaserId = $purchaserProfile->id;

                $serviceUserProfile = ServiceUsersProfile::where('purchaser_id', $purchaserId)->
                    orderBy('id', 'desc')->first();

                /*if($purchaserProfile->registration_status == 'new'){
                    return '/purchaser-registration';
                }
                elseif($serviceUserProfile->registration_status == 'new') {*/
                    return '/service-registration/'.$serviceUserProfile->id;
               /* }*/
            }
            case 3 : {

                $carerProfile =  CarersProfile::findOrFail($user->id);

                if($carerProfile->registration_status == 'new')
                    return '/carer-registration';
                else
                    return'/carer-settings';
            }
            //case 4 : return '/admin';
        }


        return '/';
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
}
