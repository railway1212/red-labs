<?php

namespace App\Http\Controllers;

use App\CarersProfile;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use App\User;
use App\Page;
use App\Trust;



class HomePageController extends FrontController
{

    public function __construct() {
       parent::__construct();

       $this->template = config('settings.frontTheme').'.templates.homePage';
   }

    public function index(Request $request){
     


        $this->title = 'Holm Care | The Best Homecare for all of Greater Manchester |Call 0161 706 0288';
        $this->keywords='homecare, homecare support Manchester, live in elderly care, carer respite, Manchester, Trafford, salford, Bolton, bury, wigan, stockport, tameside, salford, oldham';
        $this->description='Homecare for the Elderly. Helping you find the best affordable care at home quickly and easily in Manchester, Bolton, Sale, Rochdale, Stockport, Salford, Oldham, Bury, Tameside, Trafford and Wigan.';
        
        
		
		//Get page data 
		$data = [];
		$page = Page::with('user')->where('slug', 'home')->first();
		$pageId = $page->id;
		$this->title = $page->meta_title;
        $this->keywords = $page->meta_keyword;
        $this->description = $page->meta_description;
        $testimonials =  DB::table('testimonials')->where('page_id','9')->get();//testimonials
        $banner =  DB::table('banners')->where('page_id', '9')->first();  //banner
        $trusts =  DB::table('trusts')->orderBy('id','desc')->get();  //trusts
        $homeFaqBg =  DB::table('contacts')->where('data_key','home_image')->first();
        $homeFaqContent =  DB::table('contacts')->where('data_key','home_contact')->first();

        $data = array_add($data,'content',$page->content);
        $data = array_add($data,'testimonials',$testimonials);
        $data = array_add($data,'banner',$banner);
        $data = array_add($data,'trusts',$trusts);
        $data = array_add($data,'homeFaqBg',$homeFaqBg->data_val);
        $data = array_add($data,'homeFaqContent',$homeFaqContent->data_val);
        
        


        $header = view(config('settings.frontTheme').'.headers.baseHeader')->render();
        $footer = view(config('settings.frontTheme').'.footers.baseFooter')->render();
        $modals = view(config('settings.frontTheme').'.includes.modals')->render();

        $this->vars = array_add($this->vars,'header',$header);
        $this->vars = array_add($this->vars,'footer',$footer);
        $this->vars = array_add($this->vars,'modals',$modals);

        $topCarers = collect();

        foreach ($this->getTopCarers() as $carer){

            $topCarers->push(CarersProfile::find($carer->id));
        }

        //dd($topCarers);

        $this->vars = array_add($this->vars,'topCarers',$topCarers);

        $this->content = view(config('settings.frontTheme').'.homePage.homePage',$data)->with($this->vars)->render();

        return $this->renderOutput();
    }

    public function getTopCarers(){

        return DB::select("SELECT cp.id, cp.first_name,cp.family_name,cp.sentence_yourself 
                          FROM `carers_profiles` as cp
                        WHERE registration_progress=20 and profiles_status_id=2  LIMIT 0,12");

    }

    public function  unsubscribe($id){

        $user = User::find($id);
        if(isset($user->email)){
            $user->subscribe=0;
            $user->save();
        }

        $header = view(config('settings.frontTheme').'.headers.baseHeader')->render();
        $footer = view(config('settings.frontTheme').'.footers.baseFooter')->render();
        $modals = view(config('settings.frontTheme').'.includes.modals')->render();

        $vars = array();
        $vars = array_add($vars,'title','Holm');
        $vars = array_add($vars,'description','');
        $vars = array_add($vars,'keywords','');
        $vars = array_add($vars,'keywords','');
        $vars = array_add($vars,'header',$header);
        $vars = array_add($vars,'footer',$footer);
        $vars = array_add($vars,'modals',$modals);



        $content = view(config('settings.frontTheme').'.homePage.ThankYouUnsubscribe')->render();
        $vars = array_add($vars,'content',$content);

        return view(config('settings.frontTheme').'.templates.homePage')->with($vars);
    }

    public function activeUser(){

    }
}
