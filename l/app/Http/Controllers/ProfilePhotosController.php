<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Artisan;

use App\ServiceUsersProfile;
use App\User;
use Illuminate\Http\Request;
use Auth;
use Intervention\Image\Facades\Image;
use Storage;

class ProfilePhotosController extends Controller
{
    public function uploadUserProfilePhoto(Request $request, $carerId)
    {
 
        $image = $request->image;
        $width = Image::make($image)->width();
        $height = Image::make($image)->height();
  
  
        if ($width / $height > 1) {
            
            // resize image to new width but do not exceed original size
            $img = Image::make($image)->widen(112, function ($constraint) {
                $constraint->upsize();
               
            });
          
         } else {
           
            // resize image to new height but do not exceed original size
            $img = Image::make($image)->heighten(112, function ($constraint) {
                $constraint->upsize();
            });
        }
      
        $img->save('public/img/profile_photos/' . $carerId . '.png');
        Cache::flush();
        $exitCode =  Artisan::call('cache:clear');
      //dd($exitCode);
        return $exitCode;
    }

    public function uploadServiceUserProfilePhoto(Request $request, $serviceUserId)
    {

        $image = $request->image;

        $width = Image::make($image)->width();
        $height = Image::make($image)->height();

        if ($width / $height > 1) {
            // resize image to new width but do not exceed original size
            $img = Image::make($image)->widen(112, function ($constraint) {
                $constraint->upsize();
            });
        } else {
            // resize image to new height but do not exceed original size
            $img = Image::make($image)->heighten(112, function ($constraint) {
                $constraint->upsize();
            });
        }

        $img->save('public/img/service_user_profile_photos/' . $serviceUserId . '.png');

        Cache::flush();
        $exitCode =  Artisan::call('cache:clear');
        return $exitCode;
    }

    private function autoRotateImage($image) {
        $orientation = $image->getImageOrientation();

        switch($orientation) {
            case \imagick::ORIENTATION_BOTTOMRIGHT:
                $image->rotateimage("#000", 180); // rotate 180 degrees
                break;

            case \imagick::ORIENTATION_RIGHTTOP:
                $image->rotateimage("#000", 90); // rotate 90 degrees CW
                break;

            case \imagick::ORIENTATION_LEFTBOTTOM:
                $image->rotateimage("#000", -90); // rotate 90 degrees CCW
                break;
        }

        // Now that it's auto-rotated, make sure the EXIF data is correct in case the EXIF gets saved with the image!
        $image->setImageOrientation(\imagick::ORIENTATION_TOPLEFT);
    }
}
