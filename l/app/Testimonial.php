<?php

namespace App;

use DateTime;
use Illuminate\Database\Eloquent\Model;

class Testimonial extends Model
{
    protected $fillable = ['client_name'];


    public function user()
    {
        return $this->belongsTo(User::class);
    }

}
