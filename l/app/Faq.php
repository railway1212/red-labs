<?php

namespace App;

use DateTime;
use Illuminate\Database\Eloquent\Model;

class Faq extends Model
{
    protected $fillable = ['title'];


    public function user()
    {
        return $this->belongsTo(User::class);
    }

}
