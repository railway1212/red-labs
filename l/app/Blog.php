<?php

namespace App;

use DateTime;
use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    protected $fillable = ['title', 'body'];


    public function user()
    {
        return $this->belongsTo(User::class);
    }

//    public function tags()
//    {
//        return $this->belongsToMany('App\Tag')->withTimestamps();
//    }

    public function getCreated_atAttribute($value){
        return date('d/m/Y',strtotime($value));
    }
}
