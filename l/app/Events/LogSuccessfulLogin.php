<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Support\Facades\DB;
use App\User;

class LogSuccessfulLogin
{
    /**
     * Create the event listener.
     *
     * @param  Request  $request
     * @return void
     */
    public function __construct(User $user)
    {
        //$this->request = $request;
        
		$userId = $user->id;
		$ipAddress =  \Request::ip();
		$location = \Location::get( $ipAddress );
		$country = $location->countryCode;
		$state = $location->regionName;
		$city = $location->cityName;	
		$zip = $location->zipCode;

		DB::table('ip_monitoring')->insert(
				[
				  'ip_address' => $ipAddress,
				  'country'   => $country ,
				  'state'   => $state ,
				  'city'   => $city ,
				  'zip'   => $zip ,
				  'user_id'    => $userId
				]
		);
 
        
    }
      /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
