<?php

namespace App\Events;

use App\Appointment;
use App\CarersProfile;
use App\PurchasersProfile;
use App\User;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;


class AppointmentCompletedEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Appointment $appointment)
    {
        //Analyze for bonuses;
        $booking = $appointment->booking;
        $users = [];
        $users[] = $booking->bookingCarer;
        $users[] = $booking->bookingPurchaser;
	
        foreach ($users as $user){
          
            if($user->referral_code !== null){
             
                $hours = $user->completed_appointments_hours;
                //$referralUser = User::where('own_referral_code', $user->referral_code)->first();
               
                if($hours >= 20){
                    $referralUser = User::where('own_referral_code', $user->referral_code)->first();
                   
                    if ($user->isCarer())
                      $referralUserName = CarersProfile::where('id', $user->id)->pluck('like_name')[0];
                 
                    else $referralUserName = PurchasersProfile::where('id', $user->id)->pluck('like_name')[0];
                    //Если еще не получал бонусов
                
                    if(!$user->bonusPayouts()->where('bonus_type_id', 2)->where('referral_user_id', $referralUser->id)->get()->count()) {
                        $val=$user->bonusPayouts()->create([
                            'bonus_type_id' => 2,
                            'amount' => 100,
                            'referral_user_id' => $referralUser->id,
                            'is_primary' => 0,
                        ]);
                       
                        $referralUser->bonusPayouts()->create([
                            'bonus_type_id' => 2,
                            'amount' => 100,
                            'referral_user_id' => $user->id,
                            'is_primary' => $val->id,
                        ]);

                        $text = view(config('settings.frontTheme') . '.emails.bonuses_assigned')->with([
                            'user' => $user, 'referralUser' => $referralUser, 'referralUserName'=>$referralUserName, 'sendTo' => 'user'
                        ])->render();

                        DB::table('mails')
                            ->insert(
                                [
                                    'email' => $user->email,
                                    'subject' => 'Referral bonus',
                                    'text' => $text,
                                    'time_to_send' => Carbon::now(),
                                    'status' => 'new'
                                ]);
                        DB::table('mails')
                            ->insert(
                                [
                                    'email' => 'nik@holm.care',
                                    'subject' => 'Referral bonus',
                                    'text' => $text,
                                    'time_to_send' => Carbon::now(),
                                    'status' => 'new'
                                ]);

                        $text = view(config('settings.frontTheme') . '.emails.bonuses_assigned')->with([
                            'user' => $user, 'referralUser' => $referralUser, 'referralUserName'=>$referralUserName, 'sendTo' => 'referralUser'
                        ])->render();

                        DB::table('mails')
                            ->insert(
                                [
                                    'email' => $referralUser->email,
                                    'subject' => 'Referral bonus',
                                    'text' => $text,
                                    'time_to_send' => Carbon::now(),
                                    'status' => 'new'
                                ]);

                        DB::table('mails')
                            ->insert(
                                [
                                    'email' => 'nik@holm.care',
                                    'subject' => 'Referral bonus',
                                    'text' => $text,
                                    'time_to_send' => Carbon::now(),
                                    'status' => 'new'
                                ]);

                    }
                }
            }
        }
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
