<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StripeTransfer extends Model
{
    public $incrementing = false;
    protected $fillable = [
        'id',
        'booking_id',
        'bonus_id',
        'connected_account_id',
        'amount',
      	'appointment_id',
    ];

    /**
     * Relations
     */
    public function booking(){
        return $this->belongsTo(Booking::class);
    }
}
