<?php

namespace App;

use App\Interfaces\Constants;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Auth;
use App\Holiday;
use DB;

class Appointment extends Model implements Constants
{
    protected $fillable = ['booking_id', 'date_start', 'date_end', 'amount_for_purchaser', 'amount_for_carer', 'status_id', 'carer_status_id', 'carer_status_date', 'purchaser_status_id', 'purchaser_status_date', 'time_from', 'time_to', 'periodicity', 'price_for_purchaser', 'price_for_carer', 'batch', 'payout', 'reminder_sent', 'use_register_code','live_in_care_type'];

    /**
     * Relations
     */
    public function assistance_types()
    {
        return $this->belongsToMany('App\AssistanceType', 'appointments_assistance_types');
    }
    public function booking()
    {
        return $this->belongsTo('App\Booking');
    }
    public function disputePayment()
    {
        return $this->belongsTo('App\DisputePayment','id','appointment_id');
    }
    public function transaction()
    {
        return $this->belongsTo('App\AppointmentPayment','id','appointment_id');
    }
    public function appointmentStatus ()
    {
        return $this->belongsTo('App\AppointmentStatus','status_id','id');
    }
    public function appointmentStatusCarer ()
    {
        return $this->belongsTo('App\AppointmentUserStatus','carer_status_id','id');
    }
    public function appointmentStatusPurchaser ()
    {
        return $this->belongsTo('App\AppointmentUserStatus','purchaser_status_id','id');
    }

    public function overviews(){
        return $this->hasMany(AppointmentOverview::class, 'appointment_id');
    }

    /**
     * Accessors
     */
    public function getAssistanceTypesTextAttribute(){

        return implode(', ', array_pluck($this->assistance_types, 'name'));
    }
    
    public function getFormattedDateStartAttribute()
    {
        return date('d-m-Y', strtotime($this->date_start));
    }

    public function getHoursAttribute(){
//        $timeFrom = (float)$this->time_from;
//        $timeTo = (float)$this->time_to;

        $timeFrom = \Carbon\Carbon::createFromFormat('H.i', $this->time_from);
        $timeTo = \Carbon\Carbon::createFromFormat('H.i', $this->time_to);

        $hours = 0;
		//echo "Time From ".$timeFrom."+++ TIme TO". $timeTo;
        if ($timeTo > $timeFrom) {
                $diff_in_minutes = $timeFrom->diffInMinutes($timeTo);
        } elseif ($timeTo == $timeFrom) {
            $diff_in_minutes = 0;
        } else { //$timeTo < $timeFrom
            $diff_in_minutes = $timeTo->diffInMinutes($timeFrom);
            $diff_in_minutes = 1440 - $diff_in_minutes;
        }
        $hours = intdiv($diff_in_minutes, 60) . "." . ($diff_in_minutes % 60);
//        $hours = $diff_in_minutes;
 
        return $hours;
    }

    public function getPriceAttribute(){
        $price = 0;
        $timeFrom = round($this->time_from);
        $timeTo = round($this->time_to);
		
        if($timeTo > $timeFrom){
          for ($i = $timeFrom; $i < $timeTo; $i++) {
            $price += $this->getHourPrice($i, $this->date_start);
          }
        } elseif ($timeTo == $timeFrom) {
            for ($i = $timeFrom; $i < $timeFrom + 24; $i++) {
                $price += $this->getHourPrice($i, $this->date_start);
            }
        } else {
          for ($i = 0; $i < $timeTo; $i++) {
            $price += $this->getHourPrice($i, $this->date_start);
          }

        }

        return $price;
    }

    public function getCarerPriceAttribute(){
		
        if($this->price_for_carer)
            return $this->price_for_carer;
        $price = 0;
		if($this->periodicity == 'live_in_carer'){
        	if($this->live_in_care_type == 1){
				$liveDetails 		= 	LiveCarer::find(1);
           		$perDayprice 		= 	$liveDetails->carer_rate;
				$price 				=	$perDayprice;
			}else if($this->live_in_care_type == 2){
				$liveDetails 		= 	LiveCarer::find(2);
           		$perWeekprice 		= 	$liveDetails->carer_rate;
				$price 				=	$perWeekprice/7;
			}
        }else{
        	$timeFrom = floor($this->time_from);
            $timeTo = floor($this->time_to);
            if($timeTo > $timeFrom){ //echo 11;
              for ($i = $timeFrom; $i < $timeTo; $i++) {
                $price += $this->getCarerHourPrice($i, $this->date_start);
              }

            } elseif ($timeTo == $timeFrom) {// echo 12;
                for ($i = $timeFrom; $i < $timeFrom + 24; $i++) {
                    $price += $this->getCarerHourPrice($i, $this->date_start);
                }
            } else { //echo 13;
                for ($i = 0; $i < 24 - ($timeFrom - $timeTo); $i++) {
                    $price += $this->getCarerHourPrice($i, $this->date_start);
                }
            }
            $price += $this->getCarerFractionPrice($this->time_from, $this->time_to);
         // echo "<pre> asfg"; print_r($price); echo "</pre>"; exit;
		}
        return $price;
    }

    public function getPurchaserPriceAttribute(){
       // echo $this->id.' ID';
//        echo $this->booking->appointments()->first()->id.' ';
//        $user = Auth::user();
        $user = $this->booking->bookingPurchaser;
        if($this->price_for_purchaser)
            return $this->price_for_purchaser;
	//echo "periodicity".$this->periodicity; exit;
        $price = 0;
		if($this->periodicity == 'live_in_carer'){
        	if($this->live_in_care_type == 1){
				$liveDetails 		= 	LiveCarer::find(1);
           		$perDayprice 		= 	$liveDetails->purchaser_rate;
				$price 				=	$perDayprice;
			}else if($this->live_in_care_type == 2){
				$liveDetails 		= 	LiveCarer::find(2);
           		$perWeekprice 		= 	$liveDetails->purchaser_rate;
				$price 				=	$perWeekprice/7;
			}

        }else{
          
            $timeFrom = floor($this->time_from);
            $timeTo = floor($this->time_to);
 
            if ($user->user_type_id == 1 && (($user->use_register_code == 1 && $this->id == $this->booking->appointments()->first()->id) || ($this->use_register_code == 1))) {
                if ($timeTo > $timeFrom || $timeTo == '00.00') {
                    if ($timeTo == '00.00') {
                        $timeTo = '24.00';
                    }
                    for ($i = $timeFrom + 1; $i < $timeTo; $i++) {
                        $price += $this->getPurchaserHourPrice($i, $this->date_start);
                    }
                } elseif ($timeTo == $timeFrom) {
                    for ($i = $timeFrom + 1; $i < $timeFrom + 24; $i++) {
                        $price += $this->getPurchaserHourPrice($i, $this->date_start);
                    }
                } else { // $timeTo < $timeFrom
                 
                    for ($i = 1; $i < 24 - ($timeFrom - $timeTo); $i++) {
                            $price += $this->getPurchaserHourPrice($i, $this->date_start);
                        }
                }
              
            } else { 
              // echo 1223;
             // //echo $timeFrom."+++".$timeTo;
                //Пройтись по целым
                if ($timeTo > $timeFrom || $timeTo == '00.00') {// echo 12;
                    if ($timeTo == '00.00') {
                        $timeTo = '24.00';
                    }
                    for ($i = $timeFrom; $i < $timeTo; $i++) {
                        $price += $this->getPurchaserHourPrice($i, $this->date_start);
                    }
                } elseif ($timeTo == $timeFrom && ($timeTo-$timeFrom) < 0) { //echo 23;
                    for ($i = $timeFrom; $i < 24; $i++) {
                        $price += $this->getPurchaserHourPrice($i, $this->date_start);
                    }
                }elseif ($timeTo == $timeFrom && ($timeTo-$timeFrom) == 0) {
                    for ($i = $timeFrom; $i < $timeFrom ; $i++) {
                        $price += $this->getPurchaserHourPrice($i, $this->date_start);
                    }
                } else { // $timeTo < $timeFrom
                  //echo 34;
                    for ($i = 0; $i < 24 - ($timeFrom - $timeTo); $i++) {
                            $price += $this->getPurchaserHourPrice($i, $this->date_start);
                        }
                }
             // echo "Price".$price;
            }
          
//echo $this->time_from."_____________".$this->time_to;
            $price += $this->getPurchaserFractionPrice($this->time_from, $this->time_to);
        }
      
        return $price;
    }

    private function recalculateTimeFraction($timeFraction){
        switch (round($timeFraction, 2)){
            case 0.15: return 0.25;
            case 0.30: return 0.50;
            case 0.45: return 0.75;
          	case 0: return 0;
            default: return 0;
        }
    }


    private function getPurchaserFractionPrice($from, $to){
     // echo $from."$$$$$$$".$to;
        $fractionPrice = 0;
        $hourFrom = floor($from);
        $timeFromFraction = $from - $hourFrom;
      //echo "timeFromFraction".$timeFromFraction;
        $timeFromFraction = $this->recalculateTimeFraction($timeFromFraction);
	
        $hourTo = floor($to);
        $timeToFraction = $to - $hourTo;
        $timeToFraction = $this->recalculateTimeFraction($timeToFraction);
     // echo "$timeToFraction".$timeToFraction;
        $carerProfile = $this->booking->bookingCarerProfile;

        //Высчитаем from
        if ($this->isDayHoliday($this->date_start)) {
            $fractionPrice -= $carerProfile->holiday_price * $timeFromFraction;
        } elseif($this->isHourNight($hourFrom)){
            $fractionPrice -=  $carerProfile->night_price * $timeFromFraction;
        } else {
            $fractionPrice -= $carerProfile->price * $timeFromFraction;
        }
      
        //Высчитаем to
        if ($this->isDayHoliday($this->date_end)) {
            $fractionPrice += $carerProfile->holiday_price * $timeToFraction;
        } elseif($this->isHourNight($hourTo)){
            $fractionPrice +=  $carerProfile->night_price * $timeToFraction;
        } else {
            $fractionPrice += $carerProfile->price * $timeToFraction;
        }
//echo "timeToFraction".$timeToFraction."++".$fractionPrice;
        return $fractionPrice;
    }

    private function getCarerFractionPrice($from, $to){
      
        $fractionPrice = 0;
        $hourFrom = floor($from);
        $timeFromFraction = $from - $hourFrom;
        $timeFromFraction = $this->recalculateTimeFraction($timeFromFraction);

        $hourTo = floor($to);
        $timeToFraction = $to - $hourTo;
        $timeToFraction = $this->recalculateTimeFraction($timeToFraction);
        $carerProfile = $this->booking->bookingCarerProfile;

        //Высчитаем from
        if ($this->isDayHoliday($this->date_start)) {
            $fractionPrice -= $carerProfile->holiday_wage * $timeFromFraction;
        } elseif($this->isHourNight($hourFrom)){
            $fractionPrice -=  $carerProfile->night_wage * $timeFromFraction;
        } else {
            $fractionPrice -= $carerProfile->wage * $timeFromFraction;
        }
        //Высчитаем to
        if ($this->isDayHoliday($this->date_end)) {
            $fractionPrice += $carerProfile->holiday_wage * $timeToFraction;
        } elseif($this->isHourNight($hourTo)){
            $fractionPrice +=  $carerProfile->night_wage * $timeToFraction;
        } else {
            $fractionPrice += $carerProfile->wage * $timeToFraction;
        }

        return $fractionPrice;
    }

    private function getHourPrice(int $hour, $date) : float {
        $user = Auth::user();
        $carerProfile = $this->booking->bookingCarerProfile;
        if($user && $user->user_type_id == 3){
            if ($this->isDayHoliday($date)) {
                return $carerProfile->holiday_wage;
            } elseif($this->isHourNight($hour)){
                return $carerProfile->holiday_wage;
            } else {
                return $carerProfile->wage;
            }
        } else {
            if ($this->isDayHoliday($date)) {
                return isset($carerProfile->holiday_price)?$carerProfile->holiday_price:0;
            } elseif($this->isHourNight($hour)){
                return isset($carerProfile->night_price)?$carerProfile->night_price:0;
            } else {
                return isset($carerProfile->price) ? $carerProfile->price : 0;
            }
        }
    }

    /*private function getCarerHourPrice(int $hour, $date) : float {
		
        $carerProfile = $this->booking->bookingCarerProfile;
  
    	 if ($this->isDayHoliday($date)) {
          return $carerProfile->holiday_wage;
           
     } elseif($this->isHourNight($hour)){
          if($carerProfile->night_wage != null)
           return $carerProfile->night_wage;
           /* this condition is on 18 june 2019
           else
           return 0.00;
        } else { 
            return $carerProfile->wage;
        }
      
    }*/
  
  	private function getCarerHourPrice(int $hour, $date) : float {
		
        $carerProfile = $this->booking->bookingCarerProfile;
        if ($this->isDayHoliday($date)) { //echo 11;
            return $carerProfile->holiday_price;
        } elseif($this->isHourNight($hour)){ //echo 22;
           return $carerProfile->night_price;
        } else { //echo 33;
            return $carerProfile->price;
        }
    }


    private function getPurchaserHourPrice(int $hour, $date) : float {
  
		//echo |"Date Hour".$date."++".$hour;
        $carerProfile = $this->booking->bookingCarerProfile;
    	//echo "<p-re>";print_r($carerProfile->night_price); echo "<p-re>"; exit;
        if ($this->isDayHoliday($date)) {
          return $carerProfile->holiday_purchaser_price;
        } elseif($this->isHourNight($hour)){
        
            return $carerProfile->night_purchaser_price;
        } else {
            return $carerProfile->purchaser_price;
        }
    }


    public function getCancelableAttribute(){
      
   		$minutes =  \Carbon\Carbon::parse(date("Y-m-d ", strtotime($this->date_start)).' '.$this->time_from)->diffInMinutes();
		$hours =   $minutes / 60 ;
		return($hours > 24 && !$this->is_past);
		
       // return (\Carbon\Carbon::parse(date("Y-m-d ", strtotime($this->date_start)).' '.$this->time_from)->diffInHours() > 24 && !$this->is_past);
    }

    public function getIsPastAttribute(){
        return \Carbon\Carbon::parse(date("Y-m-d ", strtotime($this->date_start)).' '.$this->time_from)->isPast();
    }

    public function getFormattedTimeFromAttribute(){
        return date("h:i A", strtotime(str_replace('.', ':', $this->time_from)));
    }

    public function getFormattedTimeToAttribute(){
        return date("h:i A", strtotime(str_replace('.', ':', $this->time_to)));
    }

	/*public function getLiveInCareTypeAttribute(){
		return $this->live_in_care_type;
    }*/

    private function isHourNight(int $hour){
     
        $nightHours = ['from' => 20, 'to' => 8];
      
        return $hour >= $nightHours['from'] || $hour < $nightHours['to'];
    }

    private function isDayHoliday($date){
     
        $dt = Carbon::parse($date);
     
        $sql = 'SELECT * FROM holidays WHERE  date >= \''.$dt->format("Y-m-d").'\' AND date < \''.$dt->addDays(1)->format("Y-m-d").'\'';
     
        $res = DB::select($sql);
      	$res_count = count($res).'.0';
      
     
  
     // return $dt->dayOfWeek == 1  || count($res) > 0; commented on 18 june 2019
        return $dt->dayOfWeek == 1  || $res_count > 0;
    }
	
}
