<?php

namespace App;

use DateTime;
use Illuminate\Database\Eloquent\Model;

class Banner extends Model
{
    protected $fillable = ['page_id'];


    public function user()
    {
        return $this->belongsTo(User::class);
    }

}
