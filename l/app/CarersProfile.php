<?php

namespace App;

use App\Interfaces\Constants;
use DateTime;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\SearchController;
use App\Postcode;

class CarersProfile extends Model implements Constants
{
    public function user()
    {
        return $this->belongsTo(User::class, 'id');
    }

    public function Postcode()
    {
        return $this->belongsTo('App\Postcode');
    }

    public function ServicesTypes()
    {
        return $this->belongsToMany('App\ServiceType', 'carer_profile_service_type', 'carer_profile_id', 'service_type_id');
    }

    public function AssistantsTypes()
    {
        return $this->belongsToMany('App\AssistanceType', 'carer_profile_assistance_type', 'carer_profile_id', 'assistance_types_id');
    }

    public function WorkingTimes()
    {
        return $this->belongsToMany('App\WorkingTime', 'carer_profile_working_time', 'carer_profile_id', 'working_times_id');
    }

    public function Languages()
    {
        return $this->belongsToMany('App\Language', 'carer_profile_language', 'carer_profile_id', 'language_id');
    }

    public function CarerReferences()
    {
        return $this->belongsToMany('App\CarerReference', 'carer_profile_reference', 'carer_profile_id', 'reference_id');
    }

    public function CarerWages() //todo why belongsTo?
    {
        return $this->belongsTo('App\CarerWages', 'carer_wages', 'id', 'carer_id');
    }

    public function CarerWage() //todo why belongsTo?
    {
        return $this->hasOne(CarerWages::class, 'carer_id');
    }

    public function getImgUrlAttribute(){
        return '/img/profile_photos/'.$this->id.'.png?a='.rand(1, 9999);
    }

    public function profileStatus(){
        return $this->belongsTo('App\UserStatus','profiles_status_id','id');
    }

    public function setDoBAttribute($value)
    {
        $date = DateTime::createFromFormat('d/m/Y', $value);

        $this->attributes['DoB'] = $date->format('Y-m-d H:i:s');

    }

    public function getDoBAttribute($value)
    {
        return date('d/m/Y',strtotime($value));
    }


    public function setDriverLicenceValidUntilAttribute($value)
    {
        $date = DateTime::createFromFormat('d/m/Y', $value);
        $this->attributes['driver_licence_valid_until'] = $date->format('Y-m-d H:i:s');

    }

    public function getDriverLicenceValidUntilAttribute($value)
    {
        return date('d/m/Y',strtotime($value));
    }

    public function setDateCertificateAttribute($value)
    {
        $date = DateTime::createFromFormat('d/m/Y', $value);
        $this->attributes['date_certificate'] = $date->format('Y-m-d H:i:s');

    }


    /**
     * @param $value
     * @return bool|false|string
     */
    public function getDateCertificateAttribute($value)
    {
        return (!empty($value))?date('d/m/Y',strtotime($value)):false;
    }

    public function setCarInsuranceValidUntilAttribute($value)
    {
        $date = DateTime::createFromFormat('d/m/Y', $value);
        $this->attributes['car_insurance_valid_until'] = $date->format('Y-m-d H:i:s');

    }

    public function getCarInsuranceValidUntilAttribute($value)
    {
        return date('d/m/Y',strtotime($value));
    }

    public function setDbsDateAttribute($value)
    {
		 //dd($value);
		 $date = DateTime::createFromFormat('d/m/Y', $value);
        if($date)
            $this->attributes['dbs_date'] = $date->format('Y-m-d H:i:s');
        else
            $this->attributes['dbs_date'] = null;

    }
    public function getDbsDateAttribute($value)
    {
        return date('d/m/Y',strtotime($value));
    }

    public function getRateAttribute()
    {
        $res = DB::SELECT('SELECT
                      CEILING(AVG(o.punctuality)) as avg_punctuality,
                      CEILING(AVG(o.friendliness)) as avg_friendliness,
                      CEILING(AVG(o.communication)) as avg_communication,
                      CEILING(AVG(o.performance)) as avg_performance,
                      CEILING(((AVG(o.punctuality) + AVG(o.friendliness) + AVG(o.communication) + AVG(o.performance)) /4)) as avg_total
                    FROM appointments_overviews o
                    LEFT JOIN appointments a ON o.appointment_id = a.id
                    LEFT JOIN bookings b  ON a.booking_id = b.id
                    LEFT JOIN users u ON b.carer_id = u.id
                    WHERE u.id = ' . $this->id . ' AND o.accept = 1');
        $data = $res[0];
        return $data;
    }

    public function bookings()
    {
     
        return $this->hasMany('App\Booking','carer_id','id');
    }

    public function carerReviews()
    {
        return DB::select("SELECT sup.`id`, sup.`first_name`, sup.`family_name`, sup.`town`, ao.`comment`, 
(ao.`punctuality`+ao.`friendliness`+ao.`communication`+ao.`performance`)/4 as raiting,DATE_FORMAT(ao.`created_at`, '%m/%d/%Y') as created_at
FROM `bookings`
INNER JOIN `appointments` as a ON `bookings`.`id` = a.`booking_id`
INNER JOIN `appointments_overviews` as ao ON a.`id` = ao.`appointment_id`
INNER JOIN `service_users_profiles` as sup ON `bookings`.`service_user_id`= sup.`id`
WHERE `bookings`.`carer_id` = " . $this->id . " and ao.`accept` = 1 order by ao.`created_at` desc");

    }

    public function getEmailAttribute()
    {
        $res = DB::select("select email from users where id = ".$this->id);

        $data = $res[0]->email;

        return $data;
    }

    public function getOwnReferralCodeAttribute()
    {
        $res = DB::select("select own_referral_code from users where id = ".$this->id);

        $data = $res[0]->own_referral_code;

        return $data;
    }


    /**
     * @return false
     */
    public function getNtaAttribute()
    {
        return false;
    }

    /**
     * @return string
     */

    public function getUserTypeAttribute()
    {
        return 'carer';
    }

    public function getShortNameAttribute()
    {
        return $this->first_name.' '.$this->family_name[0].'.';
    }
    
    public function getFullNameAttribute()
    {
        return $this->first_name.' '.$this->family_name;
    }

    //Wages (carer gets)
    public function getWageAttribute(){
        if($this->CarerWage)
           return $this->CarerWage->hour_rate;
       // return self::CARER_RATE_DAY;
    }
    public function getNightWageAttribute(){
       if($this->CarerWage)
            return $this->wage * 1.2;
      // return $this->wage * 1.2;
    }
    public function getHolidayWageAttribute(){
        if($this->CarerWage)
           return $this->wage * 1.5;
        //return self::CARER_RATE_HOLIDAYS;
    }

  
    //prices (Carer pay)
    public function getPriceAttribute(){
    
    	$fee = Fees::find(1);
      	if($fee){
       
         return $fee->carer_rate;
       	}

     //return self::PURCHASER_RATE_DAY;
    }
    public function getNightPriceAttribute(){
      $fee = Fees::find(2);
      if($fee){
            return $fee->carer_rate;
       }

        //return self::PURCHASER_RATE_NIGHT;
    }
    public function getHolidayPriceAttribute(){
        $fee = Fees::find(3);
        if($fee){
           return $fee->carer_rate;
       }

       // return self::PURCHASER_RATE_HOLIDAYS;
    }
  
  //prices (Purchaser pay)
    public function getPurchaserPriceAttribute(){
    
    	$fee = Fees::find(1);
      	if($fee){
       
         return $fee->purchaser_rate;
       	}

     //return self::PURCHASER_RATE_DAY;
    }
    public function getNightPurchaserPriceAttribute(){
      $fee = Fees::find(2);
      if($fee){
            return $fee->purchaser_rate;
       }

        //return self::PURCHASER_RATE_NIGHT;
    }
    public function getHolidayPurchaserPriceAttribute(){
        $fee = Fees::find(3);
        if($fee){
           return $fee->purchaser_rate;
       }

       // return self::PURCHASER_RATE_HOLIDAYS;
    }

    public static function getFilteredCarers($data = null){
      $tempArray = [];
      $lastLoggedFlag = 0;
      
        if($data == null)            
            return DB::select("Select cp.first_name, cp.family_name, cp.sentence_yourself, cp.town,
            cp.postcode from carers_profiles cp order by id desc");
        $where = 'left join review r on cp.id=r.carer_id';
        $where .= " where cp.profiles_status_id = 2 ";//Только подтвержденные админом
        $sort_by_distance = false;
        $page = (int)$data['page']-1;
        $having = ' having 1 ';
        $sort = '';
        $postCode = false;
        $from = false;
        $distance_is_filled = false;

        //echo "<pre>"; print_r($data); echo "<pre>"; exit;
        $cares = [];//Общий массив чекнутых care types
        // dd($cares);
        if(isset($data['types']))
        {
          	$lastLoggedFlag = 1;
            for ($i = 0; $i < count($data['types']); $i++) {
                $cares[$i] = $data['types'][$i];
            }
            (implode(',',$cares)) == "('')"? $careTypes = '': $careTypes = (implode(',',$data['types']));
            $where .= " and cpat.assistance_types_id in ($careTypes)";
            $having .= " AND count(distinct cpat.assistance_types_id) = " . count($data['types']) . ' ' ;
        }

        if(isset($data['post_code'])) {
          	$lastLoggedFlag = 1;
            $postCode = $data['post_code'];
            $postCode = str_replace(' ', '', trim($data['post_code']));
            if(strlen($postCode) > 2){
                $postCode = substr_replace($postCode, " ", 2, 0); 
            }
            // $where .= " and cp.postcode LIKE '%".$postCode."%'";
            
            if(!self::isExsistPostCode($postCode) && !self::isFirstPartPostcodeExist($postCode)) 
            return ['data' => [], 'pages' => 1, 'count' => 0,'error'=>'postcode not supported' ];                
        }
        
        if(isset($data['other'])){
          	$lastLoggedFlag = 1;
            (in_array('male', $data['other'])? $where .= " and cp.gender = 'Male' ": '');
            (in_array('female', $data['other'])? $where .= " and cp.gender = 'Female' ": '');
            (in_array('have_car', $data['other'])? $where .= " and cp.have_car = 'Yes' ": '');
            (in_array('work_with_pets', $data['other'])? $where .= " and (cp.work_with_pets != 'No' and cp.work_with_pets is not null) ": '');
        }

    
        if(isset($data['sort']) && $data['sort'] !== 'false'){
          	$lastLoggedFlag = 1;
            $sort_order = $data['sort_order'];
            if($data['sort'] == 'id') $sort = ' order by cp.id '.$sort_order.' ';
            elseif($data['sort'] == 'rating') $sort = ' order by r.avg_total '.$sort_order.' ';
            else{
                if($data['sort'] == 'distance') $sort_by_distance = true;

            }
        }
            
        // echo $sort_by_distance; exit;
        $languages = [];//Общий массив чекнутых языков
        if(isset($data['languages'])){
          	$lastLoggedFlag = 1;
            for ($i = 0; $i < count($data['languages']); $i++) {
                $languages[$i] = $data['languages'][$i];
            }
          
            (implode(',',$languages)) == "('')"? $languagesKnown = '': $languagesKnown = (implode(',',$data['languages']));
          
            $where .= " and cpl.language_id in ($languagesKnown)";
            $having .= " AND count(distinct cpl.language_id) = " . count($data['languages']) . ' ' ;
        }
      
        $dayWord = '';
        if(isset($data['date'])){
          	$lastLoggedFlag = 1;
            $daysOfWeek = array('Sunday', 'Monday', 'Tuesday', 'Wednesday','Thursday','Friday', 'Saturday');
            $date = DateTime::createFromFormat("d/m/Y", $data['date']);
            if($date){
                $dayNumber = $date->format("w");
                $dayWord = $daysOfWeek[$dayNumber];
            }
        }
        ($dayWord == '')? '': $where .= " and (LOWER(wt.name) like CONCAT(LOWER('$dayWord'), '%') or wt.id in (1, 2, 3, 4))";
//echo "<pre>"; print_r($data['type_of_service'][0]); echo "<pre>"; exit;
        if(isset($data['type_of_service'])) {
          
            $service_type = implode(',',$data['type_of_service']);
            $where .= " and cpst.service_type_id in ($service_type) ";
        }

    //    $sql_query = "Select cp.first_name, cp.family_name, cp.sentence_yourself, cp.town,
    //     cp.postcode, cp.id , r.avg_total, r.creview from 
    //     carers_profiles cp
    //     $where
    //     group by cp.first_name, cp.family_name, cp.sentence_yourself, cp.town,
    //     cp.postcode, cp.id, r.avg_total, r.carer_id, r.creview
    //     ".$having.$sort;
      
                 
        $sql_query = "Select cp.id , cp.first_name, cp.family_name, cp.sentence_yourself, cp.town,
        cp.postcode, r.avg_total, r.creview from (
        carers_profiles cp,
        carer_profile_assistance_type cpat,
        carer_profile_language cpl,
        carer_profile_working_time cpwt,
        working_times wt,
        carer_profile_service_type cpst
        )
        $where
        and cp.id = cpat.carer_profile_id
        and cp.id = cpl.carer_profile_id
        and cp.id = cpwt.carer_profile_id
        and cpwt.working_times_id = wt.id
        and cp.id = cpst.carer_profile_id
        group by cp.id, cp.first_name, cp.family_name, cp.sentence_yourself, cp.town,
        cp.postcode, r.avg_total, r.carer_id, r.creview
        ".$having.$sort;
        
        $pages = 'SELECT COUNT(*) as POSTS_COUNT  FROM ('.$sql_query.') a';
        $all_count = DB::select($pages)[0]->POSTS_COUNT;
        $pages_count =  (int)ceil ($all_count/100);
        $offset = 100 * $page;
      
      if(isset($data['sort']) && $data['sort']=='false'){
        $sql_query.='ORDER BY cp.id DESC';
      }
     
        if(!$sort_by_distance){
        	//$sql_query.='ORDER BY cp.id DESC';
            $sql_query.=' limit 100 ';
            $sql_query.=" offset $offset ";
        }
      
     	$carers =  DB::select($sql_query);
    // echo "<pre>sql_query ------- "; print_r($sql_query); echo "<pre>"; exit;
        // echo $postCode."<br> call here";
        //echo "<pre>"; print_r($sql_query); echo "<pre>"; //exit;
        foreach($carers as $carer){
            $carer->href_profile = '/carer/profile/'.$carer->id;
            if(file_exists(public_path('/img/profile_photos/' . $carer->id . '.png'))) $carer->image_profile = '/l/public/img/profile_photos/'. $carer->id . '.png?a='.rand(1, 9999);
            else $carer->image_profile = "/l/public/img/no_photo.png";
          
            $carer->price = Auth::check() && Auth::user()->id == $carer->id ? \App\CarersProfile::find($carer->id)->first()->wage : \App\CarersProfile::find($carer->id)->first()->price;
           
            $carer->creview = ($carer->creview === null) ? 0 : $carer->creview;
            
            
            if(!$postCode){
                if(Auth::check()) {
                    switch (Auth::user()->user_type_id) {
                        case 1:
                            $purchaser = PurchasersProfile::find(Auth::user()->id);
                            if ($purchaser->active_user != null) {
                                $servise = ServiceUsersProfile::find($purchaser->active_user);
                                $from = urlencode(trim(($servise->postcode)));
                            } else
                                $from = (trim($purchaser->town . ' ' . $purchaser->address_line1));
                            break;
                        case 2:
                            $servise = ServiceUsersProfile::find(Auth::user()->id);
                            $from = urlencode(trim(($servise->postcode)));
                            break;
                        case 3:
                            $carer = CarersProfile::find(Auth::user()->id);
                            $from = urlencode(trim(($carer->postcode)));
                            break;
                        case 4:
                            $from = false;
                            break;
                    }
                }
            }else $from =$postCode." Manchester";

            if($from)
            {              
                $to = urlencode(trim($carer->postcode));
                $distance = SearchController::getDistance($from, $to);
                $distance = (float)explode(' ', $distance)[0];
                $carer->distance = ($distance == '0') ? '0' : $distance;
                $carer->distance = (int)round($distance*100);
                $distance_is_filled = true;
            }
        }
      //  echo "<pre>"; print_r($data); echo "<pre>"; exit;

      	if($sort_by_distance && $distance_is_filled){           
            usort($carers, function($a, $b) {
              if(isset($a->distance)){
                return $a->distance - $b->distance;
                 }
            });
          
            if($sort_order=='asc') $carers = array_reverse($carers);
            $carers = array_slice($carers, $offset, 20);
        }

         if(isset($data['sort']) && $data['sort'] !== 'false')
        {        
         	if($sort_order=='asc') $carers = array_values($carers);          
          
          //--------------commented on 21 june 2019
          //usort($carers, function($a, $b) {
         // if(isset($a->distance))
           // return $a->distance - $b->distance;
          // });
                   
        // $carers = array_values($carers);
          
          //---------------------------
          
         //print_r(array_values($carers));        
        // $carers = array_slice($carers, $offset, 10);
    }
  // echo "<pre>"; print_r($data); echo "<pre>"; exit;
        foreach($carers as &$care){
            $care->family_name = substr($care->family_name,0,20);
            if(isset($care->distance) && !empty($care->distance)){
                $care->distance /= 100;
            } else {
                $care->distance = -1;
            } 
        }
      
   //   echo "<pre> lastLoggedFlag -------- "; print_r(($lastLoggedFlag)); echo "<pre>";  exit; 
                 
          if(isset($lastLoggedFlag) && $lastLoggedFlag==0){

            //  $sql ="SELECT DISTINCT * from ip_monitoring Order by created_at DESC";
            $sql = "SELECT ipm.* FROM ip_monitoring ipm INNER JOIN (SELECT user_id, MAX(created_at) AS Maxcreated_at FROM ip_monitoring GROUP BY user_id) groupedtt ON ipm.user_id = groupedtt.user_id  AND ipm.created_at = groupedtt.Maxcreated_at order by ipm.created_at desc";
            $lastLoggedInUsers = DB::select($sql);

            foreach($lastLoggedInUsers as $loggedInData)
            {
              	foreach($carers as $carerData)
            	{
                  	if($carerData->id == $loggedInData->user_id)
                    {
                        $test = date("d-m-Y",strtotime($loggedInData->created_at));
                        $temp = array(
                            "id" => $carerData->id,
                            "first_name" => $carerData->first_name,
                            "family_name" => $carerData->family_name,
                            "sentence_yourself" => $carerData->sentence_yourself,
                            "town" => $carerData->town,
                            "postcode" => $carerData->postcode,
                            "avg_total" => $carerData->avg_total,
                            "creview" => $carerData->creview,
                            "href_profile" => $carerData->href_profile,
                            "image_profile" => $carerData->image_profile,
                            "price" => $carerData->price,
                            "distance" => $carerData->distance,
                            "created_at" => $test,
                        ); 
                      	array_push($tempArray, $temp);
                    }                  
                }             
             }
            return ['data' => $tempArray, 'count' => count($tempArray), 'distance_is_filled' => $distance_is_filled ];
          }
                   
      //echo "<pre> Cares  : "; print_r(($tempArray)); echo "<pre>";  exit;

//        return ['data' => $carers, 'pages' => $pages_count, 'count' => count($carers), 'distance_is_filled' => $distance_is_filled ];
      
      		//------ Commented on 26 June 2019
         //return ['data' => $carers, 'count' => count($carers), 'distance_is_filled' => $distance_is_filled ];
      
      	return ['data' => $carers, 'count' => count($carers), 'distance_is_filled' => $distance_is_filled ];
    }
  
   	public function getuserLastLoginAttribute($userId)
    {
		//$userId = $this->id;
		$sql ="SELECT * from ip_monitoring where user_id = '".$userId."' Order by id  DESC LIMIT 1";
		$items = DB::select($sql);
		
		if(!empty($items)){
			$item = $items[0];
			return date("d-m-Y",strtotime($item->created_at));
		}
		
		return '-';
	}

    private static function isExsistPostCode($postCode)
    {
        $sql = "SELECT count(*) AS ctn FROM postcodes p WHERE p.name = LEFT('" . $postCode . "', POSITION(' ' IN '" . $postCode . "'))";
        
        $carerResult = DB::select($sql);
        //  dd($carerResult);
        return $carerResult[0]->ctn > 0;
        // print_r($sql);
        
    }

    private static function isFirstPartPostcodeExist($postCode)
    {
        $sql = "SELECT count(*) AS ctn FROM postcodes p WHERE p.name like '%$postCode%'";
        $carerResult = DB::select($sql);
        
        return $carerResult[0]->ctn > 0;
    }

}
