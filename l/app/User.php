<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\DB;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email',
        'password',
        'user_type_id',
        'referral_code',
        'use_register_code',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'activation_key',
    ];


    /**
     * Relations
     */
    public function userPurchaser()
    {
		
        return $this->hasMany('App\Booking', 'purchaser_id', 'id');
    }

    public function userService()
    {
        return $this->hasMany('App\Booking', 'service_user_id', 'id');
    }

    public function userCarer()
    {
        return $this->hasMany('App\Booking', 'carer_id', 'id');
    }

    public function userCarerProfile()
    {
        return $this->hasOne('App\CarersProfile', 'id', 'id');
    }

    public function userPurchaserProfile()
    {
        return $this->hasOne('App\PurchasersProfile', 'id', 'id');
    }
    
    public function userIpMonitoring()
    {
        return $this->hasMany('App\IpMonitoring', 'user_id', 'id');
    }

    public function bonusPayouts()
    {
        return $this->hasMany(BonusPayout::class, 'user_id');
    }

    public function blogs()
    {
        return $this->hasMany(Blog::class);
    }

    public function documents()
    {
        return $this->hasMany(Document::class, 'user_id');
    }
    
    public function credit_cards(){
        return $this->hasMany(StripeCostumer::class, 'purchaser_id');
    }
    public function userType()
    {
        return $this->belongsTo('App\UserType');
    }
    public function getReferralUserAttribute()
    {
		
        if ($this->referral_code) {
            $referral_user = User::where('own_referral_code', $this->referral_code)->first();
            switch ($referral_user->user_type_id) {
                case 1:
                    $referral_user_name = PurchasersProfile::find($referral_user->id);
                    return $referral_user_name->first_name . ' ' . $referral_user_name->family_name;
                    break;
                case 3:
                    $referral_user_name = CarersProfile::find($referral_user->id);
                    return $referral_user_name->first_name . ' ' . $referral_user_name->family_name;
                    break;
            }
            return null;
        } else {
            return null;
        }
    }
    
    public function getImgUrlAttribute(){
        return '/img/profile_photos/'.$this->id.'.png?a='.rand(1, 9999);
    }


    /**
     * Accessors
     */
    public function getFullNameAttribute(){
        switch ($this->user_type_id){
            case 1:
                $profile = $this->userPurchaserProfile()->first();
                return $profile->first_name.' '.$profile->family_name;
                break;
            case 3:
                $profile = $this->userCarerProfile()->first();
                return $profile->first_name.' '.$profile->family_name;
                break;
        }
    }

    public function getFirstNameAttribute(){
        switch ($this->user_type_id){
            case 1:
                $profile = $this->userPurchaserProfile()->first();
                return $profile->first_name;
                break;
            case 3:
                $profile = $this->userCarerProfile()->first();
                return $profile->first_name;
                break;
        }
    }
    public function getShortFullNameAttribute(){
        return $this->first_name.' '.mb_substr($this->family_name,0,1).'.';
    }

    public function getFamilyNameAttribute(){
        switch ($this->user_type_id){
            case 1:
                $profile = $this->userPurchaserProfile()->first();
                return $profile->family_name;
                break;
            case 3:
                $profile = $this->userCarerProfile()->first();
                return $profile->family_name;
                break;
        }
    }

    public function getBonusBalanceAttribute(){
         $res = DB::select('SELECT SUM(amount) as amount FROM bonus_transactions WHERE user_id = '.$this->id);
        /*$usedBonus = DB::select('SELECT SUM(amount_for_purchaser) as amount FROM bookings WHERE purchaser_id = '.$this->id.' AND payment_method="bonus_wallet"');
        
		$bonusBalance= (float)$res[0]->amount - (float)$usedBonus[0]->amount;*/
        return $res[0]->amount;
        //return $bonusBalance;
    }

    public function getPaidBonusesAttribute(){
        $res = DB::select('SELECT SUM(amount) as amount FROM bonus_payouts WHERE payout = 1 AND is_primary = 1 AND user_id = '.$this->id);
        return (int)$res[0]->amount;
    }

    public function getProfileLinkAttribute(){
        switch ($this->user_type_id){
            case 1:
                break;
            case 3:
                return '/carer/profile/'.$this->id;
                break;
        }
    }


    public function getCompletedAppointmentsHoursAttribute()
    {
        if ($this->user_type_id === 3) {
            $sql = 'SELECT a.id FROM appointments a LEFT JOIN bookings b ON a.booking_id = b.id WHERE a.status_id = 4  AND b.carer_id = ' . $this->id;
         
        } else {
            $sql = 'SELECT a.id FROM appointments a LEFT JOIN bookings b ON a.booking_id = b.id WHERE a.status_id = 4  AND b.purchaser_id = ' . $this->id;
          
        }
	
      	$res = DB::select($sql);
     
       	$appointments = Appointment::findMany(array_pluck($res, 'id'));
 		$hours = 0;
        foreach ($appointments as $appointment)
         $hours += $appointment->hours;
      
		return $hours;
    }


    public function getAccountStatusAttribute() {

        //check for blocking purchaser account

        if ($this->isPurchaser() && $this->userPurchaserProfile->profiles_status_id==5)
            return 'blocked';

        return;


    }

    /**
     * Helpers methods
     */
    public function userName()
    {
        if ($this->user_type_id == 3) {
            return $this->userCarerProfile->first_name.' '.mb_substr($this->userCarerProfile->family_name,0,1).'.';
        }

        if ($this->user_type_id == 1) {
            return $this->userPurchaserProfile->first_name.' '.mb_substr($this->userPurchaserProfile->family_name,0,1).'.';
        }
        return $this->id;
    }

    public function isCarer()
    {
        if ($this->user_type_id == 3) {
            return true;
        }
        return false;
    }

    public function isPurchaser()
    {
        if ($this->user_type_id == 1) {
            return true;
        }
        return false;
    }

    public function isAdmin()
    {
        if ($this->user_type_id == 4) {
            return true;
        }
        return false;
    }

    public function isReregistrationCompleted()
    {
        if ($this->user_type_id == 3) { //carer
            if ($this->userCarerProfile->registration_status != 'new') {
                return true;
            }
        }
        if ($this->user_type_id == 1) { //purchaser
            if ($this->userPurchaserProfile->registration_status != 'new') {
                //return true;
                return true;
            }
        }
        return false;
    }

    public function hasBookingsWith(ServiceUsersProfile $profile){
        return Booking::where('carer_id', $this->id)
            ->where('service_user_id', $profile->id)
            ->get()
            ->count();
    }
    
    public function getIpAddressAttribute()
    {
		$userId = $this->id;
		$sql ="SELECT * from ip_monitoring where user_id = '".$userId."' Order by id  DESC LIMIT 1";
		$items = DB::select($sql);
		
		if(!empty($items)){
			$item = $items[0];
			return $item->ip_address;
		}
		return '';
	}
	
    public function getLocationAttribute()
    {
		$userId = $this->id;
		$sql ="SELECT * from ip_monitoring where user_id = '".$userId."' Order by id  DESC LIMIT 1";
		$items = DB::select($sql);
		
		if(!empty($items)){
			$item = $items[0];
			return $item->city.", ".$item->state.", ".$item->country." - ".$item->zip;
		}
		return '-';
	}
    
    public function getLastLoginAttribute()
    {
		$userId = $this->id;
		$sql ="SELECT * from ip_monitoring where user_id = '".$userId."' Order by id  DESC LIMIT 1";
		$items = DB::select($sql);
		
		if(!empty($items)){
			$item = $items[0];
			return date("d-m-Y",strtotime($item->created_at));
		}
		
		return '-';
	}
    
    
}
