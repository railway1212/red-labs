<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AppointmentOverview extends Model
{
    protected $table = 'appointments_overviews';
    protected $fillable = [
        'appointment_id',
        'punctuality',
        'friendliness',
        'communication',
        'performance',
        'comment',
    ];


    public function appointment()
    {
        return $this->belongsTo(Appointment::class);
    }
}
