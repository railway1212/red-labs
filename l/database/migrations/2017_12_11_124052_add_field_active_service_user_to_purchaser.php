<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldActiveServiceUserToPurchaser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table('purchasers_profiles', function(Blueprint $table) {
            $table->integer('active_user')->nullable()->default(null)->after(' ');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('purchasers_profiles', function($table) {
            $table->dropColumn('active_user');
        });
    }
}
