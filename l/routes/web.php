<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//Route::get('robots.txt', 'HomeController@robots');

Route::get('/rahul', 'CarerController@test');

Route::get('customer-registration/{step_token?}', 'Registration\CustomerRegistrationController@index');
Route::post('customer-registration/ajax', 'Registration\CustomerRegistrationController@ajax');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/testcron', 'HomeController@testCron')->name('testCron');

//----
Route::get('/', 'HomePageController@index')->name('mainHomePage');
Route::post('/update/activeUser', 'HomePageController@activeUser')->name('activeUserHomePage');
//---- static page ----
//Route::get('../about', 'AboutController@index')->name('AboutPage');
Route::get('../about-us', 'AboutController@index')->name('AboutPage');
Route::get('/jobs', 'AboutController@job')->name('JobPage');
Route::get('/onepage', 'AboutController@onepage')->name('OnePage');
Route::get('../faq', 'FaqController@index')->name('FaqPage');
Route::get('../contact', 'ContactController@index')->name('ContactPage');
Route::get('/contact/thank', 'ContactController@thank')->name('ThankPage');
Route::post('../contact', 'ContactController@send')->name('ContactSendMail');
Route::get('../blog', 'BlogController@index')->name('BlogPage');
Route::get('/blog/filter/{month}-{year}', 'BlogController@viewFilter')->name('BlogFilter');
//Route::get('/blog/{blogId}', 'BlogController@view')->name('BlogViewPage');
Route::get('/blog/{slug}', ['uses' => 'BlogController@view'])->name('BlogViewPage');

Route::get('/search/autocomplete', 'SearchController@autocomplete')->name('autocomplete');// auto_searchpostcode
Route::get('/search', 'SearchController@index')->name('searchPage');
Route::post('/search', 'SearchController@index')->name('searchPagePost');
Route::get('/search/page/{page}', 'SearchController@index')->name('searchPagePaginate');
Route::post('/search/page/{page}', 'SearchController@index')->name('searchPagePaginatePost');

Route::get('../terms', 'TermsController@index')->name('TermsPage');

Route::get('/welcome-carer', 'CarerController@welcome')->name('welcomeCarer');
Route::get('../carer-jobs-manchester', 'CarerController@welcome')->name('welcomeCarer');
Route::get('../home-care-jobs', 'CarerController@homeCareJobs')->name('homeCareJobs');
Route::get('/carer-settings/booking/{status?}/{id?}', 'CarerController@bookingFilter')->name('carerBooking'); //synonym for ImCarerPage
Route::get('/carer-settings/{id?}', 'CarerController@index')->name('carerSettings'); //synonym for ImCarerPage

Route::get('/carer/profile/{user_id}', 'CarerController@profile')->name('carerPublicProfile'); //synonym for
Route::get('/address/', 'CarerController@address_autocomplete')->name('carerGetAddress'); //synonym for
Route::get('/noaddress/', 'CarerController@getNoAddress')->name('carerGetNoAddress'); //synonym for
Route::get('/carer/review/{user_id}', 'CarerController@review')->name('carerReview'); //synonym for
Route::get('/carer/profile/{user_id}/reviews', 'CarerController@reviews')->name('carerReviews');
Route::get('/carer/appointment/{user_id}', 'CarerController@appointment')->name('carerAppointment'); //synonym for
// ImCarerPage

//Route::get('/carer-settings/booking/{status}', 'CarerController@bookingFilter')->name('carerBookingStatus'); //synonym for ImCarerPage

Route::get('/carer-settings/profile/{user_id}', 'CarerController@profile')->name('carerPublicProfile2'); //synonym for ImCarerPage




Route::get('/im-carer', 'CarerController@index')->name('ImCarerPage');
Route::post('/im-carer', 'CarerController@update')->name('ImCarerPrivatePage');
Route::get('/carer/{carer_id}', 'CarerController@carerProfile');

//Route::get('carer-registration/{stepback?}','CarerRegistrationController@index')->name('CarerRegistration');
Route::get('carer-registration/','CarerRegistrationController@index')->name('CarerRegistration');
Route::post('carer-registration','CarerRegistrationController@update')->name('CarerRegistrationPost');

Route::get('/purchaser-settings/booking/{status?}/{id?}', 'PurchaserController@bookingFilter')->name('purchaserBookingStatus'); //synonym for ImCarerPage
Route::get('/purchaser-settings/{id?}', 'PurchaserController@index')->name('purchaserSettings');
Route::post('/purchaser-settings','PurchaserController@update')->name('purchaserSettingsPost');

Route::get('/purchaser-registration/','PurchaserRegistrationController@index')->name('PurchaserRegistration');
Route::post('/purchaser-registration','PurchaserRegistrationController@update')->name('PurchaserRegistrationPost');

Route::get('/service-registration/{serviceUserProfile}','ServiceUserRegistrationController@index')->name('ServiceUserRegistration');
Route::post('/service-registration/{serviceUserProfile}','ServiceUserRegistrationController@update')->name('ServiceUserRegistration');

Route::get('/addServiceUser/{type?}','AddServiceUserController@create')->name('ServiceUserCreate');

Route::get('/serviceUser-settings/{serviceUserProfile}','ServiceUserPrivateProfileController@index')->name('ServiceUserSetting');
Route::get('/serviceUser/profile/{serviceUsersProfile}','ServiceUserPrivateProfileController@profile')->name('ServiceUserProfilePublic');
Route::get('/serviceUser/delete/{serviceUserProfile}','ServiceUserPrivateProfileController@delete')->name('ServiceUserProfileDelete');

Route::post('/serviceUser-settings/{serviceUserProfile}','ServiceUserPrivateProfileController@update')->name('ServiceUserSettingPost');
//Route::get('/serviceUser-settings/booking/{serviceUserProfile}', 'ServiceUserPrivateProfileController@booking')->name('ServiceUserBooking'); //synonym for ImCarerPage
Route::get('/serviceUser-settings/booking/{serviceUserProfile}/{status?}', 'ServiceUserPrivateProfileController@bookingFilter')->name('ServiceUserBookingStatus'); //synonym for ImCarerPage

Route::post('/bookings','Bookings\BookingsController@create');
Route::put('/bookings/{booking}','Bookings\BookingsController@update');
Route::post('/bookings/calculate_price','Bookings\BookingsController@calculateBookingPrice');
Route::get('/bookings/{booking}/modal_edit', 'Bookings\BookingsController@getModalEditBooking');
Route::get('/bookings/{booking}/details', 'Bookings\BookingsController@view_details')->name('viewBookingDetails');

Route::post('/bookings/{booking}/message','Bookings\BookingsController@create_message');
Route::post('/bookings/{booking}/setPaymentMethod','Bookings\BookingsController@setPaymentMethod')->name('setBookingPaymentMethod');
Route::post('/bookings/{booking}/accept', 'Bookings\BookingsController@accept');
Route::post('/bookings/{booking}/reject', 'Bookings\BookingsController@reject');
Route::post('/bookings/{booking}/cancel', 'Bookings\BookingsController@cancel');
Route::post('/bookings/{booking}/completed', 'Bookings\BookingsController@completed');
Route::get('/bookings/{booking}/leave_review', 'Bookings\BookingsController@leaveReviewPage');
Route::post('/bookings/{booking}/review', 'Bookings\BookingsController@createReview');

Route::post('/appointments/{appointment}/reject', 'Bookings\AppointmentsController@reject');
Route::post('/appointments/{appointment}/cancel', 'Bookings\AppointmentsController@cancelled');
Route::post('/appointments/{appointment}/completed', 'Bookings\AppointmentsController@completed');
Route::get('/appointments/{appointment}/leave_review', 'Bookings\AppointmentsController@leaveReviewPage');
Route::post('/appointments/{appointment}/review', 'Bookings\AppointmentsController@createReview');

Route::post('/document/upload/{user_id?}','DocumentsController@upload')->name('UploadDocument');
Route::get('/document/{document}/download','DocumentsController@download')->name('DownloadDocument');
Route::get('/documents/{user_id?}','DocumentsController@GetDocuments')->name('GetDocuments');
Route::post('/profile-photo/{carerId}','ProfilePhotosController@uploadUserProfilePhoto');
Route::post('/service-user-profile-photo/{serviceUserId}','ProfilePhotosController@uploadServiceUserProfilePhoto');

Route::post('/credit_card','CreditCardsController@store')->name('CreditCards');
Route::get('/credit_card/{card_id}','CreditCardsController@destroy')->name('DeleteCreditCards');

Route::get('/search-carers', 'SearchController@searchCarers');


// registration mail
Route::get('/thank-you-carer', 'CarerRegistrationController@sendContinueRegistration')->name('thankYou'); //mail - continue registration
Route::get('/carer-registration-completed', 'CarerRegistrationController@sendCompleteRegistration')->name('welcomeNewCarer'); //mail - completed registration

Route::get('/thank-you', 'PurchaserRegistrationController@sendContinueRegistration')->name('thankYouUser'); //mail - continue registration
Route::get('/thank-you-user/{id}', 'ServiceUserRegistrationController@sendContinueRegistration')->name('thankYouSrvUser'); //mail - continue registration

Route::get('/user-registration-completed/{id}', 'ServiceUserRegistrationController@sendCompleteRegistration')->name('serviceUserRegistrationComplete'); //mail - completed registration
//^^^registration mail

Route::get('/invite/refer-users', 'ReferNewUserController@index')->name('inviteReferUsers');
Route::post('/invite/refer-users', 'ReferNewUserController@create')->name('inviteReferUsersPost');
Route::get('/invite/thank-you', 'ReferNewUserController@show')->name('thankForInvite');

Route::get('../privacy-policy', 'PrivacyPolicyController@index')->name('privacy_policy');
Route::get('/enter', 'LoginWindowController@index')->name('session_timeout');
Route::get('/unsubscribe/{id}', 'HomePageController@unsubscribe')->name('unsubscribe');
Route::get('/bookings/{booking}/purchase', 'Bookings\PaymentsController@payment_form')->name('bookingPurchase');

Route::group(['middleware' => ['auth']], function () {
    //todo все роуты, на которые не могут заходить не залогиненные должны быть в этой группе
    //todo а не дублировать проверку на каждой странице

});

//alexx
Route::get('/completeBookingIfAppointmentsCompleted/{appointment_id}', 'Bookings\AppointmentsController@completeBookingIfAppointmentsCompleted');
//Route::post('/saveCard/{cardData}', 'Bookings\PaymentsController@saveCard');
// /alexx


Route::group(['prefix' => 'admin','middleware'=> 'admin','namespace' => 'Admin'],function() {

    Route::get('/', 'AdminController@adminHomePage')->name('adminHomePage');
    Route::resource('/user','User\UserController', ['except' => ['show']]);
    Route::get('/user/getCarerImage/{id}','User\UserController@getCarerImage');
    Route::get('/user/page/{p?}','User\UserController@index')->name('user_pagination');;
    Route::resource('/booking','Booking\BookingController', ['only' => ['index']]);
//    Route::get('/blog/{id}', 'Blog\BlogController@create');
    Route::resource('/blog','Blog\BlogController');
//    Route::resource('/tags','Blog\TagController');

    Route::get('/user/review_management','ReviewManagementController@index')->name('ReviewManagement');
    Route::post('/user/review_management','ReviewManagementController@index')->name('ReviewManagement');
    Route::get('/user/review_management/{id}','ReviewManagementController@edit')->name('ReviewManagementEdit');
    Route::post('/user/review_management/{id}','ReviewManagementController@edit')->name('ReviewManagementEdit');
  
  	Route::get('/user/reviewed_management','ReviewManagementController@reviewed')->name('ReviewedManagement');
  	Route::post('/user/reviewed_management','ReviewManagementController@reviewed')->name('ReviewedManagement');
  	Route::get('/user/reviewed_management/{id}','ReviewManagementController@reviewed_edit')->name('ReviewManagementEdited');
    Route::post('/user/reviewed_management/{id}','ReviewManagementController@reviewed_edit')->name('ReviewManagementEditrd');

    Route::get('/financial','FinancialController@index')->name('financial');
    Route::get('/statistic','StatisticController@index')->name('statistic');
    Route::get('/statistic/download','StatisticController@exportStaticticsToXls')->name('statisticDownload');
    Route::get('/fees','FeesController@index')->name('fees');
   Route::get('/liveincarer','LiveincarerController@index')->name('liveincareres'); 
   Route::post('/liveincarer','LiveincarerController@update')->name('liveincarerpost');
    Route::get('/post-codes','PostCodesController@index')->name('PostCodes');
    Route::post('/post-codes','PostCodesController@index')->name('PostCodesPost');
    Route::post('/fees','FeesController@update')->name('feespost');
    Route::get('/holidays','HolidaysController@index')->name('holidays');
    Route::post('/holidays','HolidaysController@update')->name('holidaysPost');
    Route::post('/holidays/delete/{id}','HolidaysController@delete')->name('holidaysDeletePost');

    Route::get('/settings','SettingsController@index')->name('settingsAdmin');
    Route::post('/settings','SettingsController@update')->name('settingsAdminPost');
    Route::delete('/settings/{id}', 'SettingsController@deleteAdmin')->name('settingsAdminDelete');
	
  	Route::get('/add-carer-wages','CarerWagesController@create')->name('CarerWagesAdd');
    Route::get('/carer-wages','CarerWagesController@index')->name('CarerWages');
    Route::post('/carer-wages','CarerWagesController@update')->name('CarerWagesPost');
  	Route::post('/save-carer-wages','CarerWagesController@save')->name('CarerWagesSave');
  


    Route::get('/booking-transactions', 'AdminSitePayment@getBookingTransactions')->name('BookingTransactions');
    Route::post('/booking-transactions', 'AdminSitePayment@getBookingTransactions')->name('BookingTransactionsPost');

    Route::get('/carer-payout', 'AdminSitePayment@getPayoutsToCarers')->name('PayoutsToCarers');
    //Route::post('/carer-payout/{booking}', 'AdminSitePayment@makePayoutToCarer')->name('makePayoutToCarer');
    Route::post('/carer-payout/{appointment_id}', 'AdminSitePayment@makePayoutToCarer')->name('makePayoutToCarer');

    Route::get('/purchaser-payout', 'AdminSitePayment@getPayoutsToPurchasers')->name('PayoutsToPurchasers');
    Route::post('/purchaser-payout/{booking}', 'AdminSitePayment@makePayoutToPurchaser')->name('makePayoutToPurchaser');
  

    Route::get('/dispute-payout', 'DisputePayoutsController@index')->name('DisputePayouts');
    Route::put('/dispute-payout/{dispute_payout_id}/detailsOfDispute', 'DisputePayoutsController@setDetailsOfDispute');
    Route::put('/dispute-payout/{dispute_payout_id}/detailsOfDisputeResolution', 'DisputePayoutsController@detailsOfDisputeResolution');
    Route::post('/dispute-payout/{dispute_payout_id}/payoutToCarer', 'DisputePayoutsController@payoutToCarer');
    Route::post('/dispute-payout/{dispute_payout_id}/payoutToPurchaser', 'DisputePayoutsController@payoutToPurchaser');

    Route::get('/bonuses-carers', 'BonusController@getBonusesCarer')->name('BonusesCarers');
    Route::get('/bonuses-purchasers', 'BonusController@getBonusesPurchaser')->name('BonusesPurchasers');
    Route::post('/payout-bonus/{bonus_id}', 'BonusController@payoutBonus');
    Route::post('/cancel-bonus/{bonus_id}', 'BonusController@cancelBonus');

    //alexx
    Route::get('/filterCarerWages/{carerId}/{filter}', 'CarerWagesController@filterCarerWages');
    //returns all appointment statuses of a booking
  
   //add export on all admin pages 
	Route::get('/booking/download','Booking\BookingController@exportBookingToXls')->name('bookingDownload');
	Route::get('/booking-transactions/download','AdminSitePayment@exportTransactionToXls')->name('bookingTransactionDownload');
	Route::get('/purchaser-payout/download','AdminSitePayment@exportPurchaserPaymentToXls')->name('purchaserPaymentDownload');
	Route::get('/carer-payout/download','AdminSitePayment@exportCarerPaymentToXls')->name('carerPaymentDownload');
	Route::get('/dispute-payout/download','DisputePayoutsController@exportDisputePayoutToXls')->name('disputePayoutsDownload');
	Route::get('/bonuses-carers/download','BonusController@exportBonusCarersToXls')->name('bonusCarersDownload');
	Route::get('/bonuses-purchasers/download','BonusController@exportBonusPurchaserToXls')->name('bonusPurchaserDownload');
Route::get('/booking/accountant-report-download','Booking\BookingController@exportAccountantReportToXls')->name('accountantReportDownload');
Route::resource('/page','Page\PageController');
Route::resource('/faq','Faq\FaqController');
Route::resource('/trust','Trust\TrustController');
Route::resource('/testimonial','Testimonial\TestimonialController');
Route::resource('/banner','Banner\BannerController');  
Route::get('/contact','Contact\ContactController@index')->name('contactget');
Route::post('/contact-update','Contact\ContactController@update')->name('contactpost');  
Route::get('/gdpr','User\UserController@UserGdpr')->name('gdpr');
Route::get('/gdpr/download','User\UserController@exportGdprToXls')->name('gdprDownload');
Route::get('/ip-monitoring','User\UserController@ipMonitoring')->name('IpMonitoring');
  
Route::get('/booking/{booking_id}/cancelBooking','Booking\BookingController@cancelBooking');
Route::get('/booking/{appointment_id}/cancelAppointment','Booking\BookingController@cancelAppointment');
Route::get('/bonuses-purchasers/credit-wallet/', 'BonusController@CreditWallet');  
  
});

Route::get('/check24/{date}/{time}', 'Bookings\BookingsController@appointmentIsMinimum24HoursAhead');

Route::post('/increase-postcode-search/{postcode}', 'SearchController@increasePostCode');
Route::get('/get-referral-code/{userId}', 'CarerController@getReferralCode');
Route::post('/update-referral-code/{userId}/{code}', 'CarerController@updateReferralCode');

Route::get('/test-cronjob', 'CarerController@testCronjob');


Route::get('/test_document_upload', function (){
    return view('test_document_upload');
});

Route::get('/appan', function (){
    return view('resources/views/Holm/purchaserProfiles/Booking/NewAnAppointment');
});

Route::get('/test_stripe', function (){
    $user = \App\User::find(2);
    $card = $user->credit_cards()->first();
    \App\Helpers\Facades\PaymentTools::createCharge(2000, $card->token, 209);
});

/*Route::group(['prefix' => 'laravel-filemanager', 'middleware' => ['web', 'auth']], function () {
     \UniSharp\LaravelFilemanager\Lfm::routes();
 });*/

Route::get('/test', function () {
  
 // echo K1rkhnmmn=
   //return bcrypt('K1rkhnmmn=');
  $res = DB::select("SELECT a.id FROM appointments a JOIN bookings b ON a.booking_id = b.id WHERE UNIX_TIMESTAMP(a.date_start) = CURDATE() AND DATE_ADD(NOW(), INTERVAL 1 HOUR) > UNIX_TIMESTAMP(a.time_from) AND a.reminder_sent = 0 AND b.status_id = 5 AND a.status_id < 3");
  echo "<pre>"; print_r($res); echo "<pre>"; exit;
//    $pathes = [
//        'img/profile_photos',
//        'img/service_user_profile_photos',
//    ];
//
//    foreach ($pathes as $path) {
//        $photos = File::allFiles($path);
//        foreach ($photos as $photo) {
//            $name = $photo->getPathname();
//
//            try {
//                $img = Image::make($name);
//                $width = $img->width();
//                $height = $img->height();
//
//                if ($width / $height > 1) {
//                    // resize image to new width but do not exceed original size
//                    $img = $img->widen(112, function ($constraint) {
//                        $constraint->upsize();
//                    });
//                } else {
//                    // resize image to new height but do not exceed original size
//                    $img = $img->heighten(112, function ($constraint) {
//                        $constraint->upsize();
//                    });
//                }
//
//                $img->save($name);
//            } catch (\Exception $e) {
//                echo 'error ' . $name . '<br>';
//            }
//        }
//    }
//    $a = 1.3;
//    $b = 1.3;
//
//    $sum = $a + $b;
//
//    $a = \Carbon\Carbon::createFromFormat('H.i', $a);
//    $aa = $a->addHours($b);
//    dd($aa->diffInHours($a))
//    $b = \Carbon\Carbon::createFromFormat('H.i', $b);
////    $a = \Carbon\Carbon::parse($a);
//    $a->addHours($b);
//    dd($a);
});
