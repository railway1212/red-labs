<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::put('/document/{document}','DocumentsController@update')->name('UpdateDocument');

//Route::delete('/document/{document}/item','DocumentsController@destroy')->name('DeleteDocument');

Route::delete('/document/{document}/admin','DocumentsController@destroy')->name('DeleteDocument');
Route::get('/document/{document}','DocumentsController@getDocument')->name('GetDocument');
 Route::get('/document/{document}/preview','DocumentsController@getPreview')->name('GetDocument');

    Route::post('/login', [
    'uses' => 'LoginController@login']);


// Route::post('/logout', [
//     'uses' => 'Auth\LoginController@logout']);

Route::post('/logout', [
    'uses' => 'LoginController@logout']);

//  Route::post('/create', [           
//          'uses' => 'PasswordResetController@create']);

        Route::group([    
            'namespace' => 'Auth',    
            'middleware' => 'api',    
            'prefix' => 'password'
        ], function () {    
            Route::post('create', 'PasswordResetController@create');
            Route::get('find/{token}', 'PasswordResetController@find');
            Route::post('reset', 'PasswordResetController@reset');
        });


Route::get('/Servicetype', [
    'uses' => 'ServiceTypeController@Servicetype']);


Route::get('/Language', [
    'uses' => 'ServiceTypeController@Language']);


Route::get('/AssistanceType', [
    'uses' => 'ServiceTypeController@AssistanceType']);



Route::get('/SearchList', [
    'uses' => 'SearchListController@SearchList']);
