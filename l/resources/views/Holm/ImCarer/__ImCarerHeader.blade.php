<header class="header">
    <div class="container">
        <div class="headerContainer">
            <a href="\" class="themeLogo">

            </a>
            <a href="#" class="xsNav">
        <span class="">
          <i class="fa fa-navicon"></i>
        </span>
            </a>
            <div class="collapseBox">
                <a href="{{ route('ImCarerPage') }}" class="carerSelf">
                    i am a carer
                </a>
                <div class="headerNav_container">
                    <div class="headerNav">
                        <a href="http://37.0.25.139:81/find-a-carer/" class="  headerNav__link">
                            book a carer
                        </a>

                        <a href="#" class=" headerNav__link">
                            about us
                        </a>
                    </div>
                    <div class="headerSocial">
                        <a href="https://www.facebook.com/HomeCareManchester/" class="centeredLink headerSocial__link headerSocial__link--facebook">
                            <i class="fa fa-facebook"></i>
                        </a>
                        <a href="https://twitter.com/holmcare" class="centeredLink headerSocial__link headerSocial__link--twitter">
                            <i class="fa fa-twitter"></i>
                        </a>
                    </div>
                    @if (Route::has('login'))
                        <div class="top-right links">
                            @include(config('settings.frontTheme').'.includes.loginOnPages')
                        </div>
                    @endif
                </div>
            </div>



        </div>
    </div>
</header>

<section class="mainSection ">

    <div class="container">
        <div class="breadcrumbs">
            <a href="/" class="breadcrumbs__item">
                Home
            </a>
            <span class="breadcrumbs__arrow">></span>
            <a href="{{ route('ImCarerPage') }}" class="breadcrumbs__item">
                i am a carer
            </a>

        </div>

    </div>

    <div class="container">

    </div>

</section>
