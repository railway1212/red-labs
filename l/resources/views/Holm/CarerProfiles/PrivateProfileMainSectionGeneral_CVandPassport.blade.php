
<div class="profileRow">
    <div class="profileField profileField--half">
        <h2 class="profileField__title ordinaryTitle">
              <span class="ordinaryTitle__text ordinaryTitle__text--smaller">
                cv
              </span>
        </h2>
        <div class="addContainer">
          <input disabled class="pickfiles"  type="file" />
          <span class="pickfiles-delete generalcvicon">X</span>
          <!--<img id="additional_documents_cv" class="pickfiles_img"/>-->
          <!-------------   CV    --------------------------------->
          
            @php 
            $newDocuments = json_decode(json_encode($newDocuments), true);
            $key = array_search('ADDITIONAL_DOCUMENTS_CV', array_column($newDocuments, 'type'));
            @endphp
          
          	 @php
            if(isset($newDocuments) && isset($key))
            {     
          		if(isset($newDocuments[$key]) && $newDocuments[$key]['type']=='ADDITIONAL_DOCUMENTS_CV')
          		{
            @endphp
             <img id="additional_documents_cv" class="pickfiles_img" src="{{(isset($key) && $key != '') ?  URL::to('/').'/storage/documents/'.$newDocuments[$key]['file_name'] : ''}}" > 
            @php }
            }
            @endphp                
            <img id="additional_documents_cv" class="pickfiles_img"/>
                      
            <a class="add add--moreHeight">
                <i class="fa fa-plus-circle"></i>
                <div class="add__comment add__comment--smaller"></div>
            </a>
          
           
        </div>
        <div style="display: none" class="addInfo">
            <input disabled type="text" name="additional_documents_cv" class="addInfo__input" placeholder="Name">
        </div>
    </div>
    <div class="profileField profileField--half">
        <h2 class="profileField__title ordinaryTitle">
              <span class="ordinaryTitle__text ordinaryTitle__text--smaller">
                Photographic proof of your ID (passport or driving licence is acceptable)
              </span>
        </h2>
        <div class="addContainer">
          <input disabled class="pickfiles" accept="" type="file" />
           <span class="pickfiles-delete generalicon">X</span>
          <!--<img id="passport" class="pickfiles_img"/>-->
          	@php 
            $newDocuments = json_decode(json_encode($newDocuments), true);
            $key = array_search('PASSPORT', array_column($newDocuments, 'type'));
            @endphp
          
          
           	@php
          
           	if(isset($newDocuments) && isset($key))
            { 
          		if(isset($newDocuments[$key]) && $newDocuments[$key]['type']=='PASSPORT')
              {                  
            @endphp
             <img id="passport" class="pickfiles_img" src="{{(isset($key) && $key != '') ?  URL::to('/').'/storage/documents/'.$newDocuments[$key]['file_name'] : ''}}" >
            @php }
            }
            @endphp                
                      <img id="passport" class="pickfiles_img"/>
          <!-------------   Passport    --------------------------------->
            <a class="add add--moreHeight">
                <i class="fa fa-plus-circle"></i>
                <div class="add__comment add__comment--smaller"></div>
            </a>
          
          
          
        </div>
      
     
        <div style="display: none" class="addInfo">
            <input disabled type="text" name="passport" class="addInfo__input" placeholder="Name">
        </div>
    </div>
</div>
