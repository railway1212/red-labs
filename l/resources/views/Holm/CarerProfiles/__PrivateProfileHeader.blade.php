<header class="header">
    <div class="container">
        <div class="headerContainer">
            <a href="/" class="themeLogo">

            </a>
            <a href="#" class="xsNav">
        <span class="">
          <i class="fa fa-navicon"></i>
        </span>
            </a>
            <div class="collapseBox">
                <a href="{{ route('welcomeCarer') }}" class="carerSelf">
                    i am a carer
                </a>
                <div class="headerNav_container">
                    <div class="headerNav">
                        <a href="/l/search" class="  headerNav__link">
                            find a carer
                        </a>

                        <a href="/about-us" class=" headerNav__link">
                            about us
                        </a>
                    </div>
                    <div class="headerSocial">
                        <a href="https://www.facebook.com/HolmCareUK/" class="centeredLink headerSocial__link headerSocial__link--facebook">
                            <i class="fa fa-facebook"></i>
                        </a>
                        <a href="https://twitter.com/holmcare" class="centeredLink headerSocial__link headerSocial__link--twitter">
                            <i class="fa fa-twitter"></i>
                        </a>
                    </div>

                    @if (Route::has('login'))
                        <div class="top-right links">
                            @include(config('settings.frontTheme').'.includes.loginOnPages')
                        </div>
                    @endif

 {{--                   <a href="#" class="registeredCarer">
                        <div class="profilePhoto registeredCarer__img">
                            <img src="/l/public/img/no_photo.png" src="/public/img/profile_photos/{{$carerProfile->id}}.png" alt="{{$carerProfile->first_name}}">
                        </div>
                        <h2 class="profileName">
                            Rosie P.
                        </h2>
                        <span class="registeredCarer__ico">
              <i class="fa fa-sign-out" aria-hidden="true"></i>
            </span>
                    </a>--}}

                </div>
            </div>

        </div>
    </div>
</header>
