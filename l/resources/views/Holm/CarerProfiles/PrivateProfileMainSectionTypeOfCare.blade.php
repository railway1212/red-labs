<div id="carerTypeCare" class="borderContainer">
    <div class="profileCategory">
        <h2 class="profileCategory__title">TYPE OF CARE OFFERED</h2>
        <a href="#" class="btn btn-info btn-edit"><span class="fa fa-pencil" data-id="carerPrivateTypeCare"></span> EDIT</a>
        <button type="button" class="btn btn-success hidden" id="load" data-loading-text="<i class='fa fa-spinner
        fa-spin '></i> Processing"><i class="fa fa-floppy-o"></i>  Save</button>
    </div>
    <span>Please <a target="_blank" href="{{route('ContactPage')}}">contact us</a> if you would like to add Dementia Care, Wounds or Medicine management. Only qualified care workers can do so.</span>

</div>
{!! Form::model($carerProfile, ['method'=>'POST','route'=>'ImCarerPrivatePage','id'=>'carerPrivateTypeCare']) !!}
{!! Form::hidden('id',$carerProfile->id) !!}
{!! Form::hidden('stage','carerPrivateTypeCare') !!}

<div class="borderContainer">
    @foreach($typeServices as $service)
        <div class="profileRow profileRow--start">
            <div class="profileField profileField--fourth">
                <div class="checbox_wrap">
            {!! Form::checkbox('typeService['.$service->id.']', null,($carerProfile->ServicesTypes->contains('id', $service->id)? 1 : null),
            array('class' =>'checkboxNew','id'=>'checkSr'.$service->id )) !!}
            <label for="checkSr{{$service->id}}"> <span>{{$service->name}}</span></label>
                </div>
            </div>


            @foreach($typeCares->splice(0,3) as $typeCare)
                <div class="profileField profileField--fourth">
                    <div class="checbox_wrap">
                        {!! Form::checkbox('typeCare['.$typeCare->id.']', null,
                        ($carerProfile->AssistantsTypes->contains('id', $typeCare->id)? 1 : null),array('class' =>
                        'checkboxNew','id'=>'check'.$typeCare->id,(in_array($typeCare->name,
                        ['MEDICATION / TREATMENTS','DRESSINGS AND WOUND MANAGEMENT','DEMENTIA CARE'])&&!Auth::user()->isAdmin())?'onclick="return false;" data-edit="false"':''
                        )) !!}
                        <label for="check{{$typeCare->id}}"> <span>{{$typeCare->name}}</span></label>
                    </div>
                    @if(in_array($typeCare->name,
                        ['MEDICATION / TREATMENTS','DRESSINGS AND WOUND MANAGEMENT','DEMENTIA CARE'])&&!Auth::user()->isAdmin())
                        <span class="help-block blink" style="display: none;">{{$cannotEdit}}</span>
                    @endif
                </div>
            @endforeach
        </div>
    @endforeach
</div>
{!! Form::close()!!}
