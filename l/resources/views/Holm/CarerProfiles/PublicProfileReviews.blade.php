<section class="mainSection">
    <div class="container">
        <div class="breadcrumbs">
            <a href="/" class="breadcrumbs__item">
                Home
            </a>
            <span class="breadcrumbs__arrow">&gt;</span>
            <a href="/l/search" class="breadcrumbs__item">
                Carers
            </a>
            <span class="breadcrumbs__arrow">&gt;</span>
            <a href="{{route('carerPublicProfile',[$carerProfile->id])}}" class="breadcrumbs__item">

                {!! $carerProfile->first_name.' '.mb_substr($carerProfile->family_name,0,1).'.'!!}

            </a>
            <span class="breadcrumbs__arrow">&gt;</span>
            <a href="{{ route('carerReviews', [ $carerProfile->id ]) }}" class="breadcrumbs__item">
                Reviews
            </a>

        </div>

        <div class="reviews-container">
            <h2 class="reviews-container__title">
                reviews
            </h2>
            <div class="reviews-container__box">
                @foreach($reviews as $review)
                    <div class="review-column">
                        <div class="review review-column__item">
                            <div class="review__item singleReview">
                                <div class="reviewHead">
                                    <div class="reviewer">
                                        <a href="{{ route('ServiceUserProfilePublic',[$review->id]) }}"
                                           class="profilePhoto   singleReview__photo">
                                            <img src="/l/public/img/service_user_profile_photos/{{$review->id}}.png?a=.{{rand(1, 9999)}}" onerror="this.src='/l/public/img/no_photo.png'" alt="avatar">
                                        </a>
                                        <div class="reviewer__info">
                                            <h2 class="profileName">
                                                <a href="Service_user_Public_profile_page.html"> {{$review->first_name}}
                                                    {{mb_substr($review->family_name,0,1)}}.</a>

                                            </h2>
                                            <p class="reviewLocation">
                                                {{$review->town}}
                                            </p>
                                        </div>
                                    </div>
                                    <div class="singleReview__rate">
                                        <div class="profileRating ">
                            <span class="profileRating__item {{($review->raiting>0)? ' active' : '' }}"><i
                                        class="fa fa-heart"></i></span>
                                            <span class="profileRating__item {{( $review->raiting>1)? ' active' : '' }}"><i
                                                        class="fa fa-heart"></i></span>
                                            <span class="profileRating__item {{( $review->raiting>2)? ' active' : '' }}"><i
                                                        class="fa fa-heart"></i></span>
                                            <span class="profileRating__item {{( $review->raiting>3)? ' active' : '' }}"><i
                                                        class="fa fa-heart"></i></span>
                                            <span class="profileRating__item {{($review->raiting>4)? ' active' : '' }}"><i
                                                        class="fa fa-heart"></i></span>
                                        </div>

                                    </div>
                                </div>
                                <div class="singleReview__text">
                                    <p>
                                        {{ $review->comment }}
                                    </p>
                                </div>
                                <span class="singleReview__date">
                                    {{ Carbon\Carbon::parse($review->created_at)->format("d/m/Y") }}
                                </span>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>

        </div>


    </div>

</section>
