@extends(config('settings.frontTheme').'.layouts.ImCarer')

@section('description')
    Care work is not just a job, but a way of life. Holm recruits the best carers. We have support worker and personal assistant jobs  in Manchester, Bolton, Sale, Rochdale, Stockport, Salford, Oldham, Bury, Tameside, Trafford and Wigan
@endsection

@section('header')
    {!! $header !!}
@endsection

@section('content')
    {!! $content !!}
@endsection

@section('footer')
    {!! $footer !!}
@endsection

@section('modals')
    {!! $modals !!}
@endsection
