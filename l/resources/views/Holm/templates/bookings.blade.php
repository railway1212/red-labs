@extends(config('settings.frontTheme').'.layouts.carerPrivateProfile')

@section('header')
    {!! $header !!}
@endsection

@section('content')
    {!! $content !!}
@endsection

@section('footer')
    {!! $footer !!}
@endsection
