<div id="signUpdiv" class="modal fade" role="dialog">
    <div class="login" style="position: fixed; top:50%; left:50%; transform: translate(-50%, -50%);">
        <div class="login__header">
            <h2>Are You</h2>
            <a href="#" data-dismiss="modal" class="close closeModal">
                <i class="fa fa-times"></i>
            </a>
            {{--        <a href="/close" class="closeModal"
                       onclick="event.preventDefault();document.getElementById('sign_up_div').style.display = 'none';">
                        <i class="fa fa-times"></i>
                    </a>--}}
        </div>
        <div class="who-you-are">
            <div class="who-you-are__box">
                <a href="{{route('CarerRegistration').'#progress'}}" class="who-you-are__item">
                    A Care Worker?
                </a>
                <a href="{{route('PurchaserRegistration').'#progress'}}" class="who-you-are__item">
                    Buying Care?
                </a>
            </div>
        </div>
    </div>
</div>

<div id="confirm-cancel" class="modal fade" role="dialog">
<div class="login">
    <div class="login__header">
        <p style="color: #fff;font-size: 1.563em;text-transform: uppercase;">Confirm action</p>
        <a href="#" data-dismiss="modal" class="close closeModal">
            <i class="fa fa-times"></i>
        </a>
    </div>
    <div class="who-you-are">
        <p class="who-you-are__text">Are you sure ?</p>
        <div class="who-you-are__box">
            <a href="#" class="who-you-are__item" onclick="confirmCancel()" data-dismiss="modal">
                Yes
            </a>
            <a href="#" class="who-you-are__item" data-dismiss="modal">
                No
            </a>
        </div>
    </div>
</div>
</div>

<div id="confirm-cancel-appointment" class="modal fade" role="dialog">
    <div class="login">
        <div class="login__header">
            <p style="color: #fff;font-size: 1.563em;text-transform: uppercase;">Confirm action</p>
            <a href="#" data-dismiss="modal" class="close closeModal">
                <i class="fa fa-times"></i>
            </a>
        </div>
        <div class="who-you-are">
            <p class="who-you-are__text">Are you sure ?</p>
            <div class="who-you-are__box">
                <a href="#" class="who-you-are__item" onclick="confirmCancelAppointment()" data-dismiss="modal">
                    Yes
                </a>
                <a href="#" class="who-you-are__item" data-dismiss="modal">
                    No
                </a>
            </div>
        </div>
    </div>
</div>

<div id="confirm-dispute-appointment" class="modal fade" role="dialog">
    <div class="login">
        <div class="login__header">
            <p style="color: #fff;font-size: 1.563em;text-transform: uppercase;">Confirm action</p>
            <a href="#" data-dismiss="modal" class="close closeModal">
                <i class="fa fa-times"></i>
            </a>
        </div>
        <div class="who-you-are">
            <p class="who-you-are__text">Are you sure ?</p>
            <div class="who-you-are__box">
                <a href="#" class="who-you-are__item" onclick="confirmDisputeAppointment()" data-dismiss="modal">
                    Yes
                </a>
                <a href="#" class="who-you-are__item" data-dismiss="modal">
                    No
                </a>
            </div>
        </div>
    </div>
</div>

<div id="leave-review-appointment" class="modal fade" role="dialog">
    <div class="login">
        <div class="login__header">
            <p style="color: #fff;font-size: 1.563em;text-transform: uppercase;">Confirm action</p>
            <a href="#" data-dismiss="modal" class="close closeModal">
                <i class="fa fa-times"></i>
            </a>
        </div>
        <div class="who-you-are">
            <p class="who-you-are__text">Please Leave a Review</p>
            <div class="who-you-are__box">
                <a href="#" class="who-you-are__item" onclick="leaveReviewForAppointment()" data-dismiss="modal">
                    Now
                </a>
                <a href="#" class="who-you-are__item" onclick="location.reload()" data-dismiss="modal">
                    Later
                </a>
            </div>
        </div>
    </div>
</div>

<div id="confirm-reject-booking" class="modal fade" role="dialog">
    <div class="login">
        <div class="login__header">
            <p style="color: #fff;font-size: 1.563em;text-transform: uppercase;">Confirm action</p>
            <a href="#" data-dismiss="modal" class="close closeModal">
                <i class="fa fa-times"></i>
            </a>
        </div>
        <div class="who-you-are">
            <p class="who-you-are__text">Are you sure ?</p>
            <div class="who-you-are__box">
                <a href="#" class="who-you-are__item" onclick="confirmRejectBooking()" data-dismiss="modal">
                    Yes
                </a>
                <a href="#" class="who-you-are__item" data-dismiss="modal">
                    No
                </a>
            </div>
        </div>
    </div>
</div>
<div id="login" class="modal fade" role="dialog">
    <div class="login" style="position: fixed; top:50%; left:50%; transform: translate(-50%, -50%);">

        <div class="login__header">
            <h2>login</h2>
            <a href="#" data-dismiss="modal" class="close closeModal">
                <i class="fa fa-times"></i>
            </a>
            {{--        <a href="/close" class="closeModal"
                       onclick="event.preventDefault();document.getElementById('login').style.display = 'none';">
                        <i class="fa fa-times"></i>
                    </a>--}}
        </div>
        <div class="loader"></div>
        <div class="login__body">

            {{--<form  class="login__form">--}}
            <form id="login__form" class="login__form" method="POST" action="{{ route('login') }}">
                {{ csrf_field() }}
                <div class="formField">
                    <h2 class="formLabel questionForm__label">
                        Email
                    </h2>
                    <div class="inputWrap">
                        <input type="email" class="formInput " placeholder="Your email"
                               name="email">
                    </div>
                </div>
                <div class="formField">
                    <h2 class="formLabel questionForm__label">
                        Password
                    </h2>
                    <div class="inputWrap">
                        <input type="password" class="formInput " placeholder="******"
                               name="password">
                    </div>
                </div>
                <span class="error-block-text">
                    <h3><strong></strong></h3>
                </span>
                <span class="success-block">
                    <h3><strong></strong></h3>
                </span>
            </form>

        </div>
        <div class="login__footer">
            <div class="login__row">
                <div class="checbox_wrap checbox_wrap--signedIn ">
                    <input type="checkbox" class="checkboxNew" id="check1"/>
                    <label for="check1"> <span>Stay signed in</span></label>
                </div>
                <div class="roundedBtn login__btn blogin">
                    <a href="toLogin" class="roundedBtn__item"
                       onclick="event.preventDefault();login_ajax(document.getElementById
                       ('login__form'));$('' +
                        '.blogin').hide();">
                        login
                    </a>
                </div>
                <div class="roundedBtn login__btn btry" style="display: none">
                    <a href="toLogin" class="roundedBtn__item"
                       onclick="event.preventDefault();refreshLoginForm(document.getElementById('login__form')); $('.btry').hide();">
                        try again
                    </a>
                </div>
            </div>


            <a href="{{route('password.request')}}" class="login__forgot">
                Forgot password?
            </a>
        </div>

    </div>
</div>
<div id="findMessage" class="modal fade" role="dialog">
    <div class="login" style="position: fixed; top:50%; left:50%; transform: translate(-50%, -50%);">
        <div class="login__header">
            <p style="color: #fff;font-size: 1.563em;text-transform: uppercase;">We're opening soon!</p>
            <a href="#" data-dismiss="modal" class="close closeModal">
                <i class="fa fa-times"></i>
            </a>
        </div>
        <div class="who-you-are">
            <div class="who-you-are__box">
                <span>
                    <p>Hi!</p>
                    <p>
                        Please note: We are not yet taking booking for carers. We will contact all registered users when we are open. You can register by pressing the sign up button at the top of the screen.
                    </p>
                    <p>See you soon!</p>
                    <p>The Holm Team</p>
                </span>
            </div>
        </div>
    </div>
</div>

<div id="PurchaserMessage" class="modal fade" role="dialog">
    <div class="login" style="position: fixed; top:50%; left:50%; transform: translate(-50%, -50%);">
        <div class="login__header">
            <p style="color: #fff;font-size: 1.563em;text-transform: uppercase;">We're opening soon!</p>
            <a href="#" data-dismiss="modal" class="close closeModal">
                <i class="fa fa-times"></i>
            </a>
        </div>
        <div class="who-you-are">
            <div class="who-you-are__box">
                <span>
                    <p>Please note:</p>
                    <p>We are not yet taking booking for carers.</p>
                    <p>We will be starting in early February.</p>
                    <p>We will contact all registered users when we are open.</p>
                    <p>Best wishes</p>
                    <p>The Holm Team</p>
                </span>
            </div>
        </div>
    </div>
</div>

<div id="NeedAddCard" class="modal fade" role="dialog">
    <div class="login" style="position: fixed; top:50%; left:50%; transform: translate(-50%, -50%);">
        <div class="login__header">
            <p style="color: #fff;font-size: 1.563em;text-transform: uppercase;">We're not ready!</p>
            <a href="#" data-dismiss="modal" class="close closeModal">
                <i class="fa fa-times"></i>
            </a>
        </div>
        <div class="who-you-are">
            <div class="who-you-are__box">
                <span>
                    <p>Sorry. To book a carer you need to add a payment card. Click <a href="{{route('purchaserSettings')}}">here</a> to add you payment details.</p>
                </span>
            </div>
        </div>
    </div>
</div>

<div id="serviceUserPhotoUploadErrorModal" class="modal fade" role="dialog">
    <div class="login" style="position: fixed; top:50%; left:50%; transform: translate(-50%, -50%);">
        <div class="login__header">
            <p style="color: #fff;font-size: 1.563em;text-transform: uppercase;">Sorry.</p>
            <a href="#" data-dismiss="modal" class="close closeModal" onclick="$('#step').submit();">
                <i class="fa fa-times"></i>
            </a>
        </div>
        <div class="who-you-are">
            <div class="who-you-are__box">
                <span>
                    <p>Something went wrong.</p>
                    <p>Please re-upload photo or use another one.</p>
                </span>
            </div>
        </div>
    </div>
</div>


<div id="wrongEmail" class="modal fade" role="dialog">
    <div class="login" style="position: fixed; top:50%; left:50%; transform: translate(-50%, -50%);">
        <div class="login__header">
            <p style="color: #fff;font-size: 1.563em;text-transform: uppercase;">Wrong email!</p>
            <a href="#" data-dismiss="modal" class="close closeModal">
                <i class="fa fa-times"></i>
            </a>
        </div>
        <div class="who-you-are">
            <div class="who-you-are__box">
                <span>
                    <p>Please note:</p>
                    <p>Best wishes</p>
                    <p>The Holm Team</p>
                </span>
            </div>
        </div>
    </div>
</div>
