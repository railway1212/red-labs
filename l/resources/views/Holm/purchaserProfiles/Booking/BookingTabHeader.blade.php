<section class="mainSection">
    <div class="container">
    <div class="justifyContainer justifyContainer--smColumn">
    <div class="breadcrumbs">
        <a href="{{route('mainHomePage')}}" class="breadcrumbs__item">
            Home
        </a>
        <span class="breadcrumbs__arrow">></span>
         <a href="#" class="breadcrumbs__item">
           My Bookings
         </a>
    </div>
    <div class="bookingGroup">
</div>
</div>

        <div class="bookingSwitcher">
            <a href="/l/purchaser-settings/{{$purchaserProfile->id}}" class="bookingSwitcher__link bookingSwitcher__link--active">Profile
                settings</a>
            <a href="/l/purchaser-settings/booking/all/{{$purchaserProfile->id}}" class="bookingSwitcher__link">My
                bookings {!! $bookingCount ? '<span>+'.$bookingCount.'</span>' : '' !!}</a>
        </div>
    </div>
</section>
