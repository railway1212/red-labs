@include(config('settings.frontTheme').'.purchaserProfiles.Booking.BookingTabHeader')
<section class="mainSection">
    <div class="container">
        <div class="justifyContainer justifyContainer--smColumn">
            <div class="bookingNav">
                <a href="/l/purchaser-settings/booking/all/{{$userID}}" class="bookingNav__link centeredLink">
            <span class="bookingNav__text">
              ALL
            </span>
                </a>
                <a href="/l/purchaser-settings/booking/pending/{{$userID}}" class="bookingNav__link centeredLink">
            <span class="bookingNav__text">
              pending
            </span>
                </a>
                <a href="/l/purchaser-settings/booking/progress/{{$userID}}" class="bookingNav__link centeredLink">
            <span class="bookingNav__text">
              in progress
            </span>
                </a>
                <a href="/l/purchaser-settings/booking/completed/{{$userID}}" class="bookingNav__link centeredLink">
            <span class="bookingNav__text">
              completed
            </span>
                </a>
                <a href="/l/purchaser-settings/booking/canceled/{{$userID}}" class="bookingNav__link centeredLink">
            <span class="bookingNav__text">
              cancelled
            </span>
                </a>
                <a href="/l/purchaser-settings/booking/favourite/{{$userID}}" class="bookingNav__link centeredLink">
            <span class="bookingNav__text">
            favourite carers
            </span>
                </a>
            </div>
            <a href="javascript: window.print();" class="printIco">
                <img src="/l/public/img/print.png"  alt="">
            </a>


            @if($status == 'progress')
                <div class="total">
                    <div class="total__item  total__item--smaller totalBox">
                        <div class="totalTitle">
                            <p>Total </p>
                        </div>
                        <p class="totalPrice totalPrice--smaller">£{{number_format($inProgressAmount,2,'.',' ')}}</p>
                    </div>
                </div>
            @endif
            @if($status == 'completed')
                <div class="total">
                    <div class="total__item  total__item--smaller totalBox">
                        <div class="totalTitle">
                            <p>Total </p>
                        </div>
                        <p class="totalPrice totalPrice--smaller">£{{number_format($completedAmount,2,'.',' ')}}</p>
                    </div>
                </div>
            @endif
        </div>

        <div class="bookings">
            @if($status == 'all' || $status == 'pending')
            <div class="bookingCard bookingCard--new">
                <div class="bookingCard__header bookingCard__header">
                    <h2>pending</h2>
                </div>
                @if($newBookings->count() > 0)
                    @foreach($newBookings as $booking)
                        <div class="bookingCard__body bookInfo">
                            <div class="bookInfo__profile">
                                <a href="l/{{$booking->bookingCarer()->first()->profile_link}}" class="profilePhoto bookInfo__photo">
                                    <img src="{{asset('public/img/profile_photos/'.$booking->bookingCarer()->first()->id.'.png')}}" onerror="this.src='/l/public/img/no_photo.png'"  alt="">
                                </a>
                                <div class="bookInfo__text">
                                    <p>
                                        You booked <a href="/l{{$booking->bookingCarer()->first()->profile_link}}">{{$booking->bookingCarer()->first()->short_full_name}}</a>
                                        for
                                        @if(!$booking->bookingServiceUser->isBlocked())
                                            <a href="/l/serviceUser-settings/{{$booking->bookingServiceUser->id}}">{{$booking->bookingServiceUser->short_full_name}}</a>
                                        @else
                                            {{$booking->bookingServiceUser->short_full_name}}
                                        @endif
                                    </p>
                                    <a href="{{url('/bookings/'.$booking->id.'/details')}}" class="">view details</a>
                                </div>

                            </div>
                            <div class="bookInfo__date">
                                <span class="bookDate">{{$booking->appointments()->get()->count()}} Appointment{{$booking->appointments()->get()->count() > 1 ? 's':''}}</span>
                                <p class="hourPrice">
                                    @if(Auth::user()->isCarer())
                                        {{$booking->hours}}h /
                                        <span>£{{number_format($booking->amount_for_carer,2,'.',' ')}}</span>
                                    @else
                                        {{$booking->hours}}h /
                                        <span>£{{number_format($booking->amount_for_purchaser,2,'.',' ')}}</span>
                                    @endif

                                </p>
                            </div>
                            <div class="bookInfo__btns">
                                <div class="roundedBtn">
                                    <button {{!in_array($booking->purchaser_status_id, [2]) ? 'disabled' : ''}} data-booking_id = "{{$booking->id}}" data-status = "accept"  class="changeBookingStatus roundedBtn__item roundedBtn__item--smalest roundedBtn__item--accept">
                                        accept
                                    </button>
                                </div>
                                <div class="roundedBtn">
                                    <button id="cancelBtn" data-booking_id = "{{$booking->id}}" data-status = "cancel" data-toggle="modal" data-target="#confirm-cancel" class="roundedBtn__item roundedBtn__item--smalest roundedBtn__item--reject">
                                        cancel
                                    </button>
                                </div>
                                <div class="roundedBtn">
                                    <button {{!in_array($booking->purchaser_status_id, [2]) ? 'disabled' : ''}} data-id="{{$booking->id}}"  class="roundedBtn__item   roundedBtn__item--alternative-smal">
                                        OFFER ALTERNATIVE TIME
                                    </button>
                                </div>
                            </div>
                        </div>
                    @endforeach
                    @if($newBookingsAll->count() >= 5)
                        <div class="moreBtn moreBtn--book ">
                            <input type="hidden" name="page" value="{{$page}}">
                            <a href="" id="moreBtnNewBookings" class="moreBtn__item moreBtn__item--book centeredLink">
                                Load More
                            </a>
                        </div>
                    @endif
                @else
                    <p align="center" class="bookDate">You do not have bookings yet</p>
                @endif
            </div>
            @endif


            @if($status == 'all' || $status == 'progress')
            <div class="bookingCard bookingCard--progress">
                <div class="bookingCard__header bookingCard__header">
                    <h2>in progress</h2>
                </div>
                @if($inProgressBookings->count() > 0)
                    @foreach($inProgressBookings as $booking)
                        <div class="bookingCard__body bookInfo">
                            <div class="bookInfo__profile">
                                <a href="/l{{$booking->bookingCarer()->first()->profile_link}}" class="profilePhoto bookInfo__photo">
                                    <img src="{{asset('public/img/profile_photos/'.$booking->bookingCarer()->first()->id.'.png')}}" onerror="this.src='/l/public/img/no_photo.png'" onerror="this.src='/img/no_photo.png'" alt="">
                                </a>
                                <div class="bookInfo__text">
                                    <p>
                                        You booked <a href="/l{{$booking->bookingCarer()->first()->profile_link}}">{{$booking->bookingCarer()->first()->short_full_name}}</a>
                                        for
                                        @if(!$booking->bookingServiceUser->isBlocked())
                                            <a href="/l/serviceUser-settings/{{$booking->bookingServiceUser->id}}">{{$booking->bookingServiceUser->short_full_name}}</a>
                                        @else
                                            {{$booking->bookingServiceUser->short_full_name}}
                                        @endif
                                    </p>
                                    <a href="{{url('/bookings/'.$booking->id.'/details')}}" class="">view details</a>
                                </div>

                            </div>
                            <div class="bookInfo__date">
                                <span class="bookDate">{{$booking->appointments()->get()->count()}} Appointment{{$booking->appointments()->get()->count() > 1 ? 's':''}}</span>
                                <p class="hourPrice">
                                    @if(Auth::user()->isCarer())
                                        {{$booking->hours}}h /
                                        <span>£{{number_format($booking->amount_for_carer,2,'.',' ')}}</span>
                                    @else
                                        {{$booking->hours}}h /
                                        <span>£{{number_format($booking->amount_for_purchaser,2,'.',' ')}}</span>
                                    @endif

                                </p>
                            </div>
                            <div class="bookInfo__btns">
                                <div class="roundedBtn">
                                    <button id="cancelBtn" {{!in_array($booking->purchaser_status_id, [3, 5]) ? 'disabled' : ''}}  data-booking_id = "{{$booking->id}}" data-status = "cancel" data-toggle="modal" data-target="#confirm-cancel" class="roundedBtn__item roundedBtn__item--smalest roundedBtn__item--cancel">
                                        cancel
                                    </button>
                                </div>
                                {{--<div class="roundedBtn">--}}
                                    {{--<button  {{!in_array($booking->purchaser_status_id, [3, 5]) || $booking->has_active_appointments ? 'disabled' : ''}}   data-booking_id = "{{$booking->id}}"  data-status = "completed"  class="changeBookingStatus roundedBtn__item roundedBtn__item--smalest roundedBtn__item--accept">--}}
                                        {{--completed--}}
                                    {{--</button>--}}
                                {{--</div>--}}

                            </div>

                        </div>
                    @endforeach
                @else
                    <p align="center" class="bookDate">You do not have bookings yet</p>
                @endif
                @if($inProgressBookingsAll->count() > 5)
                    <div class="moreBtn moreBtn--book ">
                        <input type="hidden" name="page" value="{{$page}}">
                        <a href="" id="moreBtnInProgressBookings" class="moreBtn__item moreBtn__item--book centeredLink">
                            Load More
                        </a>
                    </div>
                @endif
            </div>
            @endif



            @if($status == 'all' || $status == 'completed')
                <div class="bookingCard bookingCard--complete">
                    <div class="bookingCard__header bookingCard__header">
                        <h2>completed</h2>
                    </div>
                    @if($completedBookings->count() > 0)
                        @foreach($completedBookings as $booking)
                            <div class="bookingCard__body bookInfo">
                                <div class="bookInfo__profile">
                                    <a href="{{$booking->bookingCarer()->first()->profile_link}}" class="profilePhoto bookInfo__photo">
                                        <img src="{{asset('public/img/profile_photos/'.$booking->bookingCarer()->first()->id.'.png')}}" onerror="this.src='/l/public/img/no_photo.png'" alt="">
                                    </a>
                                    <div class="bookInfo__text">
                                        <p>
                                            You booked <a href="/l{{$booking->bookingCarer()->first()->profile_link}}">{{$booking->bookingCarer()->first()->short_full_name}}</a>
                                            for
                                            @if(!$booking->bookingServiceUser->isBlocked())
                                                <a href="/l/serviceUser-settings/{{$booking->bookingServiceUser->id}}">{{$booking->bookingServiceUser->short_full_name}}</a>
                                            @else
                                                {{$booking->bookingServiceUser->short_full_name}}
                                            @endif
                                        </p>
                                        <a href="{{url('/bookings/'.$booking->id.'/details')}}" class="">view details</a>
                                    </div>

                                </div>
                                <div class="bookInfo__date">
                                    <span class="bookDate">{{$booking->appointments()->get()->count()}} Appointment{{$booking->appointments()->get()->count() > 1 ? 's':''}}</span>
                                    <p class="hourPrice">
                                        @if(Auth::user()->isCarer())
                                            {{$booking->hours}}h /
                                            <span>£{{number_format($booking->amount_for_carer,2,'.',' ')}}</span>
                                        @else
                                            {{$booking->hours}}h /
                                            <span>£{{number_format($booking->amount_for_purchaser,2,'.',' ')}}</span>
                                        @endif

                                    </p>
                                </div>
                                {{--<div class="bookInfo__btns">    --}}
                                    {{--<div class="roundedBtn">--}}
                                        {{--<button {{$booking->overviews()->get()->count() ? 'disabled' : ''}} onclick="location.replace('{{url('/bookings/'.$booking->id.'/leave_review')}}')" class="roundedBtn__item roundedBtn__item--smalest roundedBtn__item--auto-width roundedBtn__item--forReview">--}}
                                            {{--Please Leave A Review--}}
                                        {{--</button>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            </div>
                        @endforeach
                    @else
                        <p align="center" class="bookDate">You do not have bookings yet</p>
                    @endif
                    @if($completedBookingsAll->count() > 5)
                        <div class="moreBtn moreBtn--book ">
                            <input type="hidden" name="page" value="{{$page}}">
                            <a href="" id="moreBtnCompletedBookings" class="moreBtn__item moreBtn__item--book centeredLink">
                                Load More
                            </a>
                        </div>
                    @endif
                </div>
            @endif

            @if($status == 'all' || $status == 'canceled')
                <div class="bookingCard bookingCard--complete">
                    <div class="bookingCard__header bookingCard__header">
                        <h2>cancelled</h2>
                    </div>
                    @if($canceledBookings->count() > 0)
                        @foreach($canceledBookings as $booking)
                            <div class="bookingCard__body bookInfo">
                                <div class="bookInfo__profile">
                                    <a href="{{$booking->bookingCarer()->first()->profile_link}}" class="profilePhoto bookInfo__photo">
                                        <img src="{{asset('public/img/profile_photos/'.$booking->bookingCarer()->first()->id.'.png')}}" onerror="this.src='/l/public/img/no_photo.png'" alt="">
                                    </a>
                                    <div class="bookInfo__text">
                                        <p>
                                            You booked <a href="/l{{$booking->bookingCarer()->first()->profile_link}}">{{$booking->bookingCarer()->first()->short_full_name}}</a>
                                            for
                                            @if(!$booking->bookingServiceUser->isBlocked())
                                                <a href="/l/serviceUser-settings/{{$booking->bookingServiceUser->id}}">{{$booking->bookingServiceUser->short_full_name}}</a>
                                            @else
                                                {{$booking->bookingServiceUser->short_full_name}}
                                            @endif
                                        </p>
                                        <a href="{{url('/bookings/'.$booking->id.'/details')}}" class="">view details</a>
                                    </div>

                                </div>
                                <div class="bookInfo__date">
                                    <span class="bookDate">{{$booking->appointments()->get()->count()}} Appointment{{$booking->appointments()->get()->count() > 1 ? 's':''}}</span>
                                    <p class="hourPrice">
                                        @if(Auth::user()->isCarer())
                                            {{$booking->hours}}h /
                                            <span>£{{number_format($booking->amount_for_carer,2,'.',' ')}}</span>
                                        @else
                                            {{$booking->hours}}h /
                                            <span>£{{number_format($booking->amount_for_purchaser,2,'.',' ')}}</span>
                                        @endif

                                    </p>
                                </div>
                            </div>
                        @endforeach
                    @else
                        <p align="center" class="bookDate">You do not have bookings yet</p>
                    @endif
                    @if($canceledBookingsAll->count() > 5)
                        <div class="moreBtn moreBtn--book ">
                            <input type="hidden" name="page" value="{{$page}}">
                            <a href="" id="moreBtnCanceledBookings" class="moreBtn__item moreBtn__item--book centeredLink">
                                Load More
                            </a>
                        </div>
                    @endif
                </div>
            @endif

             <!-- ---------------favourite booking -------------------------------- -->
             @if($status == 'all' || $status == 'favourite')
                <div class="bookingCard bookingCard--complete">
                    <div class="bookingCard__header bookingCard__header">
                        <h2>favourite </h2>
                    </div>
                    @if($favouriteBookings->count() > 0)
                        @foreach($favouriteBookings as $booking)
                            <div class="bookingCard__body bookInfo">
                                <div class="bookInfo__profile">
                                    <a href="/l{{$booking->bookingCarer()->first()->profile_link}}" class="profilePhoto bookInfo__photo">
                                        <img src="{{asset('public/img/profile_photos/'.$booking->bookingCarer()->first()->id.'.png')}}" onerror="this.src='/l/public/img/no_photo.png'" alt="">
                                    </a>
                                    <div class="bookInfo__text">
                                        <p>
                                             <a href="/l{{$booking->bookingCarer()->first()->profile_link}}">{{$booking->bookingCarer()->first()->short_full_name}}</a>
                                            <!-- for
                                            @if(!$booking->bookingServiceUser->isBlocked())
                                                <a href="/serviceUser-settings/{{$booking->bookingServiceUser->id}}">{{$booking->bookingServiceUser->short_full_name}}</a>
                                            @else
                                                {{$booking->bookingServiceUser->short_full_name}}
                                            @endif -->
                                        </p>
                                        <!-- <a href="{{url('bookings/'.$booking->id.'/details')}}" class="">view details</a> -->
                                    </div>

                                </div>
                                <!-- <div class="bookInfo__date">
                                    <span class="bookDate">{{$booking->appointments()->get()->count()}} Appointment{{$booking->appointments()->get()->count() > 1 ? 's':''}}</span>
                                    <p class="hourPrice">
                                        @if(Auth::user()->isCarer())
                                            {{$booking->hours}}h /
                                            <span>£{{number_format($booking->amount_for_carer,2,'.',' ')}}</span>
                                        @else
                                            {{$booking->hours}}h /
                                            <span>£{{number_format($booking->amount_for_purchaser,2,'.',' ')}}</span>
                                        @endif

                                    </p>
                                </div> -->
                    
                    <!-- <div class="viewProfile">
                            <a href="{{$booking->bookingCarer()->first()->profile_link}}" class="viewProfile__item centeredLink">
                                book carer now
                            </a>
                        </div> -->

                         <div class="bookBtn">
                <a href="/l{{$booking->bookingCarer()->first()->profile_link}}" class="bookBtn__item bookBtn__item--smaller centeredLink" >
                    book carer
                </a>
            </div>
                            </div>
                            
                        @endforeach
                    @else
                        <p align="center" class="bookDate">You do not have bookings yet</p>
                    @endif
                    @if($favouriteBookingsAll->count() > 5)
                        <div class="moreBtn moreBtn--book ">
                            <input type="hidden" name="page" value="{{$page}}">
                            <a href="" id="moreBtnfavoriteBookings" class="moreBtn__item moreBtn__item--book centeredLink">
                                Load More
                            </a>
                        </div>
                    @endif
                </div>
            @endif



        </div>

    </div>
</section>
<!-- ///////////
<!-- <div id="message-carer" class="modalWrapper modal fade">
    <div class="customModal">
        <div class="message">
            <div class="message__header">
                <div class="bookCarer">
                    <a href="" class="bookCarer__item bookCarer__item--modal centeredLink">Book carer</a>
                </div>
                <a href="#" data-dismiss="modal" aria-label="Close" class="closeModal">
                    <i class="fa fa-close"></i>
                </a>
            </div>
            
        </div>
    </div>

</div> -->

<script>
    $('.changeBookingStatus').click(function () {
        showSpinner();
        var booking_id = $(this).attr('data-booking_id');
        var status = $(this).attr('data-status');
        $.post('/l/bookings/'+booking_id+'/'+status, function (data) {
            if(data.status == 'success'){
                location.reload();
            } else {
                showErrorModal({title: 'Error', description: data.message});
            }
            hideSpinner();
        });
    });


    //called from modal #confirm-cancel
    function confirmCancel(){

        showSpinner();
        var booking_id = $('#cancelBtn').attr('data-booking_id');
        var status = $('#cancelBtn').attr('data-status');
        $.post('/l/bookings/'+booking_id+'/'+status, function (data) {
            if(data.status == 'success'){
                location.reload();
            } else {
                showErrorModal({title: 'Error', description: data.message});
            }
            hideSpinner();
        });
    }
  
  
// -- SPINNER -------
  
  function showErrorModal(message) {
    $('body').addClass('overflow-hidden')
    $('body').append(
        '<div class="error-popup-container">'+
        '<div class="error-popup">'+
        '<div class="error-popup__ico">'+
        '<span>'+
        '<i class="fa fa-times-circle"></i>'+
        '</span>'+
        '</div>'+
        '<a href="#" class="error-popup__close">'+
        '<i class="fa fa-times"></i>'+
        '</a>'+
        '<div class="error-popup__body">'+
        '<h2>'+
        message.title+
        '</h2>'+
        '<p class="info-p info-p--roboto">'+
        message.description+
        '</p>'+
        '</div>'+
        '</div>'+
        '</div>'
    );
};
function showSpinner() {
    $('body').addClass('overflow-hidden')
    $('body').append(
        '<div class="show-spinner"></div>'
    );
};

function hideSpinner() {
    $('body').removeClass('overflow-hidden')
    $( ".show-spinner" ).remove();
};

    $('#moreBtnNewBookings').on('click', function(e){
        showSpinner();
        var page = parseInt($('#moreBtnNewBookings').parent().find('input[name="page"]').val())+1;
        $('#moreBtnNewBookings').parent().find('input[name="page"]').val(page);
        $.get('/l/purchaser-settings/booking/new?page='+page, function (data) {
            if(data.result==true){
                $('#moreBtnNewBookings').parent().before(data.content);
            }
            if(data.hideLoadMore) $('#moreBtnNewBookings').hide();
            hideSpinner();
        });
    });

    $('#moreBtnInProgressBookings').on('click', function(e){
        showSpinner();
        var page = parseInt($('#moreBtnInProgressBookings').parent().find('input[name="page"]').val())+1;
        $('#moreBtnInProgressBookings').parent().find('input[name="page"]').val(page);
        $.get('/l/purchaser-settings/booking/progress?page='+page, function (data) {
            if(data.result==true){
                $('#moreBtnInProgressBookings').parent().before(data.content);
            }
            if(data.hideLoadMore) $('#moreBtnInProgressBookings').hide();
            hideSpinner();
        });
    });

    $('#moreBtnCompletedBookings').on('click', function(e){
        showSpinner();
        var page = parseInt($('#moreBtnCompletedBookings').parent().find('input[name="page"]').val())+1;
        $('#moreBtnCompletedBookings').parent().find('input[name="page"]').val(page);
        $.get('/l/purchaser-settings/booking/completed?page='+page, function (data) {
            if(data.result==true){
                $('#moreBtnCompletedBookings').parent().before(data.content);
            }
            if(data.hideLoadMore) $('#moreBtnCompletedBookings').hide();
            hideSpinner();
        });
    });
    $('#moreBtnCanceledBookings').on('click', function(e){
        showSpinner();
        var page = parseInt($('#moreBtnCanceledBookings').parent().find('input[name="page"]').val())+1;
        $('#moreBtnCanceledBookings').parent().find('input[name="page"]').val(page);
        $.get('/l/purchaser-settings/booking/canceled?page='+page, function (data) {
            if(data.result==true){
                $('#moreBtnCanceledBookings').parent().before(data.content);
            }
            if(data.hideLoadMore) $('#moreBtnCanceledBookings').hide();
            hideSpinner();
        });
    });



    $('#moreBtnfavoriteBookings').on('click', function(e){
        showSpinner();
        var page = parseInt($('#moreBtnfavoriteBookings').parent().find('input[name="page"]').val())+1;
        $('#moreBtnfavoriteBookings').parent().find('input[name="page"]').val(page);
        $.get('/l/purchaser-settings/booking/favourite?page='+page, function (data) {
            if(data.result==true){
                $('#moreBtnfavoriteBookings').parent().before(data.content);
            }
            if(data.hideLoadMore) $('#moreBtnfavoriteBookings').hide();
            hideSpinner();
        });
    });
</script>



<script>
    var times = [];
    times[2] = [5, 8, 11, 14, 17, 20, 23];
    times[3] = [6, 9, 12, 15, 18, 21, 24];
    times[4] = [7, 10, 13, 16, 19, 22, 25];
    $(document).ready(function () {
        if ($('p#1').length > 0) {
            $('p#2').hide();
            $('p#3').hide();
            $('p#4').hide();
        }
        if ($('p#2').length > 0) {
            $.each(times[2], function (index, value) {
                $('p#' + value).hide();
            });
        }
        if ($('p#3').length > 0) {
            $.each(times[3], function (index, value) {
                $('p#' + value).hide();
            });
        }
        if ($('p#4').length > 0) {
            $.each(times[4], function (index, value) {
                $('p#' + value).hide();
            });
        }
    });
</script>
