<div class="card-list-box">
    <p class="card-list__item">
        xxxx xxxx xxxx {{ substr(str_replace(' ','',$card->number), 12)}}
    </p>
    <form id="deleteCard{{$card_id}}" action="{{route('DeleteCreditCards',['card_id'=>$card_id])}}" method="DELETE">
    <div class="payment-control payment-control--for-card ">
        <a href="#" type="submit" class="payment-control__item payment-control__item--delete deleteCard">
            <i class="fa fa-trash"></i>
            <span>delete</span>
        </a>
    </div>
    </form>
</div>