<script>
    function deleteCard(form) {
        $(form).parent().remove();
        $.ajax({
            url: $(form).attr('action'),
            data: $(form).serialize(),
            type: 'get',
            async: false,
            dataType: "application/json",
            success: function (response) {
                console.log('success');
            },
            error: function (xhr, status, error) {
                var err = eval("(" + xhr.responseText + ")");
                console.log('error'+err);
            }
        });
    }
    $(document).ready(function(){
        // $('a.btn-edit').on('click',function(){
        //     $('a.btn-edit').hide();
        //     $('a.addCard').show();
        //     $('a.deleteCard').show();
        //     $('#saveBtn').show();
        // });
        $('body').on('click', 'a.deleteCard', function(e) {
           e.preventDefault();
           var form = $(this).parents('form:first');
           var formId = form.attr('id');
           //$(this).parent().parent().remove();
           deleteCard(document.getElementById(formId));
            return false;
        });
        $('a.addCard').on('click',function(e){
            e.preventDefault();
            var form = document.getElementById('addCard');
            $.ajax({
                url: $(form).attr('action'),
                data: $(form).serialize(),
                type: 'POST',
                async: false,
                dataType: "json",
                success: function (response) {
                    if (response.status == "error") {
                        var html_error = '<div class="error-card" style="color: red; font-size: 18px;">' + response.message + '</div>';
                        $('div.payment-control #saveNewCardBtn').before(html_error);
                        console.log(response.message);

                    } else if(response.status == "success")  {
                         if(response.data.card){
                            $('div.card-list h2').after(response.data.card);
                         }
                    }

                    cleanCardInputs();
                    // console.log(response);
                },
                error: function (response) {
                    console.log('error');
                }
            });
        });
    });
</script>
<!-- <div class="borderContainer">
    <div class="profileCategory">
        <h2 class="profileCategory__title">Payments Details</h2>
        <a href="#" class="btn btn-info btn-edit btn-edit"><span class="fa fa-pencil" data-id="Payment"></span> EDIT</a>
        <button type="button" class="btn btn-success hidden" id="load" data-loading-text="<i class='fa fa-spinner
        fa-spin '></i> Processing"><i class="fa fa-floppy-o"></i>  Save</button>
    </div>

    {!! Form::model($purchaserProfile, ['method'=>'POST','route'=>'purchaserSettingsPost','id'=>'Payment']) !!}
    {!! Form::hidden('id',null) !!}
    {!! Form::hidden('stage','payment') !!}
    <div class="profileRow">
        <div class="profileField">
            <h2 class="profileField__title ordinaryTitle">
                <span class="ordinaryTitle__text ordinaryTitle__text--smaller">
                  cvv <span class="requireIco">*</span>
                </span>
            </h2>
            {!! Form::text('cvv',null,['class'=>'profileField__input','placeholder'=>'CVV']) !!}
        </div>
        <div class="profileField">
            <h2 class="profileField__title ordinaryTitle">
                <span class="ordinaryTitle__text ordinaryTitle__text--smaller">
                  PAYMENT CARD NUMBER <span class="requireIco">*</span>
                </span>
            </h2>
            {!! Form::text('payment_card_number',null,['class'=>'profileField__input','placeholder'=>'PAYMENT CARD NUMBER']) !!}
        </div>


        <div class="profileField">
            <h2 class="profileField__title ordinaryTitle">
                <span class="ordinaryTitle__text ordinaryTitle__text--smaller">
                  EXPIRY DATE <span class="requireIco">*</span>
                </span>
            </h2>
            {!! Form::text('expiry_date',null,['class'=>'profileField__input','placeholder'=>'EXPIRY DATE']) !!}
        </div>
    </div>

    <div class="payment-control">
        <a href="#" class="payment-control__item payment-control__item--delete">
            <i class="fa fa-trash"></i>
            <span>delete</span>
        </a>
        <a href="#" class="payment-control__item">
            <i class="fa fa-plus"></i>
            <span>add</span>
        </a>
    </div>
    {!! Form::close()!!}
</div> -->

<div class="borderContainer">
    <div class="profileCategory">
        <h2 class="profileCategory__title">Payment details</h2>
        <a href="#" id="editPaymentDetailsBtn" class="btn btn-info btn-edit btn-edit"><span class="fa fa-pencil" data-id="addCard"></span> EDIT</a>
        <button type="button" id="saveBtn" class="btn btn-success" onclick="finishAddCard()" style="display: none"><i class="fa fa-floppy-o"></i>  Save</button>
    </div>
    <div class="row">
        <div class="col-md-6">
                <div class="card-list">
                <h2 class="formLabel">
                    List of Cards
                </h2>
                  
                
                  
                @foreach(\App\User::find(Auth::user()->id)->credit_cards as $card)

                <div class="card-list-box">
                    <p class="card-list__item">
                        xxxx xxxx xxxx {{$card->last_four}}
                    </p>
                    <form id="deleteCard{{$card->id}}" action="{{route('DeleteCreditCards',['card_id'=>$card->id])}}" method="DELETE">
                        <div class="payment-control payment-control--for-card ">
                            <a href="#" class="payment-control__item payment-control__item--delete deleteCard" style="display: none">
                                <i class="fa fa-trash"></i>
                                <span>delete</span>
                            </a>
                        </div>
                    </form>

                </div>
                @endforeach



            </div>
        </div>
        <form id="addCard" method="post" action="{{route('CreditCards')}}">
            {{ csrf_field()  }}
        <div class=" col-lg-5 col-lg-offset-1 col-md-6">
            <div class="profile-payment">
                <h2 class="formLabel">
                    Card Number
                </h2>
                <div class="inputWrap">
                    <input type="text"  name="number" class="formInput" placeholder="" onkeypress='return event.charCode >= 48 && event.charCode <= 57' maxlength="19">
                    <span class="bookPayment__ico bookPayment__ico--first">
                    <img src="/l/public/img/mc2.png" alt="">
                  </span>
                    <span class="bookPayment__ico">
                    <img src="/l/public/img/visa.png" alt="">
                  </span>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <h2 class="formLabel">
                        Valid Until
                    </h2>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="inputWrap">
                                <input type="text" name="exp_month" class="formInput" placeholder="MM" onkeypress='return event.charCode >= 48 && event.charCode <= 57' maxlength="2">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="inputWrap inputWrap--md-margin">
                                <input type="text" name="exp_year" class="formInput" placeholder="YY" onkeypress='return event.charCode >= 48 && event.charCode <= 57' maxlength="2">
                            </div>
                        </div>
                    </div>

                </div>
                <div class="col-md-6">
                    <h2 class="formLabel">
                        CVC CODE
                    </h2>
                    <div class="inputWrap">
                        <input type="text" name="cvc" class="formInput" placeholder="" onkeypress='return event.charCode >= 48 && event.charCode <= 57' maxlength="3">
                    </div>
                </div>
            </div>
            <div class="payment-control">
                <a type="submit" href="#" id="saveNewCardBtn" class="payment-control__item addCard" style="display: none">
                    Save payment details
                </a>
            </div>
        </div>
        </form>
    </div>
</div>

<script>
    function finishAddCard(){
        $('a.addCard').hide();
        $('a.deleteCard').hide();
        //$('a.payment-control__item--delete').hide();//Костыль, добавляемой кнопке не добавляется класс deleteCard
        cleanCardInputs();
        $('.formInput').addClass('profileField__input--greyBg');
        $('#saveBtn').hide();
        $('a.btn-edit').show();
    }

    function cleanCardInputs(){
        $('.formInput').val('');
    }

</script>
