<script>
$(function() {	
	$('.creditmodalbtn').click(function(){
		$('#numberOnly').removeAttr('readonly','readonly');
		$('#userId').removeAttr('readonly','readonly');
		$('#credit-wallet').modal("show");
		$('#numberOnly').val('');
	});
	
	
	$('#numberOnly').keypress(function(event) {
		if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
			event.preventDefault();
		}
	});

});
</script>


<div class="borderContainer">
    <div class="profileCategory">
        <h2 class="profileCategory__title">General</h2>
        <a href="#" class="btn btn-info btn-edit btn-edit"><span class="fa fa-pencil" data-id="PrivateGeneral"></span> EDIT</a>
        <button type="button" onclick="updateRC({{$purchaserProfile->id}})" class="btn btn-success hidden" id="load" data-loading-text="<i class='fa fa-spinner
        fa-spin '></i> Processing"><i class="fa fa-floppy-o"></i>  Save</button>
    </div>
</div>
{!! Form::model($purchaserProfile, ['method'=>'POST','route'=>'purchaserSettingsPost','id'=>'PrivateGeneral']) !!}
{!! Form::hidden('id',null) !!}
{!! Form::hidden('stage','general') !!}
<div class="borderContainer">
    <div class="profileInfoContainer">
        <div class="generalInfo">

          <div class="profilePhoto profilePhoto--change">
            <div class="formField">
              </div>
              <input hidden id="profileId" value="{{$purchaserProfile->id}}">

              <input disabled class="pickfiles_profile_photo--change" accept=".jpg,.jpeg,.png,.doc" type="file" />
              <img id="profile_photo" alt="avatar"
                   @if (file_exists(public_path('/img/profile_photos/' . $purchaserProfile->id . '.png')))
                   src="/l/public/{{$purchaserProfile->img_url}}"
                   @else
                   src="/l/public/img/no_photo.png"
                      @endif />

              <a href="#" class="profilePhoto__ico">
                  <i class="fa fa-plus-circle" aria-hidden="true"></i>
              </a>
          </div>

          <div style="display: none" class="addInfo">
              <input disabled type="text" name="profile_photo" class="addInfo__input" placeholder="Name">
          </div>

            <div class="generalInfo__text">
                @if(Auth::user()->user_type_id == 4)
                    <div class="generalInfo__elem">
                        <p>first name</p>
                        {!! Form::text('first_name',null,['class'=>'profileField__input','placeholder'=>'First name','maxlength'=>"20"]) !!}
                    </div>

                    <div class="generalInfo__elem">
                        <p>last name</p>
                        {!! Form::text('family_name',null,['class'=>'profileField__input','placeholder'=>'Family name','maxlength'=>"20"]) !!}
                    </div>
                    <div class="generalInfo__elem">
                        <p>gender</p>
                        {!! Form::select('gender', ['Male' => 'Male', 'Female' => 'Female'],null,['class'=>'profileField__input']) !!}
                    </div>
                    <div class="generalInfo__elem">
                        <p>date of birth</p>
                        <input id="datepickerPurchaser" class="profileField__input"
                               name="DoB" type="text"
                               onchange="updateDobValue()"
                               value="{{substr($purchaserProfile->DoB,6)}}">
                        <input id="DoBPurchaser" type="hidden" name="DoBPurchaser" value="{{$purchaserProfile->DoB}}">
                        {{Form::hidden('admin_edit_purchaser_flag',1,['id'=>'admin_edit_purchaser_flag'])}}
                    </div>
                @else
                    <div class="generalInfo__elem">
                        <p>first name</p><span>{{$purchaserProfile->first_name}} </span>
                           {{Form::hidden('first_name',$purchaserProfile->first_name)}}
                    </div>
                    <div class="generalInfo__elem">
                        <p>last name</p><span>{{$purchaserProfile->family_name}} </span>
                        {{Form::hidden('family_name',$purchaserProfile->family_name)}}
                    </div>
                    <div class="generalInfo__elem">
                        <p>gender</p><span>{{$purchaserProfile->gender}} </span>
                         {{Form::hidden('gender',$purchaserProfile->gender)}}
                    </div>
                    <div class="generalInfo__elem">
                        <p>year of birth</p><span>{{substr($purchaserProfile->DoB,6)}} </span>
                         {{Form::hidden('DoB',$purchaserProfile->DoB)}}
                         
                            
                    </div>
                @endif
                {{Form::hidden('purchaser_flag',1,['id'=>'purchaser_flag'])}}
                         
            </div>
        </div>
        <div class="total total--bonus ">
            <div class="total__item totalBox">
                <p class="totalPrice">£{{number_format($purchaserProfile->user->PaidBonuses, 2, '.', '')}}</p>
                <p class="totalTitle">
                    Bonus earned from referrals
                </p>
            </div>
        </div>
        
        
        

    
    
    
    </div>
    <div class="profileRow profileRow--justify">
        <div class="profileField">
            <h2 class="profileField__title ordinaryTitle">
              <span class="ordinaryTitle__text ordinaryTitle__text--smaller">
                I like to be called <span class="requireIco">*</span>
              </span>
            </h2>
            {!! Form::text('like_name',null,['class'=>'profileField__input','placeholder'=>'I like to be called']) !!}
        </div>
        
        
        <!-- For admin only -->
        @if(Auth::user()->user_type_id == 4)
        <div class="profileField">
            <h2 class="profileField__title ordinaryTitle">
              <span class="ordinaryTitle__text ordinaryTitle__text--smaller">
                Referral Code
              </span>
            </h2>
            {!! Form::text('referral_code',null,['class'=>'profileField__input','maxlength'=>"80"]) !!}
        </div>
    @endif
    <!-- / For admin only -->
    
    <div class="total total--bonus total--type3">
            <div class="total__item totalBox">
                <p class="totalPrice">£{{number_format($bonusBalance, 2, '.', '')}}</p>
                <p class="totalTitle">
                    Bonus Wallet Remaining
                </p>
            </div>
        </div>
        
    @if(Auth::user()->user_type_id == 4)
	   <div class="profileField"></div>
	   <div class="profileField"></div>	
	   <div class="total total--bonus credit-wallet-btn">	   
			  <a class="roundedBtn__item roundedBtn__item--accept creditmodalbtn">Credit Wallet</a>
	   </div>	   	
	@endif
        
    
    </div>
</div>

<div class="borderContainer">
    <h2 class="fieldCategory">
        Contacts
    </h2>
    <div class="profileRow">

        <div class="profileField">
            <h2 class="profileField__title ordinaryTitle">
              <span class="ordinaryTitle__text ordinaryTitle__text--smaller">
                Address Line 1 <span class="requireIco">*</span>
              </span>
            </h2>
            {!! Form::text('address_line1',null,['class'=>'profileField__input','data-country'=>'']) !!}

        </div>

        <div class="profileField">
            <h2 class="profileField__title ordinaryTitle">
              <span class="ordinaryTitle__text ordinaryTitle__text--smaller">
                Address Line 2
              </span>
            </h2>
            {!! Form::text('address_line2',null,['class'=>'profileField__input']) !!}

        </div>

        <div class="profileField">
            <h2 class="profileField__title ordinaryTitle">
              <span class="ordinaryTitle__text ordinaryTitle__text--smaller">
                Town / city <span class="requireIco">*</span>
              </span>
            </h2>
            <div class="profileField__input-wrap">

                {!! Form::text('town',null,['class'=>'profileField__input']) !!}
                {{--                <span class="profileField__input-ico centeredLink">
                                <i class="fa fa-map-marker" aria-hidden="true"></i>
                              </span>--}}
            </div>

        </div>



    </div>

    <div class="profileRow">
        <div class="profileField">
            <h2 class="profileField__title ordinaryTitle">
              <span class="ordinaryTitle__text ordinaryTitle__text--smaller">
                Post code <span class="requireIco">*</span>
              </span>
            </h2>
            {!! Form::text('postcode',null,['class'=>'profileField__input','data-country'=>'','id'=>'post_code_profile']) !!}
        </div>
        <div class="profileField">
            <h2 class="profileField__title ordinaryTitle">
              <span class="ordinaryTitle__text ordinaryTitle__text--smaller">
                Mobile Number <span class="requireIco">*</span>
              </span>
            </h2>
            {!! Form::text('mobile_number',null,['class'=>'profileField__input digitFilter07','maxlength'=>"11" ]) !!}

        </div>

        <div class="profileField">
            <h2 class="profileField__title ordinaryTitle">
              <span class="ordinaryTitle__text ordinaryTitle__text--smaller">
                Email ADDRESS <span class="requireIco">*</span>
              </span>
            </h2>
            {!! Form::text('user_email',$purchaserProfile->email,['class'=>'profileField__input']) !!}

        </div>
    </div>

    <div class="profileMap" style="width:100%;height:450px;display:none;">
        <div id="map_canvas" style="clear:both; height:450px;"></div>
    </div>
</div>
<!-- For admin only -->
{{--@if(Auth::user()->user_type_id == 4)--}}
    {{--<div class="borderContainer">--}}
        {{--<div class="profileCategory profileCategory--noBg">--}}
            {{--<h2 class="profileCategory__title">Referral Code</h2>--}}
        {{--</div>--}}
        {{--<div class="profileRow">--}}

            {{--<div class="profileField profileField--full-width">--}}

                {{--{!! Form::text('referral_code',null,['class'=>'profileField__input','maxlength'=>"80"]) !!}--}}

            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
{{--@endif--}}
<!-- / For admin only -->
{!! Form::close()!!}

<!--Modals to confirm cancellation --->
<div id="credit-wallet" class="modal fade" role="dialog">
    <div class="login">
        <div class="login__header">
            <p style="color: #fff;font-size: 1.563em;text-transform: uppercase;">Credit Wallet</p>
            <a href="#" data-dismiss="modal" class="close closeModal">
                <i class="fa fa-times"></i>
            </a>
        </div>
        <div class="who-you-are">
            <p class="who-you-are__text"></p>
            <div class="who-you-are__box">
				
				£<input type="text" id="numberOnly" name="wallet_amount" class="profileField__input profileField__input--grey" />
				<input type="hidden" id="userId" name="userId" value="{{$purchaserProfile->id}}"/>
				<button class="addcredit">Add</button>
				
            </div>
        </div>
    </div>
</div>

<script>
	
	$('.addcredit').click(function(){
		showSpinner();
		$('#credit-wallet').modal("hide");
		var amount =  $('#numberOnly').val();
		var userId =  $('#userId').val();
        $.ajax({
                url: "/admin/bonuses-purchasers/credit-wallet/",
                type: 'GET',
                data:'amount='+amount+'&userId='+userId,
                success: function (response) {
					hideSpinner();
					location.reload();
                    console.log('successful');
                }
            });
 
 
	 });
	
	
    $(document).ready(function() {
        $.ajax({
            url: "/l/get-referral-code/{{$purchaserProfile->id}}",
            headers: {'X-CSRF-TOKEN': $('input[name="_token"]').val()},
            type: 'GET',
            success: function (response) {
                $('input[name="referral_code"]').val(response);
            },
            error: function(response){
                console.log('error');
            }
        });
    });

    function updateRC(purchaserId){
        var code = $('input[name="referral_code"]').val();
        if(code.trim() != ''){

            $.ajax({
                url: "/l/update-referral-code/"+purchaserId+"/"+code,
                headers: {'X-CSRF-TOKEN': $('input[name="_token"]').val()},
                type: 'POST',
                success: function (response) {
                    console.log('successful');
                },
                error: function(response){
                    console.log('error');
                }
            });
        }
    }

    function updateDobValue(){
        $('#DoBPurchaser').val($('#datepickerPurchaser').val());
    }

    $(document).ready(function () {
        $(function () {
            $("#datepickerPurchaser").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: "dd/mm/yy",
                showAnim: "slideDown",
                yearRange: "0:+10"
            });
        });
    });
    
    
    
	 

</script>
