<header class="header">
    <div class="container">
        <div class="headerContainer">
            <a href="index.html" class="themeLogo"></a>
            <a href="#" class="xsNav"><span class=""><i class="fa fa-navicon"></i></span></a>
            <div class="collapseBox">
                <a href="I_am_Carer_page.html" class="carerSelf">i am a carer</a>
                <div class="headerNav_container">
                    <div class="headerNav">
                        <a href="SearchPage.html" class="  headerNav__link">book a carer</a>
                        <a href="About_Us.html" class=" headerNav__link">about us</a>
                    </div>
                    <div class="headerSocial">
                        <a href="https://www.facebook.com/HomeCareManchester/" class="centeredLink headerSocial__link headerSocial__link--facebook">
                            <i class="fa fa-facebook"></i>
                        </a>
                        <a href="https://twitter.com/holmcare" class="centeredLink headerSocial__link headerSocial__link--twitter">
                            <i class="fa fa-twitter"></i>
                        </a>
                    </div>
                    <div class="dropdownUser">
                        <a href="#" class="registeredCarer">
                            <div class="profilePhoto registeredCarer__img">
                                <img src="./public/dist/img/profile8.jpg" alt="">
                            </div>
                            <h2 class="profileName">
                                Bob M.
                                <span class="registeredCarer__type">
                  <i class="fa fa-exchange" aria-hidden="true"></i>
                  service user

                </span>
                            </h2>
                            <span class="registeredCarer__ico">
                <i class="fa fa-sign-out" aria-hidden="true"></i>
              </span>
                        </a>
                        <div class="dropdownUser__list">
                            <a  href="Service_User_Private_profile_page.html" class="dropdownUser__item">
                                <div class="profilePhoto dropdownUser__img">
                                    <img src="./public/dist/img/profile5.png" alt="">
                                </div>
                                <h2 class="profileName">
                                    SARA Z.
                                </h2>
                                <span class="dropdownUser__ico">
                  <i class="fa fa-arrow-right" aria-hidden="true"></i>
                </span>
                            </a>

                            <div class="dropdownLogout">
                                <a href="index.html" class="dropdownLogout__item">
                                    logout
                                </a>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</header>
