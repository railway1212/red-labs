<header class="header">
    <div class="container">
        <div class="headerContainer">
            <a href="index.html" class="themeLogo"></a>
            <a href="#" class="xsNav"><span class=""><i class="fa fa-navicon"></i></span></a>
            <div class="collapseBox">
                <a href="I_am_Carer_page.html" class="carerSelf">i am a carer</a>
                <div class="headerNav_container">
                    <div class="headerNav">
                        <a href="SearchPage.html" class="  headerNav__link">find a carer</a>
                        <a href="About_Us.html" class=" headerNav__link">about us</a>
                    </div>
                    <div class="headerSocial">
                        <a href="https://www.facebook.com/HomeCareManchester/"
                           class="centeredLink headerSocial__link headerSocial__link--facebook">
                            <i class="fa fa-facebook"></i>
                        </a>
                        <a href="https://twitter.com/holmcare"
                           class="centeredLink headerSocial__link headerSocial__link--twitter">
                            <i class="fa fa-twitter"></i>
                        </a>
                    </div>
                    <div class="loginBox">
                        <a href="LoginWindow.html" class=" centeredLink loginBox__link">
                            Login
                        </a>
                        <a href="Signup_P_step1.html" class=" centeredLink loginBox__link loginBox__link--active">
                            Sign up
                        </a>
                    </div>
                </div>
            </div>


        </div>
    </div>
</header>
