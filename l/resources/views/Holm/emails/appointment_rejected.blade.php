<html lang="en">
<head>
    <meta content="text/html; charset=utf-8" http-equiv="Content-Type">
    <meta name="viewport" content="width=device-width">
    <title>
        Classical
    </title>
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"
          integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,900|Open+Sans:400,600,700|Roboto:300,400,400i,500,700"
          rel="stylesheet">
    <style type="text/css">

        a:hover {
            text-decoration: none !important;
        }

        a {
            color: #71bc37;
            text-decoration: none;
        }

        @media screen and (max-width: 768px) {

            .logo {

                float: none;
                display: block;
            }
        }

        .title {
            padding: 15px;
        }
    </style>
</head>


<body style="margin: 0; background: #fff; font-size:18px">

<table cellpadding="0" cellspacing="0" border="0" align="center"
       style="margin-top: 30px;
      border-collapse: collapse;
      -webkit-box-shadow: 0px 0px 101px 0px rgba(31, 31, 33, 0.15);
      box-shadow: 0px 0px 101px 0px rgba(31, 31, 33, 0.15); width: 100%; max-width: 940px;"
       bgcolor="#ffffff"
       class="container">
    <tr>
        <td valign="top" align="left" bgcolor="#ffffff" style="padding-bottom: 40px;">
            <table cellpadding="0" cellspacing="0" border="0"
                   style=" border-collapse: collapse;background-size: 100%;   margin: 0; padding: 10px 30px;"
                   width="100%" class="content">
                <tr>
                    <td style="padding-left: 40px;" align="left" valign="middle">
                        <img src="{{asset('public/img/logo.png')}}" alt="" class="logo" style="width: 120px; float: left; ">

                    </td>
                    <td style="padding-left: 30px;">
                        <img src="{{asset('public/img/l4.png')}}" alt="" class="" style="width:  100% ; float: right; ">
                    </td>
                </tr>

            </table>
        </td>
    </tr>
    <tr>
        <td valign="top" align="left" bgcolor="#ffffff" style="font-family: 'Roboto', sans-serif;">
            <table cellpadding="0" cellspacing="0" border="0"
                   style="border-collapse: collapse; background-size: 100%;   margin: 0; " width="100%"
                   class="t-content">
                <tr>
                    <td style="padding: 30px 40px; color:#272c2b;background: #f9f9f9; " valign="top" class="">
                        <h1  style="display: inline-block;font-family: 'Lato', sans-serif;  font-size: 16px;margin-bottom: 20px; font-weight: 700;  color: #272c2b;  text-transform: uppercase;">
                            Dear {{$user_like_name}}</h1>
                        <p style=" text-align: justify; font-weight: 300; margin: 10px 0;">
                            @if($sendTo == 'carer')
                                @if($whoDisputed == 'purchaser')
                                    <a href="{{route('ServiceUserProfilePublic',['serviceUserProfile'=>$booking->service_user_id])}}?referUserProfilePublic={{$booking->service_user_id}}"
                                       style="color: #6178fc; font-weight: 900;">{{$serviceUser->full_name}}</a>
                                    is disputing the <a
                                            href="{{route('viewBookingDetails',[$booking->id])}}?refer={{$booking->id}}">appointment</a>
                                    on  {{$appointment->formatted_date_start}} at {{$appointment->formatted_time_from}}.
                                    We will be in contact with you shortly to discuss how to resolve any issues.
                                    <br/></p>
                        @elseif($whoDisputed == 'carer')
                            We have received your dispute request regarding the <a
                                    href="{{route('viewBookingDetails',[$booking->id])}}?refer={{$booking->id}}">appointment</a>
                            on {{$appointment->formatted_date_start}} at {{$appointment->formatted_time_from}}.

                            We are sorry to hear about this, and will be contacting your shortly to understand what has
                            happened.
                            <br/></p>
                        @endif
                            @elseif($sendTo == 'purchaser')
                            @if($whoDisputed == 'purchaser')
                                We have received your dispute request regarding the <a
                                        href="{{route('viewBookingDetails',[$booking->id])}}?refer={{$booking->id}}">appointment</a>
                                on {{$appointment->formatted_date_start}} at {{$appointment->formatted_time_from}}.

                                We are sorry to hear about this, and will be contacting your shortly to understand what
                                has happened.
                                <br/></p>
                            @elseif($whoDisputed == 'carer')
                                <a href="{{route('carerPublicProfile',['carerPublicProfile'=>$booking->carer_id])}}"
                                   style="color: #6178fc; font-weight: 900;">{{$carer->full_name}}</a>
                                is disputing the <a
                                        href="{{route('viewBookingDetails',[$booking->id])}}?refer={{$booking->id}}">appointment</a>
                                on  {{$appointment->formatted_date_start}} at {{$appointment->formatted_time_from}}.
                                We will be in contact with you shortly to discuss how to resolve any issues.
                                <br/></p>
                            @endif
                            @else
                            @if($whoDisputed->user_type_id == 3)
                                <a href="{{route('carerPublicProfile',['user_id'=>$booking->carer_id])}}"
                                   style="color: #6178fc; font-weight: 900;">{{$carer->full_name}}</a>
                                @else
                                <a href="{{route('ServiceUserProfilePublic',['serviceUsersProfile'=>$booking->service_user_id])}}"
                                   style="color: #6178fc; font-weight: 900;">{{$purchaser->full_name}}</a>
                                @endif
                            is disputing an appointment in <a
                                    href="{{route('viewBookingDetails',['booking' => $booking->id])}}?refer={{$booking->id}}">booking</a>
                            on {{$appointment->formatted_date_start}} at {{$appointment->formatted_time_from}}
                            using
                            @if($whoDisputed->user_type_id == 3)
                                <a href="{{route('ServiceUserProfilePublic',['serviceUsersProfile'=>$booking->service_user_id])}}"
                                   style="color: #6178fc; font-weight: 900;">{{$purchaser->full_name}}</a>
                            @else
                                <a href="{{route('carerPublicProfile',['user_id'=>$booking->carer_id])}}"
                                   style="color: #6178fc; font-weight: 900;">{{$carer->full_name}}</a>
                            @endif

                            . See <a href="{{route('DisputePayouts')}}"> here </a> for more details
                            @endif
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td valign="top" align="left" bgcolor="#ffffff" style="font-family: 'Roboto', sans-serif;">
            <table cellpadding="0" cellspacing="0" border="0" style="    margin: 0; " width="100%" class="t-content">

                <tr>
                    <td style="color:#272c2b;padding: 30px 40px; background: #fff; " valign="top" class="">
                        <p style="
                   text-transform: uppercase;
                   font-size: 14px;
                 margin-bottom:0;">
                            best wishes <br />
                            The Holm Team
                        </p>
                        <a href="{{route('mainHomePage')}}" class=""
                           style="
                    color: #373c4d;
                    text-transform: uppercase;
                    font-weight: 700;
                    text-decoration: none;">
                            Holm.care
                        </a>
                    </td>

                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td valign="top" align="left" bgcolor="#ffffff" style="font-family: 'Roboto', sans-serif;">
            <table cellpadding="0" cellspacing="0" border="0" style="    margin: 0; " width="100%" class="t-content">

                <tr>
                    <td style="padding:  0 40px 30px 40px; background: #fff; " valign="top" class="">
                        <p style="max-width: 600px;font-size: 15px; color: #373f3e;line-height: 1.6;">
                            If you do not wish to receive promotional emails from Holm, <a href="{{route('unsubscribe',['id'=>Auth::user()->id])}}" class="">unsubscribe</a> here.
                        </p>
                        <p style="max-width: 600px;font-size: 15px; color: #373f3e;line-height: 1.6;"> You will continue to receive all other emails.</p>
                    </td>
                    <td style="color:#272c2b;padding: 30px 40px; background: #fff; " valign="top" class="">
                        <ul style="float: right;">
                            <li style="list-style: none; display: inline-block;">
                                <a href="https://www.facebook.com/HomeCareManchester/"
                                   style="margin-left: 10px; color: #a5a7af; ">
                                    <img src="{{asset('public/img/s1.png')}}" alt="">
                                </a>
                            </li>
                            <li style="list-style: none;display: inline-block;">
                                <a href="https://twitter.com/holmcare" style="margin-left: 10px; color: #a5a7af;">
                                    <img src="{{asset('public/img/s2.png')}}" alt="">
                                </a>
                            </li>
                            <li style="list-style: none;display: inline-block;">
                                <a href="https://plus.google.com/communities/102900900688938220709"
                                   style="margin-left: 10px; color: #a5a7af;">
                                    <img src="{{asset('public/img/s3.png')}}" alt="">
                                </a>
                            </li>
                        </ul>
                    </td>
                </tr>
            </table>
        </td>
    </tr>


</table>


</body>
</html>
