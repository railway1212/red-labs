<?php
use App\Http\Controllers\Controller;
$footerData =  Controller::getFooterData();
$copyright = $footerData['copyright'];
$footerAddress = $footerData['footerAddress'];
$facebookLink = $footerData['facebookLink'];
$twitterLink = $footerData['twitterLink'];
$googleLink = $footerData['googleLink'];
?>


<footer class="footer">
    <div class="container">
        <div class="footerNav">
            <a href="/aboutholm/" class="footerNav__item">
                about us
            </a>
            <a href="{{route('ContactPage')}}" class="footerNav__item">
                contact
            </a>
            <a href="{{route('FaqPage')}}" class="footerNav__item">
                Frequently Asked Questions
            </a>
            <a href="{{route('BlogPage')}}" class="footerNav__item">
                blog
            </a>
        </div>
        <div class="footerContainer">
            <a href="{{route('mainHomePage')}}" class=" themeLogo themeLogo--dark">

            </a>
            <div class="footer-center-box">
                <a href="{{route('homeCareJobs')}}" class="carerSelf">
                    i am a carer
                </a>
                @if (!Auth::check())
                    @include(config('settings.frontTheme').'.includes.loginLogoutOnPages')
                @endif
            </div>



            <div class="payment">
                <a href="" class="payment__item">
                    <img src="/l/public/img/pay1.png" alt="Visa">
                </a>
                <a href="" class="payment__item">
                    <img src="/l/public/img/pay3.png" alt="Master Card">
                </a>
            </div>
        </div>

        <div class="footerExtra">


            <p class="copyright">
                {{$copyright}}
            </p>


            <div class="footerExtra__terms">
                <a href="{{route('TermsPage')}}" class="">
                    Terms and Conditions
                </a >
                <a href="{{route('privacy_policy')}}" class="">
                    privacy policy
                </a >
            </div>



            <div class="footerSocial">
                <a href="https://www.facebook.com/HomeCareManchester/" class="footerSocial__item">
                    <i class="fa fa-facebook" aria-hidden="true"></i>
                </a>
                <a href="{{$twitterLink}}" class="footerSocial__item">
                    <i class="fa fa-twitter" aria-hidden="true"></i>
                </a>
                <a href="{{$googleLink}}" class="footerSocial__item">
                    <i class="fa fa-google-plus" aria-hidden="true"></i>
                </a>
            </div>
        </div>
        <br>
        <p class="copyright" style="text-align: center; color: black">
           {{$footerAddress}}
        </p>
        
         <!----subscription button-->
        <div class="fixed_bottom_container">	
			@if(Request::path() === 'about' || Request::path() === '/')

			<div class="close_container">
			  <span aria-hidden="true">&times;</span>
			  <button type="button" class="btn btn-info btn-lg purchaserSubscription">
				  Receive more information on how to find great care
			  </button>
			</div>
			  
			@elseif(Request::path() === 'welcome-carer')  
			<div class="close_container">
			<span aria-hidden="true">&times;</span>	
			  <button type="button" class="btn btn-info btn-lg carerSubscription">
				  Receive more useful information for Carers
			  </button>
			</div>  
				
			@endif
          </div>
        <!----subscription button-->
        
        
        
    </div>
</footer>



<div class="cookie-box" id='id_cookie' style="display:none;">
    <div class="container">
        <div class="cookie">
            <p class="cookie__text">
                This site uses cookies. By continuing to browse the site, you are agreeing to our <a href="{{route('privacy_policy')}}">Use of Cookies.</a>
            </p>
            <div class="cookie__group">
                <button class="cookie-btn">
                    accept
                </button>
                <button class="cookie-btn cookie-btn--refuse">
                    refuse
                </button>
            </div>
        </div>
    </div>
</div>

<!---Add Subscriptin button for carer and purchase-->





@if(Request::path() === 'about' || Request::path() === '/')
<div class="modal fade subscription" id="purchaserSubscription" role="dialog">
<div class="modal-dialog">
  <div class="modal-content">
	<div class="modal-header text-center">
	  <button type="button" class="close" data-dismiss="modal"><i class="fa fa-times"></i></button>
	  <h4 class="modal-title">I'd like more information on finding great care</h4>
	</div>
	<div class="modal-body text-center">
	  <p> Please enter your email address and press subscribe.</p>
	  
	  <div class="mailchimp_list_form">
		<!-- Begin MailChimp Signup Form -->
			<link href="//cdn-images.mailchimp.com/embedcode/classic-10_7.css" rel="stylesheet" type="text/css">
			<style type="text/css">
				#mc_embed_signup{background:#fff; clear:left; font:14px Helvetica,Arial,sans-serif; }
				/* Add your own MailChimp form style overrides in your site stylesheet or in this style block.
				   We recommend moving this block and the preceding CSS link to the HEAD of your HTML file. */
			</style>
			<div id="mc_embed_signup">
			<form action="https://care.us18.list-manage.com/subscribe/post?u=8366f873b486d2332dfed8f9c&amp;id=7fbc1203d8" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
				<div id="mc_embed_signup_scroll">
					
					<div class="mc-field-container">
					
					<div class="mc-field-group">
						<label for="mce-EMAIL">Email Address  <span class="asterisk">*</span>
					</label>
						<input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL">
					</div>
					<div id="mce-responses" class="clear">
						<div class="response" id="mce-error-response" style="display:none"></div>
						<div class="response" id="mce-success-response" style="display:none"></div>
					</div>    
				
					</div>

					<div style="position: absolute; left: -5000px;" aria-hidden="true">
						<input type="text" name="b_8366f873b486d2332dfed8f9c_7fbc1203d8" tabindex="-1" value="">
					</div>
					<div class="sunscribe_button_container">
						<input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="subscribe_button">
					</div>
					
					<div class="clear"></div>
					
				</div>
			</form>
			</div>
			<script type='text/javascript' src='//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js'></script><script type='text/javascript'>(function($) {window.fnames = new Array(); window.ftypes = new Array();fnames[0]='EMAIL';ftypes[0]='email';}(jQuery));var $mcj = jQuery.noConflict(true);</script>
		<!--End mc_embed_signup-->
	  </div>
	</div>
  </div>
  
</div>
</div>
 
@endif  

@if(Request::path() === 'welcome-carer')   
<div class="modal fade subscription" id="carerSubscription" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header text-center">
          <button type="button" class="close" data-dismiss="modal"><i class="fa fa-times"></i></button>
          <h4 class="modal-title"> Carer Subscription</h4>
        </div>
        <div class="modal-body text-center">
            <p>Please enter your email address and press subscribe</p>
            <div class="mailchimp_list_form">
				<!-- Begin MailChimp Signup Form -->
				<link href="//cdn-images.mailchimp.com/embedcode/classic-10_7.css" rel="stylesheet" type="text/css">
				<style type="text/css">
				#mc_embed_signup{background:#fff; clear:left; font:14px Helvetica,Arial,sans-serif; }
				/* Add your own MailChimp form style overrides in your site stylesheet or in this style block.
				We recommend moving this block and the preceding CSS link to the HEAD of your HTML file. */
				</style>
					<div id="mc_embed_signup">
					<form action="https://care.us18.list-manage.com/subscribe/post?u=8366f873b486d2332dfed8f9c&amp;id=fd65d64ef3" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
						<div id="mc_embed_signup_scroll">
						
						  <div class="mc-field-container">	
							<div class="mc-field-group">
							<label for="mce-EMAIL">Email Address  <span class="asterisk">*</span>
							</label>
							<input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL">
							</div>
							<div id="mce-responses" class="clear">
							<div class="response" id="mce-error-response" style="display:none"></div>
							<div class="response" id="mce-success-response" style="display:none"></div>
							</div>    
						  </div>
						
							<div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_8366f873b486d2332dfed8f9c_fd65d64ef3" tabindex="-1" value=""></div>
							<div class="sunscribe_button_container">
								<input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="subscribe_button">
							</div>
							
						  <div class="clear"></div>
					</div>
					</form>
					</div>
				<script type='text/javascript' src='//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js'></script><script type='text/javascript'>(function($) {window.fnames = new Array(); window.ftypes = new Array();fnames[0]='EMAIL';ftypes[0]='email';}(jQuery));var $mcj = jQuery.noConflict(true);</script>
				<!--End mc_embed_signup-->
			</div>	
        </div>
      </div>
      
    </div>
  </div>
@endif

<!---Add Subscriptin button for carer and purchase-->







<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/masonry/4.2.0/masonry.pkgd.min.js"></script>
<script src="{{asset('public/js/jquery-ui.min.js')}}"></script>
<script src="{{asset('public/js/owl.carousel.min.js')}}" defer ></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDJaLv-6bVXViUGJ_e_-nR5RZlt9GUuC4M&libraries=places"></script>
<script src="{{asset('public/js/jquery.autocomplete.js')}}"></script>
<script src="{{asset('public/js/exif.js')}}"></script>
<script src="{{asset('public/js/main.min.js')}}"></script>
<script src="{{asset('public/js/search.js')}}"></script>

<!-- Facebook Pixel Code -->
<script>
    !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
        n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
        n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
        t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
        document,'script','https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '187270565198607'); // Insert your pixel ID here.
    
    fbq('track', 'AddPaymentInfo');
    fbq('track', 'Purchase', {value: '0.00', currency: 'USD'});
    fbq('track', 'Lead');
    fbq('track', 'CompleteRegistration');
    fbq('track', 'Search');
</script>

<noscript><img height="1" width="1" style="display:none"
               src="https://www.facebook.com/tr?id=187270565198607&ev=PageView&noscript=1"
    /></noscript>
<!-- DO NOT MODIFY -->
<!-- End Facebook Pixel Code -->
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-92065832-1"></script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag()
    {dataLayer.push(arguments);}
    gtag('js', new Date());
    gtag('config', 'UA-92065832-1');
</script>

<script>
$('.close_container > span').click(function(){
	$(this).parent().css('display','none');
});

$(document).on('click', '.purchaserSubscription', function () {
    $('.mce_inline_error').html('');
	$('#mce-EMAIL').removeClass('mce_inline_error');
	$('#mce-EMAIL').val('');
	$('#mce-error-response').html('');
	$('#mce-success-response').html('');
	$('#purchaserSubscription').modal('show');
});

$(document).on('click', '.carerSubscription', function () {
	$('.mce_inline_error').html('');
	$('#mce-EMAIL').removeClass('mce_inline_error');
	$('#mce-EMAIL').val('');
	$('#mce-error-response').html('');
	$('#mce-success-response').html('');
	$('#carerSubscription').modal('show');	
});

</script>
