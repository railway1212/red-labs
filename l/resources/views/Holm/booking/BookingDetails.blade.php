<section class="mainSection">
    <div class="container">
        <div class="breadcrumbs">
            <a href="/" class="breadcrumbs__item">
                Home
            </a>
            @if(Auth::user()->isCarer()||Auth::user()->isPurchaser())
                <span class="breadcrumbs__arrow">></span>
                <a href="@if(Auth::user()->isCarer()){{'/l/carer-settings/booking'}}@else{{route('purchaserBookingStatus')}}@endif" class="breadcrumbs__item">
                    My Bookings
                </a>
            @else
                <span class="breadcrumbs__arrow">></span>
                <a href="'/l/serviceUser-settings/'{{$booking->bookingServiceUser()->first()->id}}" class="breadcrumbs__item">
                    My profile 
                </a>
                <span class="breadcrumbs__arrow">></span>
                <p class="breadcrumbs__item">
                    {{$booking->bookingServiceUser()->first()->short_full_name}}
                </p>
                <span class="breadcrumbs__arrow">></span>
                <a href="/l/serviceUser-settings/booking/{{$booking->bookingServiceUser()->first()->id}}" class="breadcrumbs__item">
                    My Bookings
                </a>
            @endif

            <span class="breadcrumbs__arrow">></span>
            <p class="breadcrumbs__item">
                Booking
            </p>
        </div>


    </div>



    <div class="bookingRow">
        <div class="container">
            <div class="orderInfo">
                @if($user->user_type_id == 1 || $user->user_type_id == 4)
                    <a href="/l{{$booking->bookingCarer()->first()->profile_link}}" class="profilePhoto orderInfo__photo">
                        <img src="{{asset('public/img/profile_photos/'.$booking->bookingCarer()->first()->id.'.png')}}" onerror="this.src='/l/public/img/no_photo.png'"  alt="">
                    </a>
                    <div class="orderInfo__item orderInfo__item--rightPadding">
                        <h2 class="ordinaryTitle">
                            <span class="ordinaryTitle__text ordinaryTitle__text--bigger"><a href="{{$booking->bookingCarer()->first()->profile_link}}">{{$booking->bookingCarer()->first()->short_full_name}}</a></span>
                        </h2>
                        <div class="viewProfile">
                            <a href="/l{{$booking->bookingCarer()->first()->profile_link}}" class="viewProfile__item centeredLink">
                                view profile
                            </a>
                        </div>
                    </div>
                    <div class="orderInfo__separate"></div>
                    <div class="orderInfo__item ">
                        <div class="orderOptions">
                            <h2 class="ordinaryTitle">
                                <span class="ordinaryTitle__text ordinaryTitle__text--bigger">DATE</span>
                            </h2>
                            <span class="orderOptions__value">{{\Carbon\Carbon::parse($booking->date_from)->toFormattedDateString()}} - {{\Carbon\Carbon::parse($booking->date_to)->toFormattedDateString()}}</span>
                        </div>
                    </div>
                        <div class="orderInfo__map">
                            <div id="map" style="width: 100%;height: 100%;"></div>
                        </div>
                </div>
            @else
                @if(!$booking->bookingServiceUser->isBlocked())
                    <a href="/l{{$booking->bookingServiceUser()->first()->profile_link}}"
                       class="profilePhoto orderInfo__photo">
                        <img src="{{asset('public/img/service_user_profile_photos/'.$booking->bookingServiceUser()->first()->id.'.png')}}"
                             onerror="this.src='/l/public/img/no_photo.png'" alt="">
                    </a>
                @else
                    <img class="profilePhoto orderInfo__photo"
                         src="{{asset('public/img/service_user_profile_photos/'.$booking->bookingServiceUser()->first()->id.'.png')}}"
                         onerror="this.src='/l/public/img/no_photo.png'" alt="">
                @endif

                <div class="orderInfo__item orderInfo__item--rightPadding">
                    <h2 class="ordinaryTitle">
                        <span class="ordinaryTitle__text ordinaryTitle__text--bigger">
                            @if(!$booking->bookingServiceUser->isBlocked())
                                <a href="/l{{$booking->bookingServiceUser()->first()->profile_link}}">{{$booking->bookingServiceUser()->first()->short_full_name}}</a>
                            @else
                                {{$booking->bookingServiceUser()->first()->short_full_name}}
                            @endif
                            </span>
                    </h2>
                    <div class="viewProfile">
                        @if(!$booking->bookingServiceUser->isBlocked())
                            <a href="/l{{$booking->bookingServiceUser()->first()->profile_link}}"
                               class="viewProfile__item centeredLink">
                                view profile
                            </a>
                        @else
                            <div class="viewProfile__item centeredLink">
                                view profile
                            </div>
                        @endif

                    </div>
                </div>
                <div class="orderInfo__separate"></div>
                <div class="orderInfo__item ">
                    <div class="orderOptions">
                        <h2 class="ordinaryTitle">
                            <span class="ordinaryTitle__text ordinaryTitle__text--bigger">DATE</span>
                        </h2>
                        <span class="orderOptions__value">{{\Carbon\Carbon::parse($booking->date_from)->toFormattedDateString()}} - {{\Carbon\Carbon::parse($booking->date_to)->toFormattedDateString()}}</span>
                    </div>
                    <div class="orderOptions">
                        <h2 class="ordinaryTitle">
                            <span class="ordinaryTitle__text ordinaryTitle__text--bigger">Distance</span>
                        </h2>
                        <span id="distance" class="orderOptions__value">loading..</span>
                    </div>
                    <div class="orderOptions">
                        <h2 class="ordinaryTitle">
                            <span class="ordinaryTitle__text ordinaryTitle__text--bigger">By car</span>
                        </h2>
                        <span id="duration" class="orderOptions__value">loading..</span>
                    </div>
                </div>
                <div class="orderInfo__map">
                    <div id="map" style="width: 100%;height: 100%;"></div>
                </div>
        </div>
            @endif
        </div>
    </div>
    <div class="container">
        <div class="bookingRow bookingRow--moreMargin">
            <div class="bookingRow__content">
                <h2 class="ordinaryTitle">
                    <span class="ordinaryTitle__text ordinaryTitle__text--bigger">Booking summary:</span>
                </h2>
                @foreach($summary as $row)
                    <div class="summary-row">
                        <div class="summary-info">
                            <p>{{$row->s_name}}</p>
                            <span>{{date("M d, Y", strtotime($row->date_start))}} - {{date("M d, Y", strtotime($row->date_end))}} </span>
                        </div>
                        <div class="summary-extra">
                            <p>{{strtoupper(str_replace('single', 'once', $row->name))}}</p>
                        </div>
                        <div class="summary-extra">
                            <p>{{ date("h:i A", strtotime(str_replace('.', ':', $row->time_from))) }} - {{ date("h:i A", strtotime(str_replace('.', ':', $row->time_to))) }}</p>
                        </div>
                    </div>
                @endforeach
            </div>

        </div>
        <div class="bookStatus">
            <p class="">
                Current booking status
            </p>
            <span>
            @if($booking->status_id == 2)
                    BOOKING AWAITING CONFIRMATION
                @elseif($booking->status_id == 4)
                    BOOKING CANCELLED
                @elseif($booking->status_id == 5)
                    BOOKING CONFIRMED
                @elseif($booking->status_id == 7)
                    BOOKING COMPLETED
                @elseif($booking->status_id == 8)
                    DISPUTE PAID
                @endif
        </span>
        </div>
        <div class="innerContainer">
            <div class="orderConfirm">
                <div class="orderConfirm__btns">
                    @if($user->user_type_id !== 4)
                        @if($user->user_type_id == 3)
                            @if($booking->status_id == 2)
                                <div class="roundedBtn">
                                    <button {{!in_array($booking->carer_status_id, [2]) ? 'disabled' : ''}}  data-booking_id = "{{$booking->id}}" data-status = "accept"  class="changeBookingStatus roundedBtn__item roundedBtn__item--smaller roundedBtn__item--accept">
                                        accept
                                    </button>
                                </div>
                                <div class="roundedBtn">
                                    <button  {{!in_array($booking->carer_status_id, [2]) ? 'disabled' : ''}}  data-toggle="modal" data-target="#confirm-reject-booking"  onclick="setBookingId({{$booking->id}})" class="roundedBtn__item roundedBtn__item--smaller roundedBtn__item--reject">
                                        reject
                                    </button>
                                </div>
                                <div class="roundedBtn">
                                    <button  class="roundedBtn__item   roundedBtn__item--alternative" data-id="{{$booking->id}}">
                                        OFFER ALTERNATIVE TIME
                                    </button>
                                </div>
                            @elseif($booking->status_id == 5)
                                <div class="roundedBtn">
                                    <button id="cancelBtn" {{!in_array($booking->carer_status_id, [5]) ? 'disabled' : ''}} data-booking_id = "{{$booking->id}}" data-status = "cancel" data-toggle="modal" data-target="#confirm-cancel" class="roundedBtn__item roundedBtn__item--smalest roundedBtn__item--cancel">
                                        cancel 
                                    </button>
                                </div>
                                {{--<div class="roundedBtn">--}}
                                    {{--<button {{!in_array($booking->carer_status_id, [5]) || $booking->has_active_appointments ? 'disabled' : ''}}  data-booking_id = "{{$booking->id}}" data-status = "completed"  class="changeBookingStatus roundedBtn__item roundedBtn__item--smalest roundedBtn__item--accept">--}}
                                        {{--completed--}}
                                    {{--</button>--}}
                                {{--</div>--}}
                            @endif
                        @else
                            @if($booking->status_id == 2)
                                <div class="roundedBtn">
                                    <button  {{!in_array($booking->purchaser_status_id, [2]) ? 'disabled' : ''}} data-booking_id = "{{$booking->id}}" data-status = "accept"  class="changeBookingStatus roundedBtn__item roundedBtn__item--smaller roundedBtn__item--accept">
                                        accept
                                    </button>
                                </div>
                                <div class="roundedBtn">
                                    <button id="cancelBtn" {{!in_array($booking->purchaser_status_id, [1, 2]) ? 'disabled' : ''}} data-booking_id = "{{$booking->id}}" data-status = "cancel" data-toggle="modal" data-target="#confirm-cancel" class="roundedBtn__item roundedBtn__item--smaller roundedBtn__item--reject">
                                        cancel
                                    </button>
                                </div>
                                <div class="roundedBtn">
                                    <button  class="roundedBtn__item   roundedBtn__item--alternative" data-id="{{$booking->id}}">
                                        OFFER ALTERNATIVE TIME
                                    </button>
                                </div>
                            @elseif($booking->status_id == 5)
                                <div class="roundedBtn">
                                    <button id="cancelBtn" {{!in_array($booking->purchaser_status_id, [5]) ? 'disabled' : ''}} data-booking_id = "{{$booking->id}}" data-status = "cancel" data-toggle="modal" data-target="#confirm-cancel" class=" roundedBtn__item roundedBtn__item--smalest roundedBtn__item--cancel">
                                        cancel
                                    </button>
                                </div>
                                {{--<div class="roundedBtn">--}}
                                    {{--<button {{!in_array($booking->purchaser_status_id, [5]) || $booking->has_active_appointments ? 'disabled' : ''}}  data-booking_id = "{{$booking->id}}" data-status = "completed"  class="changeBookingStatus roundedBtn__item roundedBtn__item--smalest roundedBtn__item--accept">--}}
                                        {{--completed--}}
                                    {{--</button>--}}
                                {{--</div>--}}

                            @endif
                        @endif
                    @endif
                </div>
                <div class="total">
                    <div class="total__item  totalBox">
                        <div class="totalTitle">
                            <p>Total </p>
                            <span>{{$booking->hours}} hours</span>                         
                        </div>
                    
                        @if(Auth::user()->isCarer())
                            <p class="totalPrice">£{{number_format($booking->amount_for_carer,2,'.',' ')}}</p>
                        @else
                            <p class="totalPrice">£{{number_format($booking->amount_for_purchaser,2,'.',' ')}}</p>
                        @endif
                    </div>
                </div>

            </div>
        </div>

        {{--<div class="bookConfirm">--}}
            {{--<p class="bookConfirm__info ">--}}
                {{--Please      <a href="{{route('ContactPage')}}" > Contact us   </a>--}}
                {{--if there is a problem with the booking--}}

            {{--</p>--}}
        {{--</div>--}}

        @php($i = 1)
        @foreach($appointments as $appointment)
      	 <?php
				$disputePayouts=\App\DisputePayout::where('appointment_id',$appointment->id)->where('status','!=','pending')->count();
		 ?>
      
      
            @if(in_array($appointment->status_id, [1, 2, 3]))
                @if($appointment->is_past)
                    @php($class = 'appointment-status--progress')
                @else
                    @php($class = 'appointment-status--coming')
                @endif
            @else
                @php($class = 'appointment-status--completed')
            @endif
            <div class="app-item" id="{{$i}}">
                <div class="appointment-head">
                    <span class="app-number">#{{$i}}</span>
                    <h2 class="appointment-status {{$class}}">
                        @if($appointment->status_id == 4) completed
						@elseif($disputePayouts) completed
                        @elseif($appointment->status_id == 5) cancelled
                        @elseif($appointment->status_id == 3) in dispute
                        @elseif($appointment->status_id == 6) Dispute Paid
                        @elseif(in_array($appointment->status_id, [1, 2]))
                            {{$appointment->is_past ? 'in progress' : 'Upcoming'}}
                        @endif
                    </h2>
                </div>
                <div class="row">
                    <div class="col-md-9">
                        <div class="appointment-info">
                            <p class="appointment-info__item">
                                <i class="fa fa-calendar"></i>
                                <span>{{$appointment->formatted_date_start}}</span>
                            </p>
                            <p class="appointment-info__item">
                                <i class="fa fa-clock-o"></i>
                                <span>{{$appointment->formatted_time_from}} - {{$appointment->formatted_time_to}}</span>
                            </p>
                            <p class="appointment-info__services">
                                <span>{{$appointment->assistance_types_text}}</span>
                            </p>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="app-btn">
                            @if($user->user_type_id !== 4)
                                @if($appointment->status_id == 4 && Auth::user()->isPurchaser())
                                    <div class="app-btn">
                                        <button {{in_array($appointment->id, $allReviewAppointmentsIds) ? 'disabled' : ''}} onclick="location.replace('{{url('/appointments/'.$appointment->id.'/leave_review')}}')" class="app-btn__item app-btn__item--review">
                                            Please Leave A Review
                                        </button>
                                    </div>


                                @else
                                    @php($field = $user->user_type_id == 1 ? 'purchaser_status_id' : 'carer_status_id')
                                    <button
                                            onclick="setClickedAppointmentId({{$appointment->id}})"
                                            data-toggle="modal"
                                            @if($appointment->cancelable) data-target="#confirm-cancel-appointment"
                                            @else data-target="#confirm-dispute-appointment"
                                            @endif
                                            data-appointment_id="{{$appointment->id}}" {{$booking->status_id != 5 || $appointment->status_id == 5 || !in_array($appointment->{$field}, [1])  || (!$appointment->cancelable && !$appointment->is_past)  ? 'disabled' : ''}}  data-appointment_id = "{{$appointment->id}}" data-status = "<?php if($appointment->cancelable) echo 'cancel'; else echo 'dispute';?>"  class=" @if($appointment->cancelable)app-btn__item--cancel @endif app-btn__item">
                                        @if($appointment->cancelable)
                                            Cancel
										 @elseif($disputePayouts)
											Resolved
                                        @else
                                           Dispute
                                        @endif
                                    </button>
                                    @if(!$appointment->cancelable)
                                    <button
                                            data-appointment_id="{{$appointment->id}}"
                                            data-user = "{{Auth::user()->user_type_id}}"
                                            data-carer_status_id="{{$appointment->carer_status_id}}"
                                            onclick="completeAppointment(this)"
                                            {{$booking->status_id != 5 || !in_array($appointment->{$field}, [1]) || !$appointment->is_past ? 'disabled' : ''}}
                                            class="app-btn__item app-btn__item--complete">
                                        Completed
                                    </button>
                                @endif
                            @endif
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            @php(++$i)
        @endforeach
        <div id="comments" class="comments">
            @if($user->user_type_id !== 4 )
            <div class="comments__forMessage">
                <form method="post" action="{{url('/bookings/'.$booking->id.'/message')}}" onsubmit="return messageLengthCheck(this)">
                    <div class="messageBox">
                        <h2 class="formLabel">
                            <a name="comments"></a>
                            Your Message
                        </h2>
                        <textarea name="message" class="messageBox__item doNotCount"  placeholder="Type your message"></textarea>
                        <span id="two-chars-error" class="help-block" style="display: none"><strong>Please write a longer message before sending.</strong></span>
                        <div class="roundedBtn roundedBtn--center">
                            <button type="submit" class=" roundedBtn__item roundedBtn__item--send
                      roundedBtn__item--smaller">
                                send
                            </button>
                        </div>
                    </div>
                </form>
            </div>
            @endif

            <div class="innerContainer">
                @foreach($bookingMessage as $message)
                    @if($message->type == 'message')
                        <div class="comment">
                            @if($message->sender == 'carer')
                            <a href="Service_user_Public_profile_page.html" class="profilePhoto comment__photo">
                                <img src="{{asset('public/img/profile_photos/'.$booking->bookingCarer()->first()->id.'.png')}}" onerror="this.src='/l/public/img/no_photo.png'" alt="">
                            </a>
                            @elseif($message->sender == 'service_user')
                            <a href="Service_user_Public_profile_page.html" class="profilePhoto comment__photo">
                                <img src="{{asset('public/img/service_user_profile_photos/'.$booking->bookingServiceUser()->first()->id.'.png')}}" onerror="this.src='/l/public/img/no_photo.png'"  alt="">
                            </a>
                            @endif
                            <div class="comment__info">
                                <div class="commentHeader">
                                    <h2 class="profileName">
                                        @if($message->sender == 'carer')
                                            <a href="/l{{$booking->bookingCarer()->first()->profile_link}}">
                                                {{$booking->bookingCarer()->first()->short_full_name}}
                                            </a>
                                        @elseif($message->sender == 'service_user')
                                            <a href="Service_user_Public_profile_page.html">
                                                {{$booking->bookingServiceUser()->first()->short_full_name}}
                                            </a>
                                        @endif
                                    </h2>
                                    <p class="commentHeader__date">
                                        <span>{{Carbon\Carbon::parse($message->created_at)->format('g:i A')}}</span>
                                        <span>{{Carbon\Carbon::parse($message->created_at)->format('d.m.Y')}}</span>
                                    </p>
                                </div>
                                <div class="commentText">
                                    <p>
                                        {{$message->text}}
                                    </p>
                                </div>
                            </div>
                        </div>
                    @elseif($message->type == 'status_change')
                        <div class="bookConfirm bookConfirm--with-time bookConfirm--with-border">
                            <h2 class="bookConfirm__title">
                                <span><i class="fa fa-check"></i></span>
                                @if(strtolower($message->new_status) == 'pending')
                                    booking awaiting confirmation
                                @elseif(strtolower($message->new_status) == 'in_progress')
                                    booking confirmed
                                @elseif(strtolower($message->new_status) == 'completed')
                                    booking completed
                                @elseif(strtolower($message->new_status) == 'canceled')
                                    booking cancelled
                                @endif
                            </h2>
                            <p class="bookConfirm__time">
                                <span>{{Carbon\Carbon::parse($message->created_at)->format('g:i A')}}</span>
                                <span>{{Carbon\Carbon::parse($message->created_at)->format('d.m.Y')}}</span>
                            </p>
                        </div>
                    @endif
                @endforeach
            </div>
        </div>
    </div>
</section>

{{--<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDJaLv-6bVXViUGJ_e_-nR5RZlt9GUuC4M"></script>--}}
<script>
    $(document).ready(function () {
    // Handler for .ready() called.
    @if($place)
        $('html, body').animate({
        scrollTop: $('#comments').offset().top
    });
    @endif
});

    $('.changeAppointmentStatus').click(function () {
        var appointment_id = $(this).attr('data-appointment_id');
        var status = $(this).attr('data-status');
       $.post('/l/appointments/'+appointment_id+'/'+status, function (data) {
            if(data.status == 'success'){
                location.reload();
            }
        }).done(function( msg ) {

        }).fail(function( jqXHR, textStatus ) {
            console.debug( jqXHR );
        });
    });

    function completeBookingIfAppointmentsCompleted(appointment_id){
        $.ajax({
            url : '/l/completeBookingIfAppointmentsCompleted/'+appointment_id,
            type : "get",
            async: false,//for "Leave review" button to load after this request is completed
            success : function(data) {
                if(data.status == 'completed')
                    console.log('booking completed');
            },
            error: function() {
                console.log('Error');
            }
        });
    }

    function DistanceMatrixService() {
      var origin1 = 'England {{$carerProfile->postcode}}';
      var destinationA = 'England {{$serviceUserProfile->postcode}}';

        // console.log({'carerProfile': origin1, 'serviceUserProfile': destinationA})

      var service = new google.maps.DistanceMatrixService();
      service.getDistanceMatrix(
        {
          origins: [origin1],
          destinations: [destinationA],
          travelMode: 'DRIVING',
          unitSystem: google.maps.UnitSystem.IMPERIAL,
        }, callback);

      function callback(response, status) {
          // console.log(response.rows[0].elements[0].status)
        if(response.rows[0].elements[0].status === 'OK'){
          // convert km to miles
          // var convert = parseInt(response.rows[0].elements[0].distance.text)*0.62137;

          // $('#distance').html(convert.toFixed(3) + ' (miles)');
          $('#distance').html(response.rows[0].elements[0].distance.text);
          $('#duration').html(response.rows[0].elements[0].duration.text);
        }
        if(response.rows[0].elements[0].status === 'NOT_FOUND' || response.rows[0].elements[0].status === 'ZERO_RESULTS'){
          $('#distance').html('(not found)');
          $('#duration').html('(not found)');
        }
      }
    }

    var geocoder;
    var map;
    function initialize() {
      geocoder = new google.maps.Geocoder();
      var mapOptions = {
        zoom: 17
      }
      map = new google.maps.Map(document.getElementById('map'), mapOptions);
    }

    function codeAddress() {
                {{--var address = '{{$carerProfile->postcode}}';--}}
      			  var address_postcode = '{{$serviceUserProfile->postcode}}';
				  var addr = ('{{$serviceUserProfile->address_line1}}'!='не указан')?'{{$serviceUserProfile->address_line1}}':'';
		   var address = '{{$serviceUserProfile->town}}'+' '+ addr+' '+address_postcode;	

        {{--      if({{$user->user_type_id}} !== 1 && {{$user->user_type_id}} !== 4){--}}
        {{--        address = '{{$serviceUserProfile->postcode}}';--}}
        // }
			//alert(address);

      geocoder.geocode( { 'address': address}, function(results, status) {
        if (status == 'OK') {
          map.setCenter(results[0].geometry.location);
          var marker = new google.maps.Marker({
              map: map,
              position: results[0].geometry.location
          });
        } else {
          alert('Geocode was not successful for the following reason: ' + status);
        }
      });
    }

    $(document).ready(function(){
        initialize();
        codeAddress();
        if({{$user->user_type_id}} !== 1){
          DistanceMatrixService();
        }
    });
//changes status of
    $('.changeBookingStatus').click(function () {

      showSpinner();
        var booking_id = $(this).attr('data-booking_id');
        var status = $(this).attr('data-status');
        $.post('/l/bookings/'+booking_id+'/'+status, function (data) {
            if(data.status == 'success'){
                location.reload();
            } else {
                showErrorModal({title: 'Error', description: data.message});
            }
            hideSpinner();
        });
    });

    var clickedAppointmentId = 0;
    function setClickedAppointmentId(id){
        clickedAppointmentId = id;
    }

    function leaveReviewForAppointment(){
        location.replace('/l/appointments/'+clickedAppointmentId+'/leave_review');
    }

    function confirmCancelAppointment(){
     
        showSpinner();
        $.post('/l/appointments/'+clickedAppointmentId+'/cancel', function (data) {
            if(data.status == 'success'){
                location.reload();
            } else {
                showErrorModal({title: 'Error', description: data.message});
            }
            hideSpinner();
        });
    }

    var shouldReload = true;

    function completeAppointment(button) {
        clickedAppointmentId = $(button).attr('data-appointment_id');
        var carerStatus = $(button).attr('data-carer_status_id');
        var currentUserIsCarer = $(button).attr('data-user');
        if((carerStatus == 2) && (currentUserIsCarer != 3)){
            console.log('while');
            $('#leave-review-appointment').modal('show');
            shouldReload = false;
        }
        console.log('after');
        $.post('/l/appointments/'+clickedAppointmentId+'/completed', function (data) {
            if(data.status == 'success'){
                console.log('status is success');
                completeBookingIfAppointmentsCompleted(clickedAppointmentId);
                if(shouldReload) {
                    location.reload();
                } else{
                    shouldReload = true;
                }
            }
        }).done(function( msg ) {

        }).fail(function( jqXHR, textStatus ) {
            console.log( 'ERROR ');
            console.debug()
        });
    }

    function confirmDisputeAppointment(){
        showSpinner();
        //reject был ранее заменен на dispute
        $.post('/l/appointments/'+clickedAppointmentId+'/reject', function (data) {
            if(data.status == 'success'){
                location.reload();
            } else {
                showErrorModal({title: 'Error', description: data.message});
            }
            hideSpinner();
        });
    }

    function confirmCancel(){
     

      showSpinner();
        var booking_id = $('#cancelBtn').attr('data-booking_id');
        var status = $('#cancelBtn').attr('data-status');
        $.post('/l/bookings/'+booking_id+'/'+status, function (data) {
            if(data.status == 'success'){
                location.reload();
            } else {
                showErrorModal({title: 'Error', description: data.message});
            }
            hideSpinner();
        });
    }
          
  function showSpinner() {

      $('body').addClass('overflow-hidden')
      $('body').append(
          '<div class="show-spinner"></div>'
      );
  };

  function hideSpinner() {
      $('body').removeClass('overflow-hidden')
      $( ".show-spinner" ).remove();
  };

    function messageLengthCheck(form) {
        var messageText = $(form).find('textarea[name="message"]').val();
        if(messageText.length < 2){
            $('#two-chars-error').show();
            return false;
        } else{
            $('#two-chars-error').hide();
            return true;
        }
    }

    var bookingId = 0;
    function setBookingId(id){
        bookingId = id;
    }
    function confirmRejectBooking(){
        showSpinner();
        $.post('/l/bookings/'+bookingId+'/reject', function (data) {
            if(data.status == 'success'){
                location.reload();
            } else {
                showErrorModal({title: 'Error', description: data.message});
            }
            hideSpinner();
        });
    }
</script>
