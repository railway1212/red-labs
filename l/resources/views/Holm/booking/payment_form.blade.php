<style>
    .button_disabled {
        background: #d7d7d7 !important;
        color: #f8f4f4;
    }
</style>
<link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css"
      rel="stylesheet">
<section class="mainSection">
    <div class="container">
        <div class="breadcrumbs">
            <a href="/" class="breadcrumbs__item">
                Home
            </a>
            <span class="breadcrumbs__arrow">&gt;</span>
            <a href="/l/serviceUser-settings/booking/{{$serviceUser->id}}" class="breadcrumbs__item">
                Booking Payment
            </a>
        </div>
        <div class="bookPaymentWrap">
            <h2 class="paymentDetails">
                Payment details
            </h2>
            <div class="bookPayment">
                <div>
                    <div class="paySwitch">
                        <a href="Checkout_for_Purchaser__CARD.html" class="paySwitch__item paySwitch__item--active bookPayment__form-switch">
                            card
                        </a>
                        <a href="Checkout_for_Purchaser__BONUS.html" class="paySwitch__item bonusPay-header-switch">Bonus wallet</a>
                    </div>
                    <form action="" class="bookPayment__form bookPayment__form-header">
                        <div class="card-list">
                            @foreach(\App\User::find(Auth::user()->id)->credit_cards as $card)
                            <div class="card-list__item">
                                <input type="radio" class="theme-radio" value="{{$card->id}}" id="radio-payment{{$card->id}}" name="card_id">
                                <label for="radio-payment{{$card->id}}"><span> xxxx xxxxx xxxx {{$card->last_four}}</span></label>
                            </div>
                            @endforeach
                            <div class="card-list__item">
                                <input type="radio" class="theme-radio new_card" id="radio-payment0" name="card_id">
                                <label for="radio-payment0"><span> NEW CARD</span></label>
                            </div>
                        </div>
                        <div class="bookPayment__field carDiv">
                            <h2 class="formLabel">
                                Card Number
                            </h2>
                            <div class="inputWrap">
                                <input type="text" class="formInput" id="cardNumber" onkeypress='return event.charCode >= 48 && event.charCode <= 57' maxlength="19">
                                <span class="bookPayment__ico"><img src="{{asset("public/img/visa.png")}}" alt=""></span>
                                <span style="right: 42px;" class="bookPayment__ico"><img src="{{asset("public/img/mc2.png")}}"
                                                                              alt=""></span>
                            </div>
                        </div>
                        <div class="bookPayment__row bookPayment__row--xs-column carDiv">
                            <div class="bookPayment__halfColumn   bookPayment__halfColumn--xs-full">
                                <h2 class="formLabel">
                                    Valid Until
                                </h2>
                                <div class="bookPayment__row">
                                    <div class="bookPayment__halfColumn">
                                        <div class="formField">
                                            <div class="inputWrap">
                                                <input type="text" id="cardMonth" class="formInput" placeholder="MM" onkeypress='return event.charCode >= 48 && event.charCode <= 57' maxlength="2">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="bookPayment__halfColumn">
                                        <div class="formField">
                                            <div class="inputWrap">
                                                <input type="text" id="cardYear" class="formInput" placeholder="YY" onkeypress='return event.charCode >= 48 && event.charCode <= 57' maxlength="2">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="bookPayment__halfColumn bookPayment__halfColumn--xs-full">
                                <div class="formField">
                                    <h2 class="formLabel">
                                        cvc code
                                    </h2>
                                    <div class="inputWrap">
                                        <input type="text" id="cardCVC" class="formInput" onkeypress='return event.charCode >= 48 && event.charCode <= 57' maxlength="3">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="paymentCheckbox carDiv">
                            <div class="checkBox_item">
                                <input disabled type="checkbox" name="checkbox" class="customCheckbox"
                                       id="buttonPaymentCard1">
                                <label for="buttonPaymentCard1"> save payment details?</label>
                            </div>
                            <input type="hide" name="save_card" class="customCheckbox" id="save_card">
                        </div>
                        <div class="roundedBtn roundedBtn--center">
                            <button disabled href="#"  class="roundedBtn__item roundedBtn__item--confirm"
                                     id="buttonPaymentCard">
                                Confirm Payment
                            </button>
                        </div>
                    </form>
                    <div class="bonusPay-header" style="display: none">
                        <div class="bonusPay">
                            <div class="bonusPay__item">
                                <p>
                                    Bonuses balance
                                </p>
                                <p>
                                    Bonuses balance after payment
                                </p>
                            </div>
                            <div class="bonusPay__item bonusPay__item--border">
                         <span class="bonusPay__label">
                           £{{$user->bonus_balance}}
                         </span>
                                <span class="bonusPay__label">
                           £{{number_format($user->bonus_balance - number_format($booking->price,2,'.',' '),2,'.','')}}
                         </span>
                            </div>

                        </div>
                        <div class="roundedBtn roundedBtn--center">
                            <button class=" roundedBtn__item roundedBtn__item--confirm"
                                    id="buttonPaymentBonuses">
                                Confirm Payment
                            </button>
                        </div>
                    </div>
                </div>
                <div class="bookPayment__total">
                    <div class="paymentTotal">
                        <div class="paymentTotal__item">
                            <p class="paymentTotal__name">

                                Total Hours
                            </p>
                            <p class="paymentTotal__name">

                                Total Price
                            </p>
                        </div>
                        <div class="paymentTotal__separate">

                        </div>
                        <div class="paymentTotal__item">
                            <p class="paymentTotal__value">
                                {{$booking->hours}} Hour{{($booking->hours > 1 ? 's' : '')}}
                            </p>
                            <p class="paymentTotal__value">
                                £{{number_format($booking->price,2,'.',' ')}}
                            </p>
                        </div>

                    </div>
                    <div class="paymentNote">
                        <h2>Please, note!</h2>
                        <p>
                            Payment is taken after booking request confirmed by Carer
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js">    
</script>

<script>
    $('input[type="text"]').attr("readonly", true);
    $('.carDiv').hide();

    $('input[type="radio"]').on("click",function(){
        $('#buttonPaymentCard').attr('disabled',false);
        $('input[type="text"]').attr("readonly", !$('.new_card').is(':checked'));
        $('#buttonPaymentCard1').attr("disabled", !$('.new_card').is(':checked'));
        if(!$('.new_card').is(':checked')){
            $('.carDiv').hide();
        }else{ $('.carDiv').show();}
    });

    $('#buttonPaymentCard1').on('click',function(){
     
        if (this.checked) {
            $("#save_card").val(true);
         
        } else {
            $("#save_card").val(false);
        }
    });

    $('#buttonPaymentBonuses').click(function () {
        showSpinner();
        $.post('{{route('setBookingPaymentMethod', ['booking' => $booking->id])}}', {'payment_method' : 'bonus_wallet'}, function( data ) {
            if(data.status == 'success'){
                $('.bookPaymentWrap').css('border-top','none');
                $('.bookPaymentWrap').html(`
              <div class="thank">
                <h2 class="thank__title">
                  Thank you!
                </h2>
                <span class="successIco">
                  <i class="fa fa-check" aria-hidden="true"></i>
                </span>
                <p class="info-p">
                  Your carer has now received your booking request and will respond shortly. If you have not heard back within 24 hours, please do contact us.
                </p>
              </div>
            `);
            } else {
                showErrorModal({title: 'Payment Error', description: 'We’re sorry, but you do not have enough credits in your bonus wallet.<br>Please use an alternative form of payment.'});
            }
            hideSpinner();
            var padding = ($(document).height()-$('header').outerHeight()-$('section').outerHeight()-$('footer').outerHeight()-20)/2;
            $('.thank').css('paddingTop',padding).css('paddingBottom',padding);
        });
    });

    $('.new_card').on('click',function(){

    });

    $('#buttonPaymentCard').click(function (e) {
        e.preventDefault();
       showSpinner();
        var cardNumber = $('#cardNumber').val();
        var cardMonth = $('#cardMonth').val();
        var cardYear = $('#cardYear').val();
        var cardCVC = $('#cardCVC').val();
        var data = {};
        if(!$('.new_card').is(':checked'))
        data = {
            'card_id': $('input[name="card_id"]').val(),
            'payment_method': 'credit_card'
            };
        else
        data = {
            'payment_method': 'credit_card',
            'save_card': $('#save_card').val(),
            'card_number': cardNumber,
            'card_month': cardMonth,
            'card_year': cardYear,
            'card_cvc': cardCVC};

        $.post('{{route('setBookingPaymentMethod', ['booking' => $booking->id])}}',
            data,
            function( data ) {
            if(data.status == 'success'){
                $('.bookPaymentWrap').css('border-top','none');
                $('.bookPaymentWrap').html(`
              <div class="thank">
                <h2 class="thank__title">
                  Thank you!
                </h2>
                <span class="successIco">
                  <i class="fa fa-check" aria-hidden="true"></i>
                </span>
                <p class="info-p">
                  Your carer has now received your booking request and will respond shortly. If you have not heard back within 24 hours, please do contact us.
                </p>
              </div>
            `);
            } else {
             	//toastr.warning('Please check your payment details');
              showErrorModal({title: 'Payment Error', description: 'Please check your payment details'});
            }
            hideSpinner();
        });
    });
  
  
  // -- SPINNER -------
function showSpinner() {
    $('body').addClass('overflow-hidden')
    $('body').append(
        '<div class="show-spinner"></div>'
    );
};

function hideSpinner() {
    $('body').removeClass('overflow-hidden')
    $( ".show-spinner" ).remove();
};

function showErrorModal(message) {
    $('body').addClass('overflow-hidden')
    $('body').append(
        '<div class="error-popup-container">'+
        '<div class="error-popup">'+
        '<div class="error-popup__ico">'+
        '<span>'+
        '<i class="fa fa-times-circle"></i>'+
        '</span>'+
        '</div>'+
        '<a href="#" class="error-popup__close">'+
        '<i class="fa fa-times"></i>'+
        '</a>'+
        '<div class="error-popup__body">'+
        '<h2>'+
        message.title+
        '</h2>'+
        '<p class="info-p info-p--roboto">'+
        message.description+
        '</p>'+
        '</div>'+
        '</div>'+
        '</div>'
    );
};


</script>
