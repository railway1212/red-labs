<section class="mainSection">
    <div class="container">
        <div class="justifyContainer justifyContainer--smColumn">
            <div class="breadcrumbs">
                <a href="{{route('mainHomePage')}}" class="breadcrumbs__item">
                    Home
                </a>
                <span class="breadcrumbs__arrow">></span>
                <a href="{{route('privacy_policy')}}" class="breadcrumbs__item">
                    Privacy Policy
                </a>


            </div>
        </div>
        <div class="privacy">
            <div class="privacy__title">
                <h2 class="lightTitle lightTitle--bigger">
                    <?php  echo $title; ?>
                </h2>
            </div>
          <?php echo $content; ?>
          
           <!-- <div class="privacy__item">
                <p>
                    Last updated: 18th October 2017
                </p>
                <p>
                    This privacy policy (“Policy”) describes how Holm and its related companies collect, use and share personal data of users of this website, http://holm.care
                </p>
                <p>
                    Please take a moment to read this Policy which explains how we collect, use, disclose and transfer the personal information that you provide to us on our websites or mobile applications, when you visit our site.
                </p>
                <p>
                    By visiting our site and providing us with your personal data, you acknowledge and agree that your personal data may be processed for the purposes identified in this Policy.

                </p>
                <p>
                    You can find out more about Holm and the Terms of Use for using our site at
                    <a href="{{route('TermsPage')}}">terms and condition</a>.
                </p>
            </div>
            <div class="privacy__item">
                <h2>
                    WHO WE ARE & CONTACT INFORMATION
                </h2>
                <p>
                    Holm is a company with registered offices at New Care Ltd, Kemp House, 160 City Road, London, EC1V 2NX.
                </p>
                <p>
                    We welcome your comments about this Policy. If you have any questions about how we use your personal data, please do not hesitate to contact us at:
                </p>
                <p>
                    New Care Ltd, Kemp House, 160 City Road, London, EC1V 2NX , E-mail: info@holm.care or Call: 0161 706 0288
                </p>
            </div>
            <div class="privacy__item">
                <h2>
                    WHAT WE COLLECT
                </h2>
                <p>
                    We obtain information about you in a range of ways:
                </p>
                <p class="privacy__subtitle">
            <span>
              Information You Give Us.
            </span>
                </p>
                <p>
                    We collect your name, email address, phone number and demographic information
                    (such as your gender and occupation) as well as other information you directly
                    give us on our site. In addition, we may also collect personal information from you when you correspond with use (for example, when you send us e-mail communications or letters, or by telephone).
                </p>
                <p class="privacy__subtitle">
            <span>
              Information We Get From Others.
            </span>
                </p>
                <p>
                    We may get information about you from other sources. We may add this to information we get from this site.
                </p>
                <p class="privacy__subtitle">
            <span>
              Information Automatically Collected.
            </span>
                </p>
                <p>
                    We automatically log information about you and your computer.
                    For example, when visiting our site, we log your computer's operating system type,
                    browser type, browser language, the website you visited before browsing to our site, pages you viewed, how long you spent on a page, access times and information about your use of and actions on our site.
                </p>
            </div>
            <div class="privacy__item">
                <h2>
                    COOKIES
                </h2>
                <p>
                    Our site (like most platforms) uses cookies and other similar technology such as ‘web beacons’.
                </p>
                <p class="privacy__subtitle">
            <span>
              What are Cookies?
            </span>
                </p>
                <p>
                    Cookies are text files containing small amounts of information which are downloaded to your computer or mobile device when you visit a website or mobile application. Cookies are then sent back to the originating site on each subsequent visit, or to another site that recognises that cookies.
                    You can find out more information about cookies at www.allaboutcookies.org.
                </p>
                <p>
                    Cookies are widely used in order to make sites work, or to work more efficiently, as well as to provide information to the owners of the platform.
                </p>
                <p>
                    We use cookies to enhance the online experience of our visitors (for example, by remembering your language and/or product preferences) and to better understand how our site is used. Cookies may tell us, for example, whether you have visited our site before or whether you are a new visitor. They can also help to ensure that adverts you see online are more relevant to you and your interests.
                </p>
                <p class="privacy__subtitle">
            <span>
              Types of Cookies
            </span>
                </p>
                <p>
                    There are two broad categories of cookies:
                </p>
                <p class="privacy__subtitle">
            <span>
              First Party Cookies,
            </span>served directly by us to your computer or mobile device.
                    They are used only by us to recognize your computer or mobile device when it
                    revisits our site.
                </p>
                <p class="privacy__subtitle">
            <span>
              Third party cookies
            </span>which are served by a service provider on our site, and can be used
                    by the service provider to recognize your computer or mobile device when it
                    visits other sites. Third party cookies are most commonly used for platform analytics or advertising purposes.

                </p>
                <p>
                    Cookies can remain on your computer or mobile device for different periods of time. Some cookies are 'session cookies', meaning that they exist only while your browser is open. These are deleted automatically once you close your browser. Other cookies are 'permanent' cookies, meaning that they survive after your browser is closed. They can be used by the site to recognise your computer or mobile device when you open your browser and browse the Internet again.

                </p>
                <p class="privacy__subtitle">
           <span>
             What cookies do we use?
           </span>
                </p>
                <p>Our Site uses the following types of cookies:</p>
                <p class="privacy__subtitle">
                    <span>Essential Cookies:</span> These cookies are essential to provide you with services available through our site and to use some of its features, such as access to secure areas. Without these cookies, services you have asked for, like transactional pages and secure login accounts, would not be possible.

                </p>
                <p class="privacy__subtitle">
           <span>
             Analytics Cookies:
           </span> These cookies are used to collect information about how visitors use our site.
                    The information gathered does not identify any individual visitor and is aggregated.
                    It includes the number of visitors to our site, the sites that referred them to our site and the pages that they visited on our site.We use this information to help operate our site more efficiently, to gather broad demographic information and to monitor the level of activity on our site.

                </p>
                <p class="privacy__subtitle">
           <span>
             Performance Cookies:
           </span> These cookies collect information about how visitors use the site,
                    so that we can analyse traffic and understand how our visitors use our sites.
                    We use Google Analytics for this purpose. Google Analytics uses its own cookies.
                    These cookies don’t collect information that identify a visitor. All information these cookies collect is aggregated and therefore anonymous. It is only used to improve how the site works. You can find out more information about Google Analytics cookies here: <a href="https://developers.google.com/analytics/resources/concepts/gaConceptsCookies">https://developers.google.com/analytics/resources/concepts/gaConceptsCookies</a>  You can avoid the use of Google Analytics relating to your use of our site by downloading and installing the browser plugin available via this link: <a href="http://tools.google.com/dlpage/gaoptout?hl=en-GB">http://tools.google.com/dlpage/gaoptout?hl=en-GB</a>

                </p>
                <p class="privacy__subtitle">
                    <span>Targeted and advertising cookies:</span>
                    These cookies track your browsing habits to enable us to show advertising
                    which is more likely to be of interest to you.
                    These cookies use information about your browsing history to group you with other users
                    who have similar interests. Based on that information, and with our permission,
                    third party advertisers can place cookies to enable them to show adverts
                    which we think will be relevant to your interests while you are on third party websites.
                    You can disable cookies which remember your browsing habits and target advertising at you
                    by visiting <a href="http://www.youronlinechoices.com/uk/your-ad-choices">http://www.youronlinechoices.com/uk/your-ad-choices</a>. If you choose to remove targeted or advertising cookies, you will still see adverts but they may not be relevant to you.
                    Even if you do choose to remove cookies by the companies listed at the above link,
                    not all companies that serve online behavioural advertising are included in this list, and so you may still receive some cookies and tailored adverts from companies that are not listed.

                </p>
                <p class="privacy__subtitle">
           <span>
             Functionality Cookies:
           </span> These cookies allow our site to remember choices you make (such as your user name, or the region you are in) and provide enhanced, more personal features. These cookies can also be used to remember changes you have made to text size, fonts and other parts of web pages that you can customize. They may also be used to provide services you have asked for such as watching a video or commenting on a blog. The information these cookies collect may be anonymized and they cannot track your browsing activity on other websites.
                </p>
                <p class="privacy__subtitle">
           <span>
             Social Media Cookies:
           </span> These cookies are used when you share information using a social
                    media sharing button or “like” button on our sites or you link your account or
                    engage with our content on or through a social networking site such as Facebook,
                    Twitter or Google+. The social network will record that you have done this.
                    This information may be linked to targeting/ advertising activities
                </p>
                <p class="privacy__subtitle">
           <span>
             How to control or delete cookies
           </span>
                </p>
                <p>
                    You have the right to choose whether or not to accept cookies and we have
                    explained how you can exercise this right below. However, please note that if you do not
                    accept our cookies, you may experience some inconvenience in your use of our site.
                </p>
                <p>
                    You have the right to choose whether or not to accept cookies and we have explained
                    how you can exercise this right below. However, please note that if you do not accept our cookies, you may experience some inconvenience in your use of our site.
                </p>
                <p class="privacy__subtitle">
                    Further information about cookies, including how to see what cookies have been set on your computer or mobile device and how to manage and delete them, visit www.allaboutcookies.org and www.youronlinechoices.eu
                </p>
                <p class="privacy__subtitle">
           <span>
             Pixel Tags
           </span>
                </p>
                <p>
                    We may also use pixel tags (which are also known as web beacons and clear GIFs) on our site to track the actions of users on our site. Unlike cookies, which are stored on the hard drive of your computer or mobile device by a website, pixel tags are embedded invisibly on web pages. Pixel tags measure the success of our marketing campaigns and compile statistics about usage of the site, so that we can manage our content more effectively. The information we collect using pixel tags is not linked to our users’ personal data.
                </p>
                <p class="privacy__subtitle">
           <span>
             IP Addresses
           </span>
                </p>
                <p>
                    We may collect information about your computer or mobile device, including where
                    available your IP address, operating system, login times and browser type.
                    We use this information to better understand how visitors use our site and for
                    internal reporting purposes. We may anonymise and share this information with
                    advertisers, sponsors and/or other businesses.
                </p>

            </div>
            <div class="privacy__item">
                <h2>
                    USE OF PERSONAL DATA
                </h2>
                <p>
                    We use your personal data for the following purposes:
                </p>
                <ul>
                    <li>
                        to operate, maintain, and improve our sites, products and services;
                    </li>
                    <li>
                        to process and deliver the contest entries and rewards;
                    </li>
                    <li>
                        to respond to comments and questions and provide customer service;
                    </li>
                    <li>
                        to send information including confirmations, invoices, technical notices, updates, security alerts and support and administrative messages;
                    </li>
                    <li>
                        to communicate about promotions, upcoming events, and other news about products and services offered by us and our selected partners;
                    </li>
                    <li>
                        to link or combine user information with other personal data;
                    </li>
                    <li>
                        to protect, investigate, and deter against fraudulent, unauthorized, or illegal activity;
                    </li>
                    <li>
                        to provide and deliver products and services at customer's request.
                    </li>
                </ul>
            </div>
            <div class="privacy__item">
                <h2>
                    SHARING OF PERSONAL DATA

                </h2>
                <p>
                    Please be aware that we share some of your personal information with other users in order to assist communication and improve the services offered. This is not an exhaustive list, but may include details such as names, addresses, health information, types of help needed, qualifications, personal preferences etc.

                </p>
                <p>
                    We do not rent, sell or share personal information about you with other people or nonaffiliated companies outside Holm except under the following circumstances:

                </p>
                <ul>
                    <li>
                        With your consent. For example, you may let us share personal data with others for their own marketing uses. Those uses will be subject to their privacy policies.
                    </li>
                    <li>
                        When we do a business deal, or negotiate a business deal, involving the sale or
                        transfer of all or substantially all of our business or assets.
                        These deals can include any merger, financing, acquisition, or bankruptcy transaction or proceeding.
                    </li>
                    <li>
                        For legal, protection, and safety purposes;
                        <ul>
                            <li>
                                to comply with laws or any regulatory obligation;
                            </li>
                            <li>
                                to respond to lawful requests and legal processes;
                            </li>
                            <li>
                                to protect the rights and property of Holm, our agents, customers and others. This includes enforcing our agreements, policies and Terms of Use.

                            </li>
                            <li>
                                in an emergency. This includes protecting the safety of our employees and agents, our customers, or any person.

                            </li>
                        </ul>
                    </li>
                    <li>
                        With those who need it to do work for us.

                    </li>
                </ul>
                <p>We may also share aggregated and/ or anonymised data with others for their own uses.
                </p>
            </div>
            <div class="privacy__item">
                <h2>
                    INTERNATIONAL TRANSFER OF PERSONAL DATA
                </h2>
                <p>
                    Your personal data may be collected, processed and/or otherwise transferred outside the European Economic Area and may be processed not only in the country in which it was collected but also in other countries, including the United States, where data protection and privacy laws and regulations may not offer the same level of protection as in other parts of the world. By providing your personal data to us, you consent to such transfer, collection and/or processing in the United States of your personal data, which includes the use of cookies as described in this Policy, which includes the use of cookies as described in this Policy.
                </p>
            </div>
            <div class="privacy__item">
                <h2>
                    PROTECTING YOUR INFORMATION
                </h2>
                <p>
                    We want you to feel confident about using our site, and we are committed to
                    protecting the personal information we collect.
                    We limit access to personal information about you to employees who reasonably need
                    access to it, to provide products or services to you or in order to do their jobs.
                    We have appropriate technical and organizational physical, electronic, and procedural
                    safeguards to protect the personal information that you provide to us against unauthorized or unlawful processing and against accidental loss, damage or destruction.

                </p>
            </div>
            <div class="privacy__item">
                <h2>
                    RETENTION PERIOD
                </h2>
                <p>
                    We will retain your personal data for the period necessary to fulfill the purposes outlined in this Policy unless a longer retention period is required or permitted by law.

                </p>
            </div>
            <div class="privacy__item">
                <h2>
                    HOW TO EDIT, DELETE OR ACCESS YOUR PERSONAL DATA

                </h2>
                <p>
                    You have the right to ask for a copy of any personal information that we hold about you in our records, to correct any inaccuracies and to update any out of date information. You can also ask us not to send you direct marketing communications (however please note that we may continue to send you service related, i.e. non-marketing communications, such as email updates). If you wish to exercise any of these rights, or wish to object to our use of your personal information, please write to us at the address given above.
                </p>
            </div>
            <div class="privacy__item">
                <h2>
                    CHANGES TO THIS POLICY
                </h2>
                <p>
                    From time to time we may change this Policy. If we make any changes, we will change the Last Updated date above. If you do not agree to these changes, please do not continue to use our site. If material changes are made to this Policy, we will notify you by email or by placing a prominent notice on the site.
                </p>
            </div>-->

        </div>

    </div>
</section>
