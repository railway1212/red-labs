<section class="mainSection ">

    <div class="container">
        <div class="breadcrumbs">
            <a href="/" class="breadcrumbs__item">
                Home
            </a>
            <span class="breadcrumbs__arrow">&gt;</span>
            <a href="/job" class="breadcrumbs__item">
                Job
            </a>
        </div>
    </div>
    <div class="faqWrap">
        <div class="container">
            <div class="home">
                <div class="home__title">
            <h1 class="ordinaryTitle faq__title">

                Job description for Holm Care CTO

            </h1>
                </div></div>
            <div class="terms__item">

                <span>
                    Are you excited by the opportunity to develop a company’s technological backbone from the ground up?
                    Our healthcare start-up is looking to hire a Chief Technical Officer.
                    Your main task is to solve problems and come up with technical solutions to the many challenges we
                    face in building a product that helps elderly people to find the care they need at home.
                </span>
                <p><b>We offer</b></p>
                <ul style="list-style:disc;padding-left: 40px">
                    <li>A great opportunity to build a profitable international company.</li>
                    <li>The chance to build and work on something truly meaningful that can impact thousands of lives.</li>
                    <li>We know the current website isn't technically challenging, but you will have the opportunity to
                        shape the product, and the technologies of the future in healthcare.</li>
                    <li>Join a dynamic, international and ambitious team</li>
                </ul>
                <p><b>We expect you to</b></p>
                <ul style="list-style:disc;padding-left: 40px">
                    <li>Have previous experience building and running the technical side of an early stage startup</li>
                    <li>Not just be able to code, but understand problems, solve them and communicate with all stakeholders.</li>
                    <li>Coordinate the work of our remote development team.</li>
                    <li>Provide the vision for all technical aspects and technological resources our website and future developments.</li>
                    <li>Be comfortable working in English.</li>
                    <li>Work closely with the non-technical co-founders.</li>
                </ul>
                <p>If you think you're perfect for the position, please email us at <a href="mailto:nik@holm.care">nik@holm.care</a> with your CV and application. We promise to get back to you shortly</p>
            </div>
        </div>
    </div>

</section>
