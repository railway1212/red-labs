<section class="mainSection">
    <div class="container">
        <div class="justifyContainer justifyContainer--smColumn">
            <div class="breadcrumbs">
                <a href="{{route('mainHomePage')}}" class="breadcrumbs__item">
                    Home
                </a>
                <span class="breadcrumbs__arrow">&gt;</span>
                <a href="{{route('ContactPage')}}" class="breadcrumbs__item">
                    Contact Us
                </a>
            </div>
        </div>
        <div class="contactBox">
            <div class="contactBox__item">
                <h2 class="contactBox__title bigTitle">Write to Us</h2>
                <p class="contactBox__info"><?php echo $writeUs; ?></p>
                {{--<form class="contactForm">--}}
                    {!! Form::model(null, ['method'=>'POST','route'=>'ContactSendMail',
                    'id'=>'contactSendMail','class'=>"contactForm"]) !!}
                    <div class="contactForm__column">
                        <h2 class="fieldTitle">name</h2>
                        <div class="contactForm__field">
                            <input type="text" name="name" class="contactForm__item" placeholder="Name">
                            <span class="contactForm__ico"><i class="fa fa-user"></i></span>
                        </div>
                    </div>
                    <div class="contactForm__column">
                        <h2 class="fieldTitle">email adress <span> * </span></h2>
                        <div class="contactForm__field">
                            <input type="email" name="email" class="contactForm__item" required placeholder="Your email">
                            <span class="contactForm__ico"><i class="fa fa-envelope"></i></span>
                        </div>
                    </div>
                    <div class="contactForm__column">
                        <h2 class="fieldTitle">Phone Number</h2>
                        <div class="contactForm__field">
                            <input type="text" name="phone" class="contactForm__item" placeholder="Your phone" maxlength="11">
                            <span class="contactForm__ico"><i class="fa fa-mobile"></i></span>
                        </div>
                    </div>

                    <div class="contactForm__column">
                        <h2 class="fieldTitle">Topic</h2>
                        <div class="contactForm__field">
                            <select type="text" name="topic" class="contactForm__item contactForm__item--select">
                                <option value="Select topic">Select topic</option>
                                <option value="I would like to work for Holm">I would like to work for Holm</option>
                                <option value="I would like more information about Holm">I would like more information about Holm</option>
                                <option value="I need help finding a carer">I need help finding a carer</option>
                                <option value="I need help registering as a care worker">I need help registering as a care worker</option>
                                <option value="I need help registering to buy care">I need help registering to buy care</option>
                                <option value="I need help editing my profile">I need help editing my profile</option>
                                <option value="I have a payment question">I have a payment question</option>
                                <option value="I would like to change a booking">I would like to change a booking</option>
                                <option value="I need help with a recent appointment">I need help with a recent appointment</option>
                                <option value="I would like Holm to come to my town / city">I would like Holm to come to my town / city</option>
                                <option value="My account was blocked">My account was blocked</option>
                                <option value="Something else">Something else</option>
                            </select>
                            <span class="contactForm__ico"><i class="fa fa-file"></i></span>
                        </div>
                    </div>

                    <div class="contactForm__column">
                        <h2 class="fieldTitle">Message</h2>
                        <div class="contactForm__field contactForm__field--area ">
                            <textarea type="text" name="message" class="contactForm__item contactForm__item--area" maxlength="500"></textarea>
                            <span class="contactForm__ico"><i class="fa fa-pencil"></i></span>
                        </div>
                    </div>
                    <div class="roundedBtn contactForm__btn">
                        <button class=" roundedBtn__item roundedBtn__item--contact ">
                            submit
                        </button>
                    </div>
                {!! Form::close() !!}
            </div>
            <div class="contactBox__item">
                <h2 class="contactBox__title bigTitle">Address &amp; contacts</h2>
                <ul class="contactList">
                    <li class="contactList__item">
              <span class="contactList__ico">
               <i class="fa fa-map-marker" aria-hidden="true"></i>
              </span>
                        <span class="contactList__text">
                            <?php echo $address; ?>
                            <!--Holm<br>
                            Federation House <br>
                            2 Federation Street <br>
                            Manchester<br>
                            M4 4BF-->
             
              </span>
                    </li>
                    <li class="contactList__item">
              <span class="contactList__ico">
               <i class="fa fa-mobile" aria-hidden="true"></i>
              </span>
                        <span class="contactList__text">
              <a href="tel: {{$phoneNumber}} "> {{$phoneNumber}}  </a>   @if($contactTime) {{$contactTime}} @endif
              </span>
                    </li>
                    <li class="contactList__item">
              <span class="contactList__ico">
               <i class="fa fa-envelope" aria-hidden="true"></i>
              </span>
                        <span class="contactList__text contactList__text--email">
                <a href="mailto:{{$emailAddress}}">{{$emailAddress}}</a><p></p>
              </span>
                    </li>
                </ul>
                <div class="contactMap">
                    <?php echo $googleMap; ?>
                   <!-- <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2374.0138142151663!2d-2.241986684156271!3d53.48608298000856!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x487bb1c79e93a16b%3A0x915802faaa70a249!2sHolm+Care!5e0!3m2!1sru!2sua!4v1521807097284"
                            width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>-->
                </div>
            </div>
        </div>
    </div>
</section>
