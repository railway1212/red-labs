<link rel="stylesheet" href="{{asset('public/css/tokenfield-typeahead.css')}}">
<script src="{{asset('public/js/typeahead.jquery.min.js')}}"></script>

<section class="searchSection">
    <div class="container">
        <div class="breadcrumbs">
            <a href="/" class="breadcrumbs__item">
                Home
            </a>
            <span class="breadcrumbs__arrow">&gt;</span>
            <a href="/find-a-carer" class="breadcrumbs__item">
                Search results
            </a>

        </div>
        {!! Form::model($carerResult, ['method'=>'GET','route'=>'searchPagePost','id'=>'carerSearchForm']) !!}
        {!! Form::hidden('stage','carerSearch') !!}
        <input type="hidden" name="sort-rating" id="sort-rating" value="0">
        <input type="hidden" name="sort-rating-order" id="sort-rating-order" value="asc">
        <input type="hidden" name="sort-distance-order" id="sort-distance-order" value="asc">
        <input type="hidden" name="sort-distance" id="sort-distance" value="0">
        <input type="hidden" name="sort-id" id="sort-id" value="0">
        <input type="hidden" name="sort-id-order" id="sort-id-order" value="asc">
        {{csrf_field()}}
        <div class="row">
            <div class="col-sm-4 col-md-3">
                <div class="filterHead filterBox">
                    <h2 class="filterHead__title">Filter</h2>
                    <a href="#" class="filterHead__link">
                        Filter by
                        <span><i class="fa fa-angle-down"></i></span>
                    </a>
                </div>
                <div class="searchFilter hiddenFilter filterBox hiddenFilter--visible">
                
                    <div class="filterGroup">
                        <h2 class="filterGroup__title">
                            TYPE OF SERVICE
                        </h2>
                        <a href="#" class="filterGroup__link">TYPE OF SERVICE
                            <span><i class="fa fa-angle-down"></i></span>
        
                        </a>
                        <div class="filterGroup__box filterGroup__box--active">
                           
                    <div class="checkBox_item">
                        {!! Form::checkbox('typeService[SINGLE / REGULAR VISIT]', null,((isset($requestSearch['typeService']))&&in_array('SINGLE / REGULAR VISIT',$requestSearch['typeService'])? 1 : null),
                           array('class' => 'customCheckbox ','id'=>'typeServicebox1','onclick'=>"$('#load-more').val(0);carerSearchAjax('".env('APP_ENV')."')")) !!}
                        <label for="typeServicebox1">SINGLE / REGULAR VISIT</label>
                        </div>
                                
                                
                        <div class="checkBox_item">
                        {!! Form::checkbox('typeService[LIVE IN CARE]', null,((isset($requestSearch['typeService']))&&in_array('LIVE IN CARE',$requestSearch['typeService'])? 1 : null),
                               array('class' => 'customCheckbox ','id'=>'typeServicebox2','onclick'=>"$('#load-more').val(0);carerSearchAjax('".env('APP_ENV')."')")) !!}
                            <label for="typeServicebox2">LIVE IN CARE</label>
                        </div>


                        <div class="checkBox_item">

                        {!! Form::checkbox('typeService[RESPITE CARE]', null,((isset($requestSearch['typeService']))&&in_array('RESPITE CARE',$requestSearch['typeService'])? 1 : null),
                          array('class' => 'customCheckbox ','id'=>'typeServicebox3','onclick'=>"$('#load-more').val(0);carerSearchAjax('".env('APP_ENV')."')")) !!}
                        <label for="typeServicebox3">RESPITE CARE</label>
                    </div>
                            

                         <!-- {!! Form::select('typeService',[''=>'TYPE OF SERVICE','1'=>'SINGLE / REGULAR VISITS','2'=>'LIVE IN CARE','3'=>'RESPITE CARE'],null,['id'=>'typeService','class'=>'formSelect','onchange'=>"$('#load-more').val(0);carerSearchAjax('".env('APP_ENV')."')"]) !!} -->
                        </div>
                    </div>
                    <div class="filterGroup">
                        <h2 class="filterGroup__title">
                            type of care
                        </h2>
                        <a href="#" class="filterGroup__link">type of care
                            <span><i class="fa fa-angle-down"></i></span>
                        </a>
                        <div class="filterGroup__box filterGroup__box--active">
                            @foreach($typeCare as $care)
                                <div class="checkBox_item">

                                    {!! Form::checkbox('typeCare['.$care->id.']', null,((isset($requestSearch['typeCare']))&&in_array($care->id,$requestSearch['typeCare'])? 1 : null),
                                                array('class' => 'customCheckbox ','id'=>'typeCarebox'.$care->id,'onclick'=>"$('#load-more').val(0);carerSearchAjax('".env('APP_ENV')."')")) !!}
                                    <label for="typeCarebox{{$care->id}}">{{$care->name}}</label>
                                </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="filterGroup">
                        <h2 class="filterGroup__title">
                            OTHER PREFERENCES
                        </h2>
                        <a href="#" class="filterGroup__link"> OTHER PREFERENCES
                            <span><i class="fa fa-angle-down"></i></span>
                        </a>
                        <div class="filterGroup__box filterGroup__box--active">
                            <div class="checkBox_item">
                                {!! Form::checkbox('gender[Male]', null,((isset($requestSearch['gender']))&&in_array('Male',$requestSearch['gender'])? 1 : null),
                                                array('class' => 'customCheckbox ','id'=>'boxgender1','onclick'=>"$('#load-more').val(0);carerSearchAjax('".env('APP_ENV')."')")) !!}
                                <label for="boxgender1">male</label>
                            </div>
                            <div class="checkBox_item">

                                {!! Form::checkbox('gender[Female]', null,((isset($requestSearch['gender']))&&in_array('Female',$requestSearch['gender'])? 1 : null),
                                                array('class' => 'customCheckbox ','id'=>'boxgender2','onclick'=>"$('#load-more').val(0);carerSearchAjax('".env('APP_ENV')."')")) !!}
                                <label for="boxgender2">female</label>
                            </div>
                            <div class="checkBox_item">

                                {!! Form::checkbox('have_car', null,((isset($requestSearch['have_car']))? 1 : null),
                                               array('class' => 'customCheckbox ','id'=>'have_car','onclick'=>"$('#load-more').val(0);carerSearchAjax('".env('APP_ENV')."')")) !!}
                                <label for="have_car">HAS OWN TRANSPORT</label>
                            </div>
                            <div class="checkBox_item">
                                {!! Form::checkbox('work_with_pets', null,((isset($requestSearch['work_with_pets']))? 1 : null),
                                               array('class' => 'customCheckbox ','id'=>'work_with_pets','onclick'=>"$('#load-more').val(0);carerSearchAjax('".env('APP_ENV')."')")) !!}
                                <label for="work_with_pets">WORKS WITH PETS</label>
                            </div>
                        </div>
                    </div>
                    <div class="filterGroup filterGroup--languages">
                        <h2 class="filterGroup__title">
                            Languages
                        </h2>
                        <a href="#" class="filterGroup__link"> Languages
                            <span><i class="fa fa-angle-down"></i></span>
                        </a>
                        <div class="filterGroup__box filterGroup__box--active">
                            <div class="scrollFilter">
                              
                                @foreach($languages as $lang)
                                    <div class="checkBox_item">
                                        {!! Form::checkbox('language['.$lang->id.']', null,((isset($requestSearch['language']))&&in_array($lang->id,$requestSearch['language'])? 1 : null),
                                               array('class' => 'customCheckbox ','id'=>'languagebox'.$lang->id,'onclick'=>"$('#load-more').val(0);carerSearchAjax('".env('APP_ENV')."')")) !!}

                                        <label for="languagebox{{$lang->id}}">{{$lang->carer_language}} </label>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-8 col-md-9">
                <div class="filterHead filterHead--xs-column filterBox">
                    <a href="#" class="sortLinkXs">sort by <span><i class="fa fa-angle-down"></i></span></a>
                    <div class="sortFilters hiddenSort hiddenSort--visible">
                        <div class="sortFilters__item">
                            <!-- <input type="text" name="zonalManagerName" class="sortField disable" id="search_text" placeholder="POST CODE" >
                            <input type="hidden" name="postCode" class="form-control" id="postCode"> -->
                         
                           <!-- {!! Form::text('postcode',null,['class'=>'formInput personalForm__input','placeholder'=>'Your postcode','data-country'=>'Manchester,United Kingdom','maxlength'=>'10']) !!} -->
                           
                {!! Form::text('postCode',(isset($requestSearch['postCode'])?$requestSearch['postCode']:''),['class'=>'sortField disable','id'=>'search_text','placeholder'=>'POST CODE','maxlength'=>16,'onkeydown'=>"if(event.keyCode==13){carerSearchAjax('".env('APP_ENV')."');}"]) !!}
                    
                            <span class="fieldIco"><i class="fa fa-map-marker" aria-hidden="true"></i></span>
                        </div>
                        <div class="sortFilters__item">
                            <input type="text" name="findDate" onchange="carerSearchAjax('{{env('APP_ENV')}}');" class="sortField datepicker" placeholder="DATE">
                            <span class="fieldIco"><i class="fa fa-calendar" aria-hidden="true"></i></span>
                        </div>
                        <!-- <div class="sortFilters__item">
                            {!! Form::select('typeService',[''=>'TYPE OF SERVICE','1'=>'SINGLE / REGULAR VISITS','2'=>'LIVE IN CARE','3'=>'RESPITE CARE'],null,['id'=>'typeService','class'=>'formSelect','onchange'=>"$('#load-more').val(0);carerSearchAjax('".env('APP_ENV')."')"]) !!}
                        </div> -->
                    </div>
                </div>

                <div class="resultHeader Paginator">
                    <p class="resultHeader__info">RESULT <span class="cares_count"></span> CARERS</p>
                    <div class="sortLink">
                        SORT BY &nbsp;<a href="#" class="sortLink__item sort-rating"> <span> </span> Rating
                        </a>
                        <p> &nbsp; - &nbsp; </p>
                        <a href="#" class="sortLink__item sort-id">
                            MOST RECENT
                        </a>
                        <p class=""> &nbsp; - &nbsp; </p>
                        <a href="#" class="sortLink__item sort-distance ">
                            Distance
                        </a>
                    </div>
                </div>

                <div class="card error-text result nhide not_found" style="margin-top:5px;">
                    <div class="card-block">
                    <p class="text-uppercase">Sorry</p>
                        <p class="text-left">
                            <h3>
                            No Carers meet your exact criteria.
                            Please try again by deselecting the least important requirements,
                            or <a href="{{route('ContactPage')}}">contact us</a> if you need further help
                        </h3>
                        </p>
                    </div>
                </div>
                <div class="card error-text result nhide post_code_error">
                    <div class="card-block">
                        <p class="text-uppercase">Sorry</p>
                        <h3 style="font-size: 24px;">Holm is not yet available in this area. Please <a href="/contact">contact us</a>
                            to request Holm in your area. Many thanks!
                        </h3>
                    </div>
                </div>
                @if(env('APP_ENV') == 'production')
                <div class="card error-text result">
                    <div class="card-block">
                        {{--<p class="text-uppercase">Sorry</p>--}}
                        {{--<p class="text-left">--}}
                        {{--No Carers meet your exact criteria.--}}
                        {{--Please try again by deselecting the least important requirements,--}}
                        {{--or <a href="{{route('ContactPage')}}">contact us</a> if you need further help--}}
                        {{--</p>--}}
                        <p>Hi!</p>
                        <p>Please note: We are not yet taking booking for carers. We are due to start in early February.</p>
                        <p>We will contact all registered users when we are open.</p>
                        <p>You can register by pressing the sign up button at the top of the screen.</p>

                        <p>See you soon!</p>
                        <p>The Holm Team</p>
                    </div>
                </div>
                @else
                    <div class="carer-result">

                    </div>
                @endif
                <div class="loader" id="loader-search" style="display: none"></div>
                {{--@for ($pages = 1; $pages <= ceil($carerResultCount/$perPage); $pages++)--}}
                    {{--<a class="sortLink__item {{($pages==$page)?'active':''}} " href="/search/page/{{ $pages }}">--}}
                        {{--{{ $pages }}--}}
                    {{--</a>--}}
                    {{--<span class="sortLink__separate"></span>--}}
                {{--@endfor--}}
                    <div class="pagination-container">
                        <div class="cust-pagination">
                            <div class="cust-pagination__group">
                                <a href="#" class="paginationArrow paginationArrow--left">
                                    <span><i class="fa fa-angle-left"></i></span>
                                </a>

                            </div>

                            <a href="#" class="paginationArrow paginationArrow--right">
                                <span><i class="fa fa-angle-right"></i></span>
                            </a>
                        </div>
                    </div>
            </div>

        </div>
        {!! Form::close() !!}
    </div>
</section>
<!-- <script>
    var postCodeAutoCplUrl      =   '/search/autocomplete';
    var env_var                 =   "{{env('APP_ENV')}}";
    console.log(env_var); 
    $(document).ready(function(){

        $(".datepicker").datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: "dd/mm/yy",
            showAnim: "slideDown",
            minDate: "+0D",
            maxDate: "+20Y",
            yearRange: "0:+10"
        });
    });
    
    $('#search_text').typeahead({                
        source: function (q, process) {
               // var moduleType  =   "CALL-CENTER";
            return $.get(postCodeAutoCplUrl, {
                terms: q,
                //moduleType: moduleType
            }, function (response) {
                var data = [];
                if(response != ''){
                    var resp            =   $.parseJSON(response);
                    var totErrorLen     =   resp.length;        
                    //console.log("Responce",resp);     return; 
                    for(var i =0;i < totErrorLen;i++){
                        data.push(resp[i]['post_code'] + "#" + resp[i]['result']);
                    }
                }                       
                return process(data);
            });
        },
        highlighter: function (item) {
            var parts   =   item.split('#'),
            html        =   '<div class="typeahead">';
            html        +=  '<div class="pull-left margin-small">';
            // html        +=  '<div class="text-left"><strong>' + parts[0] + ' ' + parts[1] +'</strong></div>';
            html        +=  '<div class="text-left"><strong>' + parts[0] + ' ' + '</strong></div>';

            // html        +=  '<div class="text-left">' + parts[2] + " "+ parts[3] + '</div>';
            html        +=  '</div>';
            html        +=  '<div class="clearfix"></div>';
            html        +=  '</div>';
            return html;
        },
        updater: function (item) {
            var parts   =   item.split('#');
            $("#postCode").val(parts[0]);
            carerSearchAjax(env_var);
            return parts[0];

        },
    });

</script> -->
