<section class="mostPopular">
    <div class="container">
        <div class="section_title homeContent">
		   <?php echo  $content; ?>
            <!--<h1 class="lightTitle">
                Home Care Help for the Elderly
            </h1>
            <h1 class="carers-below">
                Whether it’s long term support, respite, or an emergency, you can use Holm to get help at home quickly
                and easily.
            </h1>-->
        </div>
    </div>
</section>

@if($trusts)
<section class="advantages_section association_container">   
	<div class="container">
		<div class="advantages advantages_cust">
			<div class="row row-centered">
            <?php $i=1;  ?>
            @foreach($trusts as $trust)
                <div class="col-xs-12 @if($i==2 || $i==5) col-sm-3 @else col-sm-2 @endif col-centered" >
					<div class="">
						<div class="singleAdvantage__img">
							
							<a href="{{$trust->url}}" target="_blank" title="{{$trust->title}}" @if($i==5) style="width:100%;" @endif>
								<img alt="{{$trust->image_alt}}" src="{{asset('public/image_Association')}}/{{$trust->image_path}}" >
							</a>

                            @if($i==5)
                                <br>
                                <p style="text-align:left;">{{$trust->title}}</p>
                            @endif
							
						</div>
					</div>
				</div>

            <?php $i++; ?>
            @endforeach
			 </div>
		</div>
	</div>
</section>
@endif
