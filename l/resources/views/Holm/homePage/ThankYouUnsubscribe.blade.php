<section class="mainSection">
    <div class="container">
        <div class="breadcrumbs">
            <a href="{{route('mainHomePage')}}" class="breadcrumbs__item">
                Home
            </a>
            <span class="breadcrumbs__arrow">&gt;</span>
            <a href="{{route('ContactPage')}}" class="breadcrumbs__item">
                Thank you
            </a>
        </div>
        <div class="thank">
            <h2 class="thank__title">
                Thank you for your response!
            </h2>
            <span class="successIco">
          <i class="fa fa-check" aria-hidden="true"></i>
        </span>
            <p class="info-p">
                We will remove you from our promotional email updates.
            </p>
            <p class="info-p">
                You will still receive emails regarding your use of Holm.
            </p>
        </div>
    </div>
</section>