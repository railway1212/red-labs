
    <p class="resultHeader__info">Showing {{($page*$perPage)<$countAll?$page*$perPage:$countAll}} of {{$countAll}} CARERS</p>
    <div class="sortLink">
        SORT BY  &nbsp;<a href="#" class="sortLink__item  sort-rating"> <span> </span> rating
        </a>
        <p> &nbsp; - &nbsp; </p>
        <a href="#" class="sortLink__item sort-id">
            MOST RECENT
        </a>
        @if(Auth::check())
        <p> &nbsp; - &nbsp; </p>
        <a href="#" class="sortLink__item sort-distance">
            Distance
        </a>
            @endif
    </div>
