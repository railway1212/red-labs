<section class="mainSection">
    <div class="container">
        <div class="justifyContainer justifyContainer--smColumn">
            <div class="breadcrumbs">
                <a href="{{route('mainHomePage')}}" class="breadcrumbs__item">
                    Home
                </a>
                <span class="breadcrumbs__arrow">&gt;</span>
                <a href="{{route('TermsPage')}}" class="breadcrumbs__item">
                    Terms and Conditions
                </a>
            </div>
        </div>
        <div class="terms">
            <div class="terms__title">
                <h2 class="lightTitle lightTitle--bigger">
                    <?php  echo $title; ?>
                </h2>
            </div>
			<?php echo $content; ?>	
            <!--<div class="terms__item">
                <h2>
                    Definitions of terms:
                </h2>
                <p>
                    “Holm Service” means an online marketplace for care provided via the Holm platform.<br>

                    “Carers” means individuals working on a self-employed basis and delivering care services via the
                    Holm platform.<br>

                    “Care seekers”, “customers” means individuals using the Holm Service to search for care, engage with
                    carers and enter into a contractual relationship with carers.
                </p>
            </div>
            <div class="terms__item">
                <h2>
                    1. General Terms of Holm Care Service
                </h2>
                <p>
                    1.1 About Holm Service
                </p>
                <p>
                    Holm “service” provides an online platform for self-employed carers and care seekers to connect with
                    one another, communicate and arrange care contracts between one another. This is facilitated through
                    providing services which include but are not limited to:
                </p>
                <ul>
                    <li>- A searchable online database of self-employed carers;</li>
                    <li>- Communication tools to facilitate interaction;</li>
                    <li>- Payment system to enable transactions between self-employed carers and care seekers;</li>
                    <li>- Monitoring tools for customers to monitor care activity;</li>
                    <li>- Feedback tools on care provision.</li>
                </ul>
                <p></p>

                <p>
                    1.2 Introductory agency service
                </p>

                <p>
                    Holm is classified as an ‘introductory agency’ in line with CQC guidance. Holm provides an online
                    marketplace to facilitate the process of finding home care. Holm does not directly supply carers to
                    care seekers. Holm does not employ carers, nor does Holm Care act as an employment agency or care
                    agency. In line with CQC regulations, Holm is not permitted to make changes to the care plan,
                    provide rotas or effect control over the delivery of care.
                </p>

                <p>
                    1.3 Limitations of service
                </p>

                <p>
                    All the tools provided by Holm Service are used at the customer’s and self-employed carers’ own risk
                    and there is no guarantee that these tools will work optimally, not be subject to downtime, or
                    removed from service at any point in time. Holm does not accept any liability for losses or damages
                    caused by the temporary unavailability of the service or technical errors.

                </p>

                <p>
                    Holm disclaims any liability for controversies, losses, injury, accidents, claims or damages arising
                    out of the use of the online tools it provides, the engagement of carers or the provision of care
                    services by carers.
                </p>


            </div>


            <div class="terms__item">
                <h2>
                    2. Rules for User conduct and use of service
                </h2>
                <p>
                    2.1 Eligibility to use the site and services; representations and warranties
                    By registering and using this site you confirm that:

                </p>
                <ul>
                    <li>- you accept and will abide by our terms and conditions;</li>
                    <li>- you are 18 years old or over and have the right to form legally binding contracts under UK
                        law;
                    </li>
                    <li>- payment system to enable transactions between self-employed carers and care seekers;</li>
                    <li>- all the information provided by you on the site is correct and accurate, including any stated
                        care requirements or specific needs.
                    </li>
                </ul>

                <p></p>
                <p>
                    2.2 Carer selection

                </p>
                <p>
                    By registering and using Holm Service as a person seeking care you agree that it is your
                    responsibility to select an appropriate carer for yourself, your family member, any friend/associate
                    or any other person you are acting for. Holm can help you in the process of introducing and
                    recommending carers, however Holm will not select a carer for you and the final choice remains
                </p>

                <p>2.3 Vetting and background checking of carers</p>

                <p>Holm Care makes reasonable efforts to check the identity and information provided by carers. This
                    includes visual checks of:

                </p>
                <ul>
                    <li>- passports to confirm identity and right to live and work in the UK;</li>
                    <li>- stated qualifications and training certificates where available;</li>
                    <li>- existing CRB/ DBS checks.</li>
                </ul>
                <p></p>

                <p>Where carers cannot provide visual confirmation of having passed a DBS check within the past 3 years,
                    Holm performs DBS checks via an external provider. Holm does not conduct full reference checks of a
                    carer’s stated employment history or care experience. Although Holm endeavours to interview every
                    carer who is allowed on to the platform in person, Holm is not responsible for nor has it control
                    over the quality, timing, legality, reliability, responsibility, integrity or suitability of the
                    carers listed on the platform.
                </p>

                <p>Holm recommends that care seekers carry out their own assessment of a carer’s suitability before
                    engaging into a contract with them by:
                </p>
                <ul>
                    <li>- Interviewing carers;</li>
                    <li>- Verifying a carers identity via photographic ID;</li>
                    <li>- Requesting proof of experience, training, qualifications, authorisations and suitability for
                        the position they are applying for;
                    </li>
                    <li>- Following up on references of carers.</li>
                </ul>

                <p></p>
                <p>2.4 Insurance cover</p>
                <p>Care seekers should ensure that their insurance covers carers working in their home and should verify
                    whether carers have their own independent insurance cover.</p>

                <p>2.5 Code of conduct</p>
                <p>You should not discriminate against a carer on the basis of skin colour, nationality, disability,
                    belief, gender or any other potential source of discrimination.</p>
                <p>In addition, you represent and warrant that you and each member of your household (i) have never been
                    the subject of a complaint, restraining order or any other legal action involved with being arrested
                    for, charged with, or convicted of any criminal offence involving violence, abuse, neglect, theft or
                    fraud, or any offence that involves endangering the safety of others, dishonesty, negligence or
                    drugs, and are not nor have ever been on the sex offenders register or other similar list.
                </p>


                <p>2.6 Contract compliance</p>
                <p>Once you have set up a contract and submitted it to a carer, you are bound by the terms of that
                    contract. You must pay the upfront required monetary amount for the contract to be active. You must
                    negotiate with the self-employed carer regarding any deviations from the contract terms, e.g. time
                    off for holiday, illness or personal circumstances. Holm can offer no mediation or intervention
                    regarding perceived non-compliance with the contract unless the dispute resolution process is
                    activated.</p>

            </div>


            <div class="terms__item">
                <h2>
                    3. Release of liability for user conduct and disputes
                </h2>
                <p>Any agreements are legally binding agreements between the care seeker and the carer. Holm are not
                    party to an agreement and we cannot arbitrate or mediate if there is an alleged breach of contract
                    unless the dispute resolution process is activated.</p>
                <p>Any issues should be resolved directly between care seeker and carer where at all possible. Holm Care
                    does not accept any liability for claims, demands or direct or indirect damages arising from
                    disputes between care seekers and carers, however we do offer a dispute resolution pathway which
                    should be followed in the case of disagreement.</p>

            </div>

            <div class="terms__item">
                <h2>
                    4. Dispute Resolution Policy
                </h2>
                <p>4.1 Overview</p>
                <p>Holm provides a dispute resolution process where there is a disagreement between care seeker and
                    self-employed carer. Either care seeker or self-employed carer can dispute a contract. Holm will
                    examine the contract, compliance with terms, user analytics data including but not limited to visit
                    logging, messaging, geoverification, user feedback and previous self-employed carer behaviour and
                    any submissions by either party. Holm reserves the right at its sole discretion to defer payment,
                    reimburse or cease contractual payments at any time.</p>

                <p>4.2 Disputed funds</p>
                <p>The unpaid monies for a contract are held in an escrow account and Holm offers no guarantee to either
                    care seeker or self-employed carer that there will be either reimbursement or full payment if the
                    dispute resolution policy is activated. Holm will endeavour to come to an equitable outcome based
                    upon the evidence available.</p>

            </div>


            <div class="terms__item">
                <h2>
                    5. Payments, Cancellations and Refunds
                </h2>
                <p>5.1 Payments</p>
                <p>Contracts for work are made between the care seeker and the carer. By entering a contract as a care
                    seeker you agree that you have the funds available to pay for the contract value.</p>
                <p>All payments for work completed must go via Holm Service; attempts to pay outside of Holm Service
                    will lead to sanctions not limited to immediate account suspension. Any offers to pay outside of
                    Holm Service made by either party must be reported to Holm immediately.</p>

                <p>5.2 Hourly carer rates</p>
                <p>Hourly rates may be subject to change. Hourly rates should not be changed during a contract
                    agreement. Any change in hourly rates that are made before the end of a contract must result in the
                    termination of the current contract and a new contract must be agreed.</p>


                <p>5.3 Holm fees</p>
                <p>Holm charges an introductory commission fee of 20% (incl. VAT) of the hourly carer rate. This
                    commission is already included in the rate stated on each carer’s profile on Holm Service. Holm
                    reserves the right to alter the terms of its commission structure at any time.</p>

                <p>5.4 VAT</p>
                <p>Holm does not charge VAT on the service provided by self-employed carers but we charge 20% VAT on the
                    commission we take.</p>

                <p>5.5 Direct Payments and Personal Budgets</p>
                <p>If you are paying via Direct Payments or Personal Budgets, you must setup a contract on the Holm
                    platform. Payment can be made a week in arrears following provision of an Invoice for hours
                    completed.</p>

                <p>5.6 Refunds</p>
                <p>Holm does not as a general rule provide refunds or credits for purchase cancellations. However, Holm
                    may at its sole discretion issue refunds or credits when it believes they are warranted.</p>


                <p>5.7 Cancellations</p>
                <p>Holm strongly discourages cancellations of contracts unless exceptional circumstances occur. Carers
                    working through Holm depend on predictable schedules and reliable income. Cancellations of contracts
                    without sufficient warning or reason causes undue inconvenience to carers and provides no income
                    protection to carers working through Holm. It is therefore our policy that any contracted period
                    cancelled within a 24 hour period prior to the contract commencing will be billed and paid to the
                    carer. In exceptional circumstances and where both parties agree, Holm will exercise its discretion
                    to void payment in cases where cancellation has taken place within 24 hours of the contracted
                    period. Refunds for care work already contracted and subsequently cancelled will only be issued
                    where both parties agree that it is appropriate to do so.</p>

                <p>5.8 Bonus Payments</p>
                <p>Referral Bonus payments are paid to both the referrer, and the person receiving the referral code.
                    The referral code must be applied when the new user is registering an account for the first time.
                    A minimum of 20 hours of care must either be purchased, or provided by person receiving the referral
                    in order for both parties to receive the bonus. If the recipient of the payment is registered as a
                    care worker on the Holm platform, they shall receive credit added to their account, and that shall
                    be paid out on the following pay run. If the recipient of the payment is registered as a care
                    purchaser on the Holm Platform, they shall receive credit to their account, which can then solely
                    be used to purchase additional care via the Holm platform.Special bonus payments to newly registered
                    care workers when an eligible code is applied when registering an account for the first time. The
                    bonus payment is payable after the care worker has completed a minimum of 1 hour of care. The care
                    worker will receive credit added to their account, and that shall be paid out on the following pay
                    run.</p>
            </div>


            <div class="terms__item">
                <h2>
                    6. Limitation of liability
                </h2>

                <p>6.1 Responsibility for contract acceptance</p>
                <p>You acknowledge that the decision to enter into a contract with a carer is your sole responsibility
                    and that Holm gives no warranty as to the suitability, experience, history or character of any
                    carer, nor does Holm give any warranty as to the completeness, truthfulness or accuracy of any
                    information or documentation provided by the carer.</p>
                <p>6.2 Risks of introduction service</p>
                <p>You agree that you understand the risks involved in participating in an introductory agency service
                    and you hereby waive any rights to claims for damages from Holm in relation to the service.</p>
                <p>6.3 Breach of terms and conditions</p>
                <p>You agree to indemnify and hold Holm harmless from any claim or demand brought against Holm as a
                    result of you breaching these terms and conditions.</p>
                <p>6.4 Liability cap</p>
                <p>You acknowledge that Holm Care’s total liability to you whether in respect of goods or services and
                    whether based in negligence, breach of contract, misrepresentation or otherwise shall not exceed the
                    value of the total commission income Holm derived from you via the Holm Service.</p>

            </div>

            <div class="terms__item">
                <h2>
                    7. Quality assurance and background checks
                </h2>

                <p>By using this site or service you authorise Holm to carry out certain background checks and audits at
                    a time of their choosing. Holm reserves the right but not the obligation to use a third party to
                    scan your personal information on an ongoing basis against a variety of sources which may include,
                    but are not limited to, sex offender registries, social media, criminal registries and other legally
                    available databases and resources. Holm has no obligation to perform checks and releases all
                    liability associated with result from checks.</p>

            </div>


            <div class="terms__item">
                <h2>
                    8. Feedback and customer reviews
                </h2>

                <p>You will be asked to leave qualitative and quantitative feedback on your care experience. The
                    feedback will affect each carer’s ranking on Holm Service. Feedback should be provided honestly and
                    you must not attempt to falsify, manipulate or coerce a carer by threatening negative feedback. Any
                    attempts of a carer trying to influence you in our feedback or trying to change your opinion unduly
                    should be reported to us. We reserve the right to remove any defamatory, abusive or offensive
                    feedback at our discretion but are not obliged to do so. You agree to providing an exclusive and
                    perpetual right for Holm to publish these reviews.</p>
            </div>


            <div class="terms__item">
                <h2>
                    9. Contractual Obligations
                </h2>

                <p>By using the Holm Service you agree to use the service for all current and future work that takes
                    place as a result of an introduction made through Holm Service. You must not create contractual
                    agreements outside this service with carers introduced to you on Holm Service, with the deliberate
                    intention of avoiding Holm Service fees. Any severe offences will result in but are not limited to
                    the immediate termination of a care seeker’s profile and charges equivalent up to the estimated
                    earnings of a carer during a month.</p>
            </div>


            <div class="terms__item">
                <h2>
                    10. Safeguarding Policy
                </h2>

                <p>Holm takes the safety of all parties using Holm Care Service extremely seriously and complies with
                    all relevant legislation including the Care Act 2010, the Mental Capacity Act 2005 and the
                    Safeguarding of Vulnerable Adults Act. In instances where we deem a "vulnerable" adult is at risk of
                    exploitation or any type of harm and we have received evidence to indicate this the case, we will
                    comply with this legislation and refer the relevant parties to the nearest Local Authority's Adult
                    Safeguarding Team. Although Holm Service does not directly provide care, we take our duties as a
                    responsible organisation seriously and will endeavour to ensure a duty of care to all participants
                    on the platform.</p>
                <p>Holm carries out a risk assessment on all care seekers of the platform based on a combination of
                    telephone triage, written communications, medical history, oral assessment of mental capacity and
                    feedback from carer visits. In instances where there is evidence to suggest that a care seeker may
                    not be able to safely and competently use Holm Care Service, Holm Care reserves the right at its
                    absolute discretion to limit access to the platform on temporary or permanent basis.</p>
            </div>
            <div class="terms__item">
                <h2>
                    11. Complaints Policy
                </h2>

                <p>Holm takes all customer complaints seriously and is committed to learning from mistakes and improving
                    our service for both current and future customers and carers. If you have a complaint, please send
                    this by email to info@holm.care. We will normally respond within 24 hours (ie one business day). We
                    will investigate any matter referred and take appropriate action where necessary. If the complaint
                    is regarding a contract dispute, you will need to activate the dispute resolution pathway by
                    activating the disputed contract button in the client dashboard. If you have concerns regarding the
                    performance, behaviour or competence of a carer, we will investigate the matter and take
                    proportionate action ranging from verbal and written feedback to account suspension.</p>
            </div>
            <div class="terms__item">
                <h2>
                    12. Privacy Statement
                </h2>

                <p>By using the Holm services you consent that Holm may process the personal data that Holm Care
                    collects from them in accordance with Holm’s Privacy Policy.</p>
            </div>
            <div class="terms__item">
                <h2>
                    13. Governing Law
                </h2>

                <p>The terms and conditions and any dispute arising out of the site and/or the services shall be
                    governed by and construed in accordance with the law of England and Wales. The courts of England and
                    Wales shall have exclusive jurisdiction to settle any dispute or claim that arises out of or in
                    connection with these terms and conditions or in connection with this site and/ or service.</p>
            </div>
            <div class="terms__item">
                <h2>
                    14. Contact Information:
                </h2>

                <p>If you have any questions about the terms and conditions or the services provided by Holm please
                    contact us at: <a href="mailto:info@holm.care">info@holm.care</a></p>
            </div>
        </div>-->
    </div>
</section>
