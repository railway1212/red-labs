<section class="mainSection">
    <div class="container">
        <div class="breadcrumbs">
            <a href="{{route('mainHomePage')}}" class="breadcrumbs__item">
                Home
            </a>
            <span class="breadcrumbs__arrow">&gt;</span>
            <a href="{{route('ContactPage')}}" class="breadcrumbs__item">
                Thank you
            </a>
        </div>
        <div class="thank">
            <h2 class="thank__title">
                Thank you!
            </h2>
            <span class="successIco">
          <i class="fa fa-check" aria-hidden="true"></i>
        </span>
            <p class="info-p">
                Your email was successfully sent. We will contact you as soon as possible.
            </p>
        </div>
    </div>
</section>