<div itemscope itemtype="http://schema.org/LocalBusiness" style="font-size:0">
<h1><span itemprop="name"> Holm Care</span></h1>
<span itemprop="description"> Homecare for the Elderly. Helping you find the best affordable care at home quickly and easily in Manchester, Bolton, Sale, Rochdale, Stockport, Salford, Oldham, Bury, Tameside, Trafford and Wigan.</span>
<div itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
<span itemprop="streetAddress">2 Federation Street</span>
<span itemprop="addressLocality"> Manchester, UK</span>,
<span itemprop="PostalCode"> M4 4BF</span>
</div>
Phone: <span itemprop="telephone">0161 706 0288</span>
</div>


@if($banner)
<section class="home_section" title="Enter Your Postcode Search Box" @if(!empty($banner->image_name)) style="background-image:url('{{asset('public/image_Association')}}/{{$banner->image_name}}')" @endif>
    <div class="container">
        <div class="home">
            <div class="home__title">

            <?php $bannerContents =  unserialize($banner->text); ?>
                @if($bannerContents)
                   <?php echo $bannerContents[0]; ?>
                @endif
               <!-- <h1>
                    Holm Care
                </h1>
                <h2 style="font-weight: 500; font-size: 1.5em; text-transform: none">
                    Helping you find the best homecare in Greater Manchester
                </h2>-->
            </div>
            <div class="row">
                <div class="col-md-offset-1 col-md-10">
                    <div class="home__search">
                        {!! Form::model(null, ['method'=>'POST','route'=>'searchPagePost','class'=>'homeForm','id'=>'carerSearchForm']) !!}
                        {!! Form::hidden('stage','carerSearch') !!}
                            {{--<input type="text"   class="homeForm__input" placeholder="enter your postcode">--}}
                        {!! Form::text('postCode',null,['autocomplete'=>'off','class'=>'homeForm__input disable','placeholder'=>'enter your postcode','maxlength'=>16,'onkeydown'=>"if(event.keyCode==13){document.getElementById('carerSearchForm').submit();}"]) !!}

                        <button href="#" type="submit" class="homeForm__btn" onclick="event.preventDefault();document.getElementById('carerSearchForm').submit();">
                  <span>
                    <i class="fa fa-search"></i>
                  </span>
                            </button>
                        {!! Form::close() !!}
                        <a href="{{ route('searchPage') }}" class="home-search-btn">
                            Find a Carer
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endif
