<section class="mainSection">
    <div class="container">
        <div class="justifyContainer justifyContainer--smColumn">
            <div class="breadcrumbs">
                <a href="{{route('mainHomePage')}}" class="breadcrumbs__item">
                    Home
                </a>
                <span class="breadcrumbs__arrow">></span>
                <a href="{{route('AboutPage')}}" class="breadcrumbs__item">
                    About us
                </a>
            </div>
        </div>
        <div class="aboutWrap">
            @if($banner)
            <section class="about_section" title="Find a personal carer" @if(!empty($banner->image_name)) style="background-image:url('{{asset('public/image_Association')}}/{{$banner->image_name}}')" @endif>
                <div class="container">
                    <div class="about aboutbanner" >
                    <?php $bannerContents =  unserialize($banner->text); ?>
                      @if($bannerContents)
                        <?php  echo  $bannerContents[0];?>
                      @endif
                        <!--<h1 class="about__title lightTitle">
                            Find a Personal Carer
                        </h1>
                        <h1 class="about__info">
                            We are a new kind of home care service helping connect you directly to skilled elderly care
                            workers in the Manchester area.
                            We are not a care agency, and don’t charge you like an agency. Holm Care makes finding care
                            workers so much easier.

                        </h1>-->
                    </div>
                </div>
            </section>
            @endif

            <section class="advantages_section">
                <div class="container">
                    <div class="section_title aboutContent">

                        <?php echo $content;?>
                       <!-- <h2 class="lightTitle">Making Sure Everyone Receives Great Care</h2>
                        <p>We believe that everyone deserves excellent care when they need it</p>
                        <p>This is why :</p>-->
                    </div>
                    <div class="advantages advantages_cust">

                        @if($extraBlocks)
                            @foreach($extraBlocks as $extraBlock)
                                <div class="advantages__item singleAdvantage">
                                    <div class="singleAdvantage__img">
                                        <img src="{{asset('image_Association')}}/{{$extraBlock['image']}}" alt="{{$extraBlock['image_alt']}}">
                                    </div>
                                    <div class="singleAdvantage__info">
                                        <?php echo $extraBlock['content'];  ?>
                                       
                                    </div>
                                </div>
                            @endforeach
                        @endif    

                        <!--<div class="advantages__item singleAdvantage">
                            <div class="singleAdvantage__img">
                                <img src="{{asset('/img/advantage1.png')}}" alt="Every carer undergoes rigorous checks">
                            </div>
                            <div class="singleAdvantage__info">
                               
                                <p>
                                    We use years of experience to make sure only the very best carers are registered.
                                    Every carer undergoes rigorous checks
                                </p>
                            </div>
                        </div>




                        <div class="advantages__item singleAdvantage">
                            <div class="singleAdvantage__img">
                                <img src="{{asset('/img/advantage2.png')}}" alt="We suit your needs not an agency">
                            </div>
                            <div class="singleAdvantage__info">
                          
                                <p>
                                    Not only do you pick your visiting carer but you pick when they visit. We suit your
                                    needs, not an agency.
                                </p>
                            </div>
                        </div>
                        <div class="advantages__item singleAdvantage">
                            <div class="singleAdvantage__img">
                                <img src="{{asset('/img/advantage4.png')}}" alt="20% more care than through an agency">
                            </div>
                            <div class="singleAdvantage__info">
                               
                                <p>
                                    Buy at least 20% more care than you would through an agency
                                </p>

                            </div>
                        </div>
                        <div class="advantages__item singleAdvantage">
                            <div class="singleAdvantage__img">
                                <img src="{{asset('/img/advantage3.png')}}" alt="We believe a great carer is priceless">
                            </div>
                            <div class="singleAdvantage__info">
                              
                                <p>
                                    We believe a great carer is priceless. That’s why we’re helping them earn more and
                                    don’t charge agency rates
                                </p>
                            </div>
                        </div>-->

                    </div>
                </div>
            </section>  
             

           <!-- links for different trust associations -->
            @if($trusts)
            <section class="advantages_section association_container">   
                <div class="container">
                    <div class="advantages advantages_cust">
                        <div class="row row-centered">
                        <?php $i=1;  ?>
                        @foreach($trusts as $trust)
                            <div class="col-xs-12 @if($i==2 || $i==5) col-sm-3 @else col-sm-2 @endif col-centered" >
                                <div class="">
                                    <div class="singleAdvantage__img">
                                        
                                        <a href="{{$trust->url}}" target="_blank" title="{{$trust->title}}" @if($i==5) style="width:100%;" @endif>
                                            <img alt="{{$trust->image_alt}}" src="{{asset('image_Association')}}/{{$trust->image_path}}" >
                                        </a>

                                        @if($i==5)
                                            <br>
                                            <p style="text-align:left;">{{$trust->title}}</p>
                                        @endif
                                        
                                    </div>
                                </div>
                            </div>

                        <?php $i++; ?>
                        @endforeach
                        </div>
                    </div>
                </div>
            </section>
            @endif
           <!-- links for different trust associations -->
            
            <div class="aboutHead ourMission">
                <div class="row">

                   <?php echo $ourMission; ?>     

                    <!--<div class="col-sm-6">
                        <div class="aboutImg">
                            <img src="{{asset('/img/about1.png')}}" alt="Our Mission">
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="aboutInfo">
                            <h2 class="lightTitle lightTitle--uppercase lightTitle_align-l">
                                Our Mission
                            </h2>
                            <p class="info-p info-p--biger">
                                We have years of experience within social care, and yet when we needed help from an
                                agency ourselves, we understood why so many people feel frustrated.
                            </p>
                            <p class="info-p  info-p--biger">
                                We understand the difficulties in finding the perfect carer. Holm is committed to
                                improving homecare for everyone. Whether you’re buying care for yourself, a loved one,
                                or providing care, we believe that you should be treated with care and respect. We are
                                committed to making life better and more enjoyable.
                            </p>
                        </div>-->


                    </div>
                </div>
                <div class="xs-new-btn">
                    <a href="{{ route('searchPage') }}" class="new-btn">
                        Find a Carer
                    </a>
                </div>
            </div>
            <div class="row">
                <div class="col-md-offset-2 col-md-8">
                    <div class="founder">
                      
                    <?php echo $founder; ?>

                        <!--<div class="founder__img">
                            <img src="{{asset('/img/founder.jpeg')}}" alt="Founder Nik Seth">
                        </div>
                        <div class="founder__info">
                            <h2 class="bigTitle bigTitle_margin">
                                Nik Seth
                            </h2>
                            <p class="founder__subtitle">
                                Founder
                            </p>
                            <p class="info-p info-p--biger">Nik has years of experience as a registered provider of
                                care. He’s committed his professional life to helping others and making health and
                                social care better for everyone. Nik’s also campaigned for better wages for personal
                                carers, and believes they deserve much more than they are often paid.</p>
                        </div>-->


                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-10 col-sm-offset-1">
                    <div class="aboutFooter">
                        <p class="info-p info-p--biger">
                            We’re always happy to hear from you. Please let us know if we can help.
                        </p>
                        <div class="roundedBtn">
                            <a href="{{route('ContactPage')}}" class="roundedBtn__item roundedBtn__item--contactUs">
                                contact us
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
