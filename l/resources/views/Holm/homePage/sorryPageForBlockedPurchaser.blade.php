<section class="mainSection" style="padding-bottom: 126px;">
    <div class="container">

        <div class="sorry-page">
          <span class="sorry-page__ico">
            <i class="fa fa-ban" aria-hidden="true"></i>
          </span>
            <h2 class="sorry-page__title">
                Sorry!
            </h2>

            <p class="info-p info-p--roboto ">
                Your account was blocked by admin.
            </p>
            <p class="info-p info-p--roboto">
                If you feel we should reconsider this decision, please do feel free to contact us via info@holm.care
            </p>

        </div>
    </div>
</section>



