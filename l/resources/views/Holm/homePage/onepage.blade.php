<section class="mainSection ">

    <div class="container">
        <div class="breadcrumbs">
            <a href="/" class="breadcrumbs__item">
                Home
            </a>
            <span class="breadcrumbs__arrow">&gt;</span>
            <a href="/job" class="breadcrumbs__item">
                Job
            </a>
        </div>
    </div>
    <div class="faqWrap">
        <div class="container">
            <div class="home">
                <div class="home__title">
                    <h1 class="ordinaryTitle faq__title">
                        DOWNLOAD PDF
                    </h1>
                </div>
            </div>
            <div style="text-align: center;">
                <span style="text-align: center">
                    <a style="padding: 5px 10px;" href="{{asset('public/pdf/Holm_A5info_print.pdf')}}">
                        <img src="public\img\PDF_logo.png">
                    </a>
                </span>
            </div>
        </div>
    </div>
</section>
