<section class="mainSection mainSection--blog">
    <div class="globalSearch">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="searchContainer">
                        <h2 class="searchContainer__title">What are you looking for?</h2>
                        <div class="searchContainer__field">
                            <input type="text" class="searchContainer__input" placeholder="Search...">
                            <span class="searchContainer__ico">
                  <i class="fa fa-search"></i>
                </span>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <div class="container">
        <div class="breadcrumbs">
            <a href="{{route('mainHomePage')}}" class="breadcrumbs__item">
                Home
            </a>
            <span class="breadcrumbs__arrow">&gt;</span>
            <a href="{{route('BlogPage')}}" class="breadcrumbs__item">
                Blog
            </a>

        </div>

        <div class="blogWrap">
            <div class="container">
                <div class="row">
                    <div class="col-sm-4 col-md-3">
                        <div class="blogFilter">
                            <h2 class="blogFilter__title">
                                filter
                            </h2>
                            <div class="blogFilter__list">
                                @foreach($blogDate as $date)
                                <a href="{{route('BlogPage')}}/filter/{{$date->month}}-{{$date->year}}"
                                   class="blogFilter__item
@if(isset($month) && isset($year)&& $date->month==$month && $date->year==$year) active
                                           @endif
">
                                    {{$date->cdate}}
                                </a>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-8 col-md-9">
                        <div class="blogposts">
                            @foreach($blog as $single)

                                <div class="singlePost">
                                <div class="singlePost__header">
                                    <h2 class="ordinaryTitle singlePost__title">
                      <span class="ordinaryTitle__text ordinaryTitle--medium">
                      {{$single->title}}
                      </span>
                                    </h2>
                                    <div class="postAuthor">
                                        <div class="profilePhoto postAuthor__photo">
                                            <img src="public/img/founder.jpeg" alt="Founder Nik Seth">
                                        </div>
                                        <h2 class="profileName">
                                            Nik S.
                                        </h2>
                                    </div>
                                </div>
                                <div class="singlePost__content">
                                    <?php echo htmlspecialchars_decode($single->body); ?>
                         
                                    </p>
                                </div>
                                <div class="singlePost__footer">
                                    <div class="roundedBtn">
                                        <a href="{{route('BlogPage')}}/{{$single->slug}}" class="roundedBtn__item
                                        roundedBtn__item--read-more">
                                            read more
                                        </a>
                                    </div>
                                    <span class="postDate">{{$single->getCreated_atAttribute($single->published_at)
                                    }}</span>
                                </div>
                            </div>
                            @endforeach
                        </div>
                        @if(count($blog->items())>0)
                        {{$blog->render()}}
                            @endif
                    </div>
                </div>
            </div>
        </div>

    </div>
</section>
