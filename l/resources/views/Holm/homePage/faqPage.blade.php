
<meta name="keywords" content="homecare" />
<meta name="keywords" content="home care cost" />
<meta name="keywords" content="carer" />
<meta name="keywords" content="care agency" />
<meta name="keywords" content="manchester" />
<meta name="keywords" content="trafford" />
<meta name="keywords" content="salford" />
<meta name="keywords" content="bolton" />
<meta name="keywords" content="bury" />
<meta name="keywords" content="wigan" />
<meta name="keywords" content="stockport" />
<meta name="keywords" content="tameside" />
<meta name="keywords" content="salford" />
<meta name="keywords" content="oldham" />


<section class="mainSection ">

    <div class="container">
        <div class="breadcrumbs">
            <a href="/" class="breadcrumbs__item">
                Home
            </a>
            <span class="breadcrumbs__arrow">&gt;</span>
            <a href="/faq" class="breadcrumbs__item">
                FAQs
            </a>
        </div>
    </div>
    <div class="faqWrap">
        <div class="container">
            <div class="faqContainer">
				
				@if(count($faqs)>0)
					@foreach($faqs as $faq)
				  <div class="faq faq--active">
                      <a href="#" class="faq__link">
                        <h2 class="ordinaryTitle faq__title">
                            <span class="ordinaryTitle__text toggler  ordinaryTitle--medium">
                             {{$faq->title}}
                            </span>
                        </h2>
                        <span class="faq__ico">
                            <i class="fa fa-plus toggler"></i>
                        </span>
                    </a>
                    <div class="faq__content">
						    <?php echo $faq->content; ?>
                     
                    </div>
                </div>
					
					@endforeach
				@endif
				
				
<!--
                <div class="faq faq--active">
                    <a href="#" class="faq__link">
                        <h2 class="ordinaryTitle faq__title">
                            <span class="ordinaryTitle__text toggler  ordinaryTitle--medium">
                             How are you different from a care agency?
                            </span>
                        </h2>
                        <span class="faq__ico">
                            <i class="fa fa-plus toggler"></i>
                        </span>
                    </a>
                    <div class="faq__content">
                        <p>
                            All carers are self employed and decide when they’re available to work. Most agencies have a
                            tight control over their staff, but we impose no restrictions over carers. We simply expect
                            them to offer great care and make sure that people are happy and safe.
                        </p>

                        <p>
                            We are also committed to carers earning a fair living. Most agencies charge a minimum of 80%
                            commission, reducing a carer’s wage. We don’t. We make sure most of what you pay goes to the
                            carer who does the work.
                        </p>
                    </div>
                </div>
                <div class="faq faq--active">
                    <a href="#" class="faq__link">
                        <h2 class="ordinaryTitle faq__title">
                            <span class="ordinaryTitle__text toggler  ordinaryTitle--medium">
                             How does Holm work?
                            </span>
                        </h2>
                        <span class="faq__ico">
                            <i class="fa fa-plus toggler"></i>
                        </span>
                    </a>
                    <div class="faq__content">
                        <p>
                            Holm provides a safe place for families and people looking for care to find individual
                            carers. Holm helps you find quality carers in your area and then contact them directly. You
                            can do this by searching for carers using your postcode and filtering system to make sure
                            you find the carers that fulfil your needs. You can then book and communicate with them
                            through our site. The carer will then confirm the appointment or offer alternatives if
                            they’re busy. The carer will receive all the information they need to help before coming to
                            your home.
                        </p>
                        <p>
                            Once the appointment has been completed, we ask both you and the carer to confirm that
                            everything is satisfactory so that the carer can be paid. If you forget to confirm or
                            decline within 5 days of the appointment, the carer will automatically be paid. Please also
                            leave regular reviews of the carers. Other people in the Holm community value your opinion.
                        </p>
                    </div>
                </div>
                <div class="faq faq--active">
                    <a href="#" class="faq__link">
                        <h2 class="ordinaryTitle faq__title">
                            <span class="ordinaryTitle__text toggler  ordinaryTitle--medium">
                             Do I pick my own carer or do you assign one to me?
                            </span>
                        </h2>
                        <span class="faq__ico">
                            <i class="fa fa-plus toggler"></i>
                        </span>
                    </a>
                    <div class="faq__content">
                        <p>
                            Unlike most care agencies, you can choose the best carers for your needs. If you need any
                            assistance, please do let us know and we’ll be happy to help. The choice is always yours.
                        </p>
                    </div>
                </div>
                <div class="faq faq--active">
                    <a href="#" class="faq__link">
                        <h2 class="ordinaryTitle faq__title">
                            <span class="ordinaryTitle__text toggler  ordinaryTitle--medium">
                                How do I find the right carer?
                            </span>
                        </h2>
                        <span class="faq__ico"><i class="fa fa-plus toggler "></i></span>
                    </a>
                    <div class="faq__content">
                        <p>
                            Enter your postcode on the homepage and you will be provided with a list of carers in your
                            area. You can then use the options on the left of the screen. We also suggest checking other
                            people’s ratings and reviews of the carer. Please don’t forget to leave your own reviews
                            too!
                        </p>
                    </div>
                </div>
                <div class="faq faq--active">
                    <a href="#" class="faq__link">
                        <h2 class="ordinaryTitle faq__title">
                            <span class="ordinaryTitle__text toggler  ordinaryTitle--medium">
                              Can I employ more than one carer?
                            </span>
                        </h2>
                        <span class="faq__ico">
                            <i class="fa fa-plus toggler"></i>
                        </span>
                    </a>
                    <div class="faq__content">
                        <p>
                            Naturally. Carers can’t always be available when you need them and sometimes having two
                            people help at the same time can be beneficial. By using multiple carers, you can ensure you
                            receive great care whenever needed.
                        </p>
                        <p>
                            It also means that you’ll have other trusted carers to rely on if one decides they deserve a
                            well earned break! You can manage all your bookings from your personal profile page.
                        </p>
                    </div>
                </div>
                <div class="faq faq--active">
                    <a href="#" class="faq__link">
                        <h2 class="ordinaryTitle faq__title">
                            <span class="ordinaryTitle__text toggler  ordinaryTitle--medium">
                              Why can’t I find a carer where I live?
                            </span>
                        </h2>
                        <span class="faq__ico">
                            <i class="fa fa-plus toggler"></i>
                        </span>
                    </a>
                    <div class="faq__content">
                        <p>
                            Currently, we only help in the Greater Manchester area. Do please send us an email to <a
                                    href="mailto:info@holm.care">info@holm.care</a> if you think we should help people
                            in your area too.
                        </p>
                    </div>
                </div>
                <div class="faq faq--active">
                    <a href="#" class="faq__link">
                        <h2 class="ordinaryTitle faq__title">
                            <span class="ordinaryTitle__text toggler  ordinaryTitle--medium">
                              How long is a visit?
                            </span>
                        </h2>
                        <span class="faq__ico">
                            <i class="fa fa-plus toggler"></i>
                        </span>
                    </a>
                    <div class="faq__content">
                        <p>
                            We believe that 15 minutes is never enough to say hello, never mind provide great care. That
                            is why all appointments are at least one hour. You can also book longer visits, just use the
                            booking system to ask the carer.
                        </p>
                    </div>
                </div>
                <div class="faq faq--active">
                    <a href="#" class="faq__link">
                        <h2 class="ordinaryTitle faq__title">
                            <span class="ordinaryTitle__text toggler  ordinaryTitle--medium">
                                What is the minimum contract length?
                            </span>
                        </h2>
                        <span class="faq__ico">
                            <i class="fa fa-plus toggler"></i>
                        </span>
                    </a>
                    <div class="faq__content">
                        <p>
                            You can book a carer for just one hour or you can book someone for longer periods on an
                            ongoing contract. If you do have to cancel an appointment, please remember to give the carer
                            at least 24 hours notice. They are planning their lives around yours.
                        </p>
                    </div>
                </div>
                <div class="faq faq--active">
                    <a href="#" class="faq__link">
                        <h2 class="ordinaryTitle faq__title">
                            <span class="ordinaryTitle__text ordinaryTitle--medium">
                              How do I pay?
                            </span>
                        </h2>
                        <span class="faq__ico"><i class="fa fa-plus toggler"></i></span>
                    </a>
                    <div class="faq__content">
                        <p>
                            Holm uses the same secure online payment system as many well known companies. You can use a
                            variety of debit and credit cards as well as PayPal. Your payment details are taken at the
                            time of booking, but payment is only taken once the Carer has confirmed the booking. Your
                            money is held by a safe third party company until both you and the carer have confirmed the
                            visit was completed successfully.
                        </p>
                        <p>
                            The Carer is then paid the following Friday. If you have made an ongoing booking of
                            appointments, then the carer will be paid after the successful completion of each
                            appointment, rather than the completion of the whole booking.
                        </p>
                    </div>

                </div>
                <div class="faq faq--active">
                    <a href="#" class="faq__link">
                        <h2 class="ordinaryTitle faq__title">
                            <span class="ordinaryTitle__text toggler  ordinaryTitle--medium">
                             What is the carer paid?
                            </span>
                        </h2>
                        <span class="faq__ico">
                            <i class="fa fa-plus toggler"></i>
                        </span>
                    </a>
                    <div class="faq__content">
                        <p>
                            A 20% commission is deducted from your payment before being paid to the carer. This is an
                            administrative and introductory fee necessary to ensure we continue to provide this service
                            for all users. Carers are paid an minimum of £10 per hour. This is much better than
                            conventional agencies which often take 100% commission and pay carers less.
                        </p>
                        <p>
                            Carers deserve a real living wage.
                        </p>
                    </div>
                </div>
                <div class="faq faq--active">
                    <a href="#" class="faq__link">
                        <h2 class="ordinaryTitle faq__title">
                            <span class="ordinaryTitle__text toggler  ordinaryTitle--medium">
                                Can I pay carers with cash?
                            </span>
                        </h2>
                        <span class="faq__ico">
                            <i class="fa fa-plus toggler"></i>
                        </span>
                    </a>
                    <div class="faq__content">
                        <p>
                            No, it is against our terms and conditions to pay for carer services outside of Holm. Our
                            payment system offers a safe and convenient system for people to pay and be paid. It relies
                            upon the honesty of all users. Failure to use it could be to the detriment of all other
                            users.
                        </p>
                    </div>
                </div>
                <div class="faq faq--active">
                    <a href="#" class="faq__link">
                        <h2 class="ordinaryTitle faq__title">
                            <span class="ordinaryTitle__text toggler  ordinaryTitle--medium">
                                Can I make alternative arrangements with carers without Holm?
                            </span>
                        </h2>
                        <span class="faq__ico">
                            <i class="fa fa-plus toggler"></i>
                        </span>
                    </a>
                    <div class="faq__content">
                        <p>
                            No, Holm’s service is carefully balanced to ensure a quality service for everyone and makes
                            sure that we can help when needed. Trying to make alternative arrangements jeopardises the
                            carer’s future. People can be excluded from Holm for breaking our Terms and Conditions, as
                            well as having to make additional payments in order to help protect the community.
                        </p>
                    </div>
                </div>
                <div class="faq faq--active">
                    <a href="#" class="faq__link">
                        <h2 class="ordinaryTitle faq__title">
                            <span class="ordinaryTitle__text toggler  ordinaryTitle--medium">
                                What happens if I am not happy with a carer?
                            </span>
                        </h2>
                        <span class="faq__ico">
                            <i class="fa fa-plus toggler"></i>
                        </span>
                    </a>
                    <div class="faq__content">
                        <p>
                            In the unlikely event of a carer providing poor service, please contact Holm straight away.
                            We are here to help and want to resolve any problems you have. We take any allegations of
                            poor care, dishonesty, poor punctuality, incompetence or poor communication very seriously
                            and will take action immediately if you have concerns.
                        </p>
                        <p>
                            Please provide as much information as possible. Holm will use this to help you decide if you
                            need to cancel any ongoing bookings and whether or not you should be reimbursed for poor
                            service.
                        </p>
                        <p>
                            It is just as important that poor care is investigated and good care is rewarded.
                        </p>
                    </div>

                </div>
                <div class="faq faq--active">
                    <a href="#" class="faq__link">
                        <h2 class="ordinaryTitle faq__title">
                            <span class="ordinaryTitle__text toggler  ordinaryTitle--medium">
                                Can I cancel a booking?
                            </span>
                        </h2>
                        <span class="faq__ico">
                            <i class="fa fa-plus toggler"></i>
                        </span>
                    </a>
                    <div class="faq__content">
                        <p>
                            Absolutely! We understand that sometimes you need to make alternative plans. You can cancel
                            single appointments or whole bookings. We ask that you do give at least 24 hours notice
                            though, as Carers do plan their lives to help you. We can waive this rule under exceptional
                            circumstances such as emergency medical treatment. Please do contact us if you need help.
                        </p>
                        <p>
                            In the event of a whole booking being cancelled, we may contact you to see how we can help
                            and to arrange any reimbursements.
                        </p>
                    </div>
                </div>
                <div class="faq faq--active">
                    <a href="#" class="faq__link">
                        <h2 class="ordinaryTitle faq__title">
                            <span class="ordinaryTitle__text toggler  ordinaryTitle--medium">
                                How are carers checked?
                            </span>
                        </h2>
                        <span class="faq__ico">
                            <i class="fa fa-plus toggler"></i>
                        </span>
                    </a>
                    <div class="faq__content">
                        <p>
                            Only the very best carers are able to provide their services on Holm. We use our experience
                            to check their skills and qualifications. We also make sure everyone has a criminal record
                            check. We set the bar very high. Only if carers are good enough to look after our loved
                            ones, are they good enough for yours.
                        </p>
                        <p>
                            Our rating system and your personal feedback also helps ensure that everyone in the
                            community receives great care.
                        </p>
                    </div>
                </div>
                <div class="faq faq--active">
                    <a href="#" class="faq__link">
                        <h2 class="ordinaryTitle faq__title">
                            <span class="ordinaryTitle__text toggler  ordinaryTitle--medium">
                                Is Holm registered with the Care Quality Commission?
                            </span>
                        </h2>
                        <span class="faq__ico">
                            <i class="fa fa-plus toggler"></i>
                        </span>
                    </a>
                    <div class="faq__content">
                        <p>
                            No. Holm is an introductory service and does not provide care. We are therefore not required
                            to register with CQC. Individual self-employed carers do not have to register with CQC
                            either. We do, however, want to ensure the highest levels of care and continuously use your
                            feedback so that only the best carers are available on Holm.
                        </p>
                        <p>
                            According to regulatory guidelines published by the Care Quality Commission, "introductory
                            agencies" who impose no control over ongoing care provision, but introduce carers and
                            provide other ancillary services to facilitate a carer/care seeker relationship, have no
                            statutory requirement to register with the CQC. HomeTouch is therefore unable to offer
                            services such as changes to a care plan, increased frequency or intensity of care visits,
                            back up rotas or otherwise effect control over the delivery of the care by the care worker.
                            Care workers who provide personal care on a self-employed basis also have no requirement to
                            be registered with the CQC providing the services offered do not extend beyond personal
                            care. HomeTouch does however provide a suite of complementary services as part of the care
                            contract that enable a customer to monitor the carer’s performance. HomeTouch does not and
                            is unable to monitor individual care performance.
                        </p>
                    </div>
                </div>
                <div class="faq faq--active">
                    <a href="#" class="faq__link">
                        <h2 class="ordinaryTitle faq__title">
                            <span class="ordinaryTitle__text toggler  ordinaryTitle--medium">
                                What if there is a dispute?
                            </span>
                        </h2>
                        <span class="faq__ico">
                            <i class="fa fa-plus toggler"></i>
                        </span>
                    </a>
                    <div class="faq__content">
                        <p>
                            If in the unfortunate event that there is a dispute, Holm will look to mediate between all
                            parties and resolve the matter as quickly and fairly as possible.
                        </p>
                    </div>
                </div>
                <div class="faq faq--active">
                    <a href="#" class="faq__link">
                        <h2 class="ordinaryTitle faq__title">
                            <span class="ordinaryTitle__text toggler  ordinaryTitle--medium">
                            I can’t find the answer to my question
                            </span>
                        </h2>
                        <span class="faq__ico"><i class="fa fa-plus toggler"></i></span>
                    </a>
                    <div class="faq__content">
                        <p>
                            Please do contact us! We’re always happy to help.
                        </p>
                    </div>
                </div>
                <div class="faq faq--active">
                    <a href="#" class="faq__link">
                        <h2 class="ordinaryTitle faq__title">
                            <span class="ordinaryTitle__text toggler  ordinaryTitle--medium">Who do Carers work for?</span>
                        </h2>
                        <span class="faq__ico"><i class="fa fa-plus toggler"></i></span>
                    </a>
                    <div class="faq__content">
                        <p>All Care Workers listed here are self employed work as contractors for their clients.</p>
                    </div>
                </div>
                <div class="faq faq--active">

                    <a href="#" class="faq__link">
                        <h2 class="ordinaryTitle faq__title">
                            <span class="ordinaryTitle__text toggler  ordinaryTitle--medium">How much does it cost?</span>
                        </h2>
                        <span class="faq__ico"><i class="fa fa-plus toggler"></i></span>
                    </a>
                    <div class="faq__content">
                <p>Great care is priceless. That is why we help keep your costs to a minimum. Unlike most care agencies,
                    we are transparent about the rates and you don’t have to try and negotiate a better deal.
                    Our basic rates are £12 per hour 8am-8pm Monday to Saturday; £14.40 per hour 8pm-8am Monday to
                    Saturday; £18 per hour Sundays and Public Holidays.
                    </div>
                </div>
                <div class="faq faq--active">
                    <a href="#" class="faq__link">
                        <h2 class="ordinaryTitle faq__title">
                            <span class="ordinaryTitle__text toggler  ordinaryTitle--medium">I am a Care Worker. How much do I earn?</span>
                        </h2>
                        <span class="faq__ico"><i class="fa fa-plus toggler"></i></span>
                    </a>
                    <div class="faq__content">
                You’ll be earn far more than you normally would through most care agencies. For a start, each
                appointment is for a minimum of one hour, giving you lots of time to provide great care. Secondly,
                you’ll be able to earn £10 per hour 8am-8pm Monday to Saturday; £12 per hour 8pm-8am Monday to
                Saturday; £15 per hour Sundays and Public Holidays.</p>
                    </div>
                </div>-->

            </div>
        </div>
    </div>

</section>

