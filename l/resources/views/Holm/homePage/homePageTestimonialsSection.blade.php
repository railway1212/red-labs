@if($testimonials)
<section class="testimonialsSection">
    <div class="container">
        <div class="testimonialsSection__title">
            <span class="testimonialsIco">
          <i class="fa fa-quote-left" aria-hidden="true"></i>
        </span>
        </div>

        <div class="sliderContainer">
            <!-- <a href="#theCarousel1" data-slide="prev" class="sliderControl sliderControl--left centeredLink">
                <i class="fa fa-angle-left"></i>
            </a>
            <a href="#theCarousel1" data-slide="next" class="sliderControl sliderControl--right centeredLink">
                <i class="fa fa-angle-right"></i>
            </a> -->
            <div class="testimonialSlider carousel slide multi-item-carousel" id="theCarousel1">
                <div class="carousel-inner carousel-inner1 special-slide">
                    <?php $count =1; ?>
                   @foreach($testimonials as $testimonial)
                    <div class="testimonialSlider__item item @if($count==1) active @endif" id="testimonialSlider__item{{$count}}">
                       <?php  echo $testimonial->description;?>
                    </div>
                    <?php $count++; ?>
                    @endforeach
                   <!-- <div class="testimonialSlider__item item " id="testimonialSlider__item2">
                        <p>
                            I enjoy my work again now that I spend quality time with my clients. No more rushing like a headless chicken.
                        </p>
                    </div>
                    <div class="testimonialSlider__item item " id="testimonialSlider__item3">
                        <p>
                            I’m not always filling out paperwork. It’s feels great!
                        </p>
                    </div>-->
                </div>

            </div>
        </div>


        <div class="carousel slide multi-item-carousel" id="theCarousel1">
            <div class="carousel-inner carousel-inner1">
                <div class="item active">
                    <div class="testimonialsPeople" id="theCarousel_users">
                        <?php $count =1; ?>
                        @foreach($testimonials as $testimonial)
                            <a href="#" class="peopleBox" data-id="{{$count}}">
                                <div class="profilePhoto peopleBox__photo @if($count==1)activeImg @endif ">
                                    <img src="{{asset('public/image_Association')}}/{{$testimonial->image_name}}" alt="{{$testimonial->client_info}}">
                                </div>
                                <div class="peopleBox__info">
                                    <p class="profileName">{{$testimonial->client_name}}</p>
                                    <div class="people_quote">
                                        {{$testimonial->description}}
                                    </div>
                                </div>
                            </a>
                        <?php  $count++; ?>
                        @endforeach


                       <!-- <a href="#" class="peopleBox" data-id="2">
                            <div class="profilePhoto peopleBox__photo">
                                <img src="/img/John_R.png" alt="John R (35 year old man)">
                            </div>
                            <div class="peopleBox__info">
                                <p class="profileName">JOHN R. </p>
                                <div class="people_quote">
                                    I couldn’t wait for an agency. I wanted help as soon as possible. Holm really helped us. Thank you!
                                </div>
                            </div>
                        </a>
                        <a href="#" class="peopleBox" data-id="3">
                            <div class="profilePhoto peopleBox__photo">
                                <img src="/img/Ginger_P.png" alt="Ginger P (40 year old woman)">
                            </div>
                            <div class="peopleBox__info">
                                <p class="profileName">GINGER P.</p>
                                <div class="people_quote">
                                    Holm helped me find great care and we saved so much. Now we can buy more care for our money.
                                </div>
                            </div>
                        </a>-->
                    </div>
                </div>

            </div>
        </div>

    </div>
</section>
@endif
