
<div class="registration">
    <div class="registration__full">

        <p class="info-p info-p--roboto">
            <span class="accent-p">Requirements</span>       </p>
        <p class="info-p info-p--roboto">
            You must meet the following criteria:
        </p>

        <p class="info-p info-p--roboto" style="padding-top: 10px; padding-bottom: 0">- be eligible to work in the UK;</p>
        <p class="info-p info-p--roboto" style="padding: 0">- have a minimum relevant qualification of NVQ Level 2 in Health and Social Care or a Care Certificate;</p>
        <p class="info-p info-p--roboto" style="padding: 0">- a clean DBS record;</p>
        <p class="info-p info-p--roboto" style="padding: 0">- valid proof of ID (such as a passport or driving licence);</p>
        <p class="info-p info-p--roboto" style="padding: 0">- a current CV;</p>
        <p class="info-p info-p--roboto" style="padding-top: 0; padding-bottom: 10px">- two work references.</p>

        <p class="info-p info-p--roboto"s>
            It may prove useful to have all relevant documents to hand, in order to make the application process as quick and easy as possible.
        </p>
        <p class="info-p info-p--roboto"s>
            Do not worry if you don't have everything. You can submit some information later.   </p>




    </div>

</div>
<form id="step" method="POST" action="{{ route('CarerRegistrationPost') }}">
    {{ csrf_field() }}
    <input type="hidden" name="step" value = '3'>
    <input type="hidden" name="carersProfileID" value = {{$carersProfileID}}>
</form>
<div class="registrationBtns registrationBtns--center">

    <a href="next" class="registrationBtns__item"
       onclick="event.preventDefault();document.getElementById('step').submit();"
    >
        OK
    </a>
</div>

