
<section class="mainSection">

     <script>
    $(document).ready(function(){
    @if(Auth::user()->isAdmin())
        $('input,select').removeAttr('data-edit');
    @endif

        var communication = $('select[name=help_with_mobility] option:selected').text();
        if (communication == 'No' || communication == 'Please select'){
            $('select[name="comprehension"]').parent().parent().hide();
            $('select[name="vision"]').parent().parent().hide();
            $('select[name="speech"]').parent().parent().hide();
            $('select[name="hearing"]').parent().parent().hide();
        }

        var mobility = $('select[name=help_with_mobility] option:selected').text();
        if (mobility == 'No' || mobility == 'Please select'){
            $('select[name="mobility_home"]').parent().parent().hide();
            $('select[name="mobility_bed"]').parent().parent().hide();
            $('select[name="history_of_falls"]').parent().parent().hide();
            $('select[name="mobility_shopping"]').parent().parent().hide();
        }

        var hygiene = $('select[name=assistance_with_personal_hygiene] option:selected').text();
        if (hygiene == 'No' || hygiene == 'Please select'){
            $('select[name="appropriate_clothes"]').parent().parent().hide();
            $('select[name="assistance_getting_dressed"]').parent().parent().hide();
            $('select[name="assistance_with_bathing"]').parent().parent().hide();
            $('select[name="managing_toilet_needs"]').parent().parent().hide();
            $('select[name="have_incontinence"]').parent().parent().hide();
        }

        var incontinence = $('select[name=have_incontinence] option:selected').text();
        if (incontinence == 'No' || incontinence == 'Please select'){
            $('select[name="incontinence_wear"]').parent().parent().hide();
            $('select[name="choosing_incontinence_products"]').parent().parent().hide();
            $('textarea[name="incontinence_products_stored"]').parent().parent().hide();
            $('textarea[name="kind_of_incontinence"]').parent().hide();
        }
    });
     </script>
    <div class="container carer-profile">

        @include(config('settings.frontTheme').'.serviceUserProfiles/PrivateProfileMainSectionHeader')
        @include(config('settings.frontTheme').'.serviceUserProfiles/PrivateProfileMainSectionGeneral')
        @include(config('settings.frontTheme').'.serviceUserProfiles/PrivateProfileMainSectionLanguages')
        @include(config('settings.frontTheme').'.serviceUserProfiles/PrivateProfileMainSectionHome')
        @include(config('settings.frontTheme').'.serviceUserProfiles/PrivateProfileMainSectionTypeOfCare')
        @include(config('settings.frontTheme').'.serviceUserProfiles/PrivateProfileMainSectionTimeWhenCareNeed')
        @include(config('settings.frontTheme').'.serviceUserProfiles/PrivateProfileMainSectionHealth')

        @include(config('settings.frontTheme').'.serviceUserProfiles/PrivateProfileMainSectionBehaviour')
        @include(config('settings.frontTheme').'.serviceUserProfiles/PrivateProfileMainSectionNightTime')

        @include(config('settings.frontTheme').'.serviceUserProfiles/PrivateProfileMainSectionOther')

    </div>
</section>

