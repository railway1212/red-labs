<section class="mainSection">
    <div class="container">
        <div class="breadcrumbs">
            <a href="{{route('mainHomePage')}}" class="breadcrumbs__item">Home</a>
            <span class="breadcrumbs__arrow">></span>
            <a href="{{route('thankForInvite')}}" class="breadcrumbs__item">Thank you</a>
        </div>
        <div class="thank">
            <h2 class="thank__title">Thank you!</h2>
            <span class="successIco"><i class="fa fa-check" aria-hidden="true"></i></span>
            <p class="info-p">Your invite was successfully sent.</p>
        </div>
    </div>
</section>