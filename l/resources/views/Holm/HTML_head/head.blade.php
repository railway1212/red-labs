<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="keywords" content="{{$keywords??''}}" />
    <meta name="description" content="{{$description??''}}" />
     {{--<meta name="description"
          content="@yield('description','Homecare for the Elderly. Helping you find the best affordable care at home quickly and easily in Manchester, Bolton, Sale, Rochdale, Stockport, Salford, Oldham, Bury, Tameside, Trafford and Wigan.')"/> --}}

    <meta property="fb:app_id" content="1401488693436528">
    <meta property="og:image" content="{{asset('img/favicon.png', env('REDIRECT_HTTPS'))}}" />
    <meta property="og:site_name" content="Holm" />
    <meta property="og:type"  content="website" />
    <meta property="og:title" content="{{$title}}"/>
    <meta property="og:url" content="http://holm.care/" />
    <meta property="og:description" content="{{$description??''}}"/>
    <meta name="publisher" content="https://plus.google.com/u/0/111358834959511346157 "/>      
	<meta name="msvalidate.01" content="BF2404E4D4BD7C0592406F98AFB808DA" />

	<link rel="canonical" href="{{ URL::current() }}" />
    <link rel="icon" type="image/png" href="{{asset('favicon.png', env('REDIRECT_HTTPS'))}}">
    <title>{{$title}}</title>
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800" rel="stylesheet">
    <link href="{{asset('public/css/default.css', env('REDIRECT_HTTPS'))}}" rel="stylesheet">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link rel="stylesheet" href="{{asset('public/css/bootstrap.min.css', env('REDIRECT_HTTPS'))}}">
    <link rel="stylesheet" href="{{asset('public/css/main.min.css', env('REDIRECT_HTTPS'))}}">
    <link rel="stylesheet" href="{{asset('public/css/customize.css', env('REDIRECT_HTTPS'))}}">
    <link rel="stylesheet" href="{{asset('public/css/main2.min.css', env('REDIRECT_HTTPS'))}}">
    <link rel="stylesheet" href="{{asset('public/css/cupertino/jquery-ui.min.css', env('REDIRECT_HTTPS'))}}">
    <link rel="stylesheet" href="{{asset('public/css/owl.carousel.css', env('REDIRECT_HTTPS'))}}">
    <link rel="stylesheet" href="{{asset('public/css/owl.theme.default.min.css', env('REDIRECT_HTTPS'))}}">
    <link rel="stylesheet" href="{{asset('public/css/carousel/carousel.css', env('REDIRECT_HTTPS'))}}">
    <link rel="stylesheet" href="{{asset('public/css/jquery.timepicker.min.css', env('REDIRECT_HTTPS'))}}">


    @if((Auth::check() && Auth::user()->isAdmin()==true))
        <script>var is_admin = 1; </script>
    @else
        <script>var is_admin = 0; </script>
    @endif

    <script src="{{asset('public/js/jquery.min.js', env('REDIRECT_HTTPS'))}}"></script>
    <script src="{{asset('public/js/checkCookie.js', env('REDIRECT_HTTPS'))}}"></script>
    <script type="text/javascript" src="https://cdn.ywxi.net/js/1.js" async></script>
    <script src="{{asset('public/js/moment.min.js', env('REDIRECT_HTTPS'))}}"></script>

	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-NL7LFFB');</script>
	<!-- End Google Tag Manager -->
