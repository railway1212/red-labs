<div class="mainPanel">
    <h2 class="categoryTitle">
          <span class="categoryTitle__ico">
            <i class="fa fa-newspaper-o" aria-hidden="true"></i>
          </span>
        FAQs
    </h2>
    <div class="settingBtn">
        <a href="{{ route('faq.create') }}"
           class="actionsBtn actionsBtn--accept actionsBtn--big actionsBtn--no-centered">
            Create FAQ
        </a>

    </div>
    <div class="tableWrap tableWrap--margin-t">
        <table class="adminTable">
            <thead>
            <tr>
                <td class="orderNumber">
                    <span class="td-title td-title--number">
                     №
                    </span>
                </td>

                <td class=" ordninary-td ">
                    <span class="td-title td-title--title">
                    title
                    </span>

                </td>
                <td class=" ordninary-td  ">
                    <span class="td-title td-title--time">
                      date
                    </span>
                </td>
                <td class=" ordninary-td ">
                    <span class="td-title td-title--actions">
                      actions
                    </span>
                </td>
                <td class=" ordninary-td ">
                    <span class="td-title td-title--status">
                    status
                    </span>
                </td>
                
                
            </tr>
            </thead>

            <tbody>
            @foreach($faqs as $faq)
                <tr>
                    <td>
                        <span>{{ $faq->faq_order }}</span>
                    </td>

                    <td class="">
                        <span>{{ $faq->title }}</span>
                    </td>
                    <td>
                        <span>{{ $faq->created_at }}</span>
                    </td>
                    <td>
                        <div class="actionsGroup">
                            @if($faq->status == 'published')
                                <a href="{{ route('faq.edit', $faq->id) }}" class="actionsBtn actionsBtn--edit  ">
                                    edit
                                </a>
                                <a data-delete_faq_id="{{ $faq->id }}"
                                   class="actionsBtn actionsBtn--delete deleteFaq">
                                    delete
                                </a>
                            @elseif($faq->status == 'deleted')
                                <a data-publish_faq_id="{{ $faq->id }}"
                                   class="actionsBtn actionsBtn--accept publishFaq">
                                    publish
                                </a>
                                <a href="{{ route('faq.edit', $faq->id) }}" class="actionsBtn actionsBtn--edit  ">
                                    edit
                                </a>
                            @endif
                        </div>
                    </td>
                    <td>
                        <div class="profStatus profStatus--left">
                            @if($faq->status == 'published')
                                <span class="profStatus__item profStatus__item--new">published</span>
                            @elseif($faq->status == 'deleted')
                                <span class="profStatus__item profStatus__item--canceled"> deleted</span>
                            @endif
                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
   

    
    
    

<script>
    $(document).ready(function () {
        $('.adminTable').on('click', '.deleteFaq', function (e) {
            e.preventDefault();
            var faq_id = $(this).attr('data-delete_faq_id');
            $.ajax({
                url: '/admin/faq/' + faq_id,
                type: 'PUT',
                data: {'delete_faq_id': faq_id},
                success: function () {
                    location.reload();
                },
                error: function () {
                    console.log('fail');
                }
            });
        });
        
        $('.adminTable').on('click', '.publishFaq', function (e) {
            e.preventDefault();
            var faq_id = $(this).attr('data-publish_faq_id');
            $.ajax({
                url: '/admin/faq/' + faq_id,
                type: 'PUT',
                data: {'publish_faq_id': faq_id},
                success: function () {
                    location.reload();
                },
                error: function () {
                    console.log('fail');
                }
            });
        });

    
    });
</script>
