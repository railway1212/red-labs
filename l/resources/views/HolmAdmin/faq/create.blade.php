<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.5/css/bootstrap.min.css">
<link rel="stylesheet" href="http://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="http://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js"></script>



<div class="mainPanel">
    <h2 class="categoryTitle">
          <span class="categoryTitle__ico">
            <i class="fa fa-newspaper-o" aria-hidden="true"></i>
          </span>
        FAQ -> Create
    </h2>
    <div class="contentCard">
        <div class="contentCard__head contentCard__head--payment">
            <h2>FAQ</h2>
        </div>
        <form role="form" action="{{ route('faq.store') }}" method="post">
            <div class="contentBody">
				
				
                <div class="fieldRow">
                    <div class="formField formField--full ">
                        <h2 class="fieldLabel">Title</h2>
                        <div class="fieldWrap">
                            <input type="text" name="title" class="formItem formItem--without-ico"
                                   placeholder="Enter the title">
                        </div>
                    </div>
                </div>

                <div class="fieldRow">
                    <div class="formField formField--full ">
                        <h2 class="fieldLabel">Order</h2>
                        <div class="fieldWrap">
                            <input name="faq_order" type="number" class="formItem formItem--without-ico"
                                   placeholder="Enter the order" min="1">
                        </div>
                    </div>
                </div>
                
                <div class="fieldRow">
                    <div class="formField  formField--full">
                        <h2 class="fieldLabel">text</h2>
                        <div class="fieldWrap">       
                              <textarea id="content" name="content"></textarea>
							   @ckeditor('content', ['height' => 300])                    
                        </div>
                    </div>
                </div>

             

                <div class="settingBtn">
                    <input type="submit" class="actionsBtn actionsBtn--accept actionsBtn--big actionsBtn--no-centered"
                           value="Create">
                </div>
            </div>
        </form>
    </div>
</div>
<!-- Select2 -->
<script src="{{ asset('bower_components/select2/dist/js/select2.full.min.js') }}"></script>
<script>
    //Initialize Select2 Elements
    $('.select2').select2()
</script>
<!-- Bootstrap WYSIHTML5 -->
<script src="{{ asset('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}"></script>
<script>
    $(function () {
        //bootstrap WYSIHTML5 - text editor
        $('.textarea').wysihtml5()
    })
</script>
<script type="text/javascript">

		$(".tm-input").tagsManager();

	</script>
