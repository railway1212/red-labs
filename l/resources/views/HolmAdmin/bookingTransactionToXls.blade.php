<div class="mainPanel">
    <div class="statisticGroup">
        <div class="statisticBox">
            <div class="tableWrap">
				
				
                <div class="statisticHead">
                    <h2 class="statisticHead__title">
                  <span class="statisticHead__ico">
                    <i class="fa fa-area-chart" aria-hidden="true"></i>
                  </span>
                        BOOKINGS TRANSACTIONS DETAILS
                    </h2>
                </div>
                
                <table class="statisticTable">
                    <thead>	
                    <tr>
                        <td class="textCenter">
						  <span class="ordinaryTitle td-stat">
						   Transaction ID
						  </span>
                        </td>
                        <td class=" textCenter">
						  <span class="ordinaryTitle td-stat">
						   Date and Time  
						  </span>
                        </td>
                        <td class="textCenter" colspan="2">
						  <span class="ordinaryTitle td-stat">
							Purchaser  
						  </span>
                        </td>
                        <td class="textCenter" colspan="2">
						  <span class="ordinaryTitle td-stat" >
							Carer   
						  </span>
                        </td>
                        <td class="textCenter" colspan="2">
						  <span class="ordinaryTitle td-stat">
							Booking    
						  </span>
                        </td>
                        <td class="textCenter">
						  <span class="ordinaryTitle td-stat">
							Payment Type     
						  </span>
                        </td>
                        <td class="textCenter">
						  <span class="ordinaryTitle td-stat">
							Transaction Status    
						  </span>
                        </td>
                        
                        
                    </tr>
                    <tr class="extra-tr">
						
						<td class="idField">
							<span class="extraTitle"></span>
						</td>
						<td class="idField">
							<span class="extraTitle"></span>
						</td>
						
						
						<td class="idField">
							<span class="extraTitle">ID</span>
						</td>
						
						<td class="">
							<span class="extraTitle">NAME</span>
						</td>
				  

					
						<td class="idField">
							<span class="extraTitle">ID</span>
						</td>
						<td class="">
							<span class="extraTitle">NAME</span>
						</td>
									
						
						<td class="idField">
							<span class="extraTitle">ID</span>
						</td>
						<td class="">
							<span class="extraTitle">AMOUNT</span>
						</td>
								
						<td class="idField">
							<span class="extraTitle"></span>
						</td> 
						<td class="idField">
							<span class="extraTitle"></span>
						</td> 
					</tr>
                    </thead>
                    <tbody>
					@if($transactions->count() > 0)
						@foreach($transactions as $transaction)
						
							  <tr>
								<td align="center">
								  <span>
									{{$transaction->id}}
								  </span>

								</td>
								<td>
									<span>{{$transaction->created_at->format("H:i")}}</span>
									<p>{{$transaction->created_at->format("d-m-Y")}}</p>
								</td>
								
								<td class="idField">
									<span>{{($transaction->booking->bookingPurchaser->id)}}</span>
								</td>
								<td class="">

									<a href="#" class="tableLink">{{($transaction->booking->bookingPurchaserProfile->full_name)}}</a>
								</td>

								<td class="idField">
									<span>{{($transaction->booking->bookingCarer->id)}}</span>
								</td>
								<td class="">
									<a href="#" class="tableLink">{{($transaction->booking->bookingCarerProfile->full_name)??''}}</a>
								</td>

								<td class="idField">
									<span><a href="{{url('/l/bookings/'.$transaction->booking->id.'/details')}}" target="_blank">{{$transaction->booking->id}}</a></span>
								</td>
								<td class="">
									<span><i class="fa fa-gbp" aria-hidden="true"></i> {{$transaction->amount}}</span>
								</td>

									
								<td>
								  <span>
									@if($transaction->booking->payment_method == 'credit_card')
										Credit Card
									@elseif($transaction->booking->payment_method == 'bonus_wallet')
										Bonuses Wallet
									@endif
								  </span>
								</td>
								<td>
									<div class="profStatus profStatus--left">
										<span class="profStatus__item profStatus__item--complete">  completed</span>
									</div>
								</td>
							</tr>
						@endforeach
				    @else
						<tr>
							<td colspan="7" align="center">
								-
							</td>
						</tr>
					@endif
                    </tbody>
                </table>

            </div>
        </div>

</div>
</div>        
        
        
      
