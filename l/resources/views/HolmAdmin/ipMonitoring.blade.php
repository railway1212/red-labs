<div class="mainPanel">
    <h2 class="categoryTitle">
          <span class="categoryTitle__ico">
              <i class="fa fa-globe" aria-hidden="true"></i>
          </span>
        IP Monitoring
    </h2>
    
    <div class="panelHead">
       <div class="panelHead__group">
			{!! Form::open(['method'=>'GET','route'=>'IpMonitoring']) !!}
	
			<div class="filterBox">
				<div class="formField">
					{!! Form::text('dateFrom', null,['class'=>'formItem formItem--input formItem--search datepicker','placeholder'=>'From Date']) !!}
				</div>

				<div class="formField">
					{!! Form::text('dateTo', null,['class'=>'formItem formItem--input formItem--search datepicker','placeholder'=>'Date To']) !!}
				</div>

				{!! Form::submit('filter',['class'=>'actionsBtn actionsBtn--filter actionsBtn--bigger']) !!}
			</div>
			{!! Form::close()!!}
        </div>
	</div>
	
	<div class="tableWrap tableWrap--margin-t">
	 <table class="adminTable">
	    <thead>
			<tr>
			   <td class="orderNumber">
					  <span class="td-title td-title--number">
					  IP Address
					  </span>
				</td>
				<td class=" ordninary-td ordninary-td--wider no-padding-l">
					  <span class="td-title td-title--green">
					  Location
					  </span>

				</td>
				<td class="ordninary-td ordninary-td--wider">
					  <span class="td-title td-title--darkest-mint ">
						Last logged in 
					  </span>
				</td>
			</tr>
        </thead>

		<tbody>
		@if($items)
		@foreach($items as $item)
		  <tr>
			<td align="center"><span> {{$item->ip_address}} </span></td>
			<td align="center"><span> {{$item->city}}, {{$item->state}}, {{$item->country}} - {{$item->zip}} </span></td>
			<td align="center"><span> {{date("d-m-Y", strtotime($item->created_at))}} {{date("H:i", strtotime($item->created_at))}}</span></td>
			
		  </tr>
		
		@endforeach
		@else
		  <tr>
			  <td colspan="3" align="center">Please select Date.</td>
		  </tr>
		@endif

		</tbody>
	</table>
	</div>
	
</div>


 <!--Script Datepicker-->
    <script>
        $(document).ready(function(){

            $(".datepicker").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: "dd-mm-yy",
                showAnim: "slideDown"
            });
        });
    </script>
    <!-- End Script Datepicker-->

