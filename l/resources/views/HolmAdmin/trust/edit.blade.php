<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.5/css/bootstrap.min.css">
<link rel="stylesheet" href="http://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="http://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js"></script>


<div class="mainPanel">
    <h2 class="categoryTitle">
          <span class="categoryTitle__ico">
            <i class="fa fa-newspaper-o" aria-hidden="true"></i>
          </span>
        Trust -> Edit
    </h2>
    <div class="contentCard">
        <div class="contentCard__head contentCard__head--payment">
            <h2>Trust</h2>
        </div>
        <form role="form" action="{{ route('trust.update', $trust->id) }}" method="post" id="trustImageUpload" enctype="multipart/form-data">
            {{ csrf_field() }}
            {{ method_field('PATCH') }}
            <div class="contentBody">
                <div class="fieldRow">
                    <div class="formField formField--full ">
                        <h2 class="fieldLabel">Title</h2>
                        <div class="fieldWrap">
                            <input name="title" type="text" class="formItem formItem--without-ico"
                                   placeholder="Enter the title" value="{{ $trust->title }}">
                        </div>
                    </div>
                </div>

                <div class="fieldRow">
                    <div class="formField formField--full ">
                        <h2 class="fieldLabel">URL</h2>
                        <div class="fieldWrap">
                            <input name="url" type="text" class="formItem formItem--without-ico"
                                   placeholder="Enter the URL" value="{{ $trust->url }}">
                        </div>
                    </div>
                </div>
 
                <div class="fieldRow">
                    <div class="formField  formField--full">
                        <h2 class="fieldLabel">Image</h2>
                        <div class="fieldWrap">  
                            <input type="file" name="trustImage" /><br>
                          	 @if(!empty($trust->image_path))
                               <img src="{{URL::to('/')}}/public/image_Association/{{$trust->image_path}}" width="100"/><br>
                             @endif
                             <input type="text" name="image_alt" placeholder="Image alt text" value="{{$trust->image_alt}}"/>
                        </div>
                    </div>
                </div>

                <div class="settingBtn">
                    <input type="submit" class="actionsBtn actionsBtn--accept actionsBtn--big actionsBtn--no-centered"
                           value="update">
                </div>
            </div>
        </form>
    </div>
</div>
<script>
$(document).ready(function(){
	
		   
        var idLoadFiles = '#trustImageUpload';
        
	    $(idLoadFiles).find('.pickfiles').attr("disabled", false);
        $(idLoadFiles).find('.pickfiles-change').attr("disabled", false);
        $(idLoadFiles).find('.pickfiles_profile_photo--change').attr("disabled", false);
        $(idLoadFiles).find('.pickfiles_profile_photo_service_user--change').attr("disabled", false);
        $(idLoadFiles).find('.pickfiles-delete').attr("style", 'display: block');
        $(idLoadFiles).find('.addInfo__input-ford').attr("disabled", false).removeClass('profileField__input--greyBg');
        $(idLoadFiles).find('.addInfo__input-ford').attr("readonly", false).removeClass('profileField__input--greyBg');



});

</script>
