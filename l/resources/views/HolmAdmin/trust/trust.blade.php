<div class="mainPanel">
    <h2 class="categoryTitle">
          <span class="categoryTitle__ico">
            <i class="fa fa-newspaper-o" aria-hidden="true"></i>
          </span>
        Trusts
    </h2>
    <div class="settingBtn">
        <a href="{{ route('trust.create') }}"
           class="actionsBtn actionsBtn--accept actionsBtn--big actionsBtn--no-centered">
            Create Trust
        </a>

    </div>
    <div class="tableWrap tableWrap--margin-t">
        <table class="adminTable">
            <thead>
            <tr>
                <td class="orderNumber">
                    <span class="td-title td-title--number">
                     №
                    </span>
                </td>

                <td class=" ordninary-td ">
                    <span class="td-title td-title--title">
                    title
                    </span>

                </td>
                <td class=" ordninary-td  ">
                    <span class="td-title td-title--time">
                      Image
                    </span>
                </td>
                <td class=" ordninary-td ">
                    <span class="td-title td-title--actions">
                      actions
                    </span>
                </td>
                <td class=" ordninary-td ">
                    <span class="td-title td-title--status">
                    Status
                    </span>
                </td>
                
                
            </tr>
            </thead>

            <tbody>
            @foreach($trusts as $trust)
                <tr>
                    <td>
                        <span>{{ $trust->id }}</span>
                    </td>

                    <td class="">
                        <span>{{ $trust->title }}</span>
                    </td>
                    <td>
                       <div class="profStatus profStatus--left">
                            @if(!empty($trust->image_path))
                               <img src="{{URL::to('/')}}/public/image_Association/{{$trust->image_path}}" width="100"/>
                            @endif
                        </div>
                    </td>
                    <td>
                        <div class="actionsGroup">
                             @if($trust->status == 'published')
                                <a href="{{ route('trust.edit', $trust->id) }}" class="actionsBtn actionsBtn--edit  ">
                                    edit
                                </a>
                                <a data-delete_trust_id="{{ $trust->id }}"
                                   class="actionsBtn actionsBtn--delete deleteTrust">
                                    delete
                                </a>
                            @elseif($trust->status == 'deleted')
                                <a data-publish_trust_id="{{ $trust->id }}"
                                   class="actionsBtn actionsBtn--accept publishTrust">
                                    publish
                                </a>
                                <a href="{{ route('trust.edit', $trust->id) }}" class="actionsBtn actionsBtn--edit  ">
                                    edit
                                </a>
                            @endif
                        </div>
                    </td>
                    <td>
						@if($trust->status == 'published')
							<span class="profStatus__item profStatus__item--new">published</span>
						@elseif($trust->status == 'deleted')
							<span class="profStatus__item profStatus__item--canceled"> deleted</span>
						@endif
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
   

    
    
    

<script>
    $(document).ready(function () {
        $('.adminTable').on('click', '.deleteTrust', function (e) {
            e.preventDefault();
            var trust_id = $(this).attr('data-delete_trust_id');
            $.ajax({
                url: '/l/admin/trust/' + trust_id,
                type: 'PUT',
                data: {'delete_trust_id': trust_id},
                success: function () {
                    location.reload();
                },
                error: function () {
                    console.log('fail');
                }
            });
        });
        
        $('.adminTable').on('click', '.publishTrust', function (e) {
            e.preventDefault();
            var trust_id = $(this).attr('data-publish_trust_id');
            $.ajax({
                url: '/l/admin/trust/' + trust_id,
                type: 'PUT',
                data: {'publish_trust_id': trust_id},
                success: function () {
                    location.reload();
                },
                error: function () {
                    console.log('fail');
                }
            });
        });

    
    });
</script>
