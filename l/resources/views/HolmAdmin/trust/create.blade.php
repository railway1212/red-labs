<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.5/css/bootstrap.min.css">
<link rel="stylesheet" href="http://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="http://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js"></script>



<div class="mainPanel">
    <h2 class="categoryTitle">
          <span class="categoryTitle__ico">
            <i class="fa fa-newspaper-o" aria-hidden="true"></i>
          </span>
        Trust -> Create
    </h2>
    <div class="contentCard">
        <div class="contentCard__head contentCard__head--payment">
            <h2>Trust</h2>
        </div>
        <form role="form" action="{{ route('trust.store') }}" method="post" enctype="multipart/form-data">
            <div class="contentBody">
				
				
                <div class="fieldRow">
                    <div class="formField formField--full ">
                        <h2 class="fieldLabel">Title</h2>
                        <div class="fieldWrap">
                            <input type="text" name="title" class="formItem formItem--without-ico"
                                   placeholder="Enter the title">
                        </div>
                    </div>
                </div>

                <div class="fieldRow">
                    <div class="formField formField--full ">
                        <h2 class="fieldLabel">URL</h2>
                        <div class="fieldWrap">
                            <input name="url" type="text" class="formItem formItem--without-ico"
                                   placeholder="Enter the URL">
                        </div>
                    </div>
                </div>
                
                <div class="fieldRow">
                    <div class="formField  formField--full">
                        <h2 class="fieldLabel">Image</h2>
                        <div class="fieldWrap">  
                            <input type="file" name="trustImage" /><br>
                            <input type="text" name="image_alt" placeholder="Image alt text"/>
                        </div>
                    </div>
                </div>

                <div class="settingBtn">
                    <input type="submit" class="actionsBtn actionsBtn--accept actionsBtn--big actionsBtn--no-centered"
                           value="Create">
                </div>
            </div>
        </form>
    </div>
</div>
