<div class="mainPanel">
    <h2 class="categoryTitle">
          <span class="categoryTitle__ico">
            <i class="fa fa-newspaper-o" aria-hidden="true"></i>
          </span>
        Blog
    </h2>
    <div class="settingBtn">
        <a href="{{ route('blog.create') }}"
           class="actionsBtn actionsBtn--accept actionsBtn--big actionsBtn--no-centered">
            create blogpost
        </a>
        {{--<br>--}}
        {{--<a href="{{ route('tags.index') }}"--}}
        {{--class="actionsBtn actionsBtn--accept actionsBtn--big actionsBtn--no-centered">--}}
        {{--tags--}}
        {{--</a>--}}
    </div>
    <div class="tableWrap tableWrap--margin-t">
        <table class="adminTable">
            <thead>
            <tr>
                <td class="orderNumber">
                    <span class="td-title td-title--number">
                     №
                    </span>
                </td>


                <td class=" ordninary-td ordninary-td--big ">
                    <span class="td-title td-title--title">
                    title
                    </span>

                </td>
                <td class=" ordninary-td  ">
                    <span class="td-title td-title--time">
                      date
                    </span>
                </td>
                <td class=" ordninary-td  ">
                    <span class="td-title td-title--time">
                      date of publishing
                    </span>
                </td>
                <td class=" ordninary-td      ">
                    <span class="td-title td-title--author">
                      author
                    </span>
                </td>
                {{--<td class=" ordninary-td      ">--}}
                {{--<span class="td-title td-title--tags">--}}
                {{--tags--}}
                {{--</span>--}}
                {{--</td>--}}
                <td class=" ordninary-td ">
                    <span class="td-title td-title--actions">
                      actions
                    </span>
                </td>
                <td class=" ordninary-td ">
                    <span class="td-title td-title--status">
                    status
                    </span>
                </td>
            </tr>
            </thead>

            <tbody>
            @foreach($blogs as $blog)
                <tr>
                    <td>
                        <span>{{ $blog->id }}</span>
                    </td>

                    <td class="">
                        <span>{{ $blog->title }}</span>
                    </td>
                    <td>
                        <span>{{ $blog->created_at }}</span>
                    </td>
                    <td>
                        @if($blog->published_at !== null)
                            <span>{{ $blog->published_at }}</span>
                        @else
                            <span>not published</span>
                        @endif
                    </td>
                    <td>
                        @if($blog->user_id == 290)
                            <span>NIK SETH</span>
                        @endif
                    </td>
                    {{--<td>--}}
                        {{--<span>{{ $blog->tags->name }}</span>--}}
                    {{--</td>--}}
                    <td>
                        <div class="actionsGroup">
                            @if($blog->status == 'published')
                                <a href="{{ route('blog.edit', $blog->id) }}" class="actionsBtn actionsBtn--edit  ">
                                    edit
                                </a>
                                <a data-delete_blog_id="{{ $blog->id }}"
                                   class="actionsBtn actionsBtn--delete deleteBlog">
                                    delete
                                </a>
                            @elseif($blog->status == 'not published')
                                <a data-publish_blog_id="{{ $blog->id }}"
                                   class="actionsBtn actionsBtn--accept publishBlog">
                                    publish
                                </a>
                                <a href="{{ route('blog.edit', $blog->id) }}" class="actionsBtn actionsBtn--edit  ">
                                    edit
                                </a>
                                <a data-delete_blog_id="{{ $blog->id }}"
                                   class="actionsBtn actionsBtn--delete deleteBlog">
                                    delete
                                </a>
                            @elseif($blog->status == 'deleted')
                                <a data-publish_blog_id="{{ $blog->id }}"
                                   class="actionsBtn actionsBtn--accept publishBlog">
                                    publish
                                </a>
                                <a href="{{ route('blog.edit', $blog->id) }}" class="actionsBtn actionsBtn--edit  ">
                                    edit
                                </a>
                            @endif
                        </div>
                    </td>
                    <td>
                        <div class="profStatus profStatus--left">
                            @if($blog->status == 'published')
                                <span class="profStatus__item profStatus__item--new">published</span>
                            @elseif($blog->status == 'not published')
                                <span class="profStatus__item profStatus__item--not-publish"> not published</span>
                            @elseif($blog->status == 'deleted')
                                <span class="profStatus__item profStatus__item--canceled"> deleted</span>
                            @endif
                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    {{$blogs->render()}}
<!-- Modal (Pop up when detail button clicked) -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">×</span></button>
                    <h4 class="modal-title" id="myModalLabel">Task Editor</h4>
                </div>
                <div class="modal-body">
                    <form id="frmTasks" name="frmTasks" class="form-horizontal" novalidate="">

                        <div class="form-group error">
                            <label for="inputTask" class="col-sm-3 control-label">Task</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control has-error" id="task" name="task"
                                       placeholder="Task" value="">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-3 control-label">Description</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="description" name="description"
                                       placeholder="Description" value="">
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="btn-save" value="add">Save changes</button>
                    <input type="hidden" id="task_id" name="task_id" value="0">
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('.adminTable').on('click', '.deleteBlog', function (e) {
            e.preventDefault();
            var blog_id = $(this).attr('data-delete_blog_id');
            $.ajax({
                url: '/l/admin/blog/' + blog_id,
                type: 'PUT',
                data: {'delete_blog_id': blog_id},
                success: function () {
                    location.reload();
                },
                error: function () {
                    console.log('fail');
                }
            });
        });

        $('.adminTable').on('click', '.publishBlog', function (e) {
            e.preventDefault();
            var blog_id = $(this).attr('data-publish_blog_id');
            $.ajax({
                url: '/l/admin/blog/' + blog_id,
                type: 'PUT',
                data: {'publish_blog_id': blog_id},
                success: function () {
                    location.reload();
                },
                error: function () {
                    console.log('fail');
                }
            });
        });
    });
</script>
{{--<script>--}}
{{--$(document).ready(function () {--}}
{{--var url = "/ajax-crud/public/tasks";--}}

{{--//display modal form for task editing--}}
{{--$('.open-modal').click(function(){--}}
{{--var task_id = $(this).val();--}}

{{--$.get(url + '/' + task_id, function (data) {--}}
{{--//success data--}}
{{--console.log(data);--}}
{{--$('#task_id').val(data.id);--}}
{{--$('#task').val(data.task);--}}
{{--$('#description').val(data.description);--}}
{{--$('#btn-save').val("update");--}}

{{--$('#myModal').modal('show');--}}
{{--})--}}
{{--});--}}

{{--//display modal form for creating new task--}}
{{--$('#btn-add').click(function(){--}}
{{--$('#btn-save').val("add");--}}
{{--$('#frmTasks').trigger("reset");--}}
{{--$('#myModal').modal('show');--}}
{{--});--}}
{{--});--}}
{{--</script>--}}
