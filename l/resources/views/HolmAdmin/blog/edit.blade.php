<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.5/css/bootstrap.min.css">
<link rel="stylesheet" href="http://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="http://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js"></script>


<div class="mainPanel">
    <h2 class="categoryTitle">
          <span class="categoryTitle__ico">
            <i class="fa fa-newspaper-o" aria-hidden="true"></i>
          </span>
        Blog -> Edit
    </h2>
    <div class="contentCard">
        <div class="contentCard__head contentCard__head--payment">
            <h2>Blog</h2>
        </div>
        <form role="form" action="{{ route('blog.update', $blog->id) }}" method="post">
            {{ csrf_field() }}
            {{ method_field('PATCH') }}
            <div class="contentBody">
                <div class="fieldRow">
                    <div class="formField formField--full ">
                        <h2 class="fieldLabel">Title</h2>
                        <div class="fieldWrap">
                            <input name="title" type="text" class="formItem formItem--without-ico"
                                   placeholder="Enter the title" value="{{ $blog->title }}">
                        </div>
                    </div>
                </div>
                
                 <div class="fieldRow">
                    <div class="formField formField--full ">
                        <h2 class="fieldLabel">Slug</h2>
                        <div class="fieldWrap">
                            <input type="text" name="slug" class="formItem formItem--without-ico"
                                   placeholder="Enter the slug" value="{{$blog->slug}}">
                        </div>
                    </div>
                </div>
                
                
                <div class="fieldRow">
                    <div class="formField  formField--full">
                        <h2 class="fieldLabel">text</h2>
                        <div class="fieldWrap">
                            {{--<textarea class="formItem formItem--area" placeholder="Enter the text..."></textarea>--}}

<!--
                            <textarea name="body" class="textarea formItem formItem--area"
                                      style="width: 100%; height: 500px; font-size: 18px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"
                                      placeholder="Place some text here"
                                      style="width: 100%; height: 200px; font-size: 18px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">{{ $blog->body }}</textarea>
-->

                                      
                                      
                            <textarea id="body" name="body">{{ $blog->body }}</textarea>

							@ckeditor('body', ['height' => 300])          
                                      
                                      
                        </div>
                    </div>
                </div>
                
                
                
                
                
                
                 <div class="fieldRow">
                    <div class="formField formField--full ">
                        <h2 class="fieldLabel">Tags</h2>
                        <div class="fieldWrap">
                               <input type="text" name="tags" class="" data-role="tagsinput" placeholder="Enter the tags" value="{{$blog->tags}}"/>     
                        </div>
                    </div>
                  </div>
                  
                 <div class="fieldRow">
                    <div class="formField formField--full ">
                        <h2 class="fieldLabel">Meta Title</h2>
                        <div class="fieldWrap">
                                <input type="text" name="meta_title" class="formItem formItem--without-ico"
                                   placeholder="Enter the meta title" value="{{$blog->meta_title}}">     
                        </div>
                    </div>
                  </div>
                  
                 <div class="fieldRow">
                    <div class="formField formField--full ">
                        <h2 class="fieldLabel">Meta Keywords</h2>
                        <div class="fieldWrap">
                                <input type="text" name="meta_keywords" class="formItem formItem--without-ico"
                                   placeholder="Enter the meta keywords" value="{{$blog->meta_keywords}}">     
                        </div>
                    </div>
                  </div>
                  
                 <div class="fieldRow">
                    <div class="formField formField--full ">
                        <h2 class="fieldLabel">Meta Description</h2>
                        <div class="fieldWrap">
                                <input type="text" name="meta_description" class="formItem formItem--without-ico"
                                   placeholder="Enter the meta description" value="{{$blog->meta_description}}">     
                        </div>
                    </div>
                  </div>
                  
                  
                  
                

                {{--<div class="fieldRow">--}}
                {{--<div class="formField  formField--full">--}}
                {{--<h2 class="fieldLabel">tags</h2>--}}
                {{--<select class="form-control select2 select2-hidden-accessible" multiple="" data-placeholder="Select a State" style="width: 100%;" tabindex="-1" aria-hidden="true" name="tags[]">--}}
                {{--@foreach ($tags as $tag)--}}
                {{--<option value="{{ $tag->id }}">{{ $tag->name }}</option>--}}
                {{--@foreach($blog->tags as $blogTag)--}}
                {{--@if($blogTag->id == $tag->id)--}}
                {{--selected--}}
                {{--@endif--}}
                {{--@endforeach--}}
                {{-->{{ $tag->name }}</option>--}}
                {{--@endforeach--}}
                {{--</select>--}}
                {{--</div>--}}
                {{--</div>--}}
                <div class="settingBtn">
                    <input type="submit" class="actionsBtn actionsBtn--accept actionsBtn--big actionsBtn--no-centered"
                           value="update">
                </div>
            </div>
        </form>
    </div>
</div>
<!-- Select2 -->
<script src="{{ asset('bower_components/select2/dist/js/select2.full.min.js') }}"></script>
<script>
    //Initialize Select2 Elements
    $('.select2').select2()
</script>
<!-- Bootstrap WYSIHTML5 -->
<script src="{{ asset('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}"></script>
<script>
    $(function () {
        //bootstrap WYSIHTML5 - text editor
       // $('.textarea').wysihtml5()
    })
</script>
<script type="text/javascript">

$(".tm-input").tagsManager();

</script>
<script>
  var options = {
    filebrowserImageBrowseUrl: '/laravel-filemanager?type=Images',
    filebrowserImageUploadUrl: '/laravel-filemanager/upload?type=Images&_token=',
    filebrowserBrowseUrl: '/laravel-filemanager?type=Files',
    filebrowserUploadUrl: '/laravel-filemanager/upload?type=Files&_token='
  };
  
  $('#body').ckeditor(options);
</script>
