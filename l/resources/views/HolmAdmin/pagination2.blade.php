
<?php
$extraGet = '';
if(isset($_GET['userName'])) $extraGet.='&userName='.$_GET['userName'];
if(isset($profileTypeFilter)) $extraGet.='&profileType='.$profileTypeFilter;
if(isset($statusTypeFilter)) $extraGet.='&statusType='.$statusTypeFilter;
?>

@if($pages>1)
<div class="paginationContainer">
    <div class="pagination profilepagination">
        <a href="/l/{{$link.'/?page='.$previousPage.$extraGet}}" class="paginationArrow paginationArrow--left" @if(($previousPage < 1))style="pointer-events: none;" @endif >
            <span><i class="fa fa-angle-left"></i></span>
        </a>
      {{--  @if(Request::path() != 'admin/user')   
       @for($page=1; $page<=$pages; $page++)
            <a href="{{$link}}/?page={{$page.$extraGet}}" class="pagination__item @if($curr_page==$page) act @endif "><span>{{$page}}</span></a>
        @endfor
       
        @else --}}
        
        <?php 
        $page =1;
        $loop =1;
     

       $pagesLoop= (int)($pages/10) > 0 ? (int)($pages/10) : 1;
       $extraPages = $pages%10;
       
       
       
      
	   if($extraPages > 0  &&  $pages > 10 ){
		  $pagesLoop = $pagesLoop+1;
	   }
	   
	  
	   
        while($loop<=$pagesLoop){
	
		$startRange = 	$page;

		if($loop==$pagesLoop && $pages > 10){	
			$lastCount =  $pages%10;
			$endRange  =  ($startRange+$lastCount)-1;
			
		}else{
			if($pages >= 10){
		       $endRange  =  $startRange+9;
		    }else{
			  $endRange  = $startRange+($extraPages-1);
			}
	    }

	    if( $curr_page >=$startRange && $curr_page<=$endRange){
			$i=$startRange;
			while($i<=$endRange){
			?>
			 <a href="/l{{$link}}/?page={{$i.$extraGet}}" class="pagination__item singlepage  @if($curr_page==$i) act @endif ">
				 <span>
					 {{$i}}
			     </span>
			 </a>
				
			<?php
			 $i++;	
			}

		}else{	
		
			
		
		?>	
			 <a href="/l/{{$link}}/?page={{$page.$extraGet}}" class="pagination__item">
				 <span>
					 {{$startRange}} - {{$endRange}}
			     </span>
			 </a>
		<?php	
	    }
		$page = $page+10;
		$loop++;	
		}
        
        ?>
		{{-- @endif --}}
        <a href="/l/{{$link.'/?page='.$nextPage.$extraGet}}" class="paginationArrow paginationArrow--right" @if(($nextPage > $pages))style="pointer-events: none;" @endif >
            <span><i class="fa fa-angle-right"></i></span>
        </a>
    </div>
</div>
@endif
