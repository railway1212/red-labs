<div class="mainPanel">
    <h2 class="categoryTitle">
          <span class="categoryTitle__ico">
              <i class="fa fa-comment" aria-hidden="true"></i>
          </span>
        Reviews managament
    </h2>

  	<h3 style="font-size: 1.025em;font-weight: 900; text-transform: uppercase; margin:13px">Reviews to be Approved</h3>
	<div class="panelHead">
        <div class="filterGroup">
            <div class="filterBox">
                <div class="formField formField--fix-biger">
                    <form action="{{route('ReviewManagement')}}" method="post">
                    <div class="fieldWrap">
                        <input type="search" name="appointment_id" class="formItem formItem--input formItem--search" placeholder="Search...">
                        <button class="searchBtn" type="submit">
                            <i class="fa fa-search"></i>
                        </button>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="tableWrap tableWrap--margin-t">
        <table class="adminTable">
            <thead>
            <tr>
                <td class=" ordninary-td ordninary-td--small no-padding-l">
                  <span class="td-title td-title--time">
                    date
                  </span>
                </td>
                <td class=" ordninary-td ordninary-td--big ">
                  <span class="td-title td-title--darkest-mint ">
                  service user
                  </span>
                </td>
                <td class=" bigger-td   ">
                  <span class="td-title td-title--comment">
                    Comment
                  </span>
                </td>
                <td class="ordninary-td   no-padding-l" style="min-width: 85px;">
                  <span class="td-title td-title--comment">
                    appointment
                  </span>
                </td>
                <td class=" idField   " style="min-width: 85px;">
                  <span class="td-title td-title--comment">
                    booking
                  </span>
                </td>

                <td class=" ordninary-td   no-padding-l">
                  <span class="td-title td-title--light-blue">
                    actions
                  </span>
                </td>
            </tr>
            <tr class="extra-tr">
                <td>
                </td>

                <td class="for-inner">
                    <table class="innerTable ">
                        <tbody>
                        <tr>
                            <td class="idField">
                                <span class="extraTitle">id</span>
                            </td>
                            <td class="">
                                <span class="extraTitle">name</span>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
            </thead>
            <tbody>

            @foreach($reviews as $review)
                <tr>
                    <td>
                        <p>{{date('d-m-Y H:i:s',strtotime($review->created_at)) }}</p>
                    </td>
                    <td class="for-inner">
                        <table class="innerTable ">
                            <tbody>
                            <tr>
                                <td class="idField">
                                    <?php
//                                        $appointment = \App\Appointment::find($review->appointment_id);
//                                        $booking = \App\Booking::find($appointment->booking_id);
                                    ?>
                                    <span>{{$review->appointment->booking->bookingServiceUser->id}}</span>
                                </td>
                                <td class="">
                                    <!-- <a href="/l/serviceUser-settings/{{$review->appointment->booking->bookingServiceUser->id}}"
                                       class="tableLink"> -->{{$review->appointment->booking->bookingServiceUser->full_name}}<!-- </a> -->
                                </td>

                            </tr>
                            </tbody>
                        </table>
                    </td>

                    <td>
                    <span>
                        {{$review->comment}}
                    </span>
                    </td>
                    <td>
                    <span>
                        <?php
							 $appointment = \App\Appointment::find($review->appointment_id);
                            $appointmentsOfBooking = \App\Appointment::where('booking_id', $review->appointment->booking->id)->get();
                            $i = 0;
                            foreach($appointmentsOfBooking as $app){
                                $i++;//Чтоб первый элемент начинался не с 0, а с 1
                                if($app->id == $review->appointment->id){
                                    break;
                                }
                            }
                            //if($i) echo '#'.$i;
                        ?>
					  <a target="_blank" href="{{route('viewBookingDetails',['booking'=>$review->appointment->booking->id])}}#{{$i}} ">
                             {{$appointment->id}}</a>
                    </span>
                    </td>
                    <td>

                       <span>
                        <a target="_blank" href="{{route('viewBookingDetails',['booking'=>$review->appointment->booking->id])}}">
                             Detail</a>
                        </span>
                    </td>
                    <td>
                        <div class="actionsGroup">
                            <form method="post" action="{{route('ReviewManagement')}}">
                                <input name="id" type="hidden" value="{{$review->id}}">
                                <input name="method" type="hidden" value="confirm">
                                <button name="confirm" class="actionsBtn actionsBtn--accept ">
                                    confirm
                                </button>
                            </form>
                            <form>
                            <a href="{{route('ReviewManagementEdit',['id'=>$review->id])}}"
                               class="actionsBtn actionsBtn--edit">
                                edit
                            </a>
                            </form>
                            <form method="post" action="{{route('ReviewManagement')}}">
                                <input name="id" type="hidden" value="{{$review->id}}">
                                <input name="method" type="hidden" value="delete">
                                <button name="delete" class="actionsBtn actionsBtn--delete ">
                                    delete
                                </button>
                            </form>
                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    {{$reviews->render('HolmAdmin.pagination')}}
</div>
