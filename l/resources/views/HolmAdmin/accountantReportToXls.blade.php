<?php 
use App\Http\Controllers\Admin\Booking\BookingController;
?>

<div class="mainPanel">
    <div class="statisticGroup">
        <div class="statisticBox">
            <div class="tableWrap">
				
				
                <div class="statisticHead">
                    <h2 class="statisticHead__title">
                  <span class="statisticHead__ico">
                    <i class="fa fa-area-chart" aria-hidden="true"></i>
                  </span>
                       ACCOUNTANT REPORT
                    </h2>
                </div>
                
                <table class="statisticTable">
                    <thead>	
                    <tr>
					   <td class="">
						  <span class="ordinaryTitle td-stat td-stat--start">
						  User Name
						  </span>
                       </td>
                       
                        <td class="">
						  <span class="textCenter">
						  Invoice Number
						  </span>
                        </td>
                        
                        <td class="">
						  <span class="textCenter">
						  Invoice Date
						  </span>
                        </td>
                        
                        <td class="">
						  <span class="textCenter">
						 Payment Date
						  </span>
                        </td>
                        
                        <td class="">
						  <span class="textCenter">
						Description
						  </span>
                        </td>
						
                        <td class="">
						  <span class="textCenter">
						Quantity
						  </span>
                        </td>
                        
                        <td class="">
						  <span class="textCenter">
						Unit Amount
						  </span>
                        </td>
                        
                        <td class="">
						  <span class="textCenter">
						Account code
						  </span>
                        </td>
                        
                        <td class="">
						  <span class="textCenter">
						Tax Type
						  </span>
                        </td>	
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($bookings as $booking)
                        <tr class="statisticRow">
						   <td class="">
							    @if($booking->status=='purchaser')
									{{ BookingController::getDisplayFieldName($booking->purchaser_id,'purchasers_profiles',['first_name','family_name']) }}
								@else
								   {{ BookingController::getDisplayFieldName($booking->carer_id,'carers_profiles',['first_name','family_name']) }}
								@endif   
						   </td>
						   
							<td class="">
							   {{$booking->id}}
							</td>
							
							<td class="">
							   {{\Carbon\Carbon::parse($booking->created_at)->format("d-m-Y")}}
							</td>
							
							<td class="">
							    {{\Carbon\Carbon::parse($booking->tr_created)->format("d-m-Y")}}
							</td>
							
							<td class="">
								  @if($booking->status=='purchaser')
								    Payment received from purchaser
								  @else
								     Payments to carers
							      @endif
							</td>
							
							<td class=""> 1 </td>
							
							<td class="">
								{{ BookingController::getDisplayAmount($booking->amount)}}
								
							</td>
							
							<td class="">
							     @if($booking->status=='purchaser')
								    200
								  @else
								    310
							      @endif
							</td>
							
							<td class="">
							  No VAT
							</td>	
							
										
                        </tr>
                         
                   	@endforeach

                    </tbody>
                </table>

            </div>
        </div>

</div>
</div>        
        
        


