<div class="mainPanel">
    <div class="statisticGroup">
        <div class="statisticBox">
            <div class="tableWrap">
				
				
                <div class="statisticHead">
                    <h2 class="statisticHead__title">
                  <span class="statisticHead__ico">
                    <i class="fa fa-area-chart" aria-hidden="true"></i>
                  </span>
                        BONUS CARER  DETAILS
                    </h2>
                </div>
                
                <table class="statisticTable">
                    <thead>	
                    <tr>
                        <td class="textCenter">
						  <span class="ordinaryTitle td-stat">
						  NO
						  </span>
                        </td>
                        <td class="textCenter">
						  <span class="ordinaryTitle td-stat">
							Date
						  </span>
                        </td>
                        <td class="textCenter" colspan="2">
						  <span class="ordinaryTitle td-stat" >
							Benefactor    
						  </span>
                        </td>
                          
                        <td class="textCenter">
						  <span class="ordinaryTitle td-stat" >
							Invitation Code   
						  </span>
                        </td>  
                        
                        <td class="textCenter">
						  <span class="ordinaryTitle td-stat" >
							Referrer    
						  </span>
                        </td>  
                        
                        <td class="textCenter">
						  <span class="ordinaryTitle td-stat" >
							Paid Bonuses 	   
						  </span>
                        </td> 
                         
                        <td class="textCenter">
						  <span class="ordinaryTitle td-stat" >
						   Amount   
						  </span>
                        </td> 
                        <td class="textCenter">
						  <span class="ordinaryTitle td-stat" >
						   Status   
						  </span>
                        </td> 
                    </tr>
                    <tr class="extra-tr">
						
						<td class="idField">
							<span class="extraTitle"></span>
						</td>
						<td class="idField">
							<span class="extraTitle"></span>
						</td>
					
						<td class="idField">
							<span class="extraTitle">ID</span>
						</td>
						<td class="">
							<span class="extraTitle">NAME</span>
						</td>

						<td class="">
							<span class="extraTitle"></span>
						</td>	
						<td class="">
							<span class="extraTitle"></span>
						</td>	
						<td class="">
							<span class="extraTitle"></span>
						</td>	
						<td class="">
							<span class="extraTitle"></span>
						</td>
						<td class="">
							<span class="extraTitle"></span>
						</td>		
					</tr>
                    </thead>
                    <tbody>
					 @if($bonusPayouts->count())
							@foreach($bonusPayouts as $bonusPayout)
							 <tr>
								<td align="center">
									<span>{{$bonusPayout->id}}</span>
								</td>
								<td>
									<p>{{$bonusPayout->created_at->format('d-m-Y')}}</p>
								</td>
								<td class="idField">
									<span>{{$bonusPayout->user->id}}</span>
								</td>
								<td class="">
									<a href="#" class="tableLink">{{$bonusPayout->user->full_name}}</a>
								</td>
								<td>
								  <span>
									  @if($bonusPayout->user->referral_code)
										  {{$bonusPayout->user->referral_code}}
									  @elseif($bonusPayout->user->use_register_code)
										  REGISTER
									  @else
										  -
									  @endif
								  </span>
							   </td>
							   <td>
								  <span>
									  @if($bonusPayout->bonus_type_id == 2)
										  @foreach($users as $user)
											  @if($user->id == $bonusPayout->referral_user_id)
												  {{$user->first_name}} {{$user->family_name}}
											  @endif
										  @endforeach
									  @elseif($bonusPayout->user->use_register_code)
										  REGISTER
									  @else
										  -
									  @endif
								  </span>
							  </td>
							<td>
								<span> {{$bonusPayout->user->paid_bonuses}}</span>
							</td>
							<td>
								<span>{{$bonusPayout->amount}}</span>
							</td> 
							<td>
								@if(!$bonusPayout->payout)
                                   To Be Paid
                                @else
                                    Paid
                                @endif
							</td> 
							 </tr>	 
							@endforeach
						@else
							<tr>
								<td colspan="7" align="center">-</td>
							</tr>
						@endif
                    </tbody>
                </table>

            </div>
        </div>

</div>
</div>        
        
        
      

