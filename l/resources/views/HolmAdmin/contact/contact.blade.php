
<div class="mainPanel">
    <h2 class="categoryTitle">
          <span class="categoryTitle__ico">
            <i class="fa fa-newspaper-o" aria-hidden="true"></i>
          </span>
        Contact  Setting -> Edit
    </h2>
    <div class="contentCard">
        <div class="contentCard__head contentCard__head--payment">
            <h2>Contact</h2>
        </div>
        
        <form role="form" action="{{ route('contactpost') }}" enctype="multipart/form-data" method="post">
            <div class="contentBody">

                 <div class="fieldRow">
                    <div class="formField formField--full ">
                        <h2 class="fieldLabel">Write to us</h2>
                        <div class="fieldWrap">
                             <textarea id="write_us" name="write_us">@if(!empty($contact['write_us'])) {{ $contact['write_us'] }} @endif</textarea>      
                              @ckeditor('write_us', ['height' => 200])   
                        </div>
                    </div>
                </div>

                <div class="fieldRow">
                    <div class="formField formField--full ">
                        <h2 class="fieldLabel">Address</h2>
                        <div class="fieldWrap">
                             <textarea id="address" name="address">@if(!empty($contact['address'])) {{ $contact['address'] }} @endif</textarea>      
                              @ckeditor('address', ['height' => 200])   
                        </div>
                    </div>
                </div>

                <div class="fieldRow">
                    <div class="formField formField--full ">
                        <h2 class="fieldLabel">Phone Number</h2>
                        <div class="fieldWrap">
                            <input name="phone_number" type="text" class="formItem formItem--without-ico"
                                   placeholder="Enter the phone number" value="@if(!empty($contact['phone_number'])) {{ $contact['phone_number'] }} @endif">
                        </div>
                    </div>
                </div>

                <div class="fieldRow">
                    <div class="formField formField--full ">
                        <h2 class="fieldLabel">Contact Time </h2>
                        <div class="fieldWrap">
                            <input name="contact_time" type="text" class="formItem formItem--without-ico"
                                   placeholder="Enter the contact time" value="@if(!empty($contact['contact_time'])) {{ $contact['contact_time'] }} @endif">
                        </div>
                    </div>
                </div>

                <div class="fieldRow">
                    <div class="formField formField--full ">
                        <h2 class="fieldLabel">Email Address</h2>
                        <div class="fieldWrap">
                            <input name="email_address" type="text" class="formItem formItem--without-ico"
                                   placeholder="Enter the email address" value="@if(!empty($contact['email_address'])) {{ $contact['email_address'] }} @endif">
                        </div>
                    </div>
                </div>

                 <div class="fieldRow">
                    <div class="formField formField--full ">
                        <h2 class="fieldLabel">Google Map(Add Embeded Code)</h2>
                        <div class="fieldWrap">
                            <input name="google_map" type="text" class="formItem formItem--without-ico"
                                   placeholder="Enter the google map embeded code" value="@if(!empty($contact['google_map'])) {{ $contact['google_map'] }} @endif">
                        </div>
                    </div>
                </div>

                

                 <div class="fieldRow">
                    <div class="formField formField--full ">
                        <h2 class="fieldLabel">Copyright</h2>
                        <div class="fieldWrap">
                            <input name="copyright" type="text" class="formItem formItem--without-ico"
                                   placeholder="Enter the copyright" value="@if(!empty($contact['copyright'])) {{ $contact['copyright'] }} @endif">
                        </div>
                    </div>
                </div>

                <div class="fieldRow">
                    <div class="formField formField--full ">
                        <h2 class="fieldLabel">Footer Address</h2>
                        <div class="fieldWrap">
                            <input name="footer_address" type="text" class="formItem formItem--without-ico"
                                   placeholder="Enter the footer address" value="@if(!empty($contact['footer_address'])) {{ $contact['footer_address'] }} @endif">
                        </div>
                    </div>
                </div>

                <div class="fieldRow">
                    <div class="formField formField--full ">
                        <h2 class="fieldLabel">Facebook Link</h2>
                        <div class="fieldWrap">
                            <input name="facebook_link" type="text" class="formItem formItem--without-ico"
                                   placeholder="Enter the facebook link" value="@if(!empty($contact['facebook_link'])) {{ $contact['facebook_link'] }} @endif">
                        </div>
                    </div>
                </div>
                <div class="fieldRow">
                    <div class="formField formField--full ">
                        <h2 class="fieldLabel">Twitter Link</h2>
                        <div class="fieldWrap">
                            <input name="twitter_link" type="text" class="formItem formItem--without-ico"
                                   placeholder="Enter the twitter link" value="@if(!empty($contact['twitter_link'])) {{ $contact['twitter_link'] }} @endif">
                        </div>
                    </div>
                </div>

                 <div class="fieldRow">
                    <div class="formField formField--full ">
                        <h2 class="fieldLabel">Goole+ Link</h2>
                        <div class="fieldWrap">
                            <input name="google_link" type="text" class="formItem formItem--without-ico"
                                   placeholder="Enter the google+ link" value="@if(!empty($contact['google_link'])) {{ $contact['google_link'] }} @endif">
                        </div>
                    </div>
                </div>

                <div class="settingBtn">
                    <input type="submit" class="actionsBtn actionsBtn--accept actionsBtn--big actionsBtn--no-centered"
                           value="update">
                </div>
            </div>
        </form>
    </div>
</div>
