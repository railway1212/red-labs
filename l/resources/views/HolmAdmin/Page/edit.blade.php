<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.5/css/bootstrap.min.css">
<link rel="stylesheet" href="http://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="http://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js"></script>


<div class="mainPanel">
    <h2 class="categoryTitle">
          <span class="categoryTitle__ico">
            <i class="fa fa-newspaper-o" aria-hidden="true"></i>
          </span>
        Page -> Edit
    </h2>
    <div class="contentCard">
        <div class="contentCard__head contentCard__head--payment">
            <h2>page</h2>
        </div>
        <form role="form" action="{{ route('page.update', $page->id) }}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            {{ method_field('PATCH') }}
            <div class="contentBody">
                
                <div class="fieldRow">
                    <div class="formField formField--full ">
                        <h2 class="fieldLabel">Title</h2>
                        <div class="fieldWrap">
                            <input name="title" type="text" class="formItem formItem--without-ico"
                                   placeholder="Enter the title" value="{{ $page->title }}">
                        </div>
                    </div>
                </div>
                
                 <div class="fieldRow">
                    <div class="formField formField--full ">
                        <h2 class="fieldLabel">Slug</h2>
                        <div class="fieldWrap">
                            <input type="text"  class="formItem formItem--without-ico"
                                   placeholder="Enter the slug" value="{{$page->slug}}"  disabled>
                                   <input type="hidden" name="slug" value="{{$page->slug}}"/>
                        </div>
                    </div>
                </div>


                <div class="fieldRow">
                    <div class="formField  formField--full">
                        <h2 class="fieldLabel">text</h2>
                        <div class="fieldWrap">         
                            <textarea id="content" name="content">{{ $page->content }}</textarea>
							@ckeditor('content', ['height' => 300])          
                        </div>
                    </div>
                </div>

                @if($page->id==9)

                 <div class="fieldRow">
                    <div class="formField  formField--full">
                        <h2 class="fieldLabel">Home More Info Image</h2>
                        <div class="fieldWrap">  
                            <input type="file" name="home_image" /><br>
                          	 @if(!empty($contact['home_image']))
                               <img src="{{URL::to('/')}}/image_Association/{{$contact['home_image']}}" width="100"/>
                             @endif

                        </div>
                    </div>
                </div>


                <div class="fieldRow">
                    <div class="formField formField--full ">
                        <h2 class="fieldLabel">Home More Info Text</h2>
                        <div class="fieldWrap">
                             <textarea id="home_contact" name="home_contact">@if(!empty($contact['home_contact'])) {{ $contact['home_contact'] }} @endif</textarea>      
                              @ckeditor('home_contact', ['height' => 200])   
                        </div>
                    </div>
                </div>

                @endif

               


                @if($page->id==1 || $page->id==7)
                    <div style="display:none">
                        <div class="richTextwrapper fieldWrap" >
                            <div class="richtextedit">
                                <textarea id="textarea-1" class="textarea"></textarea>
                            </div>
                            <a href="javascript:void(0);" class="remove_button">Remove</a>
                        </div>
                        </br>
                    </div>    



                    <div class="fieldRow">
                        <div class="formField  formField--full">
                            <h2 class="fieldLabel">Blocks</h2>
                        </div>
                    </div>

                    <?php
                        $blockContents = unserialize($page->extra_content);
                        $count = 0;
                    ?>

                     <div class="field_wrapper">
                        @if(count($blockContents)> 0 && !empty($blockContents))
                            @foreach($blockContents as $blockContent)
                            <div class="fieldWrap">
                                <div>
                                    <input type="file" name="extra_image[{{$count}}]" /><br>
                                    @if(!empty($blockContent['image']))
                                        <img src="{{URL::to('/')}}/image_Association/{{$blockContent['image']}}" width="100" />
                                    @endif
                                    <br>
                                    <input type="text" placeholder="Image alt text" name="image_alt[{{$count}}]" value="@if(isset($blockContent['image_alt'])){{ $blockContent['image_alt']}}@endif"/>
                                </div>
                               
                                    
                                


                                <div class="richtextedit">  
                                    <textarea name="extra_text[{{$count}}]" class="textarea" id="textarea-1">{{ $blockContent['content']}}</textarea>
                                </div>
                                    @if($count==0)
                                        <a href="javascript:void(0);" class="add_button" title="Add field">Add</a>
                                    @else
                                        <a href="javascript:void(0);" class="remove_button">Remove</a>  
                                    @endif
                            </div>
                            </br> 
                            <?php $count++; ?>
                            @endforeach
                        @else

                            <div class="fieldWrap">
                                    <input type="file" name="extra_image[{{$count}}]" /><br>
                                    <textarea name="extra_text[{{$count}}]"></textarea>
                                    <a href="javascript:void(0);" class="add_button" title="Add field">Add</a>
                            </div>
                            </br> 
                        
                        @endif
                    </div> 


                @endif

                 @if($page->id==1)
                    <div class="fieldRow">
                        <div class="formField formField--full ">
                            <h2 class="fieldLabel">Our Mission</h2>
                            <div class="fieldWrap">
                                <textarea id="our_mission" name="our_mission">@if(!empty($contact['our_mission'])) {{ $contact['our_mission'] }} @endif</textarea>      
                                @ckeditor('our_mission', ['height' => 200])   
                            </div>
                        </div>
                    </div>

                    <div class="fieldRow">
                        <div class="formField formField--full ">
                            <h2 class="fieldLabel">Founder</h2>
                            <div class="fieldWrap">
                                <textarea id="founder" name="founder">@if(!empty($contact['founder'])) {{ $contact['founder'] }} @endif</textarea>      
                                @ckeditor('founder', ['height' => 200])   
                            </div>
                        </div>
                    </div>
                 @endif

                  
                 <div class="fieldRow">
                    <div class="formField formField--full ">
                        <h2 class="fieldLabel">Meta Title</h2>
                        <div class="fieldWrap">
                                <input type="text" name="meta_title" class="formItem formItem--without-ico"
                                   placeholder="Enter the meta title" value="{{$page->meta_title}}">     
                        </div>
                    </div>
                  </div>
                  
                 <div class="fieldRow">
                    <div class="formField formField--full ">
                        <h2 class="fieldLabel">Meta Keywords</h2>
                        <div class="fieldWrap">
                                <input type="text" name="meta_keywords" class="formItem formItem--without-ico"
                                   placeholder="Enter the meta keywords" value="{{$page->meta_keyword}}">     
                        </div>
                    </div>
                  </div>
                  
                 <div class="fieldRow">
                    <div class="formField formField--full ">
                        <h2 class="fieldLabel">Meta Description</h2>
                        <div class="fieldWrap">
                                <input type="text" name="meta_description" class="formItem formItem--without-ico"
                                   placeholder="Enter the meta description" value="{{$page->meta_description}}">     
                        </div>
                    </div>
                  </div>

                <div class="settingBtn">
                    <input type="submit" class="actionsBtn actionsBtn--accept actionsBtn--big actionsBtn--no-centered"
                           value="update">
                </div>
            </div>
        </form>
    </div>
</div>
<!-- Select2 -->
<script src="{{ asset('bower_components/select2/dist/js/select2.full.min.js') }}"></script>
<script>
    //Initialize Select2 Elements
    $('.select2').select2()
</script>
<!-- Bootstrap WYSIHTML5 -->
<script src="{{ asset('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}"></script>
<script>
    $(function () {
        //bootstrap WYSIHTML5 - text editor
        $('.textarea').wysihtml5()
    })
</script>
<script type="text/javascript">

$(".tm-input").tagsManager();

</script>
<script type="text/javascript">
$(document).ready(function(){
    var maxField = 10; //Input fields increment limitation
    var addButton = $('.add_button'); //Add button selector
    var wrapper = $('.field_wrapper'); //Input field wrapper
    var fieldHTML = '<div class="fieldWrap"><textarea name="text[]"/></textarea><a href="javascript:void(0);" class="remove_button">Remove</a></div>'; //New input field html 
    var x = $('.field_wrapper > div').length; //Initial field counter is 1
    var x = x-1;
    
    //Once add button is clicked
    $(addButton).click(function(){
        //Check maximum number of input fields
    var newElem = $('.richTextwrapper').first().clone();
    var num = $('.textarea').length + 1; //num is the total count of the cloned textareas
    newElem.find('.richtextedit').html('');
    newElem.find('.richtextedit').html('<input type="file" name="extra_image['+x+']" /><br><input type="text" placeholder="Image alt text" name="image_alt['+x+']"><br><textarea name="extra_text['+x+']" class="textarea" id="textarea-'+x+'"/></textarea>');
    newElem.appendTo('.field_wrapper'); 
    $('#textarea-'+x).wysihtml5(); 

        if(x < maxField){ 
            x++; //Increment field counter
         // $(wrapper).append(fieldHTML); //Add field html
        }
    });
    
    //Once remove button is clicked
    $(wrapper).on('click', '.remove_button', function(e){
        e.preventDefault();
        $(this).parent('div').remove(); //Remove field html
        x--; //Decrement field counter
    });
});
</script>

