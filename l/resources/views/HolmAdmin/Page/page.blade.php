<div class="mainPanel">
    <h2 class="categoryTitle">
          <span class="categoryTitle__ico">
            <i class="fa fa-newspaper-o" aria-hidden="true"></i>
          </span>
        Pages
    </h2>
    <div class="settingBtn">
        <a href="{{ route('page.create') }}"
           class="actionsBtn actionsBtn--accept actionsBtn--big actionsBtn--no-centered">
            Create Page
        </a>

    </div>
    <div class="tableWrap tableWrap--margin-t">
        <table class="adminTable">
            <thead>
            <tr>
                <td class="orderNumber">
                    <span class="td-title td-title--number">
                     №
                    </span>
                </td>


                <td class=" ordninary-td ">
                    <span class="td-title td-title--title">
                    title
                    </span>

                </td>
                 <td class=" ordninary-td  ">
                    <span class="td-title td-title--title">
                    slug
                    </span>

                </td>
                <td class=" ordninary-td  ">
                    <span class="td-title td-title--time">
                      date
                    </span>
                </td>
                <td class=" ordninary-td ">
                    <span class="td-title td-title--actions">
                      actions
                    </span>
                </td>
                 <td class=" ordninary-td  ">
                    <span class="td-title td-title--time">
                      Meta Title
                    </span>
                </td>
                 <td class=" ordninary-td  ">
                    <span class="td-title td-title--time">
                      Meta Keywords
                    </span>
                </td>
                 <td class=" ordninary-td  ">
                    <span class="td-title td-title--time">
                      Meta Description
                    </span>
                </td>
                
                <td class=" ordninary-td ">
                    <span class="td-title td-title--status">
                    status
                    </span>
                </td>
                
                
            </tr>
            </thead>

            <tbody>
            @foreach($pages as $page)
                <tr>
                    <td>
                        <span>{{ $page->id }}</span>
                    </td>

                    <td class="">
                        <span>{{ $page->title }}</span>
                    </td>
                    <td class="">
                        <span>{{ $page->slug }}</span>
                    </td>
                    <td>
                        <span>{{ $page->created_at }}</span>
                    </td>
                    <td>
                        <div class="actionsGroup">
                            @if($page->status == 'published')
                                <a href="{{ route('page.edit', $page->id) }}" class="actionsBtn actionsBtn--edit  ">
                                    edit
                                </a>
                                <a data-delete_page_id="{{ $page->id }}"
                                   class="actionsBtn actionsBtn--delete deleteBlog">
                                    delete
                                </a>
                            @elseif($blog->status == 'deleted')
                                <a data-publish_page_id="{{ $blog->id }}"
                                   class="actionsBtn actionsBtn--accept publishBlog">
                                    publish
                                </a>
                                <a href="{{ route('page.edit', $blog->id) }}" class="actionsBtn actionsBtn--edit  ">
                                    edit
                                </a>
                            @endif
                        </div>
                    </td>
                    <td>
                        <span>{{ $page->meta_title }}</span>
                    </td>
                    <td>
                        <span>{{ $page->meta_keyword }}</span>
                    </td>
                    <td>
                        <span>{{ $page->meta_description }}</span>
                    </td>
                    
                    <td>
                        <div class="profStatus profStatus--left">
                            @if($page->status == 'published')
                                <span class="profStatus__item profStatus__item--new">published</span>
                            @elseif($page->status == 'deleted')
                                <span class="profStatus__item profStatus__item--canceled"> deleted</span>
                            @endif
                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
   
    {{--$pages->render()--}}
    
    
    
<!-- Modal (Pop up when detail button clicked) -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">×</span></button>
                    <h4 class="modal-title" id="myModalLabel">Task Editor</h4>
                </div>
                <div class="modal-body">
                    <form id="frmTasks" name="frmTasks" class="form-horizontal" novalidate="">

                        <div class="form-group error">
                            <label for="inputTask" class="col-sm-3 control-label">Task</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control has-error" id="task" name="task"
                                       placeholder="Task" value="">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-3 control-label">Description</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="description" name="description"
                                       placeholder="Description" value="">
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="btn-save" value="add">Save changes</button>
                    <input type="hidden" id="task_id" name="task_id" value="0">
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('.adminTable').on('click', '.deletepage', function (e) {
            e.preventDefault();
            var page_id = $(this).attr('data-delete_page_id');
            $.ajax({
                url: '/admin/page/' + page_id,
                type: 'PUT',
                data: {'delete_page_id': page_id},
                success: function () {
                    location.reload();
                },
                error: function () {
                    console.log('fail');
                }
            });
        });

        $('.adminTable').on('click', '.publishBlog', function (e) {
            e.preventDefault();
            var blog_id = $(this).attr('data-publish_page_id');
            $.ajax({
                url: '/admin/blog/' + blog_id,
                type: 'PUT',
                data: {'publish_blog_id': blog_id},
                success: function () {
                    location.reload();
                },
                error: function () {
                    console.log('fail');
                }
            });
        });
    });
</script>


