<div class="mainPanel">
    <div class="statisticGroup">
        <div class="statisticBox">
            <div class="tableWrap">
				
				
                <div class="statisticHead">
                    <h2 class="statisticHead__title">
                  <span class="statisticHead__ico">
                    <i class="fa fa-area-chart" aria-hidden="true"></i>
                  </span>
                        PAYMENTS TO PURCHASERS DETAILS
                    </h2>
                </div>

                <table class="statisticTable">
                    <thead>	
                    <tr>
                        <td class="textCenter">
						  <span class="ordinaryTitle td-stat">
						   Transaction ID
						  </span>
                        </td>
                        <td class="textCenter" colspan="2">
						  <span class="ordinaryTitle td-stat">
							Purchaser  
						  </span>
                        </td>
                        <td class="textCenter" colspan="4">
						  <span class="ordinaryTitle td-stat" >
							Booking   
						  </span>
                        </td>  
                    </tr>
                    <tr class="extra-tr">
						
						<td class="idField">
							<span class="extraTitle"></span>
						</td>
					
						<td class="idField">
							<span class="extraTitle">ID</span>
						</td>
						<td class="">
							<span class="extraTitle">NAME</span>
						</td>
							
						<td class="idField">
							<span class="extraTitle">TOTAL</span>
						</td> 
						<td class="idField">
							<span class="extraTitle">PAYOUT STATUS</span>
						</td> 
					</tr>
                    </thead>
                    <tbody>
						@if($payoutsToPurchasers->count() > 0 || count($potentialPayouts) > 0)
							@foreach($potentialPayouts as $potentialPayout)
							<tr>
								<td align="center">
								-
								</td>
								<td class="idField">
									<span>{{$potentialPayout->purchaser_id}}</span>
								</td>
								<td class="nameField">
									<a href="#" class="tableLink">{{$potentialPayout->first_name.' '.$potentialPayout->family_name}}</a>
								</td>
							
								<td class="">
									<span><i class="fa fa-gbp" aria-hidden="true"></i> {{$potentialPayout->total}}</span>
								</td>
								<td>
									<div class="profStatus profStatus--left">
										<span class="profStatus__item profStatus__item--progress">pending</span>

									</div>
								</td>
									
								</tr>
								@endforeach
								@foreach($payoutsToPurchasers as $payoutsToPurchaser)
								<tr>
									<td  align="center">
									{{$payoutsToPurchaser->id}}
									</td>
									<td class="idField">
										<span>{{$payoutsToPurchaser->booking->bookingCarer->id}}</span>
									</td>
									<td class="nameField">
										<a href="#" class="tableLink">{{$payoutsToPurchaser->booking->bookingPurchaserProfile->full_name}}</a>
									</td>
										
									<td class="">
										<span><i class="fa fa-gbp" aria-hidden="true"></i> {{$payoutsToPurchaser->amount}}</span>
									</td>
									<td>
										<div class="profStatus profStatus--left">
											<span class="profStatus__item profStatus__item--new">paid</span>

										</div>
									</td>
									
								</tr>
							@endforeach
						@else
						<tr>
							<td colspan="7" align="center">
							-
							</td>
						</tr>
						@endif
                    </tbody>
                </table>

            </div>
        </div>

</div>
</div>        
        
        
      
