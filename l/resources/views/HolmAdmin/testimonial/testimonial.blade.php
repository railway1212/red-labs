<div class="mainPanel">
    <h2 class="categoryTitle">
          <span class="categoryTitle__ico">
            <i class="fa fa-newspaper-o" aria-hidden="true"></i>
          </span>
        Testimonial
    </h2>
    <div class="settingBtn">
        <a href="{{ route('testimonial.create') }}"
           class="actionsBtn actionsBtn--accept actionsBtn--big actionsBtn--no-centered">
            Create Testimonial
        </a>

    </div>
    <div class="tableWrap tableWrap--margin-t">
        <table class="adminTable">
            <thead>
            <tr>
                <td class="orderNumber">
                    <span class="td-title td-title--number">
                     №
                    </span>
                </td>

                <td class=" ordninary-td ">
                    <span class="td-title td-title--title">
                    Client Name
                    </span>

                </td>
                <td class=" ordninary-td  ">
                    <span class="td-title td-title--time">
                      Client Photo
                    </span>
                </td>
                <td class=" ordninary-td ">
                    <span class="td-title td-title--status">
                        Description
                    </span>
                </td>
                <td class=" ordninary-td ">
                    <span class="td-title td-title--actions">
                      actions
                    </span>
                </td>
            </tr>
            </thead>

            <tbody>
            @foreach($testimonials as $testimonial)
                <tr>
                    <td>
                        <span>{{ $testimonial->id }}</span>
                    </td>

                    <td class="">
                        <span>{{ $testimonial->client_name }}</span>
                    </td>
                    <td>
                       <div class="profStatus profStatus--left">
                            @if(!empty($testimonial->image_name))
                               <img src="{{URL::to('/')}}/public/image_Association/{{$testimonial->image_name}}" width="100"/>
                            @endif
                        </div>
                    </td>
                    <td>
                    <span><?php  echo strip_tags($testimonial->description); ?></span>
                    </td>
                    <td>
                        <div class="actionsGroup">
                                <a href="{{ route('testimonial.edit', $testimonial->id) }}" class="actionsBtn actionsBtn--edit  ">
                                    edit
                                </a>
                                <a data-delete_testimonial_id="{{ $testimonial->id }}"
                                   class="actionsBtn actionsBtn--delete deleteTestimonial">
                                    delete
                                </a>
                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
   

    
    
    

<script>
    $(document).ready(function () {
        $('.adminTable').on('click', '.deleteTestimonial', function (e) {
            e.preventDefault();
            var testimonial_id = $(this).attr('data-delete_testimonial_id');
            $.ajax({
                url: '/laravel/admin/testimonial/' + testimonial_id,
                type: 'PUT',
                data: {'delete_testimonial_id': testimonial_id},
                success: function () {
                    location.reload();
                },
                error: function () {
                    console.log('fail');
                }
            });
        });
    });
</script>
