<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.5/css/bootstrap.min.css">
<link rel="stylesheet" href="http://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="http://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js"></script>


<div class="mainPanel">
    <h2 class="categoryTitle">
          <span class="categoryTitle__ico">
            <i class="fa fa-newspaper-o" aria-hidden="true"></i>
          </span>
        Testimonial -> Edit
    </h2>
    <div class="contentCard">
        <div class="contentCard__head contentCard__head--payment">
            <h2>Testimonial</h2>
        </div>
        <form role="form" action="{{ route('testimonial.update', $testimonial->id) }}" method="post" id="trustImageUpload" enctype="multipart/form-data">
            {{ csrf_field() }}
            {{ method_field('PATCH') }}
            <div class="contentBody">

                <div class="fieldRow">
                    <div class="formField formField--full ">
                        <h2 class="fieldLabel">Testimonial</h2>
                        <div class="fieldWrap">
                            <input name="client_name" type="text" class="formItem formItem--without-ico"
                                   placeholder="Enter the client name" value="{{ $testimonial->client_name }}">
                        </div>
                    </div>
                </div>

                 <div class="fieldRow">
                    <div class="formField formField--full ">
                        <h2 class="fieldLabel">Client Info (Image alt text)</h2>
                        <div class="fieldWrap">
                            <input type="text" name="client_info" class="formItem formItem--without-ico"
                                   placeholder="Enter the client info" value="{{$testimonial->client_info}}">
                        </div>
                    </div>
                </div>


                <div class="fieldRow">
                    <div class="formField  formField--full">
                        <h2 class="fieldLabel">Client photo</h2>
                        <div class="fieldWrap">  
                            <input type="file" name="image" /><br>
                          	 @if(!empty($testimonial->image_name))
                               <img src="{{URL::to('/')}}/public/image_Association/{{$testimonial->image_name}}" width="100"/>
                             @endif

                        </div>
                    </div>
                </div>

                 <div class="fieldRow">
                    <div class="formField  formField--full">
                        <h2 class="fieldLabel">Text</h2>
                        <div class="fieldWrap">       
                              <textarea id="description" name="description">{{$testimonial->description}}</textarea> 
                              @ckeditor('description', ['height' => 200])                    
                        </div>
                    </div>
                </div>

                 <div class="fieldRow">
                    <div class="formField  formField--full">
                        <h2 class="fieldLabel">Select Page</h2>
                        <div class="fieldWrap">  
                            <select name="page_id">
                              <option value="9"   @if($testimonial->page_id=='9') selected @endif >Home</option>
                              <option value="7"   @if($testimonial->page_id=='7') selected @endif >I am a carer</option>
                            </select>
                        </div>
                    </div>
                </div>


                <div class="settingBtn">
                    <input type="submit" class="actionsBtn actionsBtn--accept actionsBtn--big actionsBtn--no-centered"
                           value="update">
                </div>
            </div>
        </form>
    </div>
</div>
