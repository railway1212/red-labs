<div class="mainPanel">
    <div class="statisticGroup">
        <div class="statisticBox">
            <div class="tableWrap">
				
				
                <div class="statisticHead">
                    <h2 class="statisticHead__title">
                  <span class="statisticHead__ico">
                    <i class="fa fa-area-chart" aria-hidden="true"></i>
                  </span>
                        PAYMENTS TO PURCHASERS DETAILS
                    </h2>
                </div>

                <table class="statisticTable">
                    <thead>	
                    <tr>
                        <td class="textCenter">
						  <span class="ordinaryTitle td-stat">
						   Transaction ID
						  </span>
                        </td>
                        <td class="textCenter" colspan="2">
						  <span class="ordinaryTitle td-stat">
							Carer  
						  </span>
                        </td>
                        <td class="textCenter" colspan="3">
						  <span class="ordinaryTitle td-stat" >
							Appointment   
						  </span>
                        </td>  
                    </tr>
                    <tr class="extra-tr">
						
						<td class="idField">
							<span class="extraTitle"></span>
						</td>
					
						<td class="idField">
							<span class="extraTitle">ID</span>
						</td>
						<td class="">
							<span class="extraTitle">NAME</span>
						</td>
							
						<td class="idField">
							<span class="extraTitle">TOTAL</span>
						</td> 
						<td class="idField">
							<span class="extraTitle">DETAILS</span>
						</td> 
						<td class="idField">
							<span class="extraTitle">PAYOUT STATUS</span>
						</td> 
					</tr>
                    </thead>
                    <tbody>
					 @if(count($all) > 0)
						@foreach($all as $one)
							@if(isset($one->total))
							 <tr>
								<td align="center">  -   </td>
								<td class="idField">
									<span>{{$one->carer_id}}</span>
								</td>
								<td class="nameField">
									<a href="#" class="tableLink">{{$one->first_name.' '.$one->family_name}}</a>
								</td>
								
								<td class="">
									<span><i class="fa fa-gbp" aria-hidden="true"></i> {{$one->total}}</span>
								</td>
								<td class="">
									<a href="{{ route('viewBookingDetails', ['booking' => $one->booking_id]) }}">
										<span><i aria-hidden="true"></i>{{ $one->id }}</span>
									</a>
								</td>
								<td>
								    <span class="profStatus__item profStatus__item--progress">pending</span>
								</td> 
							 </tr>
							@else
							<tr>
								<td align="center">
									<a href="https://dashboard.stripe.com/test/payments/{{$one->id}}" target="_blank">{{$one->id}}</a>
								</td>
								<td class="idField">
									<span>
										@if($one->booking_id > 0)	
											{{$one->booking->bookingCarer->id}}
										@elseif($one->bonus_id > 0)
											 <?php
											 $bonusPayout = \App\BonusPayout::where('id', $one->bonus_id)->pluck('user_id')->toArray();
											 echo $bonusPayout[0];
											 ?>	
										@endif
									</span>
								</td>
								<td class="nameField">
									@if($one->booking_id > 0)	
										<a href="/l/carer-settings/{{$one->booking->bookingCarer->id}}"  class="tableLink">{{$one->booking->bookingCarerProfile->full_name}}</a> 
										
									@elseif($one->bonus_id > 0)
										<?php
										   $bonusUser = App\CarersProfile::where('id', $bonusPayout[0])->get();
										?>
										<a href="/l/carer-settings/{{$bonusPayout[0]}}"  class="tableLink">
										   {{$bonusUser[0]->first_name}} {{$bonusUser[0]->family_name}}
										</a> 
										
									@endif	
								</td>
								<td class="">
									<span>{{$one->amount/100}}</span>
								</td>
								<td class="">
									 <?php       
										if($one->bonus_id > 0){
											
											echo 'Bonus Payout';
											
										}else{
											$allApps = \App\Appointment::where('booking_id', $one->booking_id)->pluck('id')->toArray();
											for ($i = 0; $i < count($allApps); $i++) {
												echo "<a href='/l/bookings/" . $one->booking_id . "/details'>" . $allApps[$i] . "<a/><br>";
											}
										}     
                                    ?>
								</td>
								<td>
								   Paid
								</td>
							</tr>	
						    @endif
                         @endforeach
                     @endif	
                     @if(false)
                       @foreach($potentialPayouts as $potentialPayout)
                       <tr>
						 <td align="center">  - </td>
						  <td class="idField">
                             <span>{{$potentialPayout->carer_id}}</span>
						 </td>
						 <td class="nameField">
							<a href="#" class="tableLink">{{$potentialPayout->first_name.' '.$potentialPayout->family_name}}</a>
						 </td>
						<td class="">
							<span><i class="fa fa-gbp" aria-hidden="true"></i> {{$potentialPayout->total}}</span>
						</td>
						<td class="">
							<span><i class="fa fa-gbp"
									 aria-hidden="true"></i>{{ $potentialPayout->id }}</span>
						</td>booking
						<td class="nameField">
							<?php $allApps = \App\Appointment::where('booking_id', $potentialPayout->booking_id)->pluck('id')->toArray();
								$types = \App\AppointemntAssistanceType::whereIn('appointment_id', $allApps)->pluck('assistance_type_id')->toArray();
								$assistTypes = \App\AssistanceType::whereIn('id', $types)->pluck('name')->toArray();
								for($i=0;$i<count($assistTypes);$i++){
									echo $assistTypes[$i]."<br>";
								}
							?>
						</td>
						<td>
							<div class="profStatus profStatus--left">
								<span class="profStatus__item profStatus__item--progress">pending</span>
							</div>
						</td>
					   </tr>
					   @endforeach
					   @foreach($transfers as $transfer)
						<tr>
							<td  align="center">
								<a href="https://dashboard.stripe.com/test/payments/{{$transfer->id}}" target="_blank">{{$transfer->id}}</a>
							</td>
							<td class="idField">
								<span>{{$transfer->booking->bookingCarer->id}}</span>
							</td>
							<td class="nameField">
								<a href="#" class="tableLink">{{$transfer->booking->bookingCarerProfile->full_name}}</a>
							</td>
							<td>
								<span><i class="fa fa-gbp" aria-hidden="true"></i> {{$transfer->amount/100}}</span>
							</td>
							<td class="nameField">
								<?php $allApps = \App\Appointment::where('booking_id', $potentialPayout->booking_id)->pluck('id')->toArray();
								$types = \App\AppointemntAssistanceType::whereIn('appointment_id', $allApps)->pluck('assistance_type_id')->toArray();
								$assistTypes = \App\AssistanceType::whereIn('id', $types)->pluck('name')->toArray();
								for($i=0;$i<count($assistTypes);$i++){
									echo $assistTypes[$i]."<br>";
								}
								?>
							</td>
							<td> Paid </td>
						</tr>
					   @endforeach
                     @else
						<tr>
							<td colspan="7" align="center">
								-
							</td>
						</tr>
                     @endif
                     	
                    </tbody>
                </table>

            </div>
        </div>

</div>
</div>        
        
        
      
