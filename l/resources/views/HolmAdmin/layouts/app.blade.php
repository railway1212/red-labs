<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Homecare for the Elderly. Helping you find affordable care quickly and easily">
    <meta name="keywords" content="some, words">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{$title}}</title>
    <link rel="icon" type="image/png" href="{{asset('favicon.png')}}">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,900" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('public/css/admin/main2.min.css')}}">
    <link rel="stylesheet" href="{{asset('public/css/admin/main4.min.css')}}">
    <link rel="stylesheet" href="{{asset('public/css/admin/main.min.css')}}">
    <link rel="stylesheet" href="{{asset('public/css/admin/custom.css')}}">
    <link rel="stylesheet" href="{{asset('public/css/cupertino/jquery-ui.min.css')}}">
    <!-- Select2 -->
    <link rel="stylesheet" href="{{ asset('public/bower_components/select2/dist/css/select2.min.css') }}">
    <!-- bootstrap wysihtml5 - text editor -->
    <link rel="stylesheet" href="{{ asset('public/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}">
    <style>
        .modal-dialog {
            position: relative;
            top: 30%;
            left: 50%;
            width: auto;
            margin: 100px 300px auto 300px !important;
        }
    </style>


    <script  src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="{{asset('public/js/jquery-ui.min.js')}}"></script>
    <script src="{{asset('public/js/bootstrap-notify.min.js')}}"></script>
    <script src="{{asset('public/js/admin/main.js')}}"></script>
</head>
<body>
<div class="adminWrapper">
    <div class="adminBox">
        <div class="sidePanel">
            @yield('logoInfo')
            @yield('navigation')
        </div>
        @yield('content')
    </div>
</div>




</body>
</html>
