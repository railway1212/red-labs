<div class="mainPanel">
    <h2 class="categoryTitle">
          <span class="categoryTitle__ico">
            <i class="fa fa-check-square" aria-hidden="true"></i>
          </span>
        dispute payouts
    </h2>
    <div class="panelHead">
        {!! Form::open(['method'=>'GET','route'=>'DisputePayouts']) !!}
        <div class="filterGroup">
            <div class="filterBox">
                <div class="formField  formField--fix-biger">
                    <div class="fieldWrap">
                        {!! Form::text('userName',null,['class'=>'formItem formItem--input formItem--search','maxlength'=>'60','placeholder'=>'Search name']) !!}
                    </div>
                </div>
            </div>
            {!! Form::submit('search',['class'=>'actionsBtn actionsBtn--filter actionsBtn--bigger']) !!}

        </div>
        {{--<div class="filterBox">--}}
            {{--<label for="buyer">By buyer--}}
                {{--{!! Form::radio('type','buyer', true,['id'=>'buyer']) !!}--}}
            {{--</label>--}}
            {{--<label for="carer">By carer--}}
                {{--{!! Form::radio('type','carer', false,['id'=>'carer']) !!}--}}
            {{--</label>--}}
        {{--</div>--}}

        <div class="panelHead__group">
            <div class="filterBox">
                <h2 class="filterBox__title themeTitle">
                   FILTER BY
                </h2>

                <div class="formField formField--fixed">
                    {{ Form::select('whoPaid', ['ALL', 'PAID TO CARER', 'PAID TO PURCHASER'], null, ['class' => 'formItem formItem--select']) }}
                    {{--<select class="formItem formItem--select">--}}
                        {{--<option value="#">--}}
                            {{--ALL--}}
                        {{--</option>--}}
                        {{--<option value="#">--}}
                            {{--PAID TO CARER--}}
                        {{--</option>--}}
                        {{--<option value="#">--}}
                            {{--PAID TO PURCHASER--}}
                        {{--</option>--}}
                    {{--</select>--}}
                </div>
            </div>
            {!! Form::submit('filter',['class'=>'actionsBtn actionsBtn--filter actionsBtn--bigger']) !!}
            {{--<a href="#" class="actionsBtn actionsBtn--filter actionsBtn--bigger">--}}
                {{--filter--}}
            {{--</a>--}}
            
<!--
            <a href="#" class="print">
                <i class="fa fa-print" aria-hidden="true"></i>
            </a>
-->
          	<a href="{{route('disputePayoutsDownload')}}?userName={{ app('request')->input('userName') }}&whoPaid={{ app('request')->input('whoPaid') }}" class="actionsBtn actionsBtn--filter actionsBtn--bigger">
					Export to CSV
		    </a>  
            
        </div>
        {!! Form::close()!!}

    </div>


    <div class="tableWrap tableWrap--margin-t">
        <table class="adminTable">
            <thead>
            <tr>
                <td class="orderNumber">
                  <span class="td-title td-title--number">
                   №
                  </span>
                </td>
                <td class=" ordninary-td ordninary-td--wider no-padding-l">
                  <span class="td-title td-title--green">
                  purchaser
                  </span>

                </td>

                <td class="ordninary-td ordninary-td--wider">
                  <span class="td-title td-title--orange">
                   carer
                  </span>
                </td>
                <td class="ordninary-td ordninary-td--wider">
                  <span class="td-title td-title--booking ">
                    booking
                  </span>
                </td>
                <td class="ordninary-td">
                  <span class="td-title td-title--transaction ">
                    transaction
                  </span>
                </td>
                <td class="bigger-td bigger-td--middle">
                  <span class="td-title td-title--appointment ">
                    appointment
                  </span>
                </td>
                <td class="ordninary-td  ordninary-td--big">
                  <span class="td-title td-title--admin-comment ">
                    admin comments
                  </span>
                </td>

                <td class="ordninary-td   ">
                  <span class="td-title td-title--actions ">
                    actions
                  </span>
                </td>
                <td class="ordninary-td   ">
                  <span class="td-title td-title--payout-status ">
                    payout status
                  </span>
                </td>

            </tr>

            <tr class="extra-tr">
                <td></td>
                <td class="for-inner">
                    <table class="innerTable ">
                        <tbody><tr>
                            <td class="idField">
                                <span class="extraTitle">id</span>
                            </td>
                            <td class="nameField">
                                <span class="extraTitle">name</span>
                            </td>

                        </tr>

                        </tbody></table>
                </td>
                <td class="for-inner">
                    <table class="innerTable ">
                        <tbody><tr>
                            <td class="idField">
                                <span class="extraTitle">id</span>
                            </td>
                            <td class="nameField">
                                <span class="extraTitle">name</span>
                            </td>

                        </tr>

                        </tbody></table>
                </td>
                <td class="for-inner">
                    <table class="innerTable innerTable--no-fixed">
                        <tbody><tr>
                            <td class="idField">
                                <span class="extraTitle">id</span>
                            </td>
                            <td class="nameField">
                                <span class="extraTitle">Booking details</span>
                            </td>

                        </tr>

                        </tbody></table>
                </td>
                <td class="for-inner">
                    <table class="innerTable ">
                        <tbody><tr>
                            <td class=" ">
                                <span class="extraTitle">id</span>
                            </td>
                        </tr>

                        </tbody></table>
                </td>
                <td class="for-inner">
                    <table class="innerTable ">
                        <tbody><tr>

                            <td class=" ">
                                <span class="extraTitle">appointment <br>id</span>
                            </td>
                            <td class=" ">
                                <span class="extraTitle">from</span>
                            </td>
                            <td class=" ">
                                <span class="extraTitle">to</span>
                            </td>
                            <td class="">
                                <span class="extraTitle">amount carer</span>
                            </td>
                            <td class="">
                                <span class="extraTitle">amount purchaser</span>
                            </td>
                        </tr>

                        </tbody></table>
                </td>
                <td class="for-inner">
                    <table class="innerTable ">
                        <tbody><tr>
                            <td class="">
                                <span class="extraTitle">Details of <br>dispute</span>
                            </td>
                            <td class=" ">
                                <span class="extraTitle">Details of <br>Dispute Resolution</span>
                            </td>

                        </tr>

                        </tbody></table>
                </td>
                <td>

                </td>
                <td>

                </td>

            </tr>


            </thead>

            <tbody>
                @if($disputePayouts->count() > 0)
                    @foreach($disputePayouts as $disputePayout)
                    <tr>
                        <td align="center">
                            {{$disputePayout->id}}
                        </td>
                        <td class="for-inner">
                            <table class="innerTable ">
                                <tbody><tr>
                                    <td class="idField">
                                        <span>{{$disputePayout->appointment->booking->bookingPurchaserProfile->id}}</span>
                                    </td>
                                    <td class="nameField">
                                        <a href="/l/purchaser-settings/{{$disputePayout->appointment->booking->bookingPurchaserProfile->id}}" class="tableLink">{{$disputePayout->appointment->booking->bookingPurchaserProfile->full_name}}</a>
                                    </td>
                                </tr>
                                </tbody></table>
                        </td>
                        <td class="for-inner">
                            <table class="innerTable ">
                                <tbody><tr>
                                    <td class="idField">
                                        <span>{{$disputePayout->appointment->booking->bookingCarerProfile->id}}</span>
                                    </td>
                                    <td class="nameField">
                                        <a href="/l/carer-settings/{{$disputePayout->appointment->booking->bookingCarerProfile->id}}" class="tableLink" target="_blank">{{$disputePayout->appointment->booking->bookingCarerProfile->full_name}}</a>
                                    </td>
                                </tr>
                                </tbody></table>
                        </td>
                        <td class="for-inner">
                            <table class="innerTable ">
                                <tbody><tr>
                                    <td class="idField">
                                        <span>{{$disputePayout->appointment->booking->id}}</span>
                                    </td>
                                    <td class="nameField">
                                        <a href="{{url('/l/bookings/'.$disputePayout->appointment->booking->id.'/details')}}" class="tableLink">Go to workroom</a>
                                    </td>
                                </tr>
                                </tbody></table>
                        </td>
                        <td class="for-inner">
                            <table class="innerTable ">
                                <tbody><tr>
                                    <td class=" " style="overflow-x: scroll;">
                                        <span>
                                            @if($disputePayout->appointment->booking->payment_method == 'credit_card')
                                                @if(isset($disputePayout->appointment->booking->transaction))
                                                    <a href="https://dashboard.stripe.com/test/payments/{{$disputePayout->appointment->booking->transaction->id}}"
                                                       target="_blank">
                                                        {{$disputePayout->appointment->booking->transaction->id}}</a>
                                                    (via credit card)
                                                @else
                                                    free hour
                                                @endif
                                            @elseif($disputePayout->appointment->booking->payment_method == 'bonus_wallet')
                                                {{$disputePayout->appointment->booking->transaction->id}} (via bonuses)
                                            @endif
                                        </span>
                                    </td>
                                </tr>

                                </tbody></table>
                        </td>
                        <td class="for-inner">
                            <table class="innerTable ">
                                <tbody><tr>
                                    <td class=" ">
                                        <span>{{$disputePayout->appointment->id}}</span>
                                        
                                    </td>

                                    <td class=" ">
                                        <span>
                                            {{\Carbon\Carbon::parse($disputePayout->appointment->date_start)->format("d-m-Y")}}
                                            <br>
                                            {{\Carbon\Carbon::parse($disputePayout->appointment->time_from)->format("h:i A")}}
                                        </span>
                                    </td>
                                    <td class=" ">
                                        <span>{{\Carbon\Carbon::parse($disputePayout->appointment->time_to)->format("h:i A")}}</span>

                                    </td>
                                    <td class="">
                                        <span>{{$disputePayout->appointment->price_for_carer}}</span>

                                    </td>
                                    <td class="">
                                        <span>{{$disputePayout->appointment->price_for_purchaser}}</span>
                                    </td>


                                </tr>

                                </tbody></table>
                        </td>
                        <td class="for-inner">
                            <table class="innerTable ">
                                <tbody><tr>
                                    <td class=" ">
                                        <div class="formField">
                                            <div class="fieldWrap">
                                                <input type="search" data-dispute_payout_id="{{$disputePayout->id}}" class="detailsOfDispute formItem formItem--without-ico " placeholder="Text" value="{{$disputePayout->details_of_dispute}}">
                                            </div>
                                        </div>
                                    </td>
                                    <td class=" ">
                                        <div class="formField">
                                            <div class="fieldWrap">
                                                <input type="search" data-dispute_payout_id="{{$disputePayout->id}}" class="detailsOfDisputeResolution formItem formItem--without-ico " placeholder="Text" value="{{$disputePayout->details_of_dispute_resolution}}">
                                            </div>
                                        </div>
                                    </td>
                                </tr>

                                </tbody></table>
                        </td>
                        <td>
                            <div class="actionsGroup">
                                @if($disputePayout->status == 'pending')
                                <button data-dispute_payout_id="{{$disputePayout->id}}" class="makePayoutToCarer actionsBtn actionsBtn--accept actionsBtn--wider">
                                    pay out to carer
                                </button>
                                <button data-dispute_payout_id="{{$disputePayout->id}}" class="makePayoutToPurchaser actionsBtn actionsBtn--view actionsBtn--wider">
                                    pay out to purchaser
                                </button>
                                @else
                                -
                                @endif
                            </div>

                        </td>
                        <td>
                            <div class="profStatus profStatus--left">
                                @if($disputePayout->status == 'carer')
                                    <span class="profStatus__item profStatus__item--new">PAID TO Carer</span>
                                @elseif($disputePayout->status == 'purchaser')
                                    <span class="profStatus__item profStatus__item--new">PAID TO Purchaser</span>
                                @else
                                    <span class="profStatus__item profStatus__item--progress">pending</span>
                                @endif
                            </div>
                        </td>

                    </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="9" align="center">
                            -
                        </td>
                    </tr>
                @endif
            </tbody>
        </table>
    </div>
</div>

<script>
    $('.detailsOfDispute').on('input', function () {
        var dispute_payout_id = $(this).attr('data-dispute_payout_id');
        var text = $(this).val();
        $.ajax({url :'/l/admin/dispute-payout/' + dispute_payout_id + '/detailsOfDispute', type: 'PUT', data: {'details_of_dispute': text}});
    });
    $('.detailsOfDisputeResolution').on('input', function () {
        var dispute_payout_id = $(this).attr('data-dispute_payout_id');
        var text = $(this).val();
        $.ajax({url :'/l/admin/dispute-payout/' + dispute_payout_id + '/detailsOfDisputeResolution', type: 'PUT', data: {'details_of_dispute_resolution': text}});
    });

    $('.makePayoutToCarer').click(function () {
        var dispute_payout_id = $(this).attr('data-dispute_payout_id');
        $.post('/l/admin/dispute-payout/' + dispute_payout_id + '/payoutToCarer', function (data) {
            if(data.status == 'success'){
                location.reload();
            }
        });
    });
    $('.makePayoutToPurchaser').click(function () {
        var dispute_payout_id = $(this).attr('data-dispute_payout_id');
        $.post('/l/admin/dispute-payout/' + dispute_payout_id + '/payoutToPurchaser', function (data) {
            if(data.status == 'success'){
                location.reload();
            }
        });
    });
</script>
