<div class="mainPanel">
    <h2 class="categoryTitle">
          <span class="categoryTitle__ico">
              <i class="fa fa-user-secret" aria-hidden="true"></i>
          </span>
        GDPR
    </h2>
    
    <div class="panelHead">
       <div class="panelHead__group">
			{!! Form::open(['method'=>'GET','route'=>'gdpr']) !!}
			<div class="filterBox">
				<div class="formField formField--fix-biger"style="margin-right: 10px">
					<div class="fieldWrap">
						{!! Form::text('userName',null,['class'=>'formItem formItem--input formItem--search','maxlength'=>'60','placeholder'=>'Search name']) !!}
					</div>
				</div>

				{!! Form::submit('filter',['class'=>'actionsBtn actionsBtn--filter actionsBtn--bigger']) !!}
			</div>
			{!! Form::close()!!}
			
			<a href="{{route('gdprDownload')}}?userName={{ app('request')->input('userName') }}" class="actionsBtn actionsBtn--filter actionsBtn--bigger">
				Export to CSV
			</a>

        </div>
	</div>
	
	
	<div class="financialsContainer">
		@if($user)
	
		
			<div class="financialsCard">
				<div class="financialsCard__header">
					<div class="financialsCard__title">
						<h2>
							Profile Details
						</h2>
					</div>
				</div>
				
				<div class="financialsCard__header"><h3>General</h3></div>
				
				<div class="row financialsCard__header">
					<div class="col-md-6"> First Name</div>
					<div class="col-md-6"> {{$user->first_name}}</div>
				</div>

				<div class="row financialsCard__header">
					<div class="col-md-6"> Last Name </div>
					<div class="col-md-6">{{$user->family_name}}</div>
				</div>
				<div class="row financialsCard__header">
					<div class="col-md-6"> Gender </div>
					<div class="col-md-6">{{$user->gender}}</div>
				</div>
				<div class="row financialsCard__header">
					<div class="col-md-6"> Date of birth </div>
					<div class="col-md-6">{{$user->DoB}}</div>
				</div>
				
				<div class="row financialsCard__header">
					<div class="col-md-6"> I like to be called </div>
					<div class="col-md-6">{{$user->like_name}}</div>
				</div>
				
				<div class="row financialsCard__header">
					<div class="col-md-6"> Own referral Code </div>
					<div class="col-md-6">{{$user->own_referral_code}}</div>
				</div>
				
				<div class="row financialsCard__header">
					<div class="col-md-6"> Registered On </div>
					<div class="col-md-6"> {{$general->created_at}}</div>
				</div>
				
				<div class="financialsCard__header"><h3>Contact</h3></div>
				
				<div class="row financialsCard__header">
					<div class="col-md-6"> Address Line 1 </div>
					<div class="col-md-6">{{$user->address_line1}}</div>
				</div>
				
				<div class="row financialsCard__header">
					<div class="col-md-6"> Address Line 2 </div>
					<div class="col-md-6">{{$user->address_line2}}</div>
				</div>
				
				<div class="row financialsCard__header">
					<div class="col-md-6">Town / city  </div>
					<div class="col-md-6">{{$user->town}}</div>
				</div>
				
				<div class="row financialsCard__header">
					<div class="col-md-6"> Post code</div>
					<div class="col-md-6">{{$user->postcode}}</div>
				</div>
				
				<div class="row financialsCard__header">
					<div class="col-md-6"> Mobile Number</div>
					<div class="col-md-6">{{$user->mobile_number}}</div>
				</div>
				
				<div class="row financialsCard__header">
					<div class="col-md-6"> Email Address</div>
					<div class="col-md-6">{{$user->user_email}}</div>
				</div>
				
				@if($user->user_type == 'carer')
				<div class="financialsCard__header"><h3>Bank Account</h3></div>
				  
				@else
				<div class="financialsCard__header"><h3>Payment Details</h3></div>
				@endif
				
			  </div>
          
		
		@else
		
		  <div class="financialsCard">
            <div class="financialsRow" style="text-align:center;display:block;">
				Please search by user id or name
            </div>
          </div>

		@endif

    </div>
    
</div>

