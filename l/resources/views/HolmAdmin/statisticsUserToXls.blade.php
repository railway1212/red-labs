<div class="mainPanel">
    <div class="statisticGroup">
        <div class="statisticBox">
            <div class="tableWrap">
				
				
                <div class="statisticHead">
                    <h2 class="statisticHead__title">
                  <span class="statisticHead__ico">
                    <i class="fa fa-area-chart" aria-hidden="true"></i>
                  </span>
                       ACCOUNTANT REPORT
                    </h2>
                </div>
                
                <table class="statisticTable">
                    <thead>	
                    <tr>
					   <td class="">
						  <span class="ordinaryTitle td-stat td-stat--start">
						  User Name
						  </span>
                       </td>
                       
                        <td class="">
						  <span class="textCenter">
						  Invoice Number
						  </span>
                        </td>
                        
                        <td class="">
						  <span class="textCenter">
						  Invoice Date
						  </span>
                        </td>
                        
                        <td class="">
						  <span class="textCenter">
						 Payment Date
						  </span>
                        </td>
                        
                        <td class="">
						  <span class="textCenter">
						Description
						  </span>
                        </td>
						
                        <td class="">
						  <span class="textCenter">
						Quantity
						  </span>
                        </td>
                        
                        <td class="">
						  <span class="textCenter">
						Unit Amount
						  </span>
                        </td>
                        
                        <td class="">
						  <span class="textCenter">
						Account code
						  </span>
                        </td>
                        
                        <td class="">
						  <span class="textCenter">
						Tax Type
						  </span>
                        </td>	
                    </tr>
                    </thead>
                    <tbody>
                   @if($disputePayouts->count() > 0)
						@foreach($disputePayouts as $disputePayout)
                        <tr class="statisticRow">
						   <td class="">
							  <span class="ordinaryTitle td-stat td-stat--start">
								@if($disputePayout->status == 'carer')
									{{$disputePayout->appointment->booking->bookingCarerProfile->full_name}}
								@else
									{{$disputePayout->appointment->booking->bookingPurchaserProfile->full_name}}
								@endif
							  </span>
						   </td>
						   
							<td class="">
							  <span class="textCenter">
							    {{$disputePayout->appointment->id}}
							  </span>
							</td>
							
							<td class="">
							  <span class="textCenter">
							    {{\Carbon\Carbon::parse($disputePayout->appointment->date_start)->format("d-m-Y")}}  {{\Carbon\Carbon::parse($disputePayout->appointment->time_from)->format("h:i A")}}
							  </span>
							</td>
							
							<td class="">
							  <span class="textCenter">
								 {{\Carbon\Carbon::parse($disputePayout->appointment->booking->transaction->created_at)->format("d-m-Y")}}  		  
							  </span>
							</td>
							
							<td class="">
							  <span class="textCenter">
							    @if($disputePayout->status == 'carer')
									Paid to carer
								@else
								    Paid to purchaser
								@endif
							  </span>
							</td>
							
							<td class="">
							  <span class="textCenter">
							  1
							  </span>
							</td>
							
							<td class="">
							  <span class="textCenter">
								{{$disputePayout->appointment->booking->purchaser_price}}
							  </span>
							</td>
							
							<td class="">
							  <span class="textCenter">
							    @if($disputePayout->status == 'carer')
									310
								@else
								    200
								@endif
							  </span>
							</td>
							
							<td class="">
							  <span class="textCenter">
							  No VAT
							  </span>
							</td>	
							
										
                        </tr>
                   	@endforeach
				    @else
						<tr>
							<td colspan="7" align="center">
								-
							</td>
						</tr>
					@endif
                    </tbody>
                </table>

            </div>
        </div>

</div>
</div>        
