<div class="mainPanel">
    <h2 class="categoryTitle">
          <span class="categoryTitle__ico">
            <i class="fa fa-money" aria-hidden="true"></i>
          </span>
        payouts to carers
    </h2>
    <div class="panelHead">
        {!! Form::open(['method'=>'GET','route'=>'PayoutsToCarers']) !!}
        <div class="filterBox">
            <div class="formField formField--search">
                <div class="fieldWrap">
                    {!! Form::text('userName',null,['class'=>'formItem formItem--input formItem--search','maxlength'=>'60','placeholder'=>'Search name']) !!}
                </div>

            </div>
            {!! Form::submit('filter',['class'=>'actionsBtn actionsBtn--filter actionsBtn--bigger']) !!}
        </div>
        {!! Form::close()!!}
        <div class="panelHead__group">
<!--
            <a href="#" class="print">
                <i class="fa fa-print" aria-hidden="true"></i>
            </a>
-->

		<a href="{{route('carerPaymentDownload')}}?userName={{ app('request')->input('userName') }}" class="actionsBtn actionsBtn--filter actionsBtn--bigger">
			Export to CSV
		</a> 
        </div>
    </div>

    <div class="tableWrap tableWrap--margin-t">
        <table class="adminTable">
            <thead>
                <tr>
                    <td class=" ordninary-td no-padding-l">
                      <span class="td-title td-title--transaction">
                        transaction id
                      </span>
                    </td>
                    <td class=" ordninary-td ordninary-td--wider no-padding-l">
                      <span class="td-title td-title--orange">
                      carer
                      </span>
                    </td>
                    <td class="bigger-td bigger-td--wider">
                      <span class="td-title td-title--booking ">
                        appointment
                      </span>
                    </td>
                </tr>
                <tr class="extra-tr">
                    <td></td>
                    <td class="for-inner">
                        <table class="innerTable ">
                            <tbody><tr>
                                <td class="idField">
                                    <span class="extraTitle">id</span>
                                </td>
                                <td class="nameField">
                                    <span class="extraTitle">name</span>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                    <td class="for-inner">
                        <table class="innerTable ">
                            <tbody><tr>
                                <td class="">
                                    <span class="extraTitle">total</span>
                                </td>
                                <td class="">
                                    <span class="extraTitle">details</span>
                                </td>
                                <td class="">
                                    <span class="extraTitle">actions</span>
                                </td>
                                <td class="">
                                    <span class="extraTitle">payout status</span>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
            </thead>
            <tbody>
            @if(count($all) > 0)
                @foreach($all as $one)
                    @if(isset($one->total))
                        <tr>
                            <td align="center">
                                -
                            </td>
                            <td class="for-inner">
                                <table class="innerTable ">
                                    <tbody>
                                    <tr>
                                        <td class="idField">
                                            <span>{{$one->carer_id}}</span>
                                        </td>
                                        <td class="nameField">
                                            <a href="/larvel/carer-settings/{{$one->carer_id}}" class="tableLink">{{$one->first_name.' '.$one->family_name}}</a>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </td>
                            <td class="for-inner">
                                <table class="innerTable ">
                                    <tbody>
                                    <tr>
                                        <td class="">
                                            <span><i class="fa fa-gbp" aria-hidden="true"></i> {{$one->total}}</span>
                                        </td>
                                        <td class="">
                                            <a href="{{ route('viewBookingDetails', ['booking' => $one->booking_id]) }}">
                                                <span><i aria-hidden="true"></i> {{ $one->id }}</span>
                                            </a>
                                        </td>
                                        <td class="">
                                            <!-- <div class="actionsGroup">
                                                <button data-booking_id="{{$one->booking_id}}"
                                                        class="makePayout actionsBtn actionsBtn--accept">
                                                    payout appointment
                                                </button>
                                            </div> -->
                                          	<div class="actionsGroup">
                                                <button data-booking_id="{{$one->id}}"
                                                        class="makePayout actionsBtn actionsBtn--accept">
                                                    payout appointment
                                                </button>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="profStatus profStatus--left">
                                                <span class="profStatus__item profStatus__item--progress">pending</span>

                                            </div>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    @else
                        <tr>
                            <td align="center">
                                <a href="https://dashboard.stripe.com/test/payments/{{$one->id}}"
                                   target="_blank">{{$one->id}}</a>
                            </td>
                            <td class="for-inner">
                                <table class="innerTable ">
                                    <tbody>
                                    <tr>
                                        <td class="idField">
                                            <span>
                                          @if($one->booking_id > 0)	
													{{$one->booking->bookingCarer->id}}
													
												@elseif($one->bonus_id > 0)
												 <?php
												 $bonusPayout = \App\BonusPayout::where('id', $one->bonus_id)->pluck('user_id')->toArray();
												 echo $bonusPayout[0];

												 ?>	
													
												@endif
                                          
                                          
                                          </span>
                                        </td>
                                        <td class="nameField">
                                           	@if($one->booking_id > 0)	
													<a href="/l/carer-settings/{{$one->booking->bookingCarer->id}}"  class="tableLink">{{$one->booking->bookingCarerProfile->full_name}}</a> 
													
												@elseif($one->bonus_id > 0)
												    <?php
												       $bonusUser = App\CarersProfile::where('id', $bonusPayout[0])->get();
												    ?>
													<a href="/l/carer-settings/{{$bonusPayout[0]}}"  class="tableLink">
													   {{$bonusUser[0]->first_name}} {{$bonusUser[0]->family_name}}
													</a> 
													
												@endif	
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </td>
                            <td class="for-inner">
                                <table class="innerTable ">
                                    <tbody>
                                    <tr>
                                        <td class="">
                                            <span><i class="fa fa-gbp"
                                                     aria-hidden="true"></i> {{$one->amount/100}}</span>
                                        </td>
                                        <td class="">
                                            <span><i aria-hidden="true"></i>
                                                <?php  if($one->bonus_id > 0){
													
													echo 'Bonus Payout';
													
												}else{
  													if($one->appointment_id == '' && $one->booking_id != ''){ //echo 11;
														$allApps = \App\Appointment::where('booking_id', $one->booking_id)->where('payout',1)->pluck('id')->toArray();
                                                    }else if($one->appointment_id != '' && $one->booking_id != ''){ //echo 12;
                                                      $allApps = \App\Appointment::where('id', $one->appointment_id)->where('payout',1)->pluck('id')->toArray();
                                                    }
 
													for ($i = 0; $i < count($allApps); $i++) {
														echo "<a href='/l/bookings/" . $one->booking_id . "/details'>" . $allApps[$i] . "<a/><br>";
													}
											    }
                                                ?>
                                            </span>
                                        </td>
                                        <td class="">
                                            <div class="actionsGroup">
                                                -
                                            </div>
                                        </td>
                                        <td>
                                            <div class="profStatus profStatus--left">
                                                <span class="profStatus__item profStatus__item--new">paid</span>

                                            </div>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    @endif
                @endforeach
            @endif

            @if(false)
                {{--@if($transfers->count() > 0 || count($potentialPayouts) > 0)--}}
                @foreach($potentialPayouts as $potentialPayout)
                    <tr>
                        <td align="center">
                            -
                        </td>
                        <td class="for-inner">
                            <table class="innerTable ">
                                <tbody><tr>
                                    <td class="idField">
                                        <span>{{$potentialPayout->carer_id}}</span>
                                    </td>
                                    <td class="nameField">
                                        <a href="#" class="tableLink">{{$potentialPayout->first_name.' '.$potentialPayout->family_name}}</a>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </td>
                        <td class="for-inner">
                            <table class="innerTable ">
                                <tbody><tr>
                                    <td class="">
                                        <span><i class="fa fa-gbp" aria-hidden="true"></i> {{$potentialPayout->total}}</span>
                                    </td>
                                    <td class="">
                                        <span><i class="fa fa-gbp"
                                                 aria-hidden="true"></i>{{ $potentialPayout->id }}</span>
                                    </td>
                                    <td class="nameField">
                                        <?php $allApps = \App\Appointment::where('booking_id', $potentialPayout->booking_id)->pluck('id')->toArray();
                                            $types = \App\AppointemntAssistanceType::whereIn('appointment_id', $allApps)->pluck('assistance_type_id')->toArray();
                                            $assistTypes = \App\AssistanceType::whereIn('id', $types)->pluck('name')->toArray();
                                            for($i=0;$i<count($assistTypes);$i++){
                                                echo $assistTypes[$i]."<br>";
                                            }
                                        ?>
                                    </td>
                                    <td class="nameField">
                                        <div class="actionsGroup">
                                            <button data-booking_id="{{$potentialPayout->booking_id}}" class="makePayout actionsBtn actionsBtn--accept">
                                                payout bookings
                                            </button>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="profStatus profStatus--left">
                                            <span class="profStatus__item profStatus__item--progress">pending</span>

                                        </div>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                @endforeach
                @foreach($transfers as $transfer)
                <tr>
                    <td  align="center">
                        <a href="https://dashboard.stripe.com/test/payments/{{$transfer->id}}" target="_blank">{{$transfer->id}}</a>
                    </td>
                    <td class="for-inner">
                        <table class="innerTable ">
                            <tbody><tr>
                                <td class="idField">
                                    <span>{{$transfer->booking->bookingCarer->id}}</span>
                                </td>
                                <td class="nameField">
                                    <a href="#" class="tableLink">{{$transfer->booking->bookingCarerProfile->full_name}}</a>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                    <td class="for-inner">
                        <table class="innerTable ">
                            <tbody><tr>
                                <td class="">
                                    <span><i class="fa fa-gbp" aria-hidden="true"></i> {{$transfer->amount/100}}</span>
                                </td>
                                <td class="nameField">
                                    <?php $allApps = \App\Appointment::where('booking_id', $potentialPayout->booking_id)->pluck('id')->toArray();
                                    $types = \App\AppointemntAssistanceType::whereIn('appointment_id', $allApps)->pluck('assistance_type_id')->toArray();
                                    $assistTypes = \App\AssistanceType::whereIn('id', $types)->pluck('name')->toArray();
                                    for($i=0;$i<count($assistTypes);$i++){
                                        echo $assistTypes[$i]."<br>";
                                    }
                                    ?>
                                </td>
                                <td class="nameField">
                                    <div class="actionsGroup">
                                        -
                                    </div>
                                </td>
                                <td>
                                    <div class="profStatus profStatus--left">
                                        <span class="profStatus__item profStatus__item--new">paid</span>

                                    </div>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                @endforeach
            @else
                <tr>
                    <td colspan="7" align="center">
                        -
                    </td>
                </tr>
            @endif
            </tbody>
        </table>
    </div>
    {!! $pagination !!}
</div>

<script>
    $('.makePayout').click(function () {
        showSpinner();
        var booking_id = $(this).attr('data-booking_id');
      //console.log("Booking ID : ",booking_id);return false;
        $.post('/l/admin/carer-payout/'+booking_id, function (data) {
            if(data.status == 'success'){
                location.reload();
            } else {
                showErrorModal({title: 'Error', description: data.message});
            }

            hideSpinner();
        });
    });
</script>
