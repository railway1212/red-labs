        <thead>
        <tr>
{{--            <td class="orderNumber">
                  <span class="td-title td-title--number">
                   №
                  </span>
            </td>--}}
            <td class=" ordninary-td ordninary-td--wider no-padding-l">
                  <span class="td-title td-title--green">
                  purchaser
                  </span>

            </td>
            <td class="ordninary-td ordninary-td--wider">
                  <span class="td-title td-title--darkest-mint ">
                    service user
                  </span>
            </td>
            <td class="ordninary-td ordninary-td--wider" >
                  <span class="td-title td-title--orange">
                   carer
                  </span>
            </td>
            <td class="bigger-td bigger-td--biggest">
                  <span class="td-title td-title--booking ">
                    booking
                  </span>
            </td>
            <td class="bigger-td bigger-td--bigger bookingbig">
                  <span class="td-title td-title--appointment ">
                    appointment
                  </span>
            </td>

        </tr>

        <tr class="extra-tr">
           {{-- <td></td>--}}
            <td class="for-inner">
                <table class="innerTable ">
                    <tr>
                        <td class="idField">
                            <span class="extraTitle">id</span>
                        </td>
                        <td class="nameField">
                            <span class="extraTitle">name</span>
                        </td>

                    </tr>

                </table>
            </td>
            <td class="for-inner">
                <table class="innerTable ">
                    <tr>
                        <td class="idField">
                            <span class="extraTitle">id</span>
                        </td>
                        <td class="nameField">
                            <span class="extraTitle">name</span>
                        </td>

                    </tr>

                </table>
            </td>
            <td class="for-inner">
                <table class="innerTable innerTable--no-fixed">
                    <tr>
                        <td class="idField">
                            <span class="extraTitle">id</span>
                        </td>
                        <td class="nameField">
                            <span class="extraTitle">name</span>
                        </td>

                    </tr>

                </table>
            </td>
            <td class="for-inner">
                <table class="innerTable ">
                    <tr>
                        <td class=" ">
                            <span class="extraTitle">booking <br />id</span>
                        </td>
                        <td class=" ">
                            <span class="extraTitle">Cancel <br />Booking</span>
                        </td>
                        <td class=" ">
                            <span class="extraTitle">from</span>
                        </td>
                        <td class=" ">
                            <span class="extraTitle">to</span>
                        </td>
{{--                        <td class="">
                            <span class="extraTitle">FREQUENCY</span>
                        </td>--}}
                        <td class=" ">
                            <span class="extraTitle">status</span>
                        </td>
{{--                        <td class=" ">
                            <span class="extraTitle">Carer <br /> booking <br />status</span>
                        </td>
                        <td class=" ">
                            <span class="extraTitle">Purchaser <br /> booking <br />status</span>
                        </td>--}}

                    </tr>

                </table>
            </td>
            <td class="for-inner">
                <table class="innerTable ">
                    <tr>
                        <td class=" ">
                            <span class="extraTitle">Transaction <br />id</span>
                        </td>
                        <td class=" ">
                            <span class="extraTitle">appointment <br />id</span>
                        </td>
                        <td class=" ">
                            <span class="extraTitle">Cancel <br />Appointment</span>
                        </td>
                        <td class=" ">
                            <span class="extraTitle">from</span>
                        </td>
                        <td class=" ">
                            <span class="extraTitle">to</span>
                        </td>
                        <td class="">
                            <span class="extraTitle">purchaser <br /> appointment <br /> value, <i class="fa fa-gbp" aria-hidden="true"></i></span>
                        </td>
                        <td class="">
                            <span class="extraTitle">carer <br /> appointment <br /> value, <i class="fa fa-gbp" aria-hidden="true"></i></span>
                        </td>
                        <td class=" ">
                            <span class="extraTitle">status</span>
                        </td>
                        <td class=" ">
                            <span class="extraTitle">Carer  booking <br />status</span>
                        </td>
                        <td class=" ">
                            <span class="extraTitle">Purchaser <br /> booking <br />status</span>
                        </td>

                    </tr>

                </table>
            </td>

        </tr>


        </thead>

