    <div class="mainPanel">
            <h2 class="categoryTitle">
          <span class="categoryTitle__ico">
            <i class="fa fa-user" aria-hidden="true"></i>
          </span>
                booking details
            </h2>
            <div class="panelHead">

                <div class="panelHead__group">
                    {!! Form::open(['method'=>'GET','route'=>'booking.index']) !!}
                    <div class="filterBox">
                        <div class="formField formField--fix-biger"style="margin-right: 10px">
                            <div class="fieldWrap">
                                {!! Form::text('userName',null,['class'=>'formItem formItem--input formItem--search','maxlength'=>'60','placeholder'=>'Search name']) !!}
                            </div>
                        </div>
                        &nbsp;&nbsp;&nbsp;&nbsp;
                        <h2 class="filterBox__title themeTitle">
                            filter by
                        </h2>

                        <div class="formField formField--fixed">
                            {!! Form::select('filter',['0'=>'ALL']+$bookingStatuses,
                            null,['class'=>'formItem formItem--select','style'=>'text-transform:uppercase']) !!}
                        </div>


                        <div class="formField">
                            {!! Form::text('dateFrom', null,['class'=>'formItem formItem--input formItem--search datepicker','placeholder'=>'From Date']) !!}
                        </div>

                        <div class="formField">
                            {!! Form::text('dateTo', null,['class'=>'formItem formItem--input formItem--search datepicker','placeholder'=>'Date To']) !!}
                        </div>



                        {!! Form::submit('filter',['class'=>'actionsBtn actionsBtn--filter actionsBtn--bigger']) !!}
                    </div>
                    {!! Form::close()!!}

                </div>
<!--
                <a href="#" class="print">
                    <i class="fa fa-print" aria-hidden="true"></i>
                </a>
-->
			<div class="panelHead__group">
				<a href="{{route('bookingDownload')}}?userName={{ app('request')->input('userName') }}&filter={{ app('request')->input('filter') }}&dateFrom={{ app('request')->input('dateFrom') }}&dateTo={{ app('request')->input('dateTo') }}" class="actionsBtn actionsBtn--filter actionsBtn--bigger">
					Export to CSV
				</a>
			</div>


            </div>
            @include(config('settings.theme').'.bookingsDetails.mainTable')

        </div>
        
<!--Modals to confirm cancellation --->
<div id="confirm-cancel-appointment" class="modal fade" role="dialog">
    <div class="login">
        <div class="login__header">
            <p style="color: #fff;font-size: 1.563em;text-transform: uppercase;">Confirm action</p>
            <a href="#" data-dismiss="modal" class="close closeModal">
                <i class="fa fa-times"></i>
            </a>
        </div>
        <div class="who-you-are">
            <p class="who-you-are__text">Are you sure ?</p>
            <div class="who-you-are__box">
                <a href="#" class="who-you-are__item" onclick="confirmCancelAppointment()" data-dismiss="modal">
                    Yes
                </a>
                <a href="#" class="who-you-are__item" data-dismiss="modal">
                    No
                </a>
            </div>
        </div>
    </div>
</div>
<div id="confirm-cancel-booking" class="modal fade" role="dialog">
    <div class="login">
        <div class="login__header">
            <p style="color: #fff;font-size: 1.563em;text-transform: uppercase;">Confirm action</p>
            <a href="#" data-dismiss="modal" class="close closeModal">
                <i class="fa fa-times"></i>
            </a>
        </div>
        <div class="who-you-are">
            <p class="who-you-are__text">Are you sure ?</p>
            <div class="who-you-are__box">
                <a href="#" class="who-you-are__item" onclick="confirmCancelBooking()" data-dismiss="modal">
                    Yes
                </a>
                <a href="#" class="who-you-are__item" data-dismiss="modal">
                    No
                </a>
            </div>
        </div>
    </div>
</div>
        

    <!--Script Datepicker-->
    <script>
        $(document).ready(function(){

            $(".datepicker").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: "dd-mm-yy",
                showAnim: "slideDown"
            });
        });
  

    var clickedAppointmentId = 0;
    function setClickedAppointmentId(id){
        clickedAppointmentId = id;
    }
    
    function confirmCancelAppointment(){
        showSpinner();
        $.post('/l/appointments/'+clickedAppointmentId+'/cancel', function (data) {
            if(data.status == 'success'){
                location.reload();
            } else {
                showErrorModal({title: 'Error', description: data.message});
            }
            hideSpinner();
        });
    }
    
    
    var clickedBookingId = 0;
    function setClickedBookingId(id){
        clickedBookingId = id;
    }
    
    function confirmCancelBooking(){
        showSpinner();
        $.post('/l/bookings/'+clickedBookingId+'/cancel', function (data) {
            if(data.status == 'success'){
                location.reload();
            } else {
                showErrorModal({title: 'Error', description: data.message});
            }
            hideSpinner();
        });
    }
    
    
</script>

    <!-- End Script -->
