<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.5/css/bootstrap.min.css">
<link rel="stylesheet" href="http://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="http://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js"></script>


<div class="mainPanel">
    <h2 class="categoryTitle">
          <span class="categoryTitle__ico">
            <i class="fa fa-newspaper-o" aria-hidden="true"></i>
          </span>
        Banner -> Edit
    </h2>
    <div class="contentCard">
        <div class="contentCard__head contentCard__head--payment">
            <h2>Banner</h2>
        </div>
        <form role="form" action="{{ route('banner.update', $banner->id) }}" method="post" id="trustImageUpload" enctype="multipart/form-data">
            {{ csrf_field() }}
            {{ method_field('PATCH') }}
            <div class="contentBody">
             
             <div style="display:none">
                <div class="richTextwrapper fieldWrap" >
                    <div class="richtextedit">
                        <textarea id="textarea-1" class="textarea"></textarea>
                    </div>
                    <a href="javascript:void(0);" class="remove_button">Remove</a>
                </div>
                </br>
            </div>    

            <div class="fieldRow">
                    <div class="formField  formField--full">
                        <h2 class="fieldLabel">Select Page</h2>
                        <div class="fieldWrap">  
                            <select name="page_id">
                              <option value="9" @if($banner->page_id==9) selected @endif> Home</option>
                              <option value="1" @if($banner->page_id==1) selected @endif>About</option>
                              <option value="7"  @if($banner->page_id==7) selected @endif>I am a carer</option>
                            </select>
                        </div>
                    </div>
                </div>
                
                <div class="fieldRow">
                    <div class="formField  formField--full">
                        <h2 class="fieldLabel">Image</h2>
                        <div class="fieldWrap">  
                            <input type="file" name="image" /><br>
                            @if(!empty($banner->image_name))
                               <img src="{{URL::to('/')}}/public/image_Association/{{$banner->image_name}}" width="100"/></br>
                            @endif

                            <input type="text" name="image_alt" value="{{$banner->image_alt}}" placeholder="Image alt text"/>
                        </div>
                    </div>
                </div>

                  <div class="fieldRow">
                    <div class="formField  formField--full">
                        <h2 class="fieldLabel">Text</h2>
                    </div>
                </div>
                  <?php 
                    $bannaerContents = unserialize($banner->text); 
                    $i=1;
                   ?>
                   <div class="field_wrapper">
                @if(count($bannaerContents)>0)

                    @foreach($bannaerContents as $bannerContent)
                        <div class="fieldWrap">
                        <div class="richtextedit">  
                            <textarea name="text[]" class="textarea" id="textarea-1">{{ $bannerContent }}</textarea>
                        </div>
                            @if($i==1)
                                <a href="javascript:void(0);" class="add_button" title="Add field">Add</a>
                            @else
                                <a href="javascript:void(0);" class="remove_button">Remove</a>  
                            @endif
                        </div>
                        </br>
                    
                        <?php $i++; ?>
                    @endforeach
                @else
                  
                    <div class="fieldWrap">
                    <div class="richtextedit">   
                        <textarea name="text[]"  class="textarea" id="textarea-1"></textarea>
                    </div>    
                        <a href="javascript:void(0);" class="add_button" title="Add field">Add</a>
                    </div>
                    </br>

                @endif
                </div>

                <div class="settingBtn">
                    <input type="submit" class="actionsBtn actionsBtn--accept actionsBtn--big actionsBtn--no-centered"
                           value="update">
                </div>

            </div>
        </form>
    </div>
</div>

<script src="{{ asset('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}"></script>
<script>
    $(function () {
        //bootstrap WYSIHTML5 - text editor
        $('.textarea').wysihtml5()
    })
</script>

<script type="text/javascript">
$(document).ready(function(){
    var maxField = 10; //Input fields increment limitation
    var addButton = $('.add_button'); //Add button selector
    var wrapper = $('.field_wrapper'); //Input field wrapper
    var fieldHTML = '<div class="fieldWrap"><textarea name="text[]"/></textarea><a href="javascript:void(0);" class="remove_button">Remove</a></div>'; //New input field html 
    var x = 1; //Initial field counter is 1
    
    //Once add button is clicked
    $(addButton).click(function(){
        //Check maximum number of input fields
    var newElem = $('.richTextwrapper').first().clone();
    var num = $('.textarea').length + 1; //num is the total count of the cloned textareas
    newElem.find('.richtextedit').html('');
    newElem.find('.richtextedit').html('<textarea id="textarea-'+num+'" class="textarea" name="text[]"></textarea>');
    newElem.appendTo('.field_wrapper'); 
    $('#textarea-'+num).wysihtml5(); 

        if(x < maxField){ 
            x++; //Increment field counter
         // $(wrapper).append(fieldHTML); //Add field html
        }
        
        



    });
    
    //Once remove button is clicked
    $(wrapper).on('click', '.remove_button', function(e){
        e.preventDefault();
        $(this).parent('div').remove(); //Remove field html
        x--; //Decrement field counter
    });
});
</script>
