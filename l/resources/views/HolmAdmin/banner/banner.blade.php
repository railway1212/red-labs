<div class="mainPanel">
    <h2 class="categoryTitle">
          <span class="categoryTitle__ico">
            <i class="fa fa-newspaper-o" aria-hidden="true"></i>
          </span>
        Banners
    </h2>
    <div class="settingBtn">
        <a href="{{ route('banner.create') }}"
           class="actionsBtn actionsBtn--accept actionsBtn--big actionsBtn--no-centered">
            Create Banner
        </a>

    </div>
    <div class="tableWrap tableWrap--margin-t">
        <table class="adminTable">
            <thead>
            <tr>
                <td class="orderNumber">
                    <span class="td-title td-title--number">
                     №
                    </span>
                </td>

                <td class=" ordninary-td ">
                    <span class="td-title td-title--title">
                    Page
                    </span>

                </td>
                <td class=" ordninary-td  ">
                    <span class="td-title td-title--time">
                      Image
                    </span>
                </td>
                <td class=" ordninary-td ">
                    <span class="td-title td-title--actions">
                      actions
                    </span>
                </td>
            </tr>
            </thead>

            <tbody>
            @foreach($banners as $banner)
                <tr>
                    <td>
                        <span>{{ $banner->id }}</span>
                    </td>

                    <td class="">
                    @if($banner->page_id==9)
                        Home
                    @elseif($banner->page_id==1)
                        About
                    @else
                     I am a carer
                    @endif
                       
                    </td>
                    <td>
                       <div class="profStatus profStatus--left">
                            @if(!empty($banner->image_name))
                               <img src="{{URL::to('/')}}/public/image_Association/{{$banner->image_name}}" width="100"/>
                            @endif
                        </div>
                    </td>
                    <td>
                        <div class="actionsGroup">
                                <a href="{{ route('banner.edit', $banner->id) }}" class="actionsBtn actionsBtn--edit  ">
                                    edit
                                </a>
                                <a data-delete_banner_id="{{ $banner->id }}"
                                   class="actionsBtn actionsBtn--delete deleteBanner">
                                    delete
                                </a>
                          
                        </div>
                    </td> 
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
   

    
    
    

<script>
    $(document).ready(function () {
        $('.adminTable').on('click', '.deleteBanner', function (e) {
            e.preventDefault();
            var banner_id = $(this).attr('data-delete_banner_id');
            $.ajax({
                url: '/admin/banner/' + banner_id,
                type: 'PUT',
                data: {'delete_banner_id': banner_id},
                success: function () {
                    location.reload();
                },
                error: function () {
                    console.log('fail');
                }
            });
        });
        
       
    
    });
</script>
