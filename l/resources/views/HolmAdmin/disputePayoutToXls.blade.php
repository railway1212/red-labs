<div class="mainPanel">
    <div class="statisticGroup">
        <div class="statisticBox">
            <div class="tableWrap">
				
				
                <div class="statisticHead">
                    <h2 class="statisticHead__title">
                  <span class="statisticHead__ico">
                    <i class="fa fa-area-chart" aria-hidden="true"></i>
                  </span>
                        DISPUTE PAYOUTS  DETAILS
                    </h2>
                </div>
                
                <table class="statisticTable">
                    <thead>	
                    <tr>
                        <td class="textCenter">
						  <span class="ordinaryTitle td-stat">
						  NO
						  </span>
                        </td>
                        <td class="textCenter" colspan="2">
						  <span class="ordinaryTitle td-stat">
							Purchaser  
						  </span>
                        </td>
                        <td class="textCenter" colspan="2">
						  <span class="ordinaryTitle td-stat" >
							Carer   
						  </span>
                        </td>  
                        <td class="textCenter" colspan="2">
						  <span class="ordinaryTitle td-stat" >
							Booking   
						  </span>
                        </td>  
                        <td class="textCenter">
						  <span class="ordinaryTitle td-stat" >
							Transaction   
						  </span>
                        </td>  
                        <td class="textCenter" colspan="5">
						  <span class="ordinaryTitle td-stat" >
							Appointment    
						  </span>
                        </td>  
                        <td class="textCenter" colspan="2">
						  <span class="ordinaryTitle td-stat" >
						   Admin Comments  
						  </span>
                        </td> 
                        <td class="textCenter">
						  <span class="ordinaryTitle td-stat" >
						   Payout Status
						  </span>
                        </td> 
                        
                         
                    </tr>
                    <tr class="extra-tr">
						
						<td class="idField">
							<span class="extraTitle"></span>
						</td>
					
						<td class="idField">
							<span class="extraTitle">ID</span>
						</td>
						<td class="">
							<span class="extraTitle">NAME</span>
						</td>
						
						<td class="idField">
							<span class="extraTitle">ID</span>
						</td>
						<td class="">
							<span class="extraTitle">NAME</span>
						</td>
						
						<td class="idField">
							<span class="extraTitle">ID</span>
						</td>
						<td class="">
							<span class="extraTitle">BOOKING DETAILS</span>
						</td>
						
						<td class="">
							<span class="extraTitle">ID</span>
						</td>
						
						<td class="">
							<span class="extraTitle">APPOINTMENT ID</span>
						</td>
						<td class="">
							<span class="extraTitle">FROM</span>
						</td>
						<td class="">
							<span class="extraTitle">TO</span>
						</td>
						<td class="">
							<span class="extraTitle">AMOUNT CARER</span>
						</td>
						<td class="">
							<span class="extraTitle">AMOUNT PURCHASER</span>
						</td>
						
						<td class="">
							<span class="extraTitle">DETAILS OF DISPUTE</span>
						</td>
						<td class="">
							<span class="extraTitle">DETAILS OF DISPUTE RESOLUTION</span>
						</td>
						
						<td class="">
							<span class="extraTitle"></span>
						</td>	
					</tr>
                    </thead>
                    <tbody>
					  @if($disputePayouts->count() > 0)
						@foreach($disputePayouts as $disputePayout)
						 <tr>
							<td align="center">
								{{$disputePayout->id}}
							</td>
							<td class="idField">
								<span>{{$disputePayout->appointment->booking->bookingPurchaserProfile->id}}</span>
							</td>
							<td class="nameField">
								<a href="/l/purchaser-settings/{{$disputePayout->appointment->booking->bookingPurchaserProfile->id}}" class="tableLink">{{$disputePayout->appointment->booking->bookingPurchaserProfile->full_name}}</a>
							</td>
							<td class="idField">
								<span>{{$disputePayout->appointment->booking->bookingCarerProfile->id}}</span>
							</td>
							<td class="nameField">
								<a href="/l/carer/profile/{{$disputePayout->appointment->booking->bookingCarerProfile->id}}" class="tableLink" target="_blank">{{$disputePayout->appointment->booking->bookingCarerProfile->full_name}}</a>
							</td>
							
							<td class="idField">
								<span>{{$disputePayout->appointment->booking->id}}</span>
							</td>
							<td class="nameField">
								<a href="{{url('/l/bookings/'.$disputePayout->appointment->booking->id.'/details')}}" class="tableLink">Go to workroom</a>
							</td>
							 <td class=" " style="overflow-x: scroll;">
								<span>
									@if($disputePayout->appointment->booking->payment_method == 'credit_card')
										@if(isset($disputePayout->appointment->booking->transaction))
											<a href="https://dashboard.stripe.com/test/payments/{{$disputePayout->appointment->booking->transaction->id}}"
											   target="_blank">
												{{$disputePayout->appointment->booking->transaction->id}}</a>
											(via credit card)
										@else
											free hour
										@endif
									@elseif($disputePayout->appointment->booking->payment_method == 'bonus_wallet')
										{{$disputePayout->appointment->booking->transaction->id}} (via bonuses)
									@endif
								</span>
							</td>	
							
							<td class=" ">
								<span>{{$disputePayout->appointment->id}}</span>
							</td>

							<td class=" ">
								<span>
									{{\Carbon\Carbon::parse($disputePayout->appointment->date_start)->format("d-m-Y")}}
									<br>
									{{\Carbon\Carbon::parse($disputePayout->appointment->time_from)->format("h:i A")}}
								</span>
							</td>
							<td class=" ">
								<span>{{\Carbon\Carbon::parse($disputePayout->appointment->time_to)->format("h:i A")}}</span>

							</td>
							<td class="">
								<span>{{$disputePayout->appointment->price_for_carer}}</span>
							</td>
							<td class="">
								<span>{{$disputePayout->appointment->price_for_purchaser}}</span>
							</td>

							<td>
								{{$disputePayout->details_of_dispute}}
							</td>
							<td>
								{{$disputePayout->details_of_dispute_resolution}}		
							</td>
							
							<td>
								@if($disputePayout->status == 'carer')
									<span class="profStatus__item profStatus__item--new">PAID TO Carer</span>
								@elseif($disputePayout->status == 'purchaser')
									<span class="profStatus__item profStatus__item--new">PAID TO Purchaser</span>
								@else
									<span class="profStatus__item profStatus__item--progress">pending</span>
								@endif
								
							</td>
							
							 
						 </tr>
						@endforeach
					  @else
						<tr>
							<td colspan="9" align="center">
								-
							</td>
						</tr>
                     @endif	 
                    </tbody>
                </table>

            </div>
        </div>

</div>
</div>        
        
        
      
