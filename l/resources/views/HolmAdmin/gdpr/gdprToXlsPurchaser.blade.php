


<div class="mainPanel">
 <table class="statisticTable">
  <thead>
	 <tr>
		 <td align="left">{{$user->first_name}}  {{$user->family_name}}</td>
	</thead>
   </table> 	 


	<div class="tableWrap">
		<div class="statisticHead">
			<h4 class="statisticHead__title" style="color:navy">
				Profile Details 
			</h4>

		</div>
		<table class="statisticTable">
			<tbody>
				<tr>
					<th>ID</th>
					<td align="left">{{$user->id}}</td>
				</tr>
				<tr>
					<th>First Name</th>
					<td>{{$user->first_name}}</td>
				</tr>
				
				<tr>
					<th>Family Name</th>
					<td>{{$user->family_name}}</td>
				</tr>
				<tr>
					<th>I like to be called</th>
					<td>{{$user->like_name}}</td>
				</tr>
				<tr>
					<th>Gender</th>
					<td>{{$user->gender}}</td>
				</tr>
				<tr>
					<th>Mobile Number</th>
					<td>{{$user->mobile_number}}</td>
				</tr>
				<tr>
					<th>Address Line 1</th>
					<td>{{$user->address_line1}}</td>
				</tr>
				<tr>
					<th>Address Line 2</th>
					<td>{{$user->address_line2}}</td>
				</tr>
				<tr>
					<th>Town / City</th>
					<td>{{$user->town}}</td>
				</tr>
				<tr>
					<th>Post Code</th>
					<td>{{$user->postcode}}</td>
				</tr>
				<tr>
					<th>Date of Birth</th>
					<td>{{$user->DoB}}</td>
				</tr>
				
				<!--<tr>
					<th>Registration Progress</th>
					<td>{{$user->registration_progress}}</td>
				</tr>
				<tr>
					<th>Active User</th>
					<td>{{$user->active_user}}</td>
				</tr>
				<tr>
					<th>Registration Status</th>
					<td>{{$user->registration_status}}</td>
				</tr>
				<tr>
					<th>Profile Status</th>
					<td>{{$user->profiles_status_id}}</td>
				</tr>-->
				<tr>
					<th>Purchasing Care for</th>
					<td>{{$user->purchasing_care_for}}</td>
				</tr>

				@if($user->purchasing_care_for=='Someone else')
					<tr>
						<th>People I Am Buying carer for</th>
						<td>
						    @if($service_users->count()>0)
								@foreach($service_users as $su)
									@if($su->deleted!='Yes')
										@if($su->first_name==null)
											NA,
										@else
											{{$su->first_name}},
										@endif
									@endif
								@endforeach
							@else
								--
							@endif
						</td>
					</tr>
				@endif

			</tbody>
		</table>

	</div>
</div>

@if($user->purchasing_care_for=='Someone else' && $service_users->count()>0)
	@foreach($service_users as $service_user)
		@if(!empty($service_user))
			<?php
				$service_user_language = $service_user->Languages()->get();
				$service_user_condition = $service_user->ServiceUserConditions()->get();
				$care_type = $service_user->AssistantsTypes()->get()->sortBy('id');
				$service_type = $service_user->ServicesTypes()->get()->sortBy('id');
			?>
			@include(config('settings.theme').'.gdpr.gdprToXlsPurchaserServiceUserProfile')				
		@endif			
	@endforeach
@endif
