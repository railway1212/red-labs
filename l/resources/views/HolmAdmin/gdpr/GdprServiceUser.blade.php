
<div class="financialsCard">
	<div class="financialsCard__header">
		<div class="financialsCard__title">
			<h2>
				Profile Details
			</h2>
		</div>
	</div>
	
	<div class="financialsCard__header"><h3>General</h3></div>
	
	<div class="row financialsCard__header">
		<div class="col-md-6"> First Name</div>
		<div class="col-md-6"> 
		@if($user->first_name==null)
			--
		@else
			{{$user->first_name}}
		@endif
		</div>
	</div>

	<div class="row financialsCard__header">
		<div class="col-md-6"> Last Name </div>
		<div class="col-md-6">
		@if($user->family_name==null)
			--
		@else
			{{$user->family_name}}
		@endif
		</div>
	</div>
	<div class="row financialsCard__header">
		<div class="col-md-6"> Gender </div>
		<div class="col-md-6">
		@if($user->gender==null)
			--
		@else
			{{$user->gender}}
		@endif
		</div>
	</div>
	<div class="row financialsCard__header">
		<div class="col-md-6"> Date of birth </div>
		<div class="col-md-6">
		@if($user->DoB==null)
			--
		@else
			{{$user->DoB}}
		@endif
		</div>
	</div>
	
	<div class="row financialsCard__header">
		<div class="col-md-6"> I like to be called </div>
		<div class="col-md-6">
		@if($user->like_name==null)
			--
		@else
			{{$user->like_name}}
		@endif
		
		</div>
	</div>
	
	<div class="row financialsCard__header">
		<div class="col-md-6"> Own referral Code </div>
		<div class="col-md-6">
		@if($user->own_referral_code==null)
			--
		@else
			{{$user->own_referral_code}}
		@endif
		</div>
	</div>
	
	<div class="row financialsCard__header">
		<div class="col-md-6"> Registered On </div>
		<div class="col-md-6">
		@if($user->created_at==null)
			--
		@else
			{{$user->created_at}}
		@endif
		</div>
	</div>
	
	<div class="financialsCard__header"><h3>Contact</h3></div>
	
	<div class="row financialsCard__header">
		<div class="col-md-6"> Address Line 1 </div>
		<div class="col-md-6">
		@if($user->address_line1==null)
			--
		@else
			{{$user->address_line1}}
		@endif
		</div>
	</div>
	
	<div class="row financialsCard__header">
		<div class="col-md-6"> Address Line 2 </div>
		<div class="col-md-6">
		@if($user->address_line2==null)
			--
		@else
			{{$user->address_line2}}
		@endif
		</div>
	</div>
	
	<div class="row financialsCard__header">
		<div class="col-md-6">Town / city  </div>
		<div class="col-md-6">
		@if($user->town==null)
			--
		@else
			{{$user->town}}
		@endif
		</div>
	</div>
	
	<div class="row financialsCard__header">
		<div class="col-md-6"> Post code</div>
		<div class="col-md-6">
		@if($user->postcode==null)
			--
		@else
			{{$user->postcode}}
		@endif
		</div>
	</div>
	
	<div class="row financialsCard__header">
		<div class="col-md-6"> Mobile Number</div>
		<div class="col-md-6">
		@if($user->mobile_number==null)
			--
		@else
			{{$user->mobile_number}}
		@endif
		</div>
	</div>
	
	<div class="row financialsCard__header">
		<div class="col-md-6"> Email Address</div>
		<div class="col-md-6">
		@if($user->email==null)
			--
		@else
			{{$user->email}}
		@endif
		
		</div>
	</div>


	<div class="financialsCard__header">
		<div class="financialsCard__title">
			<h2>
				About Me
			</h2>
		</div>
	</div>

	<div class="financialsCard__header"><h3>One Line About {{$user->first_name}}</h3></div>	
	<div class="row financialsCard__header">
		
		<div class="col-md-12">
		@if($user->one_line_about==null)
			--
		@else
			{{$user->one_line_about}}
		@endif
		</div>
	</div>

	<div class="financialsCard__header">
		<div class="financialsCard__title">
			<h2>
				Languages
			</h2>
		</div>
	</div>
	
	<div class="row financialsCard__header">
		
		<div class="col-md-12">
		@foreach($service_user_language as $sul)
			{{$sul->carer_language}},
		@endforeach
		</div>
	</div>

	<div class="financialsCard__header">
		<div class="financialsCard__title">
			<h2>
				Home
			</h2>
		</div>
	</div>
	
	<div class="row financialsCard__header">
		<div class="col-md-6">Home is a</div>
		<div class="col-md-6">
		@if($user->kind_of_building==null)
			--
		@else
			{{$user->kind_of_building}}
		@endif
		</div>
	</div>
	<div class="row financialsCard__header">
		<div class="col-md-6">Can keep the home safe and clean by themself</div>
		<div class="col-md-6">
		{{$user->home_safe}}
		</div>
	</div>
	<div class="row financialsCard__header">
		<div class="col-md-6">Requires assistance keeping the home safe and clean</div>
		<div class="col-md-6">
		@if($user->assistance_keeping==null)
			--
		@else
			{{$user->assistance_keeping}}
		@endif
		
		</div>
	</div>
	<div class="row financialsCard__header">
		<div class="col-md-6">Can move around home safely by themself</div>
		<div class="col-md-6">
		@if($user->assistance_moving==null)
			--
		@else
			{{$user->assistance_moving}}
		@endif
		</div>
	</div>
	<div class="row financialsCard__header">
		<div class="col-md-6">Requires assistance moving around home</div>
		<div class="col-md-6">
		@if($user->move_available==null)
			--
		@else
			{{$user->move_available}}
		@endif
		</div>
	</div>

	<div class="financialsCard__header"><h3>Entry</h3></div>
	<div class="row financialsCard__header">
		<div class="col-md-6">How should the carer enter the {{$user->first_name}}’s home?</div>
		<div class="col-md-6">
		@if($user->carer_enter==null)
			--
		@else
			{{$user->carer_enter}}
		@endif
		</div>
	</div>

	<div class="financialsCard__header"><h3>Other Home Information</h3></div>
	<div class="row financialsCard__header">
		<div class="col-md-6">Is there anything else the Carer should be aware of when entering the home?</div>
		<div class="col-md-6">
		@if($user->entering_aware==null)
			--
		@else
			{{$user->entering_aware}}
		@endif
		
		</div>
	</div>
	@if($user->other_detail!=null)
	<div class="row financialsCard__header">
		<div class="col-md-6">Please, give details </div>
		<div class="col-md-6">

		{{$user->other_detail}}
		</div>
	</div>
	@endif
	<div class="financialsCard__header"><h3>Other Inhabitants</h3></div>
	<div class="row financialsCard__header">
		<div class="col-md-6">Somebody lives with {{$user->first_name}}</div>
		<div class="col-md-6">
		@if($user->anyone_else_live==null)
			--
		@else
			{{$user->anyone_else_live}}
		@endif
		</div>
	</div>
	<div class="row financialsCard__header">
		<div class="col-md-6">please give their name and relationship to {{$user->first_name}}</div>
		<div class="col-md-6">
		@if($user->anyone_detail==null)
			--
		@else
			{{$user->anyone_detail}}
		@endif
		</div>
	</div>
	<div class="row financialsCard__header">
		<div class="col-md-6">Is the other person likely to be home during care visits?</div>
		<div class="col-md-6">
		@if($user->anyone_friendly==null)
			--
		@else
			{{$user->anyone_friendly}}
		@endif
		</div>
	</div>

	<div class="financialsCard__header"><h3>Pets</h3></div>
	<div class="row financialsCard__header">
		<div class="col-md-6">Owns pets</div>
		<div class="col-md-6">
		@if($user->own_pets==null)
			--
		@else
			{{$user->own_pets}}
		@endif
		</div>
	</div>
	@if($user->pet_detail!=null)
	<div class="row financialsCard__header">
		<div class="col-md-6 financialsCard__header">Please, give details</div>
		<div class="col-md-6">
		{{$user->pet_detail}}
		</div>
	</div>
	@endif
	<div class="row financialsCard__header">
		<div class="col-md-6">Pets friendly with strangers</div>
		<div class="col-md-6">
		@if($user->pet_friendly==null)
			--
		@else
			{{$user->pet_friendly}}
		@endif
		</div>
	</div>

	<div class="financialsCard__header"><h3>Companionship</h3></div>
	<div class="row financialsCard__header">
		<div class="col-md-6">Has regular social interaction with friends / family</div>
		<div class="col-md-6">
		@if($user->social_interaction==null)
			--
		@else
			{{$user->social_interaction}}
		@endif
		</div>
	</div>
	<div class="row financialsCard__header">
		<div class="col-md-6">Would {{$user->first_name}} like someone to visit regularly for companionship?</div>
		<div class="col-md-6">
		@if($user->visit_for_companionship==null)
			--
		@else
			{{$user->visit_for_companionship}}
		@endif
		</div>
	</div>

	<div class="financialsCard__header">
		<div class="financialsCard__title">
			<h2>
				Type of Care Needed
			</h2>
		</div>
	</div>
	<div class="row financialsCard__header">
		<div class="col-md-6"></div>
		<div class="col-md-6">
		<div class="row">
			<div class="col-md-6">
				<h3>Care:</h3><br>
				@foreach($care_type as $ct)
					{{$ct->name}}
				@endforeach 		
			</div>
			<div class="col-md-6">
			<h3>Service:</h3><br>
				@foreach($service_type as $st)
					{{$st->name}}
					<br>
				@endforeach		
			</div>
		</div>
		

		
		</div>
	</div>

	<div class="financialsCard__header">
		<div class="financialsCard__title">
			<h2>
				Time When Care Needed
			</h2>
		</div>
	</div>
	<div class="row financialsCard__header">
		<div class="col-md-6">First date for Carer </div>
		<div class="col-md-6">
		@if($user->start_date==null)
			--
		@else
			{{$user->start_date}}
		@endif
		</div>
	</div>

	<div class="financialsCard__header">
		<div class="financialsCard__title">
			<h2>
				Health		
			</h2>
		</div>
	</div>
	<div class="financialsCard__header"><h3>Conditions</h3></div>
	<div class="row financialsCard__header">
		<div class="col-md-6"></div>
		<div class="col-md-6">
		@foreach($service_user_condition as $suc)
			{{$suc->name}}
		@endforeach
		</div>
	</div>

	@if($user->conditions_detail!=null)
	<div class="row financialsCard__header">
		<div class="col-md-6">Please Give Details</div>
		<div class="col-md-6">
		{{$user->conditions_detail}}
		</div>
	</div>
	@endif
	<div class="row financialsCard__header">
		<div class="col-md-6">Long term medical conditions</div>
		<div class="col-md-6">
		@if($user->long_term_conditions==null)
			--
		@else
			{{$user->long_term_conditions}}
		@endif
		</div>
	</div>

	<div class="financialsCard__header"><h3>Dementia</h3></div>
	<div class="row financialsCard__header">
		<div class="col-md-6">Has Dementia</div>
		<div class="col-md-6">
		@if($user->have_dementia==null)
			--
		@else
			{{$user->have_dementia}}
		@endif
		</div>
	</div>

	@if($user->dementia_detail!=null)
	<div class="row financialsCard__header">
		<div class="col-md-6">Please, give details </div>
		<div class="col-md-6">
		{{$user->dementia_detail}}
		</div>
	</div>
	@endif
	


	<div class="financialsCard__header"><h3>Communication</h3></div>
	<div class="row financialsCard__header">
		<div class="col-md-6">Has difficulties understanding or communicating with other</div>
		<div class="col-md-6">
		@if($user->communication==null)
			--
		@else
			{{$user->communication}}
		@endif
		</div>
	</div>
	<div class="row financialsCard__header">
		<div class="col-md-6">Has problems understanding other people</div>
		<div class="col-md-6">
		@if($user->comprehension==null)
			--
		@else
			{{$user->comprehension}}
		@endif
		</div>
	</div>

	@if($user->comprehension_detail!=null)
	<div class="row financialsCard__header">
		<div class="col-md-6">Please, give details </div>
		<div class="col-md-6">
			{{$user->comprehension_detail}}
		</div>
	</div>
	@endif
	<div class="row financialsCard__header">
		<div class="col-md-6">Needs help with speech </div>
		<div class="col-md-6">
		@if($user->speech==null)
			--
		@else
			{{$user->speech}}
		@endif
		</div>
	</div>
	<div class="row financialsCard__header">
		<div class="col-md-6">Has serious impediments seeing</div>
		<div class="col-md-6">
		@if($user->vision==null)
			--
		@else
			{{$user->vision}}
		@endif
		</div>
	</div>

	@if($user->vision_detail!=null)
	<div class="row financialsCard__header">
		<div class="col-md-6">Please, give details</div>
		<div class="col-md-6">
			{{$user->vision_detail}}
		</div>
	</div>
	@endif

	<div class="row financialsCard__header">
		<div class="col-md-6">Has serious impediments hearing</div>
		<div class="col-md-6">
		@if($user->hearing==null)
			--
		@else
			{{$user->hearing}}
		@endif
		</div>
	</div>

	@if($user->hearing_detail!=null)
	<div class="row financialsCard__header">
		<div class="col-md-6">Please, give details </div>
		<div class="col-md-6">
			{{$user->hearing_detail}}
		</div>
	</div>
	@endif

	<div class="financialsCard__header"><h3>Medication</h3></div>
	<div class="row financialsCard__header">
		<div class="col-md-6">Requires assistance in taking medication / treatments</div>
		<div class="col-md-6">
		@if($user->assistance_in_medication==null)
			--
		@else
			{{$user->assistance_in_medication}}
		@endif
		</div>
	</div>
	
	@if($user->pet_detail!=null)
	<div class="row financialsCard__header">
		<div class="col-md-6">Please, give details </div>
		<div class="col-md-6">
			{{$user->in_medication_detail}}
		</div>
	</div>
	@endif

	<div class="financialsCard__header"><h3>Allergies</h3></div>
	<div class="row financialsCard__header">
		<div class="col-md-6">Has allergies to food / medication / anything else</div>
		<div class="col-md-6">
		@if($user->have_any_allergies==null)
			--
		@else
			{{$user->have_any_allergies}}
		@endif
		</div>
	</div>

	@if($user->allergies_detail!=null)
	<div class="row financialsCard__header">
		<div class="col-md-6">Please, give details </div>
		<div class="col-md-6">
			{{$user->allergies_detail}}
		</div>
	</div>
	@endif
	

	<div class="financialsCard__header"><h3>skin</h3></div>
	<div class="row financialsCard__header">
		<div class="col-md-6">Has risk of developing pressure sores on their skin</div>
		<div class="col-md-6">
		@if($user->skin_scores==null)
			--
		@else
			{{$user->skin_scores}}
		@endif
		</div>
	</div>

	@if($user->skin_scores_detail!=null)
	<div class="row financialsCard__header">
		<div class="col-md-6">Please, give details </div>
		<div class="col-md-6">
		{{$user->skin_scores_detail}}
		</div>
	</div>
	@endif

	<div class="row financialsCard__header">
		<div class="col-md-6">Needs assistance with changing wound dressings</div>
		<div class="col-md-6">
		@if($user->assistance_with_dressings==null)
			--
		@else
			{{$user->assistance_with_dressings}}
		@endif
		</div>
	</div>
	

	@if($user->dressings_detail!=null)
	<div class="row financialsCard__header">
		<div class="col-md-6">Please, give details </div>
		<div class="col-md-6">
			{{$user->dressings_detail}}
		</div>
	</div>
	@endif


	<div class="financialsCard__header"><h3>mobility</h3></div>
	<div class="row financialsCard__header">
		<div class="col-md-6">Requires help with mobility</div>
		<div class="col-md-6">
		@if($user->help_with_mobility==null)
			--
		@else
			{{$user->help_with_mobility}}
		@endif
		</div>
	</div>
	<div class="row financialsCard__header">
		<div class="col-md-6">Needs help moving around home</div>
		<div class="col-md-6">
		@if($user->mobility_home==null)
			--
		@else
			{{$user->mobility_home}}
		@endif
		</div>
	</div>

	@if($user->mobility_home_detail!=null)
	<div class="row financialsCard__header">
		<div class="col-md-6">Please, give details </div>
		<div class="col-md-6">
		{{$user->mobility_home_detail}}
		</div>
	</div>
	@endif
	<div class="row financialsCard__header">
		<div class="col-md-6">Needs help getting in / out of bed</div>
		<div class="col-md-6">
		@if($user->mobility_bed==null)
			--
		@else
			{{$user->mobility_bed}}
		@endif
		</div>
	</div>

	@if($user->mobility_bed_detail!=null)
	<div class="row financialsCard__header">
		<div class="col-md-6">Please, give details </div>
		<div class="col-md-6">
		{{$user->mobility_bed_detail}}
		</div>
	</div>
	@endif
	<div class="row financialsCard__header">
		<div class="col-md-6">Has a history of falls</div>
		<div class="col-md-6">
		@if($user->history_of_falls==null)
			--
		@else
			{{$user->history_of_falls}}
		@endif
		</div>
	</div>

	@if($user->falls_detail!=null)
	<div class="row financialsCard__header">
		<div class="col-md-6">Please, give details </div>
		<div class="col-md-6">
		{{$user->falls_detail}}
		</div>
	</div>
	@endif

	<div class="row financialsCard__header">
		<div class="col-md-6">Needs help going shopping, or to other local facilities / events</div>
		<div class="col-md-6">
		@if($user->mobility_shopping==null)
			--
		@else
			{{$user->mobility_shopping}}
		@endif
		</div>
	</div>
	@if($user->mobility_shopping_detail!=null)
	<div class="row financialsCard__header">
		<div class="col-md-6">Please, give details </div>
		<div class="col-md-6">
		{{$user->mobility_shopping_detail}}
		</div>
	</div>
	@endif

	<div class="financialsCard__header"><h3>Nutrition</h3></div>
	<div class="row financialsCard__header">
		<div class="col-md-6">Requires assistance with eating / drinking</div>
		<div class="col-md-6">
		@if($user->assistance_with_eating==null)
			--
		@else
			{{$user->assistance_with_eating}}
		@endif
		</div>
	</div>

	@if($user->assistance_with_eating_detail!=null)
	<div class="row financialsCard__header">
		<div class="col-md-6">Please, give details </div>
		<div class="col-md-6">
			{{$user->assistance_with_eating_detail}}
		</div>
	</div>
	@endif
	<div class="row financialsCard__header">
		<div class="col-md-6">Can prepare food for themselves</div>
		<div class="col-md-6">
		@if($user->prepare_food==null)
			--
		@else
			{{$user->prepare_food}}
		@endif
		</div>
	</div>
	<div class="row financialsCard__header">
		<div class="col-md-6">Has any preferences of food? eg. Are there any do's and don'ts?</div>
		<div class="col-md-6">
		@if($user->preferences_of_food==null)
			--
		@else
			{{$user->preferences_of_food}}
		@endif
		</div>
	</div>

	@if($user->preferences_of_food_requirements!=null)
	<div class="row financialsCard__header">
		<div class="col-md-6">Please, give details </div>
		<div class="col-md-6">
		{{$user->preferences_of_food_requirements}}
		</div>
	</div>
	@endif
	<div class="row financialsCard__header">
		<div class="col-md-6">Would like assistance with preparing meals</div>
		<div class="col-md-6">
		@if($user->assistance_with_preparing_food==null)
			--
		@else
			{{$user->assistance_with_preparing_food}}
		@endif
		</div>
	</div>
	<div class="row financialsCard__header">
		<div class="col-md-6"> Has special nutritional or belief based dietary requirements </div>
		<div class="col-md-6">
		@if($user->dietary_requirements==null)
			--
		@else
			{{$user->dietary_requirements}}
		@endif
		</div>
	</div>

	@if($user->dietary_requirements_interaction!=null)
	<div class="row financialsCard__header">
		<div class="col-md-6">Please, give details </div>
		<div class="col-md-6">
		{{$user->dietary_requirements_interaction}}
		</div>
	</div>
	@endif
	<div class="row financialsCard__header">
		<div class="col-md-6">Has other special dietary requirements</div>
		<div class="col-md-6">
		@if($user->special_dietary_requirements==null)
			--
		@else
			{{$user->special_dietary_requirements}}
		@endif
		</div>
	</div>

	@if($user->special_dietary_requirements_detail!=null)
	<div class="row financialsCard__header">
		<div class="col-md-6">Please, give details</div>
		<div class="col-md-6">
			{{$user->special_dietary_requirements_detail}}
		</div>
	</div>
	@endif
	<div class="financialsCard__header"><h3> Personal Hygiene </h3></div>
	<div class="row financialsCard__header">
		<div class="col-md-6">Requires assistance in getting dressed / bathing or toileting</div>
		<div class="col-md-6">
		@if($user->assistance_with_personal_hygiene==null)
			--
		@else
			{{$user->assistance_with_personal_hygiene}}
		@endif
		</div>
	</div>
	<div class="row financialsCard__header">
		<div class="col-md-6">Needs assistance in choosing appropriate clothes</div>
		<div class="col-md-6">
		@if($user->appropriate_clothes==null)
			--
		@else
			{{$user->appropriate_clothes}}
		@endif
		</div>
	</div>
	<div class="row financialsCard__header">
		<div class="col-md-6">Needs assistance getting dressed / undressed</div>
		<div class="col-md-6">
		@if($user->assistance_getting_dressed==null)
			--
		@else
			{{$user->assistance_getting_dressed}}
		@endif
		</div>
	</div>

	@if($user->assistance_getting_dressed_detail!=null)
	<div class="row financialsCard__header">
		<div class="col-md-6">Please, give details</div>
		<div class="col-md-6">
			{{$user->assistance_getting_dressed_detail}}
		</div>
	</div>
	@endif
	<div class="row financialsCard__header">
		<div class="col-md-6">Needs assistance with bathing / showering</div>
		<div class="col-md-6">
		@if($user->assistance_with_bathing==null)
			--
		@else
			{{$user->assistance_with_bathing}}
		@endif
		</div>
	</div>
	<div class="row financialsCard__header">
		<div class="col-md-6">How many times a week?</div>
		<div class="col-md-6">
		@if($user->bathing_times_per_week==null)
			--
		@else
			{{$user->bathing_times_per_week}}
		@endif
		</div>
	</div>
	<div class="row financialsCard__header">
		<div class="col-md-6"> Needs assistance managing their toilet needs</div>
		<div class="col-md-6">
		@if($user->managing_toilet_needs==null)
			--
		@else
			{{$user->managing_toilet_needs}}
		@endif
		</div>
	</div>
	<div class="row financialsCard__header">
		<div class="col-md-6">Needs help mobilising themselves to the toilet</div>
		<div class="col-md-6">
		@if($user->mobilising_to_toilet==null)
			--
		@else
			{{$user->mobilising_to_toilet}}
		@endif
		</div>
	</div>
	<div class="row financialsCard__header">
		<div class="col-md-6">Needs help cleaning themselves when using the toilet</div>
		<div class="col-md-6">
		@if($user->cleaning_themselves==null)
			--
		@else
			{{$user->cleaning_themselves}}
		@endif
		</div>
	</div>
	<div class="row financialsCard__header">
		<div class="col-md-6">Has incontinence</div>
		<div class="col-md-6">
		@if($user->have_incontinence==null)
			--
		@else
			{{$user->have_incontinence}}
		@endif
		</div>
	</div>

	@if($user->kind_of_incontinence!=null)
	<div class="row financialsCard__header">
		<div class="col-md-6">Please Give Details</div>
		<div class="col-md-6">
			{{$user->kind_of_incontinence}}
		</div>
	</div>
	@endif
	<div class="row financialsCard__header">
		<div class="col-md-6">Has own supply of incontinence wear</div>
		<div class="col-md-6">
		@if($user->incontinence_wear==null)
			--
		@else
			{{$user->incontinence_wear}}
		@endif
		</div>
	</div>
	<div class="row financialsCard__header">
		<div class="col-md-6">Needs help in choosing incontinence products</div>
		<div class="col-md-6">
		@if($user->choosing_incontinence_products==null)
			--
		@else
			{{$user->choosing_incontinence_products}}
		@endif
		</div>
	</div>
	<div class="row financialsCard__header">
		<div class="col-md-6">The incontinence products are stored... </div>
		<div class="col-md-6">
		@if($user->incontinence_products_stored==null)
			--
		@else
			{{$user->incontinence_products_stored}}
		@endif
		</div>
	</div>
		
	<div class="financialsCard__header">
		<div class="financialsCard__title">
			<h2>
				BEHAVIOUR
			</h2>
		</div>
	</div>
	<div class="row financialsCard__header">
		<div class="col-md-6">Has a doctor's note or court order saying that they are not able to give consent </div>
		<div class="col-md-6">
		@if($user->consent==null)
			--
		@else
			{{$user->consent}}
		@endif
		</div>
	</div>

	@if($user->consent_details!=null)
	<div class="row financialsCard__header">
		<div class="col-md-6">Please, give details</div>
		<div class="col-md-6">
			{{$user->consent_details}}
		</div>
	</div>
	@endif


	<div class="financialsCard__header">
		<div class="financialsCard__title">
			<h2>
				Night-time			
			</h2>
		</div>
	</div>
	<div class="row financialsCard__header">
		<div class="col-md-6">Has problems getting dressed for bed</div>
		<div class="col-md-6">
		@if($user->getting_dressed_for_bed==null)
			--
		@else
			{{$user->getting_dressed_for_bed}}
		@endif
		</div>
	</div>
	<div class="row financialsCard__header">
		<div class="col-md-6">Needs help getting ready for bed?</div>
		<div class="col-md-6">
		@if($user->getting_ready_for_bed==null)
			--
		@else
			{{$user->getting_ready_for_bed}}
		@endif
		</div>
	</div>
	<div class="row financialsCard__header">
		<div class="col-md-6">What time would they like someone to come and help?</div>
		<div class="col-md-6">
		@if($user->time_to_bed==null)
			--
		@else
			{{$user->time_to_bed}}
		@endif
		</div>
	</div>
	<div class="row financialsCard__header">
		<div class="col-md-6">Needs assistance keeping safe at night</div>
		<div class="col-md-6">
		@if($user->keeping_safe_at_night==null)
			--
		@else
			{{$user->keeping_safe_at_night}}
		@endif
		</div>
	</div>

	@if($user->keeping_safe_at_night_details!=null)
	<div class="row financialsCard__header">
		<div class="col-md-6">Please, give details </div>
		<div class="col-md-6">
			{{$user->keeping_safe_at_night_details}}
		</div>
	</div>
	@endif
	<div class="row financialsCard__header">
		<div class="col-md-6">What time would they like someone to help?</div>
		<div class="col-md-6">
		@if($user->time_to_night_helping==null)
			--
		@else
			{{$user->time_to_night_helping}}
		@endif
		</div>
	</div>
	<div class="row financialsCard__header">
		<div class="col-md-6">Needs help going to the toilet at night</div>
		<div class="col-md-6">
		@if($user->toilet_at_night==null)
			--
		@else
			{{$user->toilet_at_night}}
		@endif
		</div>
	</div>
	<div class="row financialsCard__header">
		<div class="col-md-6">Needs someone to help at night</div>
		<div class="col-md-6">
		@if($user->helping_toilet_at_night==null)
			--
		@else
			{{$user->helping_toilet_at_night}}
		@endif
		</div>
	</div>

	<div class="financialsCard__header">
		<div class="financialsCard__title">
			<h2>
				OTHER			
			</h2>
		</div>
	</div>
	<div class="row financialsCard__header">
		<div class="col-md-6"> Has political, religious or other beliefs </div>
		<div class="col-md-6">
		@if($user->religious_beliefs==null)
			--
		@else
			{{$user->religious_beliefs}}
		@endif
		</div>
	</div>

	@if($user->religious_beliefs_details!=null)
	<div class="row financialsCard__header">
		<div class="col-md-6"> Please, give details </div>
		<div class="col-md-6">
			{{$user->religious_beliefs_details}}
		</div>
	</div>
	@endif
	<div class="row financialsCard__header">
		<div class="col-md-6"> Has particular likes or dislikes </div>
		<div class="col-md-6">
		@if($user->particular_likes==null)
			--
		@else
			{{$user->particular_likes}}
		@endif
		</div>
	</div>

	@if($user->particular_likes_details!=null)
	<div class="row financialsCard__header">
		<div class="col-md-6"> Please, give details </div>
		<div class="col-md-6">
			{{$user->particular_likes_details}}	
		</div>
	</div>
	@endif
	<div class="row financialsCard__header">
		<div class="col-md-6"> Needs the assistance of more than one person at a time to achieve any particular task </div>
		<div class="col-md-6">
		@if($user->multiple_carers==null)
			--
		@else
			{{$user->multiple_carers}}
		@endif
		</div>
	</div>

	@if($user->multiple_carers_details!=null)
	<div class="row financialsCard__header">
		<div class="col-md-6"> Please, give details </div>
		<div class="col-md-6">
			{{$user->multiple_carers_details}}
		</div>
	</div>
	@endif
	<div class="row financialsCard__header">
		<div class="col-md-6"> Likes socialising with other people / groups </div>
		<div class="col-md-6">
		@if($user->socialising_with_other==null)
			--
		@else
			{{$user->socialising_with_other}}
		@endif
		</div>
	</div>
	<div class="row financialsCard__header">
		<div class="col-md-6"> Has interests or hobbies which Marge enjoy </div>
		<div class="col-md-6">
		@if($user->interests_hobbies==null)
			--
		@else
			{{$user->interests_hobbies}}
		@endif
		</div>
	</div>

	@if($user->interests_hobbies_details!=null)
	<div class="row financialsCard__header">
		<div class="col-md-6"> Please, give details </div>
		<div class="col-md-6">
		@if($user->interests_hobbies_details==null)
			--
		@else
			{{$user->interests_hobbies_details}}
		@endif
		</div>
	</div>
	@endif
	<div class="row financialsCard__header">
		<div class="col-md-6"> Are there any other medical conditions, disabilities, or other pieces of information not already covered which you feel may be of use? </div>
		<div class="col-md-6">
		@if($user->other_medical_conditions==null)
			--
		@else
			{{$user->other_medical_conditions}}
		@endif
		</div>
	</div>
	@if($user->other_medical_detail!=null)
	<div class="row financialsCard__header">
		<div class="col-md-6"> Please, give details </div>
		<div class="col-md-6">
		@if($user->other_medical_detail==null)
			--
		@else
			{{$user->other_medical_detail}}
		@endif
		</div>
	</div>
	@endif
  </div>
  
  
  
  <!-- Appointment table -->
<div class="financialsCard" style="overflow-x: auto; max-height:500px">
	<div class="financialsCard__header">
		<div class="financialsCard__title" style="width: 100%;">
			<h2>
				Appointment 
			</h2>
		</div>

		<div class="tableWrap tableWrap--margin-t">
		    <table class="adminTable">
		    	<thead>
		    		<tr class="extra-tr">
		    			<td class="for-inner">
			                <table class="innerTable ">
			                    <tr>
			                        <td class="">
			                            <span class="extraTitle">Appointment ID</span>
			                        </td>
			                        <td class="">
			                            <span class="extraTitle">Date</span>
			                        </td>
			                        <td class="">
			                            <span class="extraTitle">From</span>
			                        </td>
			                        <td class="">
			                            <span class="extraTitle">To</span>
			                        </td>
									<td class="">
			                            <span class="extraTitle">Carer</span>
			                        </td>
			                        <td class="">
			                            <span class="extraTitle">Status</span>
			                        </td>
			                        <td class="">
			                            <span class="extraTitle">Amount</span>
			                        </td>
			                    </tr>
			                </table>
			            </td>
		    		</tr>
		    	</thead>
		    	<tbody>
		    		<tr>
		    			<td class="for-inner">
		    				<table class="innerTable ">
		    				@foreach($servicePerchaserBooking as $pb)
		 		    				@foreach($booking_appointment=$pb->appointments as $ba)
							            <tr>
							                <td class="">
							                    <span>{{$ba->id}}</span>
							                </td>
							                <td class="">
							                   {{$ba->date_start}}
							                </td>
							                <td class="">
							                   {{$ba->time_from}}
							                </td>
							                <td class="">
							                   {{$ba->time_to}}
							                </td>
											<td class="">
												{{$pb->bookingCarerProfile->first_name}} {{$pb->bookingCarerProfile->family_name}}
							                </td>
							                <td class="">
							                	{{$ba->appointmentStatus->name}}
							                	
							                </td>
							                <td class="">
							                	<i class="fa fa-gbp" aria-hidden="true"></i>
							                   {{$ba->price_for_carer}}
							                </td>
							            </tr>
		    						@endforeach
					        @endforeach
					        </table>
		    			</td>
		    			<td class="for-inner">
		    			</td>
		    		</tr>
		    	</tbody>
		    </table>	
	    </div>
	</div>
</div>



<!-- Transaction table -->
<div class="financialsCard" style="overflow-x: auto; max-height:500px">
	<div class="financialsCard__header">
		<div class="financialsCard__title" style="width: 100%;">
			<h2>
				Transaction 
			</h2>
		</div>

		<div class="tableWrap tableWrap--margin-t">
		    <table class="adminTable">
		    	<thead>
		    		<tr class="extra-tr">
		    			<td class="for-inner">
			                <table class="innerTable ">
			                    <tr>
			                        <td class="">
			                            <span class="extraTitle">Transaction ID</span>
			                        </td>
			                        <td class="">
			                            <span class="extraTitle">Date and Time</span>
			                        </td>
			                        <td class="">
			                            <span class="extraTitle">Booking ID</span>
			                        </td>
			                        <td class="">
			                            <span class="extraTitle">Amount</span>
			                        </td>
			                        <td class="">
			                            <span class="extraTitle">Payment Type</span>
			                        </td>
			                        <td class="">
			                            <span class="extraTitle">Transaction Status</span>
			                        </td>
			                    </tr>
			                </table>
			            </td>
		    		</tr>
		    	</thead>
		    	<tbody>
		    		<tr>
		    			<td class="for-inner">
		    				<table class="innerTable ">
		    					@foreach($servicePerchaserTransaction as $pt)
		    						    <tr>
							                <td class="">
							                    <span>{{$pt->id}}</span>
							                </td>
							                <td class="">
							                   	{{$pt->created_at}}
							                </td>
							                <td class="">
							                   	{{$pt->booking_id}}
							                </td>
							                <td class="">
							                	<i class="fa fa-gbp" aria-hidden="true"></i>
							                   	{{$pt->amount}}
							                </td>
							                <td class="">
							                @if($pt->payment_method=='stripe')
							                	Credit Card 
							                @else
							                	Bonuses Wallet 
							                @endif
							                	
							                </td>
							                <td class="">
							                  	completed
							                </td>
							            </tr>
		    						
		    					@endforeach
		    						
					        </table>
		    			</td>
		    			<td class="for-inner">
		    			</td>
		    		</tr>
		    	</tbody>
		    </table>	
	    </div>
	</div>
</div>
