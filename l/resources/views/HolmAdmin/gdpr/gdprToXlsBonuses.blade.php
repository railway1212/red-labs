<?php use App\Http\Controllers\Admin\BonusController; ?>

<h4 style="color: navy;">Bonuses </h4>

<table class="adminTable">
            <thead>
            <tr>
                <td class=" ordninary-td ordninary-td--small no-padding-l">
                   Date
                </td>
                <td class=" ordninary-td ordninary-td--big ">
               
                  Benefactor name
                
                </td>
                <td class=" ordninary-td ordninary-td--small  ">
                  <span class="td-title td-title--invition">
                    Invitation Code
                  </span>

                </td>
                <td class=" ordninary-td ordninary-td--small  ">
                  <span class="td-title td-title--invition">
                    Referrer
                  </span>

                </td>
                <td class=" ordninary-td  ">
                  <span class="td-title td-title--paid-bonus">
                    Paid bonuses
                  </span>
                </td>
                <td class=" ordninary-td  no-padding-l">
                  <span class="td-title td-title--amount">
                    Amount
                  </span>
                </td>
            </tr>
            </thead>
            <tbody>
            @if($bonusPayouts->count())
                @foreach($bonusPayouts as $bonusPayout)
                    <tr>
                        <td>
                            {{$bonusPayout->created_at->format('d-m-Y')}}
                        </td>
                       <td class="">
                            {{$bonusPayout->user->full_name}}
                        </td>
                               
                        <td align="left">
                          @if(BonusController::user_referral_code($bonusPayout->id))
                                  {{BonusController::user_referral_code($bonusPayout->id)}}
                              @elseif($bonusPayout->user->use_register_code)
                                  REGISTER
                              @else
                                  -
                              @endif
                          
                        </td>
                        <td align="left">
                              @if($bonusPayout->bonus_type_id == 2)
                                  @foreach($users as $user)
                                      @if($user->id == $bonusPayout->referral_user_id)
                                        @if($user->user_type_id==1)
                                        {{$user->first_name}} {{$user->family_name}} 
                                        @else
                                        {{$user->first_name}} {{$user->family_name}}
                                        @endif
                                      @endif
                                  @endforeach
                              @elseif($bonusPayout->user->use_register_code)
                                  REGISTER
                              @else
                                  -
                              @endif

                        </td>
                        <td align="left">
                          {{$bonusPayout->user->paid_bonuses}}
                        </td>
                        <td align="left">
                           {{$bonusPayout->amount}}
                        </td>
                    </tr>
                @endforeach
                @endif
            </tbody>
        </table>
        