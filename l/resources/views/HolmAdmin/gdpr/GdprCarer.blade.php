
<div class="financialsCard">
	
<div class="financialsCard__header">
	<div class="financialsCard__title">
		<h2>
			Profile Details
		</h2>
	</div>
</div>

<div class="financialsCard__header"><h3>General</h3></div>

<div class="row financialsCard__header">
	<div class="col-md-6"> First Name</div>
	<div class="col-md-6">
		@if($user->first_name==null)
			--
		@else
			{{$user->first_name}}
		@endif
	</div>
</div>

<div class="row financialsCard__header">
	<div class="col-md-6"> Last Name </div>
	<div class="col-md-6">
		@if($user->family_name==null)
			--
		@else
			{{$user->family_name}}
		@endif
	</div>
</div>
<div class="row financialsCard__header">
	<div class="col-md-6"> Gender </div>
	<div class="col-md-6">
		@if($user->gender==null)
			--
		@else
			{{$user->gender}}
		@endif
	</div>
</div>
<div class="row financialsCard__header">
	<div class="col-md-6"> Date of birth </div>
	<div class="col-md-6">
		@if($user->DoB==null)
			--
		@else
			{{$user->DoB}}
		@endif
	</div>
</div>

<div class="row financialsCard__header">
	<div class="col-md-6"> I like to be called </div>
	<div class="col-md-6">
		@if($user->like_name==null)
			--
		@else
			{{$user->like_name}}
		@endif
	</div>
</div>

<div class="row financialsCard__header">
	<div class="col-md-6"> Own referral Code </div>
	<div class="col-md-6">
		@if($user->own_referral_code==null)
			--
		@else
			{{$user->own_referral_code}}
		@endif
	</div>
</div>

<div class="row financialsCard__header">
	<div class="col-md-6"> Registered On </div>
	<div class="col-md-6">
		@if($user->created_at==null)
			--
		@else
			{{$user->created_at}}
		@endif
	</div>
</div>

<div class="financialsCard__header"><h3>Contact</h3></div>

<div class="row financialsCard__header">
	<div class="col-md-6"> Address Line 1 </div>
	<div class="col-md-6">
		@if($user->address_line1==null)
			--
		@else
			{{$user->address_line1}}
		@endif
	</div>
</div>

<div class="row financialsCard__header">
	<div class="col-md-6"> Address Line 2 </div>
	<div class="col-md-6">
		@if($user->address_line2==null)
			--
		@else
			{{$user->address_line2}}
		@endif
	</div>
</div>

<div class="row financialsCard__header">
	<div class="col-md-6">Town / city  </div>
	<div class="col-md-6">
		@if($user->town==null)
			--
		@else
			{{$user->town}}
		@endif
	</div>
</div>

<div class="row financialsCard__header">
	<div class="col-md-6"> Post code</div>
	<div class="col-md-6">
		@if($user->postcode==null)
			--
		@else
			{{$user->postcode}}
		@endif
	</div>
</div>

<div class="row financialsCard__header">
	<div class="col-md-6"> Mobile Number</div>
	<div class="col-md-6">
		@if($user->mobile_number==null)
			--
		@else
			{{$user->mobile_number}}
		@endif
	</div>
</div>

<div class="row financialsCard__header">
	<div class="col-md-6"> Email Address</div>
	<div class="col-md-6">
		@if($user->email==null)
			--
		@else
			{{$user->email}}
		@endif
	</div>
</div>

	 {{-- @if($documents){
		@foreach($documents as $document){
			@if($document->type == 'ADDITIONAL_DOCUMENTS_CV')
			
				<div class="row financialsCard__header">
					<div class="col-md-6"> Uploaded CV </div>
					<div class="col-md-6"><a href="{{storage_path()}}/l/documents/{{$document->file_name}}" target="_blank"> CV </a></div>
				</div>
			 
			 @else if($document->type == 'PASSPORT')
			 
				<div class="row financialsCard__header">
					<div class="col-md-6"> Photographic proof of your ID </div>
					<div class="col-md-6"><a href="{{storage_path()}}/l/documents/{{$document->file_name}}" target="_blank"> ID proof </a></div>
				</div>
			 
			 
			@endif
		@endforeach
	@endif --}}

<div class="row financialsCard__header">
	<div class="col-md-6"> Work with pets </div>
	<div class="col-md-6">
		@if($user->work_with_pets==null)
			--
		@else
			{{$user->work_with_pets}}
		@endif
	</div>
</div>




<div class="row financialsCard__header">
	<div class="col-md-6"> Uploaded CV </div>
	<div class="col-md-6">
		@if($cv=="")
			No
		@else
			Yes
		@endif
	</div>
</div>

<div class="row financialsCard__header">
	<div class="col-md-6"> Able work legally in the UK </div>
	<div class="col-md-6">
		@if($user->work_UK==null)
			--
		@else
			{{$user->work_UK}}
		@endif
	</div>
</div>

<div class="row financialsCard__header">
	<div class="col-md-6"> Working Restrictions in the UK</div>
	<div class="col-md-6">
		@if($user->work_UK_restriction==null)
			--
		@else
			{{$user->work_UK_restriction}}
		@endif
	</div>
</div>
<div class="row financialsCard__header">
	<div class="col-md-6"> National Insurance Number </div>
	<div class="col-md-6">
		@if($user->national_insurance_number==null)
			--
		@else
			{{$user->national_insurance_number}}
		@endif
	</div>
</div>

<div class="row financialsCard__header">
	<div class="col-md-6"> Summary </div>
	<div class="col-md-6">
		@if($user->sentence_yourself==null)
			--
		@else
			{{$user->sentence_yourself}}
		@endif
	</div>
</div>

<div class="row financialsCard__header">
	<div class="col-md-6"> About me </div>
	<div class="col-md-6">
		@if($user->description_yourself==null)
			--
		@else
			{{$user->description_yourself}}
		@endif
	</div>
</div>


<div class="financialsCard__header"><h3>Bank Account</h3></div>

<div class="row financialsCard__header">
	<div class="col-md-6"> Sort Code  </div>
	<div class="col-md-6">
		@if($user->sort_code==null)
			--
		@else
			{{$user->sort_code}}
		@endif
	</div>
</div>

<div class="row financialsCard__header">
	<div class="col-md-6"> Account Number   </div>
	<div class="col-md-6">
		@if($user->account_number==null)
			--
		@else
			{{$user->account_number}}
		@endif
	</div>
</div>

</div>

          
<div class="financialsCard">
	<div class="financialsCard__header">
		<div class="financialsCard__title">
			<h2>
				Personal References 
			</h2>
		</div>
	</div>
	
	<table>
		<thead style="font-weight: bold; font-size: 16px;">
			<tr>
				<th>PERSON</th>
				<th>NAME</th>
				<th>JOB TITLE </th>
				<th>RELATIONSHIP</th>
				<th>PHONE NUMBER</th>
				<th>EMAIL</th>
			</tr>
		</thead>
		<tbody>
			@if($search_user_type=='carer')
			<?php $i=1;?>
				@foreach($carer_references as $cr)
					<tr>
					  <td>{{$i++}}</td>
					  <td>{{$cr->name}}</td>
					  <td>{{$cr->job_title}}</td>
					  <td>{{$cr->relationship}}</td>
					  <td>{{$cr->phone}}</td>
					  <td>{{$cr->email}}</td>
					</tr>
			
				@endforeach

			@endif
		</tbody>
	</table>
</div>		
	
		
<div class="financialsCard">
	<div class="financialsCard__header">
		<div class="financialsCard__title">
			<h2>
				QUALIFICATIONS
			</h2>
		</div>
	</div>		
	<table>
		<thead style="font-weight: bold; font-size: 16px;"> 
			<th>CERTIFICATE</th>
			<th>NAME</th>
			<th>URL</th>
		</thead>
		<tbody>

		@foreach($documents as $document)
			@if($document->type=='NVQ')
				<tr>
					<td>{{$document->type}}</td>
					<td>{{$document->title}}</td>

					<td><a target="_blank" href="{{url('')}}/l/api/document/{{$document->id}}/preview">click here</a></td>
				</tr>
			@endif
		@endforeach
		</tbody>
	</table>
		
</div>


<div class="financialsCard">
	
	<div class="financialsCard__header">
		<div class="financialsCard__title">
			<h2>
				Other Information
			</h2>
		</div>
	</div>	
	
	<div class="financialsCard__header"><h3>TYPE OF CARE OFFERED</h3></div>
	
	<div class="row financialsCard__header">
		<div class="col-md-6">
			
		</div>
		<div class="col-md-6">
				@foreach($carer_service as $cs)
				
					{{$cs->name}},
					
				@endforeach

				@foreach($carer_assistance as $ca)
					{{$ca->name}},
				@endforeach
		</div>
	</div>
	
	<div class="financialsCard__header"><h3>AVAILABILITY</h3></div>
	
	<div class="row financialsCard__header">
		<div class="col-md-6"></div>
		<div class="col-md-6">
		@if(Auth::user()->user_type_id==4)
			@if($search_user_type=='carer')
				@foreach($availibility as $a)
					{{$a->name}},
				@endforeach

			@endif
		@endif
		</div>
	</div>
	
	<div class="financialsCard__header"><h3>Languages</h3></div>
	
	<div class="row financialsCard__header">
		<div class="col-md-6"></div>

		<div class="col-md-6">
		@if(Auth::user()->user_type_id==4)
			@if($search_user_type=='carer')
				@foreach($carer_languages as $cl)
					<div class="col-md-4"> 
						{{$cl->carer_language.','}}
					</div>
				@endforeach	
			@endif
		@endif
		</div>
		
		
		
	</div>
	
	<div class="financialsCard__header"><h3>Transport</h3></div>
	
	<div class="row financialsCard__header">
		<div class="col-md-6"> Driving licence</div>
		<div class="col-md-6">
		@if(Auth::user()->user_type_id==4)
			@if($search_user_type=='carer')
				@if($cp->driving_licence=='Yes')
					Have UK/EEA Driving Licence
				@else
					Do not have a driving licence
				@endif
			
			@endif
		@endif
		</div>
	</div>

	<div class="row financialsCard__header">
		<div class="col-md-6">Car for Work</div>
		<div class="col-md-6">
		@if(Auth::user()->user_type_id==4)
			@if($search_user_type=='carer')
				@if($cp->have_car=='Yes')
					Have a car for work
				@else
					Do not have a car
				@endif
			@endif
		@endif
		</div>
	</div>

	@if($cp->have_car=='Yes')
	<div class="row financialsCard__header">
		<div class="col-md-6">Transport clients</div>
		<div class="col-md-6">
			{{$cp->use_car}}
		</div>
	</div>
	@endif

	
	@if($cp->driving_licence=='Yes')

	<div class="row financialsCard__header">
		<div class="col-md-6">UK\EEA Driving licence photo</div>
		<div class="col-md-6">
		@if(Auth::user()->user_type_id==4)
			@if($search_user_type=='carer')
				@foreach($documents as $dc)
					@if($dc->type == 'DRIVING_LICENCE_PHOTO')
						<a href="{{url('')}}/l/api/document/{{$dc->id}}/preview" target="_blank"><img src="{{url('')}}/l/api/document/{{$dc->id}}/preview" style="max-height:100px;padding:1px;border:1px solid #021a40;">
						</a>
					@endif
				@endforeach
			@endif
		@endif
		</div>
	</div>

	<div class="row financialsCard__header">
		<div class="col-md-6">UK\EEA Driving licence Number</div>
		<div class="col-md-6">
		@if(Auth::user()->user_type_id==4)
			@if($search_user_type=='carer')
				@if($cp->DBS_number!='')
					{{$cp->DBS_number}}
				@else
					Not available
				@endif
			
			@endif

		@endif
		</div>
	</div>

	<div class="row financialsCard__header">
		<div class="col-md-6">Valid until</div>
		<div class="col-md-6">
			@if($cp->driver_licence_valid_until!='')
				{{$cp->driver_licence_valid_until}}
			@else
				Not available
			@endif
		</div>
	</div>
	@endif
	
	@if($cp->have_car=='Yes')
	<div class="row financialsCard__header">
		<div class="col-md-6">Car insurance Photo </div>
		<div class="col-md-6">
			@foreach($documents as $dc)
				@if($dc->type == 'CAR_INSURANCE_PHOTO')
					<a href="{{url('')}}/l/api/document/{{$dc->id}}/preview" target="_blank"><img src="{{url('')}}/l/api/document/{{$dc->id}}/preview" style="max-height:100px;padding:1px;border:1px solid #021a40;">
					</a>
				@endif
			@endforeach
		</div>
	</div>

	<div class="row financialsCard__header">
		<div class="col-md-6">Car insurance policy</div>
		<div class="col-md-6">
			@if($cp->car_insurance_number!='')
				{{$cp->car_insurance_number}}
			@else
				Not available
			@endif
		</div>
	</div>
	
	<div class="row financialsCard__header">
		<div class="col-md-6">Valid until</div>
		<div class="col-md-6">
			@if($cp->car_insurance_valid_until !='')
				{{$cp->car_insurance_valid_until}}
			@else
				Not available
			@endif
		</div>
	</div>

	@endif

		
		
</div>	

<div class="financialsCard">
	
	<div class="financialsCard__header">
		<div class="financialsCard__title">
			<h2>
				CRIMINAL RECORDS
			</h2>
		</div>
	</div>
	
	<div class="row financialsCard__header">
		<div class="col-md-6">Have up to date DBS certificate </div>
		<div class="col-md-6">
			@if($search_user_type=='carer')
				{{$cp->DBS}}
			@endif
		</div>
	</div>
	
	<div class="row financialsCard__header">
		<div class="col-md-6">Criminal convictions </div>
		<div class="col-md-6">
			@if($cp->criminal_conviction=='Some')
				Yes, but they are very old, and for a minor offence.
			@else
				{{$cp->criminal_conviction}}
			@endif
		</div>
	</div>

	@if($cp->criminal_conviction=='Some')
	<div class="row financialsCard__header">
		<div class="col-md-6">Criminal convictions details</div>
		<div class="col-md-6">
			{{$cp->criminal_detail}}
		</div>
	</div>
	@endif
	
	<div class="row financialsCard__header">
		<div class="col-md-6">Using the new DBS update service  </div>
		<div class="col-md-6">
		@if(Auth::user()->user_type_id==4)
			@if($search_user_type=='carer')
				{{$cp->DBS_use}}

			@endif
		@endif
		</div>
	</div>
	
	@if($cp->DBS=='Yes')
	<div class="row financialsCard__header">
		<div class="col-md-6">DBS certificate Photo</div>
		<div class="col-md-6">
		@if(Auth::user()->user_type_id==4)
			@if($search_user_type=='carer')
				@foreach($documents as $dc)
					@if($dc->type == 'DBS_CERTIFICATE_PHOTO')
						<a href="{{url('')}}/l/api/document/{{$dc->id}}/preview" target="_blank"><img src="{{url('')}}/l/api/document/{{$dc->id}}/preview" style="max-height:100px;padding:1px;border:1px solid #021a40;">
						</a>
					@endif
				@endforeach

			@endif
		@endif
			
		</div>
	</div>	
	
	<div class="row financialsCard__header">
		<div class="col-md-6">DBS Update Service identifier </div>
		<div class="col-md-6">
		@if(Auth::user()->user_type_id==4)
			@if($search_user_type=='carer')
				{{$cp->DBS_identifier}}

			@endif
		@endif
		</div>
	</div>	
	
	<div class="row financialsCard__header">
		<div class="col-md-6">DBS date certificate </div>
		<div class="col-md-6">
		@if(Auth::user()->user_type_id==4)
			@if($search_user_type=='carer')
				{{$cp->dbs_date}}

			@endif
		@endif
		</div>
	</div>	
	@endif




</div>
<!-- Appointment Table -->
<div class="financialsCard" style="overflow-x: auto; max-height:500px">
	<div class="financialsCard__header">
		<div class="financialsCard__title" style="width: 100%;">
			<h2>
				Appointment 
			</h2>
		</div>

		<div class="tableWrap tableWrap--margin-t">
		    <table class="adminTable">
		    	<thead>
		    		<tr class="extra-tr">
		    			<td class="for-inner">
			                <table class="innerTable ">
			                    <tr>
			                        <td class="">
			                            <span class="extraTitle">Appointment ID</span>
			                        </td>
			                        <td class="">
			                            <span class="extraTitle">Date</span>
			                        </td>
			                        <td class="">
			                            <span class="extraTitle">From</span>
			                        </td>
			                        <td class="">
			                            <span class="extraTitle">To</span>
			                        </td>
									<td class="">
			                            <span class="extraTitle">Purchaser</span>
			                        </td>
									<td class="">
			                            <span class="extraTitle">Service User</span>
			                        </td>
			                        <td class="">
			                            <span class="extraTitle">Status</span>
			                        </td>
			                        <td class="">
			                            <span class="extraTitle">Amount</span>
			                        </td>
			                    </tr>
			                </table>
			            </td>
		    		</tr>
		    	</thead>
		    	<tbody>
		    		<tr>
		    			<td class="for-inner">
		    				<table class="innerTable ">
		    				@foreach($carer_booking as $cb)
		 		    				@foreach($booking_appointment=$cb->appointments as $ba)
							            <tr>
							                <td class="">
							                    <span>{{$ba->id}}</span>
							                </td>
							                <td class="">
							               		 <?php echo date('Y-m-d',strtotime($ba->date_start)); ?>
							                </td>
							                <td class="">
							                   {{$ba->time_from}}
							                </td>
							                <td class="">
							                   {{$ba->time_to}}
							                </td>
											<td class="">
												{{$cb->bookingPurchaserProfile->first_name}} {{$cb->bookingPurchaserProfile->family_name}}
							                </td>
											<td class="">
												{{$cb->bookingServiceUser->first_name}} {{$cb->bookingServiceUser->family_name}}
							                </td>
							                <td class="">
							                	{{$ba->appointmentStatus->name}}
							                	
							                </td>
							                <td class="">
							                	<i class="fa fa-gbp" aria-hidden="true"></i>
							                   {{$ba->price_for_carer}}
							                </td>
							            </tr>
		    						@endforeach
					        @endforeach
					        </table>
		    			</td>
		    			<td class="for-inner">
		    			</td>
		    		</tr>
		    	</tbody>
		    </table>	
	    </div>
	</div>
</div>




<!-- carer payout -->
<div class="financialsCard" style="overflow-x:auto;max-height:500px">
	
	<div class="financialsCard__header">
		<div class="financialsCard__title">
			<h2>
				Carer Payout         
			</h2>
		</div>
	</div>
	<div class="tableWrap tableWrap--margin-t">
        <table class="adminTable">
            <thead>
                <tr class="extra-tr">
                    
                    <td class="for-inner">
                        <table class="innerTable ">
                            <tbody><tr>
                                <td class="">
                                    <span class="extraTitle">Transaction id</span>
                                </td>
                                <td class="">
                                    <span class="extraTitle">total</span>
                                </td>
                                <td class="">
                                    <span class="extraTitle">Appointment Id</span>
                                </td>
                                <td class="">
                                    <span class="extraTitle">payout status</span>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
            </thead>
            <tbody>
            @if(count($all) > 0)
                @foreach($all as $one)
                    @if(!isset($one->total))
                        
                        <tr>
                            <td class="for-inner">
                                <table class="innerTable ">
                                    <tbody>
                                    <tr>
                                        <td class="">
                                           {{$one->id}}
                                        </td>
                                        <td class="">
                                            <span><i class="fa fa-gbp"
                                                     aria-hidden="true"></i> {{$one->amount/100}}</span>
                                        </td>
                                        <td class="">
                                            <span><i aria-hidden="true"></i>
                                                <?php 
                                                
                                                if($one->bonus_id > 0){
													
													echo 'Bonus Payout';
													
												}else{
													$allApps = \App\Appointment::where('booking_id', $one->booking_id)->pluck('id')->toArray();
													for ($i = 0; $i < count($allApps); $i++) {
														echo "<a href='/bookings/" . $one->booking_id . "/details'>" . $allApps[$i] . "<a/><br>";
													}
											    }
                                                
                                                
                                                
                                                
                                                ?>
                                            </span>
                                        </td>
                                        <td>
                                            <div class="profStatus profStatus--left">
                                                <span class="profStatus__item profStatus__item--new">paid</span>

                                            </div>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    @endif
                @endforeach
            @endif

                @foreach($potentialPayouts as $potentialPayout)
                    <tr>
                        <td class="for-inner">
                            <table class="innerTable ">
                                <tbody><tr>
                                    <td class="">
                                        <span>{{ $potentialPayout->id }}</span>
                                    </td>
                                    <td class="">
                                        <span><i class="fa fa-gbp" aria-hidden="true"></i> {{$potentialPayout->total}}</span>
                                    </td>
                                    <td class="">
											{{$potentialPayout->booking_id}}
                                    </td>
                                    <td>
                                        <div class="profStatus profStatus--left">
                                            <span class="profStatus__item profStatus__item--progress">pending</span>

                                        </div>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
				@endforeach
				


                @foreach($transfersCarer as $transfer)
                <tr>
                    
                    <td class="for-inner">
                        <table class="innerTable ">
                            <tbody><tr>
                            	<td  align="center" style="word-break: break-all;">
			                        <a href="https://dashboard.stripe.com/test/payments/{{$transfer->id}}" target="_blank">{{$transfer->id}}</a>
			                    </td>
                                <td class="">
                                    <span><i class="fa fa-gbp" aria-hidden="true"></i> {{$transfer->amount/100}}</span>
                                </td>
                                <td class="">
									{{$transfer->booking_id}}
                                </td>
                                <td>
                                    <div class="profStatus profStatus--left">
                                        <span class="profStatus__item profStatus__item--new">paid</span>

                                    </div>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                @endforeach
       
            </tbody>
        </table>
    </div>
</div>
<!-- carer payout -->
