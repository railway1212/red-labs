

 <table class="statisticTable">
  <thead>
	 <tr>
		 <td align="left">{{$user->first_name}}  {{$user->family_name}}</td>
	 </tr>	
	</thead>
   </table> 	 


	<div class="tableWrap">
		<div class="statisticHead">
			<h3 class="statisticHead__title" style="color:navy">
				Profile Details 
			</h3>

		</div>
		<table class="statisticTable">
			<tbody>
				<tr>
					<th>ID</th>
					<td align="left">{{$user->id}}</td>
				</tr>

				<tr>
					<th>First Name</th>
					<td>{{$user->first_name}}</td>
				</tr>
				
				<tr>
					<th>Family Name</th>
					<td>{{$user->family_name}}</td>
				</tr>
				<tr>
					<th>I like to be called</th>
					<td>{{$user->like_name}}</td>
				</tr>
				<tr>
					<th>Gender</th>
					<td>{{$user->gender}}</td>
				</tr>
				<tr>
					<th>Mobile Number</th>
					<td>{{$user->mobile_number}}</td>
				</tr>
				<tr>
					<th>Address Line 1</th>
					<td>{{$user->address_line1}}</td>
				</tr>
				<tr>
					<th>Address Line 2</th>
					<td>{{$user->address_line2}}</td>
				</tr>
				<tr>
					<th>Town / City</th>
					<td>{{$user->town}}</td>
				</tr>
				<tr>
					<th>Post Code</th>
					<td>{{$user->postcode}}</td>
				</tr>

				<tr>
					<th>Working Restrictions in the UK</th>
					<td>
						@if($user->work_UK==null)
							--
						@else
							{{$user->work_UK}}
						@endif
					</td>
				</tr>
				
				<tr>
					<th>Able work legally in the UK</th>
					<td>{{$user->work_UK}}</td>
				</tr>
				
				<tr>
					<th>Working Restrictions in the UK</th>
					<td>{{$user->work_UK_restriction}}</td>
				</tr>
				
				<tr>
					<th>National Insurance Number</th>
					<td>{{$user->national_insurance_number}}</td>
				</tr>
				
				<tr>
					<th>Summary</th>
					<td>{{$user->sentence_yourself}}</td>
				</tr>
				
				<tr>
					<th>About me</th>
					<td>{{$user->description_yourself}}</td>
				</tr>

				<tr><td style="height:20px;"></td></tr>		

				<tr>
					<th style="color:navy;">Bank Account</th>
				</tr>
				<tr>
					<th>Sort Code</th>
					<td align="left">{{$user->sort_code}}</td>
				</tr>
				
				<tr>
					<th>Account Number</th>
					<td align="left">{{$user->account_number}}</td>
				</tr>
				
				
				<tr><td style="height:20px;"></td></tr>	
				<tr>
					<td>
						<table>
							<tr>
								<th style="color:navy;">Personal References</th>
				  			</tr>
							<tr>
								<th>Name</th>
								<th>Job Title</th>
								<th>Relationship</th>
								<th>Phone Number</th>
								<th>Email</th>
							</tr>
						
							@foreach($carer_references as $cr)
								<tr>
									<td>{{$cr->name}}</td>
								  	<td>{{$cr->job_title}}</td>
								  	<td>{{$cr->relationship}}</td>
								  	<td>{{$cr->phone}}</td>
								  	<td>{{$cr->email}}</td>
								</tr>	
							@endforeach
						</table>
					</td>
				</tr>	
					
				<tr>
				  <td>
					<table>
						<tr>
							<th style="color:navy;">Qualifications</th>
						</tr>
					 	<tr>
					 	 	<th>Certificate name</th>
					 	 	<th>Uploaded doc url</th>
					 	</tr>
					
						@foreach($documents as $document)
							@if($document->type=='NVQ')
							<tr>
								<td>{{$document->title}}</td>
								<td>{{url('')}}/api/document/{{$document->id}}/preview</td>
							</tr>
							
							@endif
						@endforeach
					</table>
				  </td>
				</tr>	

					
				<tr>
					<th style="color:navy;">Type of care offered</th>

					<td>
						@foreach($carer_service as $cs)
							{{$cs->name}},
						@endforeach

						@foreach($carer_assistance as $ca)
							{{$ca->name}},
						@endforeach
					</td>
				</tr>
					
				
				<tr><th style="color: skyblue;">Availability</th>
				
					<td>
					@foreach($availibility as $a)
						{{$a->name}},
					@endforeach	
					</td>
				</tr>

				<tr><th style="color: skyblue;">Languages</th>
					<td>
						@foreach($carer_languages as $cl)
							<div class="col-md-4"> 
								{{$cl->carer_language.','}}
							</div>
						@endforeach	
					</td>
				</tr>
				<tr><td style="height:20px;"></td></tr>	
				<tr><th style="color: navy;">Transport</th></tr>
				<tr>
					<th>Driving licence</th>
					<td>
						{{$user->driving_licence}}
					</td>
				</tr>

				<tr>
					<th>Car for Work</th>
					<td>
						{{$user->have_car}}
					</td>
				</tr>

				@if($cp->have_car=='Yes')
				<tr>
					<th>Transport clients</th>
					<td>
						{{$user->use_car}}
					</td>
				</tr>
				@endif

				@if($user->driving_licence=='Yes')
				<tr>
					<th>UK\EEA Driving licence photo</th>
					<td>
						@foreach($documents as $dc)
							@if($dc->type == 'DRIVING_LICENCE_PHOTO')
								{{url('')}}/api/document/{{$dc->id}}/preview
							@endif
						@endforeach
					</td>
				</tr>
				<tr>
					<th>UK\EEA Driving licence Number</th>
					<td>
						{{$user->DBS_number}}
					</td>
				</tr>
				<tr>
					<th>Valid until</th>
					<td>
						{{$user->driving_licence_valid_until}}
					</td>
				</tr>
				@endif

				@if($cp->have_car=='Yes')
				<tr>
					<th>Car insurance Photo</th>
					<td>
						@foreach($documents as $dc)
							@if($dc->type == 'DRIVING_LICENCE_PHOTO')
								{{url('')}}/api/document/{{$dc->id}}/preview
							@endif
						@endforeach
					</td>
				</tr>

				<tr>
					<th>Car insurance policy</th>
					<td>
						@if($cp->car_insurance_number!='')
							{{$cp->car_insurance_number}}
						@else
							Not available
						@endif
					</td>
				</tr>

				<tr>
					<th>Valid until</th>
					<td>
						@if($cp->car_insurance_valid_until !='')
							{{$cp->car_insurance_valid_until }}
						@else
							Not available
						@endif
					</td>
				</tr>

				@endif
				
				<tr><td style="height:20px;"></td></tr>	
				<tr><th style="color: navy;">Criminal Records</th></tr>
				<tr>
					<th>Have up to date DBS certificate</th>
					<td>
						{{$user->DBS}}
					</td>
				</tr>
				<tr>
					<th>Criminal convictions</th>
					<td>
						@if($cp->criminal_conviction=='Some')
							Yes, but they are very old, and for a minor offence.
						@else
							{{$cp->criminal_conviction}}
						@endif
					</td>
				</tr>

				@if($cp->criminal_conviction=='Some')
				<tr>
					<th>Criminal convictions details</th>
					<td>
						{{$cp->criminal_detail}}
					</td>
				</tr>
				@endif

				@if($user->DBS == 'yes')
				<tr>
					<th>Using the new DBS update service</th>
					<td>
						{{$user->DBS_use}}
					</td>
				</tr>
				<tr>
					<th>DBS Update Service identifier</th>
					<td>
						{{$user->DBS_identifier}}
					</td>
				</tr>
				<tr>
					<th>DBS date certificate</th>
					<td>
						{{$user->dbs_date}}
					</td>
				</tr>
				@endif


				

				
