<div class="financialsCard__title" style="width: 100%;">
    <h4 style="color: navy;">
       Uploaded Images 
    </h4>
</div>

<table>
@if($documents || $profileImage)

    @if($profileImage && file_exists(public_path('/img/profile_photos/' . $user->id . '.png')) )
         <tr>
            <th>Profile Image</th>
            <td>{{url('')}}{{$profileImage}}</td>
        </tr>
    @endif
    @if($documents)
        @foreach($documents as $document)
            <?php
                $supported_image = array(
                    'gif',
                    'jpg',
                    'jpeg',
                    'png'
                );

                $src_file_name = $document->file_name;
                $ext = strtolower(pathinfo($src_file_name, PATHINFO_EXTENSION)); // Using strtolower to overcome case sensitive
                if (in_array($ext, $supported_image)) {?>
                <tr>
                    <th>
                    @if($document->title=='undefined')
                        @if($document->type=='NVQ') 
                            Qualification 
                        @else
                             {{$document->type}}   
                        @endif
                       
                        
                    @else
                      {{$document->title}}
                    @endif  
                    
                    </th>
                    <td>{{url('')}}/api/document/{{$document->id}}/preview</td>
                </tr>
                    
               <?php } ?>
           
        @endforeach
    @endif
@else
    <tr><td>No images found.</td></tr>    
@endif    
</table>
