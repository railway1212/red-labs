

<div class="mainPanel">
 <table class="statisticTable">
  <thead>
	 <tr>
		 <td align="left">{{$user->first_name}}  {{$user->family_name}}</td>
	 </tr>	
	</thead>
   </table> 	 


	<div class="tableWrap">
		<div class="statisticHead">
			<h3 class="statisticHead__title" style="color: navy;">
				Profile Details 
			</h3>

		</div>
		<table class="statisticTable">
			<tbody>
				<tr>
					<th>ID</th>
					<td align="left">{{$user->id}}</td>
				</tr>

				<tr>
					<th>First Name</th>
					<td>{{$user->first_name}}</td>
				</tr>
				
				<tr>
					<th>Last Name</th>
					<td>{{$user->family_name}}</td>
				</tr>
				<tr>
					<th>I like to be called</th>
					<td>{{$user->like_name}}</td>
				</tr>
				<tr>
					<th>Gender</th>
					<td>{{$user->gender}}</td>
				</tr>
				<tr>
					<th>Mobile Number</th>
					<td>{{$user->mobile_number}}</td>
				</tr>
				<tr>
					<th>Address Line 1</th>
					<td>{{$user->address_line1}}</td>
				</tr>
				<tr>
					<th>Address Line 2</th>
					<td>{{$user->address_line2}}</td>
				</tr>
				<tr>
					<th>Town / City</th>
					<td>{{$user->town}}</td>
				</tr>
				<tr>
					<th>Post Code</th>
					<td>{{$user->postcode}}</td>
				</tr>
				<tr>
					<th>Date of Birth</th>
					<td>{{$user->DoB}}</td>
				</tr>
				
				<!--<tr>
					<th>Registration Progress</th>
					<td>{{$user->registration_progress}}</td>
				</tr>
				<tr>
					<th>Registration Status</th>
					<td>{{$user->registration_status}}</td>
				</tr>
				<tr>
					<th>Profile Status</th>
					<td>{{$user->profiles_status_id}}</td>
				</tr>-->
				<tr>
					<th>One Line About Me</th>
					<td>{{$user->one_line_about}}</td>
				</tr>
				<tr>
					<th>Languages</th>
					<td>{{$user->carer_language}}</td>
				</tr>
				<tr>
					<th>Home Is A</th>
					<td>{{$user->kind_of_building}}</td>
				</tr>
				<tr>
					<th>Can keep the home safe and clean by themself</th>
					<td>{{$user->home_safe}}</td>
				</tr>
				<tr>
					<th>Requires assistance keeping the home safe and clean</th>
					<td>{{$user->assistance_keeping}}</td>
				</tr>
				<tr>
					<th>Can move around home safely by themself</th>
					<td>{{$user->assistance_moving}}</td>
				</tr>
				<tr>
					<th>Requires assistance moving around home</th>
					<td>{{$user->move_available}}</td>
				</tr>

				<tr><td style="height:20px;"></td></tr>	
				<tr>
					<th style="color:navy;">Entry</th>
				</tr>
				<tr>
					<td>How should the carer enter the {{$user->first_name}}’s home?</td>
					<td>{{$user->carer_enter}}</td>
				</tr>

				<tr><td style="height:20px;"></td></tr>	
				<tr>
					<th style="color:navy;">Other Home Information</th>
				</tr>
				<tr>
					<th>Is there anything else the Carer should be aware of when entering the home?</th>
					<td>{{$user->entering_aware}}</td>
				</tr>
				<tr>
					<th>Please, give details </th>
					<td>{{$user->other_detail}}</td>
				</tr>
				<tr>
					<th>Other Inhabitants</th>
					<td>{{$user->anyone_else_live}}</td>
				</tr>
				<tr>
					<th>please give their name and relationship to {{$user->first_name}}</th>
					<td>{{$user->anyone_detail}}</td>
				</tr>
				<tr>
					<th>Is the other person likely to be home during care visits?</th>
					<td>{{$user->anyone_friendly}}</td>
				</tr>

				<tr><td style="height:20px;"></td></tr>	
				<tr>
					<th style="color:navy;">Pets</th>
				</tr>
				<tr>
					<th>Owns pets</th>
					<td>{{$user->own_pets}}</td>
				</tr>
				<tr>
					<th>Please, give details</th>
					<td>{{$user->pet_detail}}</td>
				</tr>
				<tr>
					<th>Pets friendly with strangers</th>
					<td>{{$user->pet_friendly}}</td>
				</tr>

				<tr><td style="height:20px;"></td></tr>	
				<tr>
					<th style="color:navy;">Companionship</th>
				</tr>
				<tr>
					<th>Has regular social interaction with friends / family</th>
					<td>{{$user->social_interaction}}</td>
				</tr>
				<tr>
					<th>Would {{$user->first_name}} like someone to visit regularly for companionship?</th>
					<td>{{$user->visit_for_companionship}}</td>
				</tr>

				<tr><td style="height:20px;"></td></tr>	
				<tr>
					<th style="color:navy;">Type of Care Needed</th>
				</tr>
				<tr>
					<th>Care</th>
					<td>
						@foreach($care_type as $ct)
							{{$ct->name}}
						@endforeach
						@foreach($service_type as $st)
							{{$st->name}}
						@endforeach
					</td>
				</tr>

				<tr>
					<th>Service</th>
					<td>
						@foreach($service_type as $st)
							{{$st->name}}
							<br>
						@endforeach		
					</td>
				</tr>

				<tr><td style="height:20px;"></td></tr>	
				<tr>
					<th style="color:navy;">Time When Care Needed</th>
				</tr>
				<tr>
					<th>First date for Carer</th>
					<td>{{$user->start_date}}</td>
				</tr>

				<tr><td style="height:20px;"></td></tr>	
				<tr>
					<th style="color:navy;">Health</th>
				</tr>
				<tr>
					<th>Conditions</th>
					<td>
						@foreach($service_user_condition as $suc)
							{{$suc->name}}
						@endforeach
					</td>
				</tr>
				<tr>
					<th>Please Give Details</th>
					<td>
						{{$user->conditions_detail}}
					</td>
				</tr>
				<tr>
					<th>Long term medical conditions</th>
					<td>
						{{$user->long_term_conditions}}
					</td>
				</tr>

				<tr><td style="height:20px;"></td></tr>	
				<tr>
					<th style="color:navy;">Dementia</th>
				</tr>
				<tr>
					<th>Has Dementia</th>
					<td>
						{{$user->have_dementia}}
					</td>
				</tr>
				<tr>
					<th>Please, give details</th>
					<td>
						{{$user->dementia_detail}}
					</td>
				</tr>

				<tr><td style="height:20px;"></td></tr>	
				<tr>
					<th style="color:navy;">Communication</th>
				</tr>
				<tr>
					<th>Has difficulties understanding or communicating with other</th>
					<td>
						{{$user->communication}}
					</td>
				</tr>
				<tr>
					<th>Has problems understanding other people</th>
					<td>
						{{$user->comprehension}}
					</td>
				</tr>
				<tr>
					<th>Please, give details</th>
					<td>
						{{$user->comprehension_detail}}
					</td>
				</tr>
				<tr>
					<th>Needs help with speech</th>
					<td>
						{{$user->speech}}
					</td>
				</tr>
				<tr>
					<th>Has serious impediments seeing</th>
					<td>
						{{$user->vision}}
					</td>
				</tr>
				<tr>
					<th>Please, give details</th>
					<td>
						{{$user->vision_detail}}
					</td>
				</tr>
				<tr>
					<th>Has serious impediments hearing</th>
					<td>
						{{$user->hearing}}
					</td>
				</tr>
				<tr>
					<th>Please, give details </th>
					<td>
						{{$user->hearing_detail}}
					</td>
				</tr>

				<tr><td style="height:20px;"></td></tr>	
				<tr>
					<th style="color:navy;">Medication</th>
				</tr>
				<tr>
					<th>Requires assistance in taking medication / treatments</th>
					<td>
						{{$user->assistance_in_medication}}
					</td>
				</tr>
				<tr>
					<th>Please, give details </th>
					<td>
						{{$user->in_medication_detail}}
					</td>
				</tr>

				<tr><td style="height:20px;"></td></tr>	
				<tr>
					<th style="color:navy;">Allergies</th>
				</tr>
				<tr>
					<th>Has allergies to food / medication / anything else</th>
					<td>
						{{$user->have_any_allergies}}
					</td>
				</tr>
				<tr>
					<th>Please, give details</th>
					<td>
						{{$user->allergies_detail}}
					</td>
				</tr>

				<tr><td style="height:20px;"></td></tr>	
				<tr>
					<th style="color:navy;">Skin</th>
				</tr>
				<tr>
					<th>Has risk of developing pressure sores on their skin</th>
					<td>
						{{$user->skin_scores}}
					</td>
				</tr>
				<tr>
					<th>Please, give details</th>
					<td>
						{{$user->skin_scores_detail}}
					</td>
				</tr>
				<tr>
					<th>Needs assistance with changing wound dressings</th>
					<td>
						{{$user->assistance_with_dressings}}
					</td>
				</tr>
				<tr>
					<th>Please, give details</th>
					<td>
						{{$user->dressings_detail}}
					</td>
				</tr>

				<tr><td style="height:20px;"></td></tr>	
				<tr>
					<th style="color:navy;">Mobility</th>
				</tr>
				<tr>
					<th>Requires help with mobility</th>
					<td>
						{{$user->help_with_mobility}}
					</td>
				</tr>
				<tr>
					<th>Needs help moving around home</th>
					<td>
						{{$user->mobility_home}}
					</td>
				</tr>
				<tr>
					<th>Please, give details</th>
					<td>
						{{$user->mobility_home_detail}}
					</td>
				</tr>
				<tr>
					<th>Needs help getting in / out of bed</th>
					<td>
						{{$user->mobility_bed}}
					</td>
				</tr>
				<tr>
					<th>Please, give details </th>
					<td>
						{{$user->mobility_bed_detail}}
					</td>
				</tr>
				<tr>
					<th>Has a history of falls</th>
					<td>
						{{$user->history_of_falls}}
					</td>
				</tr>
				<tr>
					<th>Please, give details </th>
					<td>
						{{$user->falls_detail}}
					</td>
				</tr>
				<tr>
					<th>Needs help going shopping, or to other local facilities / events</th>
					<td>
						{{$user->mobility_shopping}}
					</td>
				</tr>
				<tr>
					<th>Please, give details</th>
					<td>
						{{$user->mobility_shopping_detail}}
					</td>
				</tr>

				<tr><td style="height:20px;"></td></tr>	
				<tr>
					<th style="color:navy;">Nutrition</th>
				</tr>
				<tr>
					<th>Requires assistance with eating / drinking</th>
					<td>
						{{$user->assistance_with_eating}}
					</td>
				</tr>
				<tr>
					<th>Please, give details</th>
					<td>
						{{$user->assistance_with_eating_detail}}
					</td>
				</tr>
				<tr>
					<th>Can prepare food for themselves</th>
					<td>
						{{$user->prepare_food}}
					</td>
				</tr>
				<tr>
					<th>Has any preferences of food? eg. Are there any do's and don'ts?</th>
					<td>
						{{$user->preferences_of_food}}
					</td>
				</tr>
				<tr>
					<th>Please, give details </th>
					<td>
						{{$user->preferences_of_food_requirements}}
					</td>
				</tr>
				<tr>
					<th>Would like assistance with preparing meals</th>
					<td>
						{{$user->assistance_with_preparing_food}}
					</td>
				</tr>
				<tr>
					<th> Has special nutritional or belief based dietary requirements </th>
					<td>
						{{$user->dietary_requirements}}
					</td>
				</tr>
				<tr>
					<th>Please, give details </th>
					<td>
						{{$user->dietary_requirements_interaction}}
					</td>
				</tr>
				<tr>
					<th>Has other special dietary requirements</th>
					<td>
						{{$user->special_dietary_requirements}}
					</td>
				</tr>
				<tr>
					<th>Please, give details</th>
					<td>
						{{$user->special_dietary_requirements_detail}}
					</td>
				</tr>

				<tr><td style="height:20px;"></td></tr>	
				<tr>
					<th style="color:navy;"> Personal Hygiene</th>
				</tr>
				<tr>
					<th>Requires assistance in getting dressed / bathing or toileting</th>
					<td>
						{{$user->assistance_with_personal_hygiene}}
					</td>
				</tr>
				<tr>
					<th>Needs assistance in choosing appropriate clothes</th>
					<td>
						{{$user->appropriate_clothes}}
					</td>
				</tr>
				<tr>
					<th>Needs assistance getting dressed / undressed</th>
					<td>
						{{$user->assistance_getting_dressed}}
					</td>
				</tr>
				<tr>
					<th>Please, give details</th>
					<td>
						{{$user->assistance_getting_dressed_detail}}
					</td>
				</tr>
				<tr>
					<th>Needs assistance with bathing / showering</th>
					<td>
						{{$user->assistance_with_bathing}}
					</td>
				</tr>
				<tr>
					<th>How many times a week?</th>
					<td>
						{{$user->bathing_times_per_week}}
					</td>
				</tr>
				<tr>
					<th>Needs assistance managing their toilet needs</th>
					<td>
						{{$user->managing_toilet_needs}}
					</td>
				</tr>
				<tr>
					<th>Needs help mobilising themselves to the toilet</th>
					<td>
						{{$user->mobilising_to_toilet}}
					</td>
				</tr>
				<tr>
					<th>Needs help cleaning themselves when using the toilet</th>
					<td>
						{{$user->cleaning_themselves}}
					</td>
				</tr>
				<tr>
					<th>Has incontinence</th>
					<td>
						{{$user->have_incontinence}}
					</td>
				</tr>
				<tr>
					<th>Please Give Details</th>
					<td>
						{{$user->kind_of_incontinence}}
					</td>
				</tr>
				<tr>
					<th>Has own supply of incontinence wear</th>
					<td>
						{{$user->incontinence_wear}}
					</td>
				</tr>
				<tr>
					<th>Needs help in choosing incontinence products</th>
					<td>
						{{$user->choosing_incontinence_products}}
					</td>
				</tr>
				<tr>
					<th>The incontinence products are stored...</th>
					<td>
						{{$user->incontinence_products_stored}}
					</td>
				</tr>

				<tr><td style="height:20px;"></td></tr>	
				<tr>
					<th style="color:navy;">Behaviour</th>
				</tr>
				<tr>
					<th>Has a doctor's note or court order saying that they are not able to give consent</th>
					<td>
						{{$user->consent}}
					</td>
				</tr>
				<tr>
					<th>Please, give details</th>
					<td>
						{{$user->consent_details}}
					</td>
				</tr>

				<tr><td style="height:20px;"></td></tr>	
				<tr>
					<th style="color:navy;">Night-time</th>
				</tr>
				<tr>
					<th>Has problems getting dressed for bed</th>
					<td>
						{{$user->getting_dressed_for_bed}}
					</td>
				</tr>
				<tr>
					<th>Needs help getting ready for bed?</th>
					<td>
						{{$user->getting_ready_for_bed}}
					</td>
				</tr>
				<tr>
					<th>What time would they like someone to come and help?</th>
					<td>
						{{$user->time_to_bed}}
					</td>
				</tr>
				<tr>
					<th>Needs assistance keeping safe at night</th>
					<td>
						{{$user->keeping_safe_at_night}}
					</td>
				</tr>
				<tr>
					<th>Please, give details</th>
					<td>
						{{$user->keeping_safe_at_night_details}}
					</td>
				</tr>
				<tr>
					<th>What time would they like someone to help?</th>
					<td>
						{{$user->time_to_night_helping}}
					</td>
				</tr>
				<tr>
					<th>Needs help going to the toilet at night</th>
					<td>
						{{$user->toilet_at_night}}
					</td>
				</tr>
				<tr>
					<th>Needs someone to help at night</th>
					<td>
						{{$user->helping_toilet_at_night}}
					</td>
				</tr>

				<tr><td style="height:20px;"></td></tr>	
				<tr>
					<th style="color:navy;">Other</th>
				</tr>
				<tr>
					<th>Has political, religious or other beliefs </th>
					<td>
						{{$user->religious_beliefs}}
					</td>
				</tr>
				<tr>
					<th>Please, give details </th>
					<td>
						{{$user->religious_beliefs_details}}
					</td>
				</tr>
				<tr>
					<th> Has particular likes or dislikes </th>
					<td>
						{{$user->particular_likes}}
					</td>
				</tr>
				<tr>
					<th>Please, give details </th>
					<td>
						{{$user->particular_likes_details}}	
					</td>
				</tr>
				<tr>
					<th> Needs the assistance of more than one person at a time to achieve any particular task </th>
					<td>
						{{$user->multiple_carers}}
					</td>
				</tr>
				<tr>
					<th> Please, give details</th>
					<td>
						{{$user->multiple_carers_details}}
					</td>
				</tr>
				<tr>
					<th> Likes socialising with other people / groups</th>
					<td>
						{{$user->socialising_with_other}}
					</td>
				</tr>
				<tr>
					<th> Has interests or hobbies which Marge enjoy</th>
					<td>
						{{$user->interests_hobbies}}
					</td>
				</tr>
				<tr>
					<th> Please, give details</th>
					<td>
						{{$user->interests_hobbies_details}}
					</td>
				</tr>
				<tr>
					<th> Are there any other medical conditions, disabilities, or other pieces of information not already covered which you feel may be of use? </th>
					<td>
						{{$user->other_medical_conditions}}
					</td>
				</tr>
				<tr>
					<th> Please, give details</th>
					<td>
						{{$user->other_medical_detail}}
					</td>
				</tr>
				

			</tbody>
		</table>

	</div>
</div>
