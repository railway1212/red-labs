<!-- Messages table -->	
<div class="financialsCard" style="overflow-x: auto; max-height:500px">
	<div class="financialsCard__header">
		<div class="financialsCard__title" style="width: 100%;">
			<h4>
				Mails 
			</h4>
		</div>
		<div class="tableWrap tableWrap--margin-t">
		    <table class="adminTable">
		    	<thead>
					<td>Subject</td>
					<td>Date</td>
					<td>Content</td>
				</thead>
				<tbody>
					
				@if(count($mails)>0)
				   @foreach($mails as $mail)	
					<tr>
						<td>{{$mail->subject}}</td>
						<td>{{$mail->time_to_send}}</td>
						<td>
							<?php 
								 $text = preg_replace('#<style[^>]*>.*?</style>#si', '', $mail->text); 
								 $text = preg_replace('#<title[^>]*>.*?</title>#si', '', $text); 
								 echo  strip_tags($text); 
							 ?>
						</td>
					</tr>
					
					@endforeach
			   @endif		
				</tbody>
			</table>
		</div>			
		
	</div>
</div>		
