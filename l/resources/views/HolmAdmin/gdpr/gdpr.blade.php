<div class="mainPanel">
    <h2 class="categoryTitle">
          <span class="categoryTitle__ico">
              <i class="fa fa-user-secret" aria-hidden="true"></i>
          </span>
        GDPR
    </h2>
    
    <div class="panelHead">
       <div class="panelHead__group">
			{!! Form::open(['method'=>'GET','route'=>'gdpr']) !!}
			 @if(count($user) > 1)
				<div><h3>Please select user or search by full name or id.</h3></div>
			   @endif
			<div class="filterBox">
				<div class="formField formField--fix-biger"style="margin-right: 10px">
					<div class="fieldWrap">
						{!! Form::text('userName',null,['class'=>'formItem formItem--input formItem--search','maxlength'=>'60','placeholder'=>'Search name']) !!}
					</div>
				</div>
				
				
		   @if(count($user) > 1)
			<div class="formField formField--fix-biger"style="margin-right: 10px">
			  <div class="fieldWrap">
			   <select name="userId" class="formItem formItem--select" id="gdprOption">
				   <option disable>Select User</option>
				 @foreach($user as $option)
				   <option value="{{$option->id}}">{{$option->first_name}} {{$option->family_name}}</option>
				 @endforeach
			   </select>
			 </div>
			</div>   
			
			@endif	

				{!! Form::submit('filter',['class'=>'actionsBtn actionsBtn--filter actionsBtn--bigger']) !!}
			</div>
			{!! Form::close()!!}
			
			@if(count($user) == 1)
			
				<a href="{{route('gdprDownload')}}?userName={{ app('request')->input('userName') }}" class="actionsBtn actionsBtn--filter actionsBtn--bigger">
					Export to CSV
				</a>
			
			@endif

        </div>
	</div>
	
	
	<div class="financialsContainer">

		@if(count($user)==1 && !empty($user))

			@if($user->user_type == 'carer')
			
				@include(config('settings.theme').'.gdpr.GdprCarer')
			
			@elseif($user->user_type == 'purchaser')
			
				@include(config('settings.theme').'.gdpr.GdprPurchaser')	
			
			@else
			
				@include(config('settings.theme').'.gdpr.GdprServiceUser')
			
			@endif
			
			  @include(config('settings.theme').'.gdpr.GdprMails')
					
					
		@else
		
		  <div class="financialsCard">
            <div class="financialsRow" style="text-align:center;display:block;">
				Please search by user id or name
            </div>
          </div>

		@endif

    </div>
    
</div>

<script>
$(document).ready(function(){	
	$('#gdprOption').change(function(){
		var opt = $(this).val();
		$('.formItem--search').val(opt);
	});
});
</script>
