@if($user->user_type == 'carer')

<!------Transaction---->

        <tr><td style="height:20px;"></td></tr>	
        <tr><th style="color: navy;">Carer Payouts</th></tr>

        <tr>
            <th>Transaction id</th>
            <th>Total</th>
            <th>Appointment Id</th>
            <th>Payout status</th>
        </tr>


        @if(count($all) > 0)
            @foreach($all as $one)
                @if(!isset($one->total))
                <tr>
                    <td align="left">
                        {{$one->id}}
                    </td>
                    <td align="left">
                        {{$one->amount/100}}
                    </td>
                    <td>
                        <?php 
                                        
                        if($one->bonus_id > 0){
                            
                            echo 'Bonus Payout';
                            
                        }else{
                            $allApps = \App\Appointment::where('booking_id', $one->booking_id)->pluck('id')->toArray();
                            for ($i = 0; $i < count($allApps); $i++) {
                                echo "<a href='/bookings/" . $one->booking_id . "/details'>" . $allApps[$i] . "<a/><br>";
                            }
                        }
                        ?>
                    </td>
                    <td>
                        paid
                    </td>
                </tr>
                @endif
            @endforeach
        @endif

    
            @foreach($potentialPayouts as $potentialPayout)
                <tr>
                    <td align="left">
                        {{ $potentialPayout->id }}
                    </td>
                    <td>
                            {{$potentialPayout->total}}
                    </td>
                    <td>
                        {{$potentialPayout->booking_id}}
                    </td>
                </tr>
            @endforeach
            @foreach($transfers as $transfer)
                <tr>
                    <td align="left">
                        {{$transfer->id}}
                    </td>
                    <td>
                        {{$transfer->amount/100}}
                    </td>
                    <td>
                        {{$transfer->booking_id}}
                    </td>
                    <td>
                        paid
                    </td>
                </tr>
            @endforeach
        
        
    </tbody>
</table>


@elseif($user->user_type == 'purchaser')

<!-- Transaction table -->
<div class="financialsCard" style="overflow-x: auto; max-height:500px">
	<div class="financialsCard__header">
		<div class="financialsCard__title" style="width: 100%;">
			<h4 style="color: navy;">
				Transactions 
			</h4>
		</div>

		<div class="tableWrap tableWrap--margin-t">
			@if(count($purchaser_transaction) > 0)
		    <table class="adminTable">
		    	<thead>
		    		<tr class="extra-tr">
		    			<td class="for-inner">
			                <table class="innerTable ">
			                    <tr>
			                        <td class="">
			                            <span class="extraTitle">Transaction ID</span>
			                        </td>
			                        <td class="">
			                            <span class="extraTitle">Date and Time</span>
			                        </td>
			                        <td class="">
			                            <span class="extraTitle">Booking ID</span>
			                        </td>
			                        <td class="">
			                            <span class="extraTitle">Amount</span>
			                        </td>
			                        <td class="">
			                            <span class="extraTitle">Payment Type</span>
			                        </td>
			                        <td class="">
			                            <span class="extraTitle">Transaction Status</span>
			                        </td>
			                    </tr>
			                </table>
			            </td>
		    		</tr>
		    	</thead>
		    	<tbody>
		    		<tr>
		    			<td class="for-inner">
		    				<table class="innerTable ">
		    					@foreach($purchaser_transaction as $pt)
		    						    <tr>
							                <td class="">
							                    <span>{{$pt->id}}</span>
							                </td>
							                <td class="">
							                   	{{$pt->created_at}}
							                </td>
							                <td class="">
							                   	{{$pt->booking_id}}
							                </td>
							                <td class="" align="left">
							                	
							                   	{{$pt->amount}}
							                </td>
							                <td class="">
							                @if($pt->payment_method=='stripe')
							                	Credit Card 
							                @else
							                	Bonuses Wallet 
							                @endif
							                	
							                </td>
							                <td class="">
							                  	completed
							                </td>
							            </tr>
		    						
		    					@endforeach
		    						
					        </table>
		    			</td>
		    			<td class="for-inner">
		    			</td>
		    		</tr>
		    	</tbody>
		    </table>
		    @else
		       No Transaction found.
		    @endif	
	    </div>
	</div>
</div>

@else




<!-- Transaction table -->
<div class="financialsCard" style="overflow-x: auto; max-height:500px">
	<div class="financialsCard__header">
		<div class="financialsCard__title" style="width: 100%;">
			<h4 style="color:navy;">
				Transactions 
			</h4>
		</div>

		<div class="tableWrap tableWrap--margin-t">
		    <table class="adminTable">
		    	<thead>
		    		<tr class="extra-tr">
		    			<td class="for-inner">
			                <table class="innerTable ">
			                    <tr>
			                        <td class="">
			                            <span class="extraTitle">Transaction ID</span>
			                        </td>
			                        <td class="">
			                            <span class="extraTitle">Date and Time</span>
			                        </td>
			                        <td class="">
			                            <span class="extraTitle">Booking ID</span>
			                        </td>
			                        <td class="">
			                            <span class="extraTitle">Amount</span>
			                        </td>
			                        <td class="">
			                            <span class="extraTitle">Payment Type</span>
			                        </td>
			                        <td class="">
			                            <span class="extraTitle">Transaction Status</span>
			                        </td>
			                    </tr>
			                </table>
			            </td>
		    		</tr>
		    	</thead>
		    	<tbody>
		    		<tr>
		    			<td class="for-inner">
		    				<table class="innerTable ">
								@if(count($servicePerchaserTransaction)>0)
			    					@foreach($servicePerchaserTransaction as $pt)
			    						    <tr>
								                <td class="">
								                    <span>{{$pt->id}}</span>
								                </td>
								                <td class="">
								                   	{{$pt->created_at}}
								                </td>
								                <td class="">
								                   	{{$pt->booking_id}}
								                </td>
								                <td class="">
								                	<i class="fa fa-gbp" aria-hidden="true"></i>
								                   	{{$pt->amount}}
								                </td>
								                <td class="">
									                @if($pt->payment_method=='stripe')
									                	Credit Card 
									                @else
									                	Bonuses Wallet 
									                @endif
								                </td>
								                <td class="">
								                  	completed
								                </td>
								            </tr>
			    						
			    					@endforeach
		    				@else
		    				  	<tr><td colspan="6"> No record found </td></tr>	
		    				@endif	
					        </table>
		    			</td>
		    			<td class="for-inner">
		    			</td>
		    		</tr>
		    	</tbody>
		    </table>	
	    </div>
	</div>
</div>



@endif