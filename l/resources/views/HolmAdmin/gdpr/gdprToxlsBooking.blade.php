
@if($user->user_type == 'carer')

<!---Appointment---->
<tr><td style="height:20px;"></td></tr>		
<tr><th style="color: navy;">Appointments</th></tr>

<tr>
    <th>Appointment ID</th>
    <th>Date</th>
    <th>From</th>
    <th>To</th>
    <th>Purchaser</th>
    <th>Service User</th>
    <th>Status</th>
    <th>Amount</th>
</tr>
@foreach($carer_booking as $cb)
    @foreach($booking_appointment=$cb->appointments as $ba)
    <tr>
        <td align="left">{{$ba->id}}</td>
        <td >{{$ba->date_start}}</td>
        <td align="left">{{$ba->time_from}}</td>
        <td align="left">{{$ba->time_to}}</td>
        <td align="left">{{$cb->bookingPurchaserProfile->first_name}} {{$cb->bookingPurchaserProfile->family_name}}</td>
        <td align="left">{{$cb->bookingServiceUser->first_name}} {{$cb->bookingServiceUser->family_name}}</td>
        <td>{{$ba->appointmentStatus->name}}</td>
        <td>{{$ba->price_for_carer}}</td>
    </tr>
    @endforeach
@endforeach





@elseif($user->user_type == 'purchaser')
<!-- Appointment table -->
<div class="financialsCard" style="overflow-x: auto; max-height:500px">
	<div class="financialsCard__header">
		<div class="financialsCard__title" style="width: 100%;">
			<h4 style="color: navy;">
				Appointments 
			</h4>
		</div>

		<div class="tableWrap tableWrap--margin-t">
		@if(count($purchaser_booking) > 0 )
		    <table class="adminTable">
		    	<thead>
		    		<tr class="extra-tr">
		    			<td class="for-inner">
			                <table class="innerTable ">
			                    <tr>
			                        <td class="">
			                            <span class="extraTitle">Appointment ID</span>
			                        </td>
			                        <td class="">
			                            <span class="extraTitle">Date</span>
			                        </td>
			                        <td class="">
			                            <span class="extraTitle">From</span>
			                        </td>
			                        <td class="">
			                            <span class="extraTitle">To</span>
			                        </td>
									<td class="">
			                            <span class="extraTitle">Carer</span>
			                        </td>
									<td class="">
			                            <span class="extraTitle">Service User</span>
			                        </td>
			                        <td class="">
			                            <span class="extraTitle">Status</span>
			                        </td>
			                        <td class="">
			                            <span class="extraTitle">Amount</span>
			                        </td>
			                    </tr>
			                </table>
			            </td>
		    		</tr>
		    	</thead>
		    	<tbody>
		    		<tr>
		    			<td class="for-inner">
		    				<table class="innerTable ">
		    				@foreach($purchaser_booking as $pb)
		 		    				@foreach($booking_appointment=$pb->appointments as $ba)
							            <tr>
							                <td class="">
							                    <span>{{$ba->id}}</span>
							                </td>
							                <td class="">
							                   {{$ba->date_start}}
							                </td>
							                <td class="" align="left">
							                   {{$ba->time_from}}
							                </td>
							                <td class="" align="left">
							                   {{$ba->time_to}}
							                </td>
											<td class="" align="left">
												{{$pb->bookingCarerProfile->first_name}} {{$pb->bookingCarerProfile->family_name}}
							                </td>
											<td class="" align="left">
												{{$pb->bookingServiceUser->first_name}} {{$pb->bookingServiceUser->family_name}}
							                </td>
							                <td class="">
							                	{{$ba->appointmentStatus->name}}
							                	
							                </td>
							                <td class="">
							                	<i class="fa fa-gbp" aria-hidden="true"></i>
							                   {{$ba->price_for_carer}}
							                </td>
							            </tr>
		    						@endforeach
					        @endforeach
					        </table>
		    			</td>
		    			<td class="for-inner">
		    			</td>
		    		</tr>
		    	</tbody>
		    </table>
		   @else
		   No appointments found. 	
		  @endif  	
	    </div>
	</div>
</div>

@else

<!-- Appointment table -->
<div class="financialsCard" style="overflow-x: auto; max-height:500px">
	<div class="financialsCard__header">
		<div class="financialsCard__title" style="width: 100%;">
			<h4 style="color:navy;">
				Appointments 
			</h4>
		</div>

		<div class="tableWrap tableWrap--margin-t">
		    <table class="adminTable">
		    	<thead>
		    		<tr class="extra-tr">
		    			<td class="for-inner">
			                <table class="innerTable ">
			                    <tr>
			                        <td class="">
			                            <span class="extraTitle">Appointment ID</span>
			                        </td>
			                        <td class="">
			                            <span class="extraTitle">Date</span>
			                        </td>
			                        <td class="">
			                            <span class="extraTitle">From</span>
			                        </td>
			                        <td class="">
			                            <span class="extraTitle">To</span>
			                        </td>
									<td class="">
			                            <span class="extraTitle">Carer</span>
			                        </td>
			                        <td class="">
			                            <span class="extraTitle">Status</span>
			                        </td>
			                        <td class="">
			                            <span class="extraTitle">Amount</span>
			                        </td>
			                    </tr>
			                </table>
			            </td>
		    		</tr>
		    	</thead>
		    	<tbody>
		    		<tr>
		    			<td class="for-inner">
		    				<table class="innerTable ">
								
		    				@foreach($servicePerchaserBooking as $pb)
		    				  @if(count($pb->appointments)>0)
		 		    				@foreach($booking_appointment=$pb->appointments as $ba)
							            <tr>
							                <td class="">
							                    <span>{{$ba->id}}</span>
							                </td>
							                <td class="">
							                   {{$ba->date_start}}
							                </td>
							                <td class="" align="left">
							                   {{$ba->time_from}}
							                </td>
							                <td class="" align="left">
							                   {{$ba->time_to}}
							                </td>
											<td class="" align="left">
												{{$pb->bookingCarerProfile->first_name}} {{$pb->bookingCarerProfile->family_name}}
							                </td>
							                <td class="">
							                	{{$ba->appointmentStatus->name}}
							                	
							                </td>
							                <td class="">
							                	<i class="fa fa-gbp" aria-hidden="true"></i>
							                   {{$ba->price_for_carer}}
							                </td>
							            </tr>
		    						@endforeach
		    					@endif	
					        @endforeach
					        </table>
		    			</td>
		    			<td class="for-inner">
		    			</td>
		    		</tr>
		    	</tbody>
		    </table>	
	    </div>
	</div>
</div>

@endif