
<div class="financialsCard">
	<div class="financialsCard__header">
		<div class="financialsCard__title">
			<h2>
				Profile Details
			</h2>
		</div>
	</div>
	
	<div class="financialsCard__header"><h3>General</h3></div>
	
	<div class="row financialsCard__header">
		<div class="col-md-6"> First Name</div>
		<div class="col-md-6">
		@if($user->first_name==null)
			--
		@else
			{{$user->first_name}}
		@endif
		</div>
	</div>

	<div class="row financialsCard__header">
		<div class="col-md-6"> Last Name </div>
		<div class="col-md-6">
		@if($user->family_name==null)
			--
		@else
			{{$user->family_name}}
		@endif
		</div>
	</div>
	
	<div class="row financialsCard__header">
		<div class="col-md-6"> Gender </div>
		<div class="col-md-6">
		@if($user->gender==null)
			--
		@else
			{{$user->gender}}
		@endif
		</div>
	</div>
	<div class="row financialsCard__header">
		<div class="col-md-6"> Date of birth </div>
		<div class="col-md-6">
		@if($user->DoB==null)
			--
		@else
			{{$user->DoB}}
		@endif
		</div>
	</div>
	
	<div class="row financialsCard__header">
		<div class="col-md-6"> I like to be called </div>
		<div class="col-md-6">
		@if($user->like_name==null)
			--
		@else
			{{$user->like_name}}
		@endif
		</div>
	</div>
	
	<div class="row financialsCard__header">
		<div class="col-md-6"> Own referral Code </div>
		<div class="col-md-6">
		@if($user->own_referral_code==null)
			--
		@else
			{{$user->own_referral_code}}
		@endif
		</div>
	</div>
	
	<div class="row financialsCard__header">
		<div class="col-md-6"> Registered On </div>
		<div class="col-md-6">
		@if($user->created_at==null)
			--
		@else
			{{$user->created_at}}
		@endif
		</div>
	</div>
	
	<div class="financialsCard__header"><h3>Contact</h3></div>
	
	<div class="row financialsCard__header">
		<div class="col-md-6"> Address Line 1 </div>
		<div class="col-md-6">
		@if($user->address_line1==null)
			--
		@else
			{{$user->address_line1}}
		@endif
		</div>
	</div>
	
	<div class="row financialsCard__header">
		<div class="col-md-6"> Address Line 2 </div>
		<div class="col-md-6">
		@if($user->address_line2==null)
			--
		@else
			{{$user->address_line2}}
		@endif
		</div>
	</div>
	
	<div class="row financialsCard__header">
		<div class="col-md-6">Town / city  </div>
		<div class="col-md-6">
		@if($user->town==null)
			--
		@else
			{{$user->town}}
		@endif
		</div>
	</div>
	
	<div class="row financialsCard__header">
		<div class="col-md-6"> Post code</div>
		<div class="col-md-6">
		@if($user->postcode==null)
			--
		@else
			{{$user->postcode}}
		@endif
		</div>
	</div>
	
	<div class="row financialsCard__header">
		<div class="col-md-6"> Mobile Number</div>
		<div class="col-md-6">
		@if($user->mobile_number==null)
			--
		@else
			{{$user->mobile_number}}
		@endif
		</div>
	</div>
	
	
	
	
	<div class="row financialsCard__header">
		<div class="col-md-6"> Email Address</div>
		<div class="col-md-6">
		
			@if(isset($user->email) )
				--
			@else
				{{$user->email}}
			@endif
		</div>
	</div>
	


	<div class="financialsCard__header"><h3>Payment Details</h3></div>
		<div class="col-md-6"></div>
		<div class="col-md-6">
		
		@if(count($credit_card)> 0)
			@foreach($credit_card as $cc)
				xxxx xxxx xxxx {{$cc->last_four}}	
			@endforeach
		@else
			--
		@endif
		
		</div>

	<div class="financialsCard__header"><h3>People I Am Buying carer for</h3></div>
		<div class="col-md-6"></div>
		<div class="col-md-6">
		@if($service_user->count()>0)
			@foreach($service_user as $su)
				@if($su->deleted!='Yes')
					@if($su->first_name==null)
						<a target="_blank" href="{{route('gdpr')}}?userName={{$su->id}}">NA</a>,
					@else
						<a target="_blank" href="{{route('gdpr')}}?userName={{$su->id}}">{{$su->first_name}}</a>,
					@endif
				@endif
			@endforeach
		@else
			--
		@endif
		</div>
  </div>



	
	
<!-- Appointment table -->
<div class="financialsCard" style="overflow-x: auto; max-height:500px">
	<div class="financialsCard__header">
		<div class="financialsCard__title" style="width: 100%;">
			<h2>
				Appointment 
			</h2>
		</div>

		<div class="tableWrap tableWrap--margin-t">
		    <table class="adminTable">
		    	<thead>
		    		<tr class="extra-tr">
		    			<td class="for-inner">
			                <table class="innerTable ">
			                    <tr>
			                        <td class="">
			                            <span class="extraTitle">Appointment ID</span>
			                        </td>
			                        <td class="">
			                            <span class="extraTitle">Date</span>
			                        </td>
			                        <td class="">
			                            <span class="extraTitle">From</span>
			                        </td>
			                        <td class="">
			                            <span class="extraTitle">To</span>
			                        </td>
									<td class="">
			                            <span class="extraTitle">Carer</span>
			                        </td>
									<td class="">
			                            <span class="extraTitle">Service User</span>
			                        </td>
			                        <td class="">
			                            <span class="extraTitle">Status</span>
			                        </td>
			                        <td class="">
			                            <span class="extraTitle">Amount</span>
			                        </td>
			                    </tr>
			                </table>
			            </td>
		    		</tr>
		    	</thead>
		    	<tbody>
		    		<tr>
		    			<td class="for-inner">
		    				<table class="innerTable ">
		    				@foreach($purchaser_booking as $pb)
		 		    				@foreach($booking_appointment=$pb->appointments as $ba)
							            <tr>
							                <td class="">
							                    <span>{{$ba->id}}</span>
							                </td>
							                <td class="">
							                   {{$ba->date_start}}
							                </td>
							                <td class="">
							                   {{$ba->time_from}}
							                </td>
							                <td class="">
							                   {{$ba->time_to}}
							                </td>
											<td class="">
												{{$pb->bookingCarerProfile->first_name}} {{$pb->bookingCarerProfile->family_name}}
							                </td>
											<td class="">
												{{$pb->bookingServiceUser->first_name}} {{$pb->bookingServiceUser->family_name}}
							                </td>
							                <td class="">
							                	{{$ba->appointmentStatus->name}}
							                	
							                </td>
							                <td class="">
							                	<i class="fa fa-gbp" aria-hidden="true"></i>
							                   {{$ba->price_for_carer}}
							                </td>
							            </tr>
		    						@endforeach
					        @endforeach
					        </table>
		    			</td>
		    			<td class="for-inner">
		    			</td>
		    		</tr>
		    	</tbody>
		    </table>	
	    </div>
	</div>
</div>



<!-- Transaction table -->
<div class="financialsCard" style="overflow-x: auto; max-height:500px">
	<div class="financialsCard__header">
		<div class="financialsCard__title" style="width: 100%;">
			<h2>
				Transaction 
			</h2>
		</div>

		<div class="tableWrap tableWrap--margin-t">
		    <table class="adminTable">
		    	<thead>
		    		<tr class="extra-tr">
		    			<td class="for-inner">
			                <table class="innerTable ">
			                    <tr>
			                        <td class="">
			                            <span class="extraTitle">Transaction ID</span>
			                        </td>
			                        <td class="">
			                            <span class="extraTitle">Date and Time</span>
			                        </td>
			                        <td class="">
			                            <span class="extraTitle">Booking ID</span>
			                        </td>
			                        <td class="">
			                            <span class="extraTitle">Amount</span>
			                        </td>
			                        <td class="">
			                            <span class="extraTitle">Payment Type</span>
			                        </td>
			                        <td class="">
			                            <span class="extraTitle">Transaction Status</span>
			                        </td>
			                    </tr>
			                </table>
			            </td>
		    		</tr>
		    	</thead>
		    	<tbody>
		    		<tr>
		    			<td class="for-inner">
		    				<table class="innerTable ">
		    					@foreach($purchaser_transaction as $pt)
		    						    <tr>
							                <td class="">
							                    <span>{{$pt->id}}</span>
							                </td>
							                <td class="">
							                   	{{$pt->created_at}}
							                </td>
							                <td class="">
							                   	{{$pt->booking_id}}
							                </td>
							                <td class="">
							                	<i class="fa fa-gbp" aria-hidden="true"></i>
							                   	{{$pt->amount}}
							                </td>
							                <td class="">
							                @if($pt->payment_method=='stripe')
							                	Credit Card 
							                @else
							                	Bonuses Wallet 
							                @endif
							                	
							                </td>
							                <td class="">
							                  	completed
							                </td>
							            </tr>
		    						
		    					@endforeach
		    						
					        </table>
		    			</td>
		    			<td class="for-inner">
		    			</td>
		    		</tr>
		    	</tbody>
		    </table>	
	    </div>
	</div>
</div>
