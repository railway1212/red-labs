
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<style>
.textCenter{text-align:center;}

</style>
<html>
<div class="mainPanel">
     <table class="statisticTable">
      <thead>
         <tr>
			 <td>Statistic</td>
			 <td align="center">{{$daterange}}</td>
	     </tr>	
	    </thead>
	   </table> 	 
   
   
         
 
    <div class="statisticGroup">
        <div class="statisticBox">
            <div class="tableWrap">
                <div class="statisticHead">
                    <h3 class="statisticHead__title">
                  <span class="statisticHead__ico">
                    <i class="fa fa-area-chart" aria-hidden="true"></i>
                  </span>
                        BOOKINGS STATISTIC
                    </h3>

                </div>
                <table class="statisticTable">
                    <thead>
                    <tr>
                        <td>
                      <span class="ordinaryTitle td-stat td-stat--start">
                       Booking
                      </span>
                        </td>
                        <td align="center" >
                    
                          Booking amount
                    
                        </td>
                        <td class="textCenter    "  align="center">
                      <span class="ordinaryTitle td-stat">
                        Bookings value, £
                      </span>
                        </td>


                    </tr>
                    </thead>
                    <tbody>
                    @foreach($bookingsStatistic as $item)
                        <tr class="statisticRow">
                            <td class="">
                          <span>
                           @if($item->status_id == '2')
                                  New
                              @elseif($item->status_id == '5')
                                  In Progress
                              @elseif($item->status_id == '7')
                                  Completed
                              @endif
                          </span>
                            </td>
                            <td class="textCenter"  align="center">
                          <span>
                            {{$item->amount}}
                          </span>
                            </td>
                            <td class="textCenter"  align="center">
                          <span>
                            {{$item->price}}
                          </span>
                            </td>
                        </tr>
                    @endforeach
                    @if($bookingsStatisticTotal[0])
                        <tr class="statisticRow statisticRow--total">
                            <td>
                          <span class="">
                            total
                          </span>
                            </td>
                            <td class="textCenter"  align="center">
                          <span>
                            {{$bookingsStatisticTotal[0]->amount}}
                          </span>
                            </td>
                            <td class="textCenter"  align="center">
                          <span>
                            {{$bookingsStatisticTotal[0]->price}}
                          </span>
                            </td>
                        </tr>
                    @endif
                    </tbody>
                </table>

            </div>
        </div>
        <div class="statisticBox">
            <div class="tableWrap">
                <div class="statisticHead">
                    <h3 class="statisticHead__title">
                  <span class="statisticHead__ico">
                    <i class="fa fa-user" aria-hidden="true"></i>
                  </span>
                        User statistics
                    </h3>

                </div>
                <table class="statisticTable">
                    <thead>
                    <tr>
                        <td class="">
                      <span class="ordinaryTitle td-stat td-stat--start">
                       User type
                      </span>
                        </td>
                        <td class="textCenter" >
                      <span class="ordinaryTitle td-stat">
                      Registered amount
                      </span>
                        </td>
                        <td class="textCenter">
                      <span class="ordinaryTitle td-stat">
                        Incomplete <br>Registrations AMOUNT
                      </span>
                        </td>


                    </tr>
                    </thead>
                    <tbody>
                    <tr class="statisticRow">
                        <td>
                      <span>
                       Purchaser
                      </span>
                        </td>
                        <td class="textCenter">
                      <span>
                        {{$usersStatistic['purchasers_amount']}}
                      </span>
                        </td>
                        <td class="textCenter">
                      <span>
                        {{$usersStatistic['incomplete_purchasers_amount']}}
                      </span>
                        </td>
                    </tr>
                    <tr class="statisticRow">
                        <td>
                      <span>
                        Service user
                      </span>
                        </td>
                        <td class="textCenter">
                      <span>
                        {{$usersStatistic['service_users_profiles']}}
                      </span>
                        </td>
                        <td class="textCenter">
                      <span>
                        {{$usersStatistic['incomplete_service_users_profiles']}}
                      </span>
                        </td>
                    </tr>
                    <tr class="statisticRow">
                        <td>
                      <span>
                        Cares
                      </span>
                        </td>
                        <td class="textCenter">
                      <span>
                        {{$usersStatistic['carers_amount']}}
                      </span>
                        </td>
                        <td class="textCenter">
                      <span>
                        {{$usersStatistic['incomplete_carers_amount']}}
                      </span>
                        </td>
                    </tr>
                    <tr class="statisticRow statisticRow--total">
                        <td>
                      <span class="">
                        total
                      </span>
                        </td>
                        <td class="textCenter">
                      <span>
                        {{$usersStatistic['purchasers_amount'] + $usersStatistic['service_users_profiles'] + $usersStatistic['incomplete_carers_amount']}}
                      </span>
                        </td>
                        <td class="textCenter">
                      <span>
                        {{$usersStatistic['incomplete_purchasers_amount'] + $usersStatistic['incomplete_carers_amount'] + $usersStatistic['incomplete_service_users_profiles']}}
                      </span>
                        </td>
                    </tr>
                    </tbody>
                </table>

            </div>
        </div>
    </div>

    <div class="statisticGroup">
        <div class="statisticBox">
            <div class="tableWrap">
                <div class="statisticHead">
                    <h3 class="statisticHead__title">
                  <span class="statisticHead__ico">
                    <i class="fa fa-thumbs-o-up" aria-hidden="true"></i>
                  </span>
                        MOST ACTIVE CARERS
                    </h3>

                </div>
                <table class="statisticTable">
                    <thead>
                    <tr>
                        <td class=" " align="center">
                      <span class="ordinaryTitle td-stat">
                       №
                      </span>
                        </td>
                        <td class="textCenter no-padding">
                      <span class="ordinaryTitle td-stat td-title td-title--orange">
                       Id
                      </span>
                        </td>
                         <td class="" align="center">
							<span class="extraTitle">Name</span>
						</td>
                        
                 
                        <td class="textCenter" style="height:40px;">
                      <span class="ordinaryTitle td-stat">
                        APPOINTMENTS <br>PER month
                      </span>
                        </td>
                    </tr>
                   

                    </thead>
                    <tbody>
                    @php($i = 1)
                    @foreach($mostActiveCarers as $item)
                        <tr class="statisticRow">
                            <td align="center">
							  <span>
							   {{$i}}
							  </span>
                            </td>
      
							<td class="idField" align="center">
								<span> {{$item->id}}</span>
							</td>
							<td class="" align="center">
								<a href="#" class="tableLink"> {{$item->first_name.' '.$item->family_name}}</a>
							</td>

                     
                            <td class="textCenter">
                            {{$item->appointments_per_last_month}}
                            </td>
                        </tr>
                        @php(++$i)
                    @endforeach

                    </tbody>
                </table>

            </div>
        </div>
        <div class="statisticBox">
            <div class="tableWrap">
                <div class="statisticHead">
                    <h3 class="statisticHead__title">
                  <span class="statisticHead__ico">
                    <i class="fa fa-male" aria-hidden="true"></i>
                  </span>
                        MOST ACTIVE PURCHASERS
                    </h3>

                </div>
                <table class="statisticTable">
                    <thead>
                    <tr>
                        <td class=" " align="center">
                      <span class="ordinaryTitle td-stat">
                       №
                      </span>
                        </td>
                        <td class=" textCenter no-padding   ">
                      <span class="ordinaryTitle td-stat td-title td-title--light-green">
                       Id
                      </span>
                        </td>
                        
                        <td class="" align="center">
							<span class="extraTitle">Name</span>
						</td>

                     
                        <td class="textCenter    ">
                      <span class="ordinaryTitle td-stat" style="height:40px;">
                        APPOINTMENTS <br>PER month
                      </span>
                        </td>


                    </tr>
                   

                    </thead>
                    <tbody>
                    @php($i = 1)
                    @foreach($mostActivePurchasers as $item)
                        <tr class="statisticRow">
                            <td align="center">
                          <span>
                           {{$i}}
                          </span>
                            </td>
                           
                               
							<td class="idField" align="center">
								<span> {{$item->id}}</span>
							</td>
							<td class="" align="center">
								<a href="#" class="tableLink"> {{$item->first_name.' '.$item->family_name}}</a>
							</td>
					  
                           
                      
                            <td class="textCenter">
                          <span>
                            {{$item->appointments_per_last_month}}
                          </span>
                            </td>
                        </tr>
                        @php(++$i)
                    @endforeach

                    </tbody>
                </table>

            </div>
        </div>
    </div>


    <div class="statisticGroup">
        <div class="statisticBox">
            <div class="tableWrap">
                <div class="statisticHead">
                    <h3 class="statisticHead__title">
                  <span class="statisticHead__ico">
                    <i class="fa fa-area-chart" aria-hidden="true"></i>
                  </span>
                        HOLM INCOME STATISTIC
                    </h3>

                </div>
                <table class="statisticTable">
                    <thead>
                    <tr>
                        <td class=" ">
                      <span class="ordinaryTitle td-stat td-stat--start">
                      PERIOD
                      </span>
                        </td>
                        <td class=" " align="center">
                      <span class="ordinaryTitle td-stat ">
                      </span>
                        </td>
                        
                        
           
                    </tr>
                    </thead>
                    <tbody>
						
						
    
                    
                    
                    
                    <tr class="statisticRow">
                        <td>
                      <span> 
						  
						  {{$daterange}}
					
                      </span>
                        </td>

                        <td class="textCenter">
                      <span>
                        {{(int)$incomeStatistic['month']->last}}
                      </span>
                        </td>
                        
                        
 
                    </tr>
                    </tbody>
                </table>

            </div>
        </div>
        <div class="statisticBox">
            <div class="tableWrap">
                <div class="statisticHead">
                    <h3 class="statisticHead__title">
                  <span class="statisticHead__ico">
                    <i class="fa fa-exchange" aria-hidden="true"></i>
                  </span>
                        Number of Transactions
                    </h3>

                </div>
                <table class="statisticTable">
                    <thead>
                    <tr>
                        <td class=" " >
                      <span class="ordinaryTitle td-stat td-stat--start">
                      PERIOD
                      </span>
                        </td>
                        <td class=" " align="center">
                      <span class="ordinaryTitle td-stat ">
                      </span>
                        </td>
        
                    </tr>
                    </thead>
                    <tbody>
						
						

                    
                    <tr class="statisticRow">
                        <td>
                      <span>
                        {{$daterange}}
                      </span>
                        </td>

                        <td class="textCenter">
                      <span>
                        {{(int)$transactionsStatistic['month']->last}}
                      </span>
                        </td>
                        
            
                    </tr>

                    </tbody>
                </table>

            </div>
        </div>
    </div>
    <div class="statisticGroup">

    </div>
    <div class="statisticGroup">
        <div class="statisticBox">
            <div class="tableWrap">
                <div class="statisticHead">
                    <h3 class="statisticHead__ico">
                    <i class="fa fa-check-square-o" aria-hidden="true"></i>
                  </span>
                        NEW PURCHASERS
                    </h3>
                </div>
                <table class="statisticTable">
                    <thead>
                    <tr>
                        <td class=" ">
                      <span class="ordinaryTitle td-stat td-stat--start">
                      PERIOD
                      </span>
                        </td>
                        <td class=" " align="center">
                      <span class="ordinaryTitle td-stat ">
                      </span>
                        </td>

                    </tr>
                    </thead>
                    <tbody>
                    <tr class="statisticRow">
                        <td>
                      <span>
                        {{$daterange}}
                      </span>
                        </td>

                        <td class="textCenter">
                      <span>
                        {{(int)$newPurchaserStatistic['month']->last}}
                      </span>
                        </td>
       
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="statisticBox">
            <div class="tableWrap">
                <div class="statisticHead">
                    <h3 class="statisticHead__title">
                  <span class="statisticHead__ico">
                    <i class="fa fa-briefcase" aria-hidden="true"></i>
                  </span>
                        New carers
                    </h3>
                </div>
                <table class="statisticTable">
                    <thead>
                    <tr>
                        <td class=" ">
                      <span class="ordinaryTitle td-stat td-stat--start">
                      PERIOD
                      </span>
                        </td>
                        <td class=" " align="center">
                      <span class="ordinaryTitle td-stat ">
                      </span>
                        </td>
                        
   
                    </tr>
                    </thead>
                    <tbody>
						

                    
                    <tr class="statisticRow">
                        <td>
                      <span>
                        {{$daterange}}
                      </span>
                        </td>

                        <td class="textCenter">
                      <span>
                        {{(int)$newCarersStatistic['month']->last}}
                      </span>
                        </td>
                        
                 
                    </tr>

                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div class="statisticGroup">
        <div class="statisticBox">
            <div class="tableWrap">
                <table class="statisticTable">
                    <thead>
                    <tr class="greenRow">
                        <td class="">
                      <span class="ordinaryTitle td-stat td-stat--start">
                      gender
                      </span>
                        </td>
                        <td class=" " align="center">
                      <span class="ordinaryTitle td-stat ">
                      MALE AMOUNT
                      </span>
                        </td>
                        <td class=" " align="center">
                      <span class="ordinaryTitle td-stat ">
                      FEMALE AMOUNT
                      </span>
                        </td>

                    </tr>
                    </thead>
                    <tbody>
                    <tr class="statisticRow">
                        <td>
                      <span>
                       Service user
                      </span>
                        </td>
                        <td class="textCenter">
                      <span>
                        {{$genderStatistic['service_users']->male}}
                      </span>
                        </td>
                        <td class="textCenter">
                      <span>
                        {{$genderStatistic['service_users']->female}}
                      </span>
                        </td>

                    </tr>
                    <tr class="statisticRow">
                        <td>
                      <span>
                       Purchaser
                      </span>
                        </td>

                        <td class="textCenter">
                      <span>
                          {{$genderStatistic['purchasers']->male}}
                      </span>
                        </td>
                        <td class="textCenter">
                      <span>
                          {{$genderStatistic['purchasers']->female}}
                      </span>
                        </td>

                    </tr>
                    <tr class="statisticRow">
                        <td>
                      <span>
                       Carer
                      </span>
                        </td>
                        <td class="textCenter">
                      <span>
                          {{$genderStatistic['carers']->male}}
                      </span>
                        </td>
                        <td class="textCenter">
                      <span>
                        {{$genderStatistic['carers']->female}}
                      </span>
                        </td>
                    </tr>
                    </tbody>
                </table>

            </div>
        </div>
        <div class="statisticBox">
            <div class="tableWrap">
                <table class="statisticTable">
                    <thead>
                    <tr class="greenRow">
                        <td class="">
                      <span class="ordinaryTitle td-stat td-stat--start">
                       age
                      </span>
                        </td>
                        <td class=" " align="center">
                      <span class="ordinaryTitle td-stat ">
                       &lt; 19
                      </span>
                        </td>
                        <td class=" " align="center">
                      <span class="ordinaryTitle td-stat ">
                       20 - 39
                      </span>
                        </td>
                        <td class=" " align="center">
                      <span class="ordinaryTitle td-stat ">
                       40 - 59
                      </span>
                        </td>
                        <td class=" " align="center">
                      <span class="ordinaryTitle td-stat ">
                      60 - 79
                      </span>
                        </td>
                        <td class=" " align="center">
                      <span class="ordinaryTitle td-stat ">
                      80 +
                      </span>
                        </td>

                    </tr>
                    </thead>
                    <tbody>
                    <tr class="statisticRow">
                        <td>
                      <span>
                       Service user
                      </span>
                        </td>
                        <td class="textCenter">
                      <span>
                      {{$ageStatistic['service_users']->{'19'} }}
                      </span>
                        </td>
                        <td class="textCenter">
                      <span>
                        {{$ageStatistic['service_users']->{'20_39'} }}
                      </span>
                        </td>
                        <td class="textCenter">
                      <span>
                        {{$ageStatistic['service_users']->{'40_59'} }}
                      </span>
                        </td>
                        <td class="textCenter">
                      <span>
                         {{$ageStatistic['service_users']->{'60_79'} }}
                      </span>
                        </td>
                        <td class="textCenter">
                      <span>
                         {{$ageStatistic['service_users']->{'80'} }}
                      </span>
                        </td>

                    </tr>
                    <tr class="statisticRow">
                        <td>
                      <span>
                       Purchaser
                      </span>
                        </td>
                        <td class="textCenter">
                      <span>
                      {{$ageStatistic['purchasers']->{'19'} }}
                      </span>
                        </td>
                        <td class="textCenter">
                      <span>
                        {{$ageStatistic['purchasers']->{'20_39'} }}
                      </span>
                        </td>
                        <td class="textCenter">
                      <span>
                        {{$ageStatistic['purchasers']->{'40_59'} }}
                      </span>
                        </td>
                        <td class="textCenter">
                      <span>
                         {{$ageStatistic['purchasers']->{'60_79'} }}
                      </span>
                        </td>
                        <td class="textCenter">
                      <span>
                         {{$ageStatistic['purchasers']->{'80'} }}
                      </span>
                        </td>

                    </tr>
                    <tr class="statisticRow">
                        <td>
                      <span>
                       Carer
                      </span>
                        </td>
                        <td class="textCenter">
                      <span>
                      {{$ageStatistic['carers']->{'19'} }}
                      </span>
                        </td>
                        <td class="textCenter">
                      <span>
                        {{$ageStatistic['carers']->{'20_39'} }}
                      </span>
                        </td>
                        <td class="textCenter">
                      <span>
                        {{$ageStatistic['carers']->{'40_59'} }}
                      </span>
                        </td>
                        <td class="textCenter">
                      <span>
                         {{$ageStatistic['carers']->{'60_79'} }}
                      </span>
                        </td>
                        <td class="textCenter">
                      <span>
                         {{$ageStatistic['carers']->{'80'} }}
                      </span>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
</html>


