<div class="mainPanel">
    <div class="statisticGroup">
        <div class="statisticBox">
            <div class="tableWrap">
				
				
                <div class="statisticHead">
                    <h2 class="statisticHead__title">
                  <span class="statisticHead__ico">
                    <i class="fa fa-area-chart" aria-hidden="true"></i>
                  </span>
                        BOOKINGS DETAILS
                    </h2>
                </div>
                
                <table class="statisticTable">
                    <thead>	
                    <tr>
                        <td class="textCenter" colspan="2">
						  <span class="ordinaryTitle td-stat">
						   Purchaser 
						  </span>
                        </td>
                        <td class=" textCenter  " colspan="2">
						  <span class="ordinaryTitle td-stat">
						   Service user 
						  </span>
                        </td>
                        <td class="textCenter    " colspan="2">
						  <span class="ordinaryTitle td-stat">
							Carer 
						  </span>
                        </td>
                        <td class="textCenter    " colspan="4">
						  <span class="ordinaryTitle td-stat" >
							Booking  
						  </span>
                        </td>
                        <td class="textCenter    " colspan="9">
						  <span class="ordinaryTitle td-stat">
							Appointment   
						  </span>
                        </td>
                    </tr>
                    <tr class="extra-tr">
						
						<td class="idField">
							<span class="extraTitle">ID</span>
						</td>
						
						<td class="">
							<span class="extraTitle">NAME</span>
						</td>
				  

					
						<td class="idField">
							<span class="extraTitle">ID</span>
						</td>
						<td class="">
							<span class="extraTitle">NAME</span>
						</td>
									
						
						<td class="idField">
							<span class="extraTitle">ID</span>
						</td>
						<td class="">
							<span class="extraTitle">NAME</span>
						</td>
								
						 
					 
						<td class="idField">
							<span class="extraTitle">BOOKING ID</span>
						</td>
						<td class="">
							<span class="extraTitle">FROM</span>
						</td>
						
						<td class="">
							<span class="extraTitle">TO</span>
						</td>
						<td class="">
							<span class="extraTitle">STATUS</span>
						</td>
								
					 
						<td class="">
							<span class="extraTitle">TRANSACTION ID</span>
						</td>
						<td class="">
							<span class="extraTitle">APPOINTMENT ID</span>
						</td>
						<td class="">
							<span class="extraTitle">FROM</span>
						</td>
						<td class="">
							<span class="extraTitle">TO</span>
						</td>
						<td class="">
							<span class="extraTitle">PURCHASER APPOINTMENT VALUE</span>
						</td>
						<td class="">
							<span class="extraTitle">CARER APPOINTMENT VALUE</span>
						</td>
						<td class="">
							<span class="extraTitle">STATUS</span>
						</td>
						<td class="">
							<span class="extraTitle">CARER BOOKING STATUS</span>
						</td>
						<td class="">
							<span class="extraTitle">PURCHASER BOOKING STATUS</span>
						</td>
					</tr>
                    </thead>
                    <tbody>
					 
                    @foreach($bookings as $booking)
                     {{$i=1 }}	
                       	@foreach($booking->appointments as $appointment)
                        <tr class="statisticRow">
							
							@if($i==1)
							
							<td align='left'>{{$booking->purchaser_id}}</td>
							<td>
								 @if(!empty($booking->bookingPurchaserProfile) && count($booking->bookingPurchaserProfile))
									<a href="#" class="tableLink">{{$booking->bookingPurchaserProfile->first_name}} {{$booking->bookingPurchaserProfile->family_name}}</a>
								 @endif
							</td>
							
							<td align='left'>{{$booking->service_user_id}}</td>
							<td>
								 @if(!empty($booking->bookingServiceUser && count($booking->bookingServiceUser)))
									<a href="#" class="tableLink">{{$booking->bookingServiceUser->first_name}} {{$booking->bookingServiceUser->family_name}}</a>
								@endif
							</td>
							
							
							<td align='left'>{{$booking->carer_id}}</td>
							<td>
								@if(!empty($booking->bookingCarerProfile && count($booking->bookingCarerProfile)))
									<a href="#" class="tableLink">{{$booking->bookingCarerProfile->first_name}} {{$booking->bookingCarerProfile->family_name}}</a>
								@endif
							</td>
							
							
						   <td class=" " align='left'>
								<span><a href="{{url('/l/bookings/'.$booking->id.'/details')}}" target="_blank">{{$booking->id}}</a></span>
							</td>
							<td class=" ">
								<span>{{$booking->date_start}}</span>
							</td>
							<td class=" ">
							   <span>{{$booking->date_end}}</span>
							</td>
							<td class=" ">
								<div class="profStatus profStatus--left">
								   <span class="profStatus__item profStatus__item--{{$booking->bookingStatus->css_name}}">{{$booking->bookingStatus->name}}</span>
								</div>
							</td>
							@else
							<td colspan="10"></td>
							
							@endif
							
						
							<td class=" ">
								@if(!empty($appointment->transaction))
								{{$appointment->transaction->id}}
								@else
								<span></span>
								@endif
							</td>
							<td class=" " align='left'>
								{{$appointment->id}}
							</td>
							<td class=" ">
								<span>{{$appointment->formatted_date_start}} {{$appointment->formatted_time_from}}</span>
							</td>
							<td class=" ">
								<span>{{$appointment->formatted_date_start}} {{date($appointment->formatted_time_to)}}</span>
							</td>
							<td class="">
								{{$appointment->purchaser_price}}
							</td>
							<td class="">
								{{$appointment->carer_price}}
							</td>
							<td class=" ">
								<div class="profStatus profStatus--left">
								<span class="profStatus__item profStatus__item--{{$appointment->appointmentStatus->css_name}}">{{$appointment->appointmentStatus->name}}</span>
								</div>
							</td>
							<td class=" ">
								<div class="profStatus profStatus--left">
								<span class="profStatus__item profStatus__item--{{$appointment->appointmentStatusCarer->css_name}}">{{$appointment->appointmentStatusCarer->name}}</span>
								</div>
							</td>
							<td class=" ">
								<div class="profStatus profStatus--left">
								<span class="profStatus__item profStatus__item--{{$appointment->appointmentStatusPurchaser->css_name}}">{{$appointment->appointmentStatusPurchaser->name}}</span>
								</div>
							</td>

							{{$i++ }}
							@endforeach
										
                        </tr>
                    @endforeach
                  
                    </tbody>
                </table>

            </div>
        </div>

</div>
</div>        
