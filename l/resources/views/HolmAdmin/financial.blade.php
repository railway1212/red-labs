<div class="mainPanel">
    <h2 class="categoryTitle">
          <span class="categoryTitle__ico">
            <i class="fa fa-gbp" aria-hidden="true"></i>
          </span>
        Financials
    </h2>

    <div class="panelHead">
        <div class="panelHead__group">
            <a href="javascript: window.print()" class="print">
                <i class="fa fa-print" aria-hidden="true"></i>
            </a>
        </div>
        <div class="panelHead__group">
			  {!! Form::open(['method'=>'GET','route'=>'financial']) !!}
			   <div class="filterBox">
				   <div class="formField">
						{!! Form::text('date', null,['class'=>'formItem formItem--input formItem--search datepicker','placeholder'=>'Date']) !!}	 
					</div>
					<div class="formField">
					  {!! Form::submit('filter',['class'=>'actionsBtn actionsBtn--filter actionsBtn--bigger']) !!}
					</div>
				</div>
			   
			  {!! Form::close()!!}
        </div>
        
        
    </div>
    
    
    
    <div class="financialsContainer">
        <div class="financialsCard">
            <div class="financialsCard__header">
                <div class="financialsCard__title">
                    <h2>
                        HOLM BALANCE
                    </h2>
                    <h2>
                        AMOUNT, £
                    </h2>
                </div>
            </div>
            <div class="financialsRow">
                <div class="financialsColumn">
                    <div class="">
                        <img src="{{asset('public/img/admin/stripe.png')}}" alt="">
                    </div>
                </div>
                <div class="financialsColumn">
                    <span>{{$balance}}</span>
                </div>
            </div>
            <div class="financialsRow financialsRow--total">
                <div class="financialsColumn">
                    <span class="total">total</span>
                </div>
                <div class="financialsColumn">
                    <span>{{$balance}}</span>
                </div>
            </div>
        </div>
        <div class="financialsCard">
            <div class="financialsCard__header">
                <div class="financialsCard__title">
                    <h2>
                        COMMISION PAYMENTS
                        <span>(how much were spent on PS fees)</span>

                    </h2>
                    <h2>
                        AMOUNT, £
                    </h2>
                </div>
            </div>
            <div class="financialsRow">
                <div class="financialsColumn">
                    <div class="">
                        <img src="{{asset('public/img/admin/stripe.png')}}" alt="">
                    </div>
                </div>
                <div class="financialsColumn">
                    <span>{{$fee}}</span>
                </div>
            </div>
            <div class="financialsRow financialsRow--total">
                <div class="financialsColumn">
                    <span class="total">total</span>
                </div>
                <div class="financialsColumn">
                    <span>{{$fee}}</span>
                </div>
            </div>
        </div>
        <div class="financialsCard">
            <div class="financialsCard__header">
                <div class="financialsCard__title">
                    <h2>
                        HOLM INCOME

                    </h2>
                    <h2>
                        AMOUNT, £
                    </h2>
                </div>
            </div>
            <div class="financialsRow">
                <div class="financialsColumn">
                    <div class="">
                        <img src="{{asset('public/img/admin/stripe.png')}}" alt="">
                    </div>
                </div>
                <div class="financialsColumn">
                    <span>{{$income}}</span>
                </div>
            </div>
            <div class="financialsRow financialsRow--total">
                <div class="financialsColumn">
                    <span class="total">total</span>
                </div>
                <div class="financialsColumn">
                    <span>{{$income}}</span>
                </div>
            </div>
        </div>
    </div>
</div>
 <!--Script Datepicker-->
    <script>
        $(document).ready(function(){

            $(".datepicker").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: "dd-mm-yy",
                showAnim: "slideDown"
            });
        });
    </script>
    <!-- End Script Datepicker-->
