<div class="mainPanel">
    <h2 class="categoryTitle">
          <span class="categoryTitle__ico">
              <i class="fa fa-stack-overflow" aria-hidden="true"></i>
          </span>
        Care's Wages / Add
    </h2>
    <div class="fees">
        <form action="{{route('feespost')}}" method="post" id="fees-form">
          	<div class="tableWrap tableWrap--margin-t">
              <div class="formField">
                  <div class="fieldWrap">
                    Select Cares Name 
                    <select id="carer_id" name="CaresWage[carer_id]" class="formItem formItem--input">
                      @foreach($carersData as  $careVal)
                      	<option value="{{$careVal['id']}}">{{$careVal['name']}}</option>
                      @endforeach
                    </select>
                  </div>
                </div>
          	</div>
        	<div class="tableWrap tableWrap--margin-t">
            <table class="adminTable">
                <thead>
                <tr>
                    <td class=" ordninary-td  ">
                    <span class="td-title td-title--fees-name">
                      fee name
                    </span>
                    </td>
                    <td class=" ordninary-td  ">
                    <span class="td-title td-title--light-blue">
                      carer rate
                    </span>
                    </td>
                    <td class=" ordninary-td   ">
                    <span class="td-title td-title--fees-type">
                      type
                    </span>
                    </td>
                    <td class=" ordninary-td ordninary-td--small  ">
                    <span class="td-title td-title--amount">
                    amount
                    </span>
                    </td>
                    <td class=" ordninary-td  ">
                    <span class="td-title td-title--orange">
                      purchaser rate
                    </span>
                    </td>
                </tr>

                <tr class="extra-tr">
                    <td>

                    </td>
                    <td>

                    </td>


                    <td class="for-inner">
                        <table class="innerTable ">
                            <tbody>
                            <tr>
                                <td class=" ">
                                    <span class="extraTitle">FLAT,  £</span>
                                </td>
                                <td class="">
                                    <span class="extraTitle">%</span>
                                </td>


                            </tr>

                            </tbody>
                        </table>
                    </td>

                    <td>

                    </td>
                    <td>

                    </td>
                </tr>


                </thead>

                <tbody>
                
                    <tr>
                    <td>
                        <span>Day Time</span>
                      	<input type="hidden" id="fee_name" name="CaresWage[fee_name]" value="Day Time" class="formItem formItem--input">
                    </td>
                    <td>
                        
                        <div class="formField formField--hour-rate">
                            <div class="fieldWrap">
                                <input type="text" id="carer_rate" name="CaresWage[carer_rate]" class="formItem formItem--input">
                               
                                
                            </div>
                        </div>
                    </td>
                    <td class=" ">
                      <div class="onoffswitch">
                        <input type="radio" name="onoffswitch" class="onoffswitch-checkbox" disabled="disabled"
                               id="myonoffswitch" dd="" checked>
                        <label class="onoffswitch-label" for="myonoffswitch">
                          <span class="onoffswitch-inner"></span>
                          <span class="onoffswitch-switch"></span>
                        </label>
                      </div>
                    </td>
                     <td class="">
                        <div class="onoffswitch">
                          <input type="radio" name="onoffswitch" class="onoffswitch-checkbox" disabled="disabled"
                                 id="myonoffswitch2" d="">
                          <label class="onoffswitch-label" for="myonoffswitch2">
                            <span class="onoffswitch-inner"></span>
                            <span class="onoffswitch-switch"></span>
                          </label>
                        </div>
                      </td>
                    <td>
                        <div class="formField formField--hour-rate">
                            <div class="fieldWrap">
                              <input type="text" id="amount" name="CaresWage[amount]" class="formItem formItem--input">
                             
                            </div>
                        </div>
                    </td>
                    <td>
                    <span class="amount">
                      
                    </span>
                    </td>
                </tr>
                
                </tbody>
            </table>
        </div>
            <div class="settingBtn settingBtn--centered">
                <a href="#" onclick="event.preventDefault();document.getElementById('fees-form').submit();" class="actionsBtn actionsBtn--accept actionsBtn--big actionsBtn--no-centered">
                    save
                </a>
            </div>
        </form>
    </div>
</div>
