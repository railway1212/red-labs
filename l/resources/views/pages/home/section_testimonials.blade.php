<section class="testimonialsSection">
    <div class="container">
        <div class="testimonialsSection__title">
            <span class="testimonialsIco">
          <i class="fa fa-quote-left" aria-hidden="true"></i>
        </span>
        </div>
        <div class="sliderContainer">
            <!-- <a href="#theCarousel1" data-slide="prev" class="sliderControl sliderControl--left centeredLink">
                <i class="fa fa-angle-left"></i>
            </a>
            <a href="#theCarousel1" data-slide="next" class="sliderControl sliderControl--right centeredLink">
                <i class="fa fa-angle-right"></i>
            </a> -->
            <div class="testimonialSlider carousel slide multi-item-carousel" id="theCarousel1">
                <div class="carousel-inner carousel-inner1 special-slide">
                    <div class="testimonialSlider__item item active" id="testimonialSlider__item1">
                        <p>
                            I got fed up zero hours contracts and not knowing my work schedule. I feel free at last to
                            do what matters.
                        </p>
                    </div>
                    <div class="testimonialSlider__item item " id="testimonialSlider__item2">
                        <p>
                            I enjoy my work again now that I spend quality time with my clients. No more rushing like a
                            headless chicken.
                        </p>
                    </div>
                    <div class="testimonialSlider__item item " id="testimonialSlider__item3">
                        <p>
                            I’m not always filling out paperwork. It’s feels great!
                        </p>
                    </div>
                </div>

            </div>
        </div>
        <div class="carousel slide multi-item-carousel" id="theCarousel1">
            <div class="carousel-inner carousel-inner1">
                <div class="item active">
                    <div class="testimonialsPeople">
                        <a href="#" class="peopleBox" data-id="4">
                            <div class="profilePhoto peopleBox__photo activeImg">
                                <img src="public//img/Annie_B.png" alt="">
                            </div>
                            <div class="peopleBox__info">
                                <p class="profileName">Annie B.<br>(25 year old woman)</p>
                                <div class="people_quote">
                                    I got fed up zero hours contracts and not knowing my work schedule. I feel free at
                                    last to do what matters.
                                </div>
                            </div>
                        </a>
                        <a href="#" class="peopleBox" data-id="2">
                            <div class="profilePhoto peopleBox__photo">
                                <img src="public/img/John_R.png" alt="">
                            </div>
                            <div class="peopleBox__info">
                                <p class="profileName">John R.<br>(35 year old man)</p>
                                <div class="people_quote">
                                    I enjoy my work again now that I spend quality time with my clients. No more rushing
                                    like a headless chicken.
                                </div>
                            </div>
                        </a>
                        <a href="#" class="peopleBox" data-id="3">
                            <div class="profilePhoto peopleBox__photo">
                                <img src="public/img/Ginger_P.png" alt="">
                            </div>
                            <div class="peopleBox__info">
                                <p class="profileName">Ginger P.<br>(40 year old woman)</p>
                                <div class="people_quote">
                                    I’m not always filling out paperwork. It’s feels great!
                                </div>
                            </div>
                        </a>
                    </div>
                </div>

            </div>
        </div>

    </div>
</section>
