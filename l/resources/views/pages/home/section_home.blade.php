<section class="home_section" title="Enter Your Postcode Search Box">
    <div class="container">
      <div class="home">
        <div class="home__title">
          <h1>
            Holm Care
          </h1>
          <p>
            Helping you find affordable care quickly and easily
          </p>
        </div>
        <div class="row">
          <div class="col-md-offset-1 col-md-10">
            <div class="home__search">
              <form class="homeForm">
                <input type="text"   class="homeForm__input" placeholder="enter your postcode">
                <a href="SearchPage.html" type="submit" class="homeForm__btn">
                  <span>
                    <i class="fa fa-search"></i>
                  </span>
                </a>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
