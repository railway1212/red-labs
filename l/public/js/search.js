$(document).ready(function () {

    if ($('.carer-result').length > 0 && $('input[name="sort-rating"]').length > 0) runSearch();
    $('body').on('click', '.cust-pagination__item', function (e) {
        if (!$(this).hasClass("points")) {
            var page = parseInt($(e.currentTarget).data('page'));
            change_page(page);
        }
    });
    $('body').on('click', '.paginationArrow--left', function (e) {
        var page = $("[data-selected='true']").data('page');
        if (page > 1) {
            page--;
            change_page(page);
        }
    });
    $('body').on('click', '.paginationArrow--right', function (e) {
        var page = $("[data-selected='true']").data('page');
        var count_pages = $("[data-page]").length;
        page++;
        if (page <= count_pages) {
            change_page(page);
        }
    });
});

function runSearch(page) {
  		
    if (typeof page === 'undefined') {
        page = 1;
    }
    var data = {};
    data['sort'] = get_sort_data();
    data['sort_order'] = get_sort_order(data['sort']);
    data['types'] = get_types();
    data['other'] = get_other();
    data['languages'] = get_languages();
    data['post_code'] = $('input[name="postCode"]').val();
    data['date'] = $('input[name="findDate"]').val();
    data['type_of_service'] = get_type_of_service();
    data['page'] = page;
    $('.carer-result').html('');
    $('#loader-search').show();

    $.get("/l/search-carers", {data: data})
        .done(function (data_response) {
            
            console.log(data_response['error']);
            var careers = data_response['data'];
            for (key in careers) {
                var career = careers[key];
                if (data_response['distance_is_filled']) career['distance'] = '<div class="result__distance"><p class="location"><span class="location__value location__value--autoWidth">' + career['distance'] + ' MI</span></p><span class="subLabel">distance</span></div>';
                 
                else career['distance'] = '';
                career['hearts'] = '';
                for (var i = 1; i <= 5; i++) {
                    if (career['avg_total'] >= i) {
                        career['hearts'] += '<span class="profileRating__item active"><i class="fa fa-heart"></i></span>';
                    } else
                        career['hearts'] += '<span class="profileRating__item"><i class="fa fa-heart"></i></span>';
                }
                if (career['sentence_yourself'] == null) career['sentence_yourself'] = '';//Не отображать NULL
                var template = '<div class="result">' +
                    '<a href="/l' + career['href_profile'] + '" class="profilePhoto  profilePhoto2"><img id="profile_photo" class="set_preview_profile_photo" src="' + career['image_profile'] + '"/></a><div class="result__info"><div class="justifyContainer"><h2 class="profileName profileName--biger"><a href="/l' + career['href_profile'] + '"> ' + career['first_name'] + ' ' + career['family_name'].substring(0, 1) + '.</a></h2><p class="hourPrice hourPrice"><span class="hourPrice__price">From £  ' + career['price'] + '</span><span class="hourPrice__timing">/hour</span></p></div><p class="info-p"> ' + career['sentence_yourself'] + ' </p><div class="justifyContainer justifyContainer--smColumn "><div class="result__city"><p class="location"><span class="location__value location__value--autoWidth"> ' + career['town'] + ' </span></p><span class="subLabel">city</span></div> ' + career['distance'] + ' ' +
                    //RATING '<div class="result__rate"><div class="profileRating "> ' + career['hearts'] + ' </div><span class="subLabel">(' + career['creview'] + ' reviews)</span></div>            ' +
                    '<div class="bookBtn">                <a href="/l' + career['href_profile'] + '" class="bookBtn__item bookBtn__item--smaller centeredLink">                    view carer                </a>            </div>        </div>    </div></div>';
                $('.carer-result').append(template);
                var pages = data_response['pages'];
                var page_template = ' <a href="#" class="paginationArrow paginationArrow--left"><span><i class="fa fa-angle-left"></i></span></a>';
                if (pages <= 9) {/////////////////////////////////////////////
                    for (var key2 = 1; key2 <= pages; key2++) {
                        if (key2 === page) var selected = 'true';
                        else var selected = 'false';
                        page_template += '<a href="#" class="cust-pagination__item  " data-page="' + key2 + '" data-selected="' + selected + '"><span>' + key2 + '</span></a>';
                    }
                } else {
                    if (page <= 5) {
                        for (var key2 = 1; key2 <= 8; key2++) {
                            if (key2 === page) var selected = 'true';
                            else var selected = 'false';
                            page_template += '<a href="#" class="cust-pagination__item  " data-page="' + key2 + '" data-selected="' + selected + '"><span>' + key2 + '</span></a>';
                        }
                        page_template += '<a href="#" class="cust-pagination__item  points"><span>...</span></a>';
                        page_template += '<a href="#" class="cust-pagination__item  " data-page="' + pages + '" data-selected="false"><span>' + pages + '</span></a>';
                    } else if (page > (pages - 5)) {
                        page_template += '<a href="#" class="cust-pagination__item  " data-page="1" data-selected="false"><span>1</span></a>';
                        page_template += '<a href="#" class="cust-pagination__item  points"><span>...</span></a>';
                        for (var key2 = (pages - 5); key2 <= pages; key2++) {
                            if (key2 === page) var selected = 'true';
                            else var selected = 'false';
                            page_template += '<a href="#" class="cust-pagination__item  " data-page="' + key2 + '" data-selected="' + selected + '"><span>' + key2 + '</span></a>';
                        }
                    } else {
                        page_template += '<a href="#" class="cust-pagination__item  " data-page="1" data-selected="false"><span>1</span></a>';
                        page_template += '<a href="#" class="cust-pagination__item  points"><span>...</span></a>';
                        for (var key2 = (page - 3); key2 <= (page + 3); key2++) {
                            if (key2 === page) var selected = 'true';
                            else var selected = 'false';
                            page_template += '<a href="#" class="cust-pagination__item  " data-page="' + key2 + '" data-selected="' + selected + '"><span>' + key2 + '</span></a>';
                        }
                        page_template += '<a href="#" class="cust-pagination__item  points"><span>...</span></a>';
                        page_template += '<a href="#" class="cust-pagination__item  " data-page="' + pages + '" data-selected="false"><span>' + pages + '</span></a>';
                    }
                }
                $('.cust-pagination__group').html(page_template);
                $('#loader-search').hide();

            }

            if (data_response['distance_is_filled']) {
                $('.distance_order').show();
            } else {
                $('input[name="sort-distance"]').val(0);
                $('.distance_order').hide();
            }

            if (typeof data_response['error'] != 'undefined') {
                $('#loader-search').hide();
                $('.pagination-container').hide();
                $('.not_found').show();
                $('.Paginator').hide();
            }
            else {
                $('.post_code_error').hide();
                if (data_response['data'].length < 1 && page === 1) {
                    $('.pagination-container').hide();
                } else {
                    $('.pagination-container').show();
                }

                if (data_response['data'].length === 0) {
                    
                    $('#loader-search').hide();
                  
                    $('.not_found').show();
                    $('.cares_count').html(data_response['count']);

                    
                } else {
                    $('.not_found').hide();
                    $('.Paginator').show();
                    $('.cares_count').html(data_response['count']);
                }
            }
            if (document.getElementsByClassName('cust-pagination__item').length == 1) {
                document.getElementsByClassName('pagination-container')[0].style.display = 'none';
            }
        });
}

function get_sort_data() {
    var sort_types = ['rating', 'distance', 'id'];
    for (key in sort_types) {
        sort_type = sort_types[key];
        input_value = $('input[name="sort-' + sort_type + '"]').val();
        if (input_value == 1) {
            return sort_type;
        }
    }
    return false;
}

function get_sort_order(sort) {
    sort_order = $('input[name="sort-' + sort_type + '-order"]').val();
    return sort_order;
}


function get_types() {
    types = $('input[name^="typeCare["]');
    checked = [];
    $(types).each(function (type) {
        if ($(this).prop('checked')) {
            var name = $(this).attr('name').match(/[0-9]/g);
            if (typeof name[0] != 'undefined') checked.push(name[0]);
        }
    });

    return checked;
}

function get_other() {
    checked = [];
    if ($('input[name="gender[Male]"]').prop('checked')) checked.push("male");
    if ($('input[name="gender[Female]"]').prop('checked')) checked.push("female");
    if ($('input[name="have_car"]').prop('checked')) checked.push("have_car");
    if ($('input[name="work_with_pets"]').prop('checked')) checked.push("work_with_pets");

    return checked;
}



function get_type_of_service() {
    checked = [];
    if ($('input[name="typeService[SINGLE / REGULAR VISIT]"]').prop('checked')) checked.push(1);
    if ($('input[name="typeService[LIVE IN CARE]"]').prop('checked')) checked.push(2);
    if ($('input[name="typeService[RESPITE CARE]"]').prop('checked')) checked.push(3);
    
console.log(checked);
    return checked;
}

function get_languages() {
    languages = $('input[name^="language["]');
    checked = [];
    $(languages).each(function (type) {
        if ($(this).prop('checked')) {
            var name = $(this).attr('name').match(/[0-9]/g);
            if (typeof name[0] != 'undefined') checked.push(name[0]);
        }
    });
    return checked;
}


function change_page(page){
    $('[data-page]').each(function (elem) {
        $($('[data-page]')[elem]).attr('data-selected', 'false');
    });
    $($("[data-page='" + page + "']")).attr('data-selected', 'true');
    runSearch(page);
}
