<?php
namespace Auxin\Plugin\CoreElements\Elementor\Modules;

use Elementor\Plugin;

class Templates_Types_Manager {
	private $docs_types = [];

	public function __construct() {
		if( ! defined( 'ELEMENTOR_PRO_VERSION' ) ){
			define( 'AUXIN_ELEMENTOR_TEMPLATE', true );
			add_action( 'elementor/documents/register', [ $this, 'register_documents' ] );
			add_action( 'elementor/dynamic_tags/register_tags', [ $this, 'register_tag' ] );
		}
	}

	public function register_documents() {
		$this->docs_types = [
			'header' => Documents\Header::get_class_full_name(),
			'footer' => Documents\Footer::get_class_full_name()
		];

		foreach ( $this->docs_types as $type => $class_name ) {
			Plugin::$instance->documents->register_document_type( $type, $class_name );
		}
	}

	/**
	 * @param \Elementor\Core\DynamicTags\Manager $dynamic_tags
	 */
	public function register_tag( $dynamic_tags ) {

        $tags = array(
            'aux-shortcode' => array(
                'file'  => AUXELS_INC_DIR . '/elementor/modules/dynamic-tags/shortcode.php',
				'class' => 'DynamicTags\Shortcode',
				'group' => 'site',
				'title' => 'Site',
			),
            'aux-post-custom-field' => array(
                'file'  => AUXELS_INC_DIR . '/elementor/modules/dynamic-tags/post-custom-field.php',
				'class' => 'DynamicTags\Post_Custom_Field',
				'group' => 'post',
				'title' => 'Post',
            )
        );

        foreach ( $tags as $tags_type => $tags_info ) {
            if( ! empty( $tags_info['file'] ) && ! empty( $tags_info['class'] ) ){
				// In our Dynamic Tag we use a group named request-variables so we need
				// To register that group as well before the tag
				\Elementor\Plugin::$instance->dynamic_tags->register_group( $tags_info['group'] , [
					'title' => $tags_info['title']
				] );

                include_once( $tags_info['file'] );
                if( class_exists( $tags_info['class'] ) ){
                    $class_name = $tags_info['class'];
                } elseif( class_exists( __NAMESPACE__ . '\\' . $tags_info['class'] ) ){
                    $class_name = __NAMESPACE__ . '\\' . $tags_info['class'];
                }
				$dynamic_tags->register_tag( $class_name );
            }
        }
	}

}