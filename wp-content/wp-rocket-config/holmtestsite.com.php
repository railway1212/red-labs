<?php
defined( 'ABSPATH' ) || die( 'Cheatin&#8217; uh?' );

$rocket_cookie_hash = '26b8cc4a597c113aa6b804c40caf6fa1';
$rocket_logged_in_cookie = 'wordpress_logged_in_26b8cc4a597c113aa6b804c40caf6fa1';
$rocket_cache_mobile_files_tablet = 'desktop';
$rocket_cache_mobile = 1;
$rocket_cache_ssl = 1;
$rocket_cache_reject_uri = '/(.+/)?feed/?|/(?:.+/)?embed/|/(index\.php/)?wp\-json(/.*|$)';
$rocket_cache_reject_cookies = 'wordpress_logged_in_.+|wp-postpass_|wptouch_switch_toggle|comment_author_|comment_author_email_';
$rocket_cache_reject_ua = 'facebookexternalhit';
$rocket_cache_query_strings = array(
  0 => 'q',
);
$rocket_secret_cache_key = '5ce98982679a4159117142';
$rocket_do_caching_mobile_files = 0;
$rocket_cache_mandatory_cookies = '';
$rocket_cache_dynamic_cookies = array();
